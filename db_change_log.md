ALTER TABLE `orders`  
  ADD coupon_amount double(8,2) DEFAULT NULL,
  ADD coupon_type int(11) DEFAULT NULL,
  ADD coupon varchar(191) DEFAULT NULL,
  ADD coupon_description varchar(191) DEFAULT NULL;
  
  --------------
 
  -- Table structure for table `coupons`
use in project root coupons.sql