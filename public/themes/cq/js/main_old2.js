$(document).ready(function(){


    //   $('.slick-track div:odd').addClass('white_color');
    // // if ($(".banner_item").hasClass("black_banner")) {
    // //   $('body').addClass('black_color');
    // // } 

    if($(window).width() < 767){
        
        $(".menu_arrow").click(function(){
            $('body').toggleClass('menu_toggle menu_open');
        });
        $(".header_menu_item").click(function(){
            $('body').addClass('menu_open');
        });

    } else {

        $(".menu_arrow").hover(function(){
            $('body').toggleClass('menu_toggle menu_open');
        });
        $(".header_menu_item").hover(function(){
            $('body').toggleClass('menu_open');
        });

    }


    $(".header_search_inner").click(function(){
        $("body").addClass('search_show');
        $(".product_filter_area").css({
            'z-index' : 10
        });
        $(".header_menu_item").css({
            'z-index' : 10
        });
        return false;
    });

    $(".full-page-search-inner img").click(function(){
        $("body").removeClass('search_show');
        return false;
    });

    $(".p_fiter").click(function(){
        $(".filter_form").show();
        return false;
    });

    $(".cancel_form").click(function(){
        $(".filter_form").hide();
        return false;
    });


    $(".two_grid").click(function(){
        /*$(".four_grid_area").removeClass('four_grid_show');
        $(".two_grid_area").addClass('two_grid_show');*/
        $(".product_grid").removeClass('four_grid_area four_grid_show');
        $(".product_grid").addClass('two_grid_area two_grid_show');
        $('.single-product').removeClass('col-md-3');
        $('.single-product-video').removeClass('col-md-6');
        $('.single-product').addClass('col-md-6');
        $('.single-product-video').addClass('col-md-12');

        $('.four_grid').removeClass('active');
        $(this).addClass('active');

        return false;
    });
    $(".four_grid").click(function(){
        /*$(".two_grid_area").removeClass('two_grid_show');
        $(".four_grid_area").addClass('four_grid_show');*/
        $(".product_grid").addClass('four_grid_area four_grid_show');
        $(".product_grid").removeClass('two_grid_area two_grid_show');
        $('.single-product').addClass('col-md-3');
        $('.single-product-video').addClass('col-md-6');
        $('.single-product').removeClass('col-md-6');
        $('.single-product-video').removeClass('col-md-12');

        $('.two_grid').removeClass('active');
        $(this).addClass('active');

        return false;
    });

    $(".s_share_ic").click(function(){
        $(".social_share").toggle();
        return false;
    });

    $(".cart_gift_checkbox label").click(function(){
        $(".cart_gift_recept_content").toggle();
    });

    $( ".popup_ic li" ).each(function() {
        $( this ).hover(
          function() {
            $( this ).addClass( "hover" );
          }, function() {
            $( this ).removeClass( "hover" );
          }
        );      
    });

    // $( ".product_grid_inner" ).each(function() {
    //    // $( this ).hover(
    //    //    function() {
    //    //      $( this ).addClass( "hover" );
    //    //    }, function() {
    //    //      $( this ).removeClass( "hover" );
    //    //    }
    //    //  ); 
    //    //.$('.product_grid_inner_thumb_grid').click 
    //    $(".product_grid_inner_thumb_grid .imge_thum_remove").click(function(){
    //     $(".product_grid_inner_thumb_grid").hide();
    //         return false;
    //     });     
    // });
    /*
    =========================================================================================
    5.  CART INCREASE DECREASE VALUE
    =========================================================================================
    */

    $('.btn-number').click(function(e){
        e.preventDefault();
        
        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                
                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                } 
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
       $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {
        
        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());
        
        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        
        
    });




    var storedScrollPos = 0;
    var scrolledLastPosForMenu = 0;

    $(window).scroll(function() {
        var scrollPos = $(window).scrollTop();
        var headerheight = $('.header_area').outerHeight();
        var windowHeight = $(window).outerHeight();
        var slidebarHeight = $('.header_menu_item').outerHeight();
        var sidebarMaxTop = (slidebarHeight - windowHeight) + headerheight + 110;
        if (windowHeight < slidebarHeight) { //menu is greater window
            if (scrollPos > storedScrollPos) {
                if ((scrollPos < sidebarMaxTop) || (scrolledLastPosForMenu < sidebarMaxTop)) {
                    let position = 0;
                    if (scrollPos > sidebarMaxTop) {
                        position = scrolledLastPosForMenu + 40;
                    } else {
                        position = scrollPos;
                    }

                    $('.header_menu_item_inner').css({
                        'top': -position
                    });
                    scrolledLastPosForMenu = position;
                }
            } else {

                if (scrolledLastPosForMenu > 0) {
                    $('.header_menu_item_inner').css({
                        'top': (-scrolledLastPosForMenu) + (storedScrollPos - scrollPos)
                    });

                    scrolledLastPosForMenu = scrolledLastPosForMenu - (storedScrollPos - scrollPos);
                }
            }
        }


        storedScrollPos = scrollPos;

        var product_desc_height = $('.prduct_desc').outerHeight();
        var productdescmaxtop = (product_desc_height - windowHeight) + headerheight + 80;
        if(scrollPos < productdescmaxtop){
            $('.prduct_desc_inner').css({
              'top' : - scrollPos
            });   
        }
          var sidelength = $(".header_area").outerHeight();
          if (scrollPos > sidelength) {
            $('.prduct_desc').addClass('fixed-sidebar');
          } else{
              $('.prduct_desc').removeClass('fixed-sidebar');  
          }

        var fixed_lenth = $(".fixed-sidebar").length;

        if ( fixed_lenth > 0) {
           if ($(window).scrollTop() >= $(".related_product").offset().top - $(".prduct_desc").innerHeight()){
                $('.prduct_desc').addClass('remove_fixed');
            } else {
                $('.prduct_desc').removeClass('remove_fixed');
            }                
        }
        
      //   var fixed_lenth2 = $(".header_menu_item").length;
      //   if ( fixed_lenth2 > 0) {
      //   	if ($(window).scrollTop() < 10) {
      //   		$('.header_menu_item').removeClass('remove_menu_fixed');
      //   	} else if ($(window).scrollTop() >= $(".footer_area").offset().top - $(".header_menu_item").innerHeight()){
      //         	$('.header_menu_item').addClass('remove_menu_fixed');
      //     	} else {
      //         	$('.header_menu_item').removeClass('remove_menu_fixed');
      //     	}  
      //   // if (!$(window).scrollTop() >= $(".footer_area").offset().top - $(".header_menu_item").innerHeight()){
      //   //       $('.header_menu_item').removeClass('remove_menu_fixed');
      //   //   }

      // }
    });

 

        /*
        =========================================================================================
        5.  BANNER SLIDER
        =========================================================================================
        */


            $('.slider').on('init', function(ev, el){
                $('video').each(function () {
                    this.play();
                });
            }); 

            $('.product_grid_inner_thumb').on('init', function(ev, el){
                $('video').each(function () {
                    this.play();
                });
            });

        /*
        =========================================================================================
        5.  RELATED SLIDER
        =========================================================================================
        */

        var related_product = jQuery("#related_product");
        related_product.owlCarousel({
            loop: true,
            margin: 30,
            lazyLoad:true,
            center: true,
            smartSpeed: 1500,                
            autoplay:true,
            nav: true,
            dots:false,
            navText: ["<img src='../themes/cq/images/arrow-left.png'>","<img src='../themes/cq/images/arrow-right.png'>"],
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1200: {
                    items: 3
                }
            }
        });


        /*
        =========================================================================================
        5.  RELATED SLIDER
        =========================================================================================
        */

        var product_thumbnail = jQuery("#product_thumbnail");
        product_thumbnail.owlCarousel({
            loop: true,
            margin: 30,
            lazyLoad:true,
            center: true,
            smartSpeed: 1500,                
            autoplay:true,
            nav: true,
            dots:false,
            navText: ["<img src='../themes/cq/images/arrow-left.png'>","<img src='../themes/cq/images/arrow-right.png'>"],
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        });

      $('.product_thumbnail').magnificPopup({
        delegate: 'a.image-link',
        type: 'image', 
        mainClass: 'singleproduct',
        gallery: {
          enabled: true,
          navigateByImgClick: true,
          preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },

        image: {
          verticalFit: false
        }
      });

});



var slideWrapper = $(".slider"),
    iframes = slideWrapper.find('.embed-player'),
    lazyImages = slideWrapper.find('.slide-image'),
    lazyCounter = 0;

// POST commands to YouTube or Vimeo API
function postMessageToPlayer(player, command){
  if (player == null || command == null) return;
  player.contentWindow.postMessage(JSON.stringify(command), "*");
}

// When the slide is changing
function playPauseVideo(slick, control){
  var currentSlide, slideType, startTime, player, video;

  currentSlide = slick.find(".slick-current");
  slideType = currentSlide.attr("class").split(" ")[1];
  player = currentSlide.find("iframe").get(0);
  startTime = currentSlide.data("video-start");

 if (slideType === "video") {
    video = currentSlide.children("video").get(0);
    if (video != null) {
      if (control === "play"){
        video.play();
      } else {
        video.pause();
      }
    }
  }
}

// Resize player
function resizePlayer(iframes, ratio) {
  if (!iframes[0]) return;
  var win = $(".slider"),
      width = win.width(),
      playerWidth,
      height = win.height(),
      playerHeight,
      ratio = ratio || 16/9;

  iframes.each(function(){
    var current = $(this);
    if (width / ratio < height) {
      playerWidth = Math.ceil(height * ratio);
      current.width(playerWidth).height(height).css({
        left: (width - playerWidth) / 2,
         top: 0
        });
    } else {
      playerHeight = Math.ceil(width / ratio);
      current.width(width).height(playerHeight).css({
        left: 0,
        top: (height - playerHeight) / 2
      });
    }
  });
}

// DOM Ready
$(function() {
  $('.slider').slick({
        slidesToShow: 1,
        autoplay: true,
        arrows: false,
        pauseOnHover:false,
        autoplaySpeed: 5000,
    });
});

// Resize event
$(window).on("resize.slickVideoPlayer", function(){  
  resizePlayer(iframes, 16/9);
});


/*
$(".video_wrapper video")[0].autoplay = true;
$(".two_grid_area .video_wrapper video")[0].autoplay = true;*/
