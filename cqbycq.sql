-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 10, 2019 at 04:05 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cqbycq`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_ship_methods`
--

DROP TABLE IF EXISTS `admin_ship_methods`;
CREATE TABLE IF NOT EXISTS `admin_ship_methods` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `courier_id` int(11) NOT NULL,
  `fee` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_ship_methods`
--

INSERT INTO `admin_ship_methods` (`id`, `name`, `courier_id`, `fee`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'UPS Ground', 1, 0, '2018-06-01 03:13:50', '2018-06-01 03:13:50', NULL),
(2, 'UPS 23', 1, 0, '2018-06-01 03:15:23', '2018-06-01 03:20:10', NULL),
(3, 'UPS 3', 1, 0, '2018-06-01 03:15:34', '2018-06-01 03:15:34', NULL),
(4, 'erw', 1, 0, '2018-06-01 03:20:58', '2018-06-01 03:21:00', '2018-06-01 03:21:00'),
(5, 'Truck', 3, 0, '2018-06-01 03:38:12', '2018-07-02 09:19:53', '2018-07-02 09:19:53'),
(6, 'asdf3', 4, 0, '2018-07-02 09:28:36', '2018-07-02 09:33:19', '2018-07-02 09:33:19'),
(7, 'with fee', 1, 35, '2018-07-20 09:44:36', '2018-07-20 09:45:01', NULL),
(8, 'Pore', 4, NULL, '2018-08-01 12:17:06', '2018-08-01 12:17:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `block_users`
--

DROP TABLE IF EXISTS `block_users`;
CREATE TABLE IF NOT EXISTS `block_users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `block_users`
--

INSERT INTO `block_users` (`id`, `user_id`, `vendor_meta_id`, `created_at`, `updated_at`) VALUES
(3, 14, 1, '2018-06-26 05:31:46', '2018-06-26 05:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `body_sizes`
--

DROP TABLE IF EXISTS `body_sizes`;
CREATE TABLE IF NOT EXISTS `body_sizes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `body_sizes`
--

INSERT INTO `body_sizes` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Junior', 1, '2018-05-17 04:13:39', '2018-05-22 14:18:18', NULL),
(2, 'sdf', 1, '2018-05-17 05:00:37', '2018-05-17 05:00:40', '2018-05-17 05:00:40'),
(3, 'Young Contemporary', 1, '2018-05-17 06:39:49', '2018-05-22 14:18:31', NULL),
(4, '222', 0, '2018-05-17 06:42:29', '2018-05-17 06:42:35', '2018-05-17 06:42:35'),
(5, 'Missy', 1, '2018-05-22 14:18:43', '2018-05-22 14:18:43', NULL),
(6, 'Plus Size', 1, '2018-05-22 14:18:51', '2018-05-22 14:18:51', NULL),
(7, 'Maternity', 1, '2018-05-22 14:18:59', '2018-05-22 14:18:59', NULL),
(8, 'tt', 3, '2018-06-22 03:13:35', '2018-06-22 03:36:26', '2018-06-22 03:36:26');

-- --------------------------------------------------------

--
-- Table structure for table `buyer_shipping_addresses`
--

DROP TABLE IF EXISTS `buyer_shipping_addresses`;
CREATE TABLE IF NOT EXISTS `buyer_shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `store_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commercial` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buyer_shipping_addresses`
--

INSERT INTO `buyer_shipping_addresses` (`id`, `user_id`, `default`, `store_no`, `location`, `address`, `unit`, `city`, `state_id`, `state_text`, `zip`, `country_id`, `phone`, `fax`, `commercial`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 0, 'fd', 'US', 'Raj', NULL, 'gsdfgfga', 10, NULL, '5656', 1, '4576767', '4545', 0, '2018-06-14 18:00:00', '2018-07-13 13:15:59', NULL),
(2, 19, 1, 'erwer', 'US', '45345', NULL, '45', 4, NULL, '34535', 1, '4545', NULL, 0, '2018-06-22 07:18:27', '2018-06-22 07:18:27', NULL),
(7, 14, 0, 'sadfa', 'US', 'asdfa', NULL, 'asdf', 4, NULL, 'asdf', 1, 'rfer', NULL, 0, '2018-06-22 09:49:57', '2018-07-13 13:15:59', NULL),
(6, 14, 1, 'bad 2', 'US', 'asdf', '2', 'dfadf', 1, NULL, '3er', 1, '3434', NULL, 1, '2018-06-22 09:49:27', '2018-07-13 13:15:59', NULL),
(8, 14, 0, 'sadfa', 'US', 'asdfa', NULL, 'asdf', 4, NULL, 'asdf', 1, 'rfer', NULL, 0, '2018-06-22 09:49:58', '2018-07-13 13:15:54', '2018-07-13 13:15:54'),
(9, 14, 0, 'asdf', 'US', 'asdf', NULL, 'asdf', 5, NULL, 'sdf', 1, '34', NULL, 0, '2018-06-22 09:50:44', '2018-06-22 10:11:47', '2018-06-22 10:11:47'),
(10, 14, 0, NULL, 'US', 'fdaf', 'd', 'dfads', 4, NULL, 'asdfad', 1, 'asdf', NULL, 0, '2018-06-22 10:58:27', '2018-07-13 13:15:57', '2018-07-13 13:15:57'),
(11, 14, 0, 'BD', 'INT', 'sdfdfdf', '45', 'rtrtr', NULL, 'erer', 'ter', 20, 'erer', NULL, 0, '2018-06-22 11:31:30', '2018-07-13 13:16:01', '2018-07-13 13:16:01'),
(13, 14, 0, 'dd', 'US', 'jhaka naka', NULL, 'rer', 3, NULL, 'rer', 1, '3434', NULL, 0, '2018-06-23 10:15:59', '2018-07-13 13:15:59', NULL),
(14, 21, 1, 'erwer', 'US', 'qwer', NULL, 'qwer', 4, NULL, 'qwer', 1, 'qwer', 'qwer', 0, '2018-07-12 07:30:13', '2018-07-12 07:30:13', NULL),
(15, 22, 1, 'adsf', 'US', 'asdf', NULL, 'asdf', 3, NULL, 'asdf', 1, 'aasdf', 'asdf', 0, '2018-07-12 07:57:38', '2018-07-12 07:57:38', NULL),
(16, 14, 0, NULL, 'US', 'New tt', NULL, 'asdf', 4, NULL, 'asdf', 1, 'fasd', 'adsf', 0, '2018-07-13 09:11:48', '2018-07-13 13:15:59', NULL),
(17, 14, 0, NULL, 'US', 'tttete', NULL, 'wer', 4, NULL, 'wer', 1, '234', '234', 0, '2018-07-13 09:16:53', '2018-07-13 13:15:59', NULL),
(18, 14, 0, NULL, 'US', 'rr4', NULL, '4535', 3, NULL, '345', 1, '345', NULL, 0, '2018-07-13 13:16:10', '2018-07-13 13:16:10', NULL),
(19, 14, 0, NULL, 'US', 'ffame', NULL, '23213', 3, NULL, '234', 1, '234', NULL, 0, '2018-07-25 07:59:12', '2018-07-25 07:59:12', NULL),
(20, 14, 0, NULL, 'US', 'e343434', '433', '43434', 1, NULL, '343', 1, '3434', NULL, 0, '2018-07-25 08:00:39', '2018-07-25 08:00:39', NULL),
(21, 23, 1, 'rqwer', 'US', 'rqwer', NULL, 'qwerqe', 3, NULL, '234324', 1, 'rqwerq', NULL, 1, '2018-09-25 09:04:16', '2018-09-25 09:04:16', NULL),
(22, 24, 1, 'asdf', 'US', 'sadf', NULL, 'asdf', 3, NULL, 'asdf', 1, 'asdfa', 'asdf', 0, '2019-01-02 04:14:56', '2019-01-02 04:14:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

DROP TABLE IF EXISTS `cart_items`;
CREATE TABLE IF NOT EXISTS `cart_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Jewelry', 0, 1, '2018-05-15 03:22:30', '2018-12-01 09:25:01', '2018-12-01 09:25:01'),
(2, 'SHOES', 9, 1, '2018-05-15 03:22:39', '2018-05-22 13:35:29', '2018-05-22 13:35:29'),
(3, 'ACCESSORIES', 0, 3, '2018-05-15 03:22:42', '2018-06-30 12:52:55', '2018-06-30 12:52:55'),
(4, 'Body Jewelry', 43, 2, '2018-05-15 03:22:44', '2018-06-29 09:32:59', NULL),
(5, 'HANDBAGS', 0, 4, '2018-05-15 03:22:47', '2018-06-28 04:12:25', '2018-06-28 04:12:25'),
(6, 'Bracelets', 43, 3, '2018-05-15 03:22:50', '2018-06-29 09:32:59', NULL),
(7, 'Bagpack', 44, 1, '2018-05-15 03:22:52', '2018-06-07 13:55:05', NULL),
(8, 'Belts', 43, 1, '2018-05-15 03:22:54', '2018-06-29 09:32:59', NULL),
(9, 'SHOES', 0, 2, '2018-05-15 03:22:57', '2018-06-30 12:52:46', '2018-06-30 12:52:46'),
(10, 'Necklaces', 1, 1, '2018-05-15 03:59:50', '2018-07-09 07:39:55', NULL),
(11, 'Bucket', 44, 2, '2018-05-15 04:00:29', '2018-06-07 13:55:05', NULL),
(12, 'Long Length', 10, 1, '2018-05-22 02:39:30', '2018-07-09 07:39:55', NULL),
(13, 'Made in Korea', 10, 2, '2018-05-22 02:39:47', '2018-07-09 07:39:55', NULL),
(14, 'Earrings', 1, 2, '2018-05-22 02:40:09', '2018-07-09 07:39:55', NULL),
(15, 'Dangle/Pendant', 14, 1, '2018-05-22 02:40:18', '2018-07-09 07:39:55', NULL),
(16, 'Adjustable Clip On', 14, 2, '2018-05-22 02:40:26', '2018-07-09 07:39:55', NULL),
(17, 'Mid Length', 10, 3, '2018-05-22 13:17:29', '2018-07-09 07:39:55', NULL),
(18, 'Chokers', 10, 4, '2018-05-22 13:17:42', '2018-07-09 07:39:55', NULL),
(19, 'Statement Necklaces', 10, 5, '2018-05-22 13:17:58', '2018-07-09 07:39:55', NULL),
(20, 'Seamless', 10, 6, '2018-05-22 13:18:10', '2018-07-09 07:36:15', '2018-07-09 07:36:15'),
(21, 'Shirt & Blouse', 10, 7, '2018-05-22 13:18:25', '2018-07-09 07:36:19', '2018-07-09 07:36:19'),
(22, 'Tank & Tube Tops', 10, 8, '2018-05-22 13:19:07', '2018-07-09 07:36:22', '2018-07-09 07:36:22'),
(23, 'T-Shirt & Polo', 10, 9, '2018-05-22 13:19:28', '2018-07-09 07:36:25', '2018-07-09 07:36:25'),
(24, 'Tunics', 10, 10, '2018-05-22 13:19:40', '2018-07-09 07:36:27', '2018-07-09 07:36:27'),
(25, 'Stud', 14, 3, '2018-05-22 13:20:17', '2018-07-09 07:39:55', NULL),
(26, 'Hoop', 14, 4, '2018-05-22 13:20:30', '2018-07-09 07:39:55', NULL),
(27, 'Earcuffs', 14, 5, '2018-05-22 13:20:42', '2018-07-09 07:39:55', NULL),
(28, 'Earring Sets', 14, 6, '2018-05-22 13:21:00', '2018-07-09 07:39:55', NULL),
(29, 'JACKETS / OUTWEAR', 1, 3, '2018-05-22 13:30:21', '2018-07-09 07:39:35', '2018-07-09 07:39:35'),
(30, 'Cape & Poncho', 29, 1, '2018-05-22 13:30:47', '2018-07-09 07:39:02', NULL),
(31, 'Coats', 29, 2, '2018-05-22 13:31:00', '2018-07-09 07:39:02', NULL),
(32, 'Hoodies', 29, 3, '2018-05-22 13:31:10', '2018-07-09 07:39:02', NULL),
(33, 'Jackets & Blazer', 29, 4, '2018-05-22 13:31:29', '2018-07-09 07:39:02', NULL),
(34, 'Shrugs & Cardigans', 29, 5, '2018-05-22 13:31:46', '2018-07-09 07:39:02', NULL),
(35, 'Sweaters', 29, 6, '2018-05-22 13:32:00', '2018-07-09 07:39:02', NULL),
(36, 'Vests', 29, 7, '2018-05-22 13:32:17', '2018-06-30 12:52:49', '2018-06-30 12:52:49'),
(37, 'Booties', 42, 1, '2018-05-22 13:33:58', '2018-06-29 09:32:59', NULL),
(38, 'Boots', 42, 2, '2018-05-22 13:34:11', '2018-06-29 09:32:59', NULL),
(39, 'Dress Shoes', 42, 3, '2018-05-22 13:34:25', '2018-06-29 09:32:59', NULL),
(40, 'Flats', 42, 4, '2018-05-22 13:34:40', '2018-06-29 09:32:59', NULL),
(41, 'Crossbody Bags', 44, 3, '2018-05-22 13:39:00', '2018-06-07 13:55:05', NULL),
(42, 'SHOES', 9, 1, '2018-05-22 13:45:19', '2018-06-29 09:32:59', NULL),
(43, 'ACCESSORIES', 3, 1, '2018-05-22 13:45:58', '2018-06-29 09:32:59', NULL),
(44, 'HANDBAGS', 5, 1, '2018-05-22 13:46:15', '2018-06-07 13:55:05', NULL),
(45, 'MEN', 0, 4, '2018-05-22 13:46:33', '2018-06-30 12:52:43', '2018-06-30 12:52:43'),
(46, 'MEN', 45, 1, '2018-05-22 13:46:53', '2018-06-29 09:32:59', NULL),
(47, 'Casual Shirts', 46, 1, '2018-05-22 13:47:15', '2018-06-29 09:32:59', NULL),
(48, 'Denim', 46, 2, '2018-05-22 13:47:36', '2018-06-29 09:32:59', NULL),
(49, 'Dress Shirts', 46, 3, '2018-05-22 13:47:50', '2018-06-29 09:32:59', NULL),
(50, 'Graphic', 46, 4, '2018-05-22 13:48:04', '2018-06-29 09:32:59', NULL),
(51, 'KIDS', 0, 5, '2018-05-22 13:48:21', '2018-06-30 12:52:23', '2018-06-30 12:52:23'),
(52, 'KIDS', 51, 1, '2018-05-22 13:48:26', '2018-06-29 09:32:59', NULL),
(53, 'Boys', 52, 1, '2018-05-22 13:48:56', '2018-06-29 09:32:59', NULL),
(54, 'Girls', 52, 2, '2018-05-22 13:49:03', '2018-06-29 09:32:59', NULL),
(55, 'Infants', 52, 3, '2018-05-22 13:49:21', '2018-06-29 09:32:59', NULL),
(56, 'MORE', 0, 7, '2018-05-22 13:50:00', '2018-06-30 12:52:14', '2018-06-30 12:52:14'),
(57, 'MORE', 56, 1, '2018-05-22 13:50:04', '2018-06-29 09:32:59', NULL),
(58, 'Cosmetics', 57, 1, '2018-05-22 13:50:22', '2018-06-29 09:32:59', NULL),
(59, 'Fixtures/Display', 57, 2, '2018-05-22 13:50:33', '2018-06-29 09:32:59', NULL),
(60, 'Key Chains', 57, 3, '2018-05-22 13:50:43', '2018-06-29 09:32:59', NULL),
(61, 'test2', 0, 8, '2018-06-29 09:32:30', '2018-06-29 09:33:09', '2018-06-29 09:33:09'),
(62, 'Accessories', 0, 2, '2018-06-30 12:52:36', '2018-12-01 09:25:04', '2018-12-01 09:25:04'),
(63, 'Second Level', 0, 3, '2018-06-30 12:53:11', '2018-07-09 07:42:20', '2018-07-09 07:42:20'),
(64, 'Second Level Item', 63, 1, '2018-06-30 12:53:22', '2018-07-09 07:39:55', NULL),
(65, 'Made in Korea', 14, 7, '2018-07-09 07:39:28', '2018-07-09 07:39:55', NULL),
(66, 'Beach Towels', 62, 1, '2018-07-09 07:41:16', '2018-07-09 07:41:16', NULL),
(67, 'Ponchos and Vests', 62, 2, '2018-07-09 07:41:29', '2018-07-09 07:41:29', NULL),
(68, 'Bags and Clutches', 62, 3, '2018-07-09 07:41:46', '2018-07-09 07:41:46', NULL),
(69, 'Handbags', 68, 1, '2018-07-09 07:42:35', '2018-07-09 07:42:35', NULL),
(70, 'Backpacks', 68, 2, '2018-07-09 07:42:44', '2018-07-09 07:42:44', NULL),
(71, 'Clutches', 68, 3, '2018-07-09 07:42:57', '2018-07-09 07:42:57', NULL),
(72, 'Cosmetic Bags', 68, 4, '2018-07-09 07:43:08', '2018-07-09 07:43:08', NULL),
(73, 'Coin Purse', 68, 5, '2018-07-09 07:43:17', '2018-07-09 07:43:17', NULL),
(74, 'Hand Made Handbags', 68, 6, '2018-07-09 07:43:35', '2018-07-09 07:43:35', NULL),
(75, 'Crossbody Bags', 68, 7, '2018-07-09 07:43:53', '2018-07-09 07:43:53', NULL),
(76, 'Fanny Packs', 68, 8, '2018-07-09 07:44:04', '2018-07-09 07:44:04', NULL),
(77, 'Parent', 0, 3, '2018-07-09 09:29:40', '2018-12-01 09:25:07', '2018-12-01 09:25:07'),
(78, 'Clothing', 0, 4, '2018-08-09 05:21:45', '2018-12-01 09:25:09', '2018-12-01 09:25:09'),
(79, 'Dresses', 78, 1, '2018-08-09 05:21:50', '2018-08-09 05:21:50', NULL),
(80, 'WOMEN', 0, 1, '2018-12-01 09:25:31', '2018-12-01 09:28:26', NULL),
(81, 'TRF', 0, 2, '2018-12-01 09:25:38', '2018-12-01 09:28:26', NULL),
(82, 'MAN', 0, 3, '2018-12-01 09:25:45', '2018-12-01 09:28:26', NULL),
(83, 'KIDS', 0, 4, '2018-12-01 09:25:51', '2018-12-01 09:28:26', NULL),
(84, 'SHOES', 0, 5, '2018-12-01 09:26:00', '2018-12-01 09:28:26', NULL),
(85, 'COATS', 80, 1, '2018-12-01 09:26:24', '2018-12-01 09:28:26', NULL),
(86, 'JACKETS', 80, 2, '2018-12-01 09:26:32', '2018-12-01 09:28:26', NULL),
(87, 'BLAZERS', 80, 3, '2018-12-01 09:26:45', '2018-12-01 09:28:26', NULL),
(88, 'DRESSES', 80, 4, '2018-12-01 09:26:54', '2018-12-01 09:28:26', NULL),
(89, 'JUMPSUITS', 80, 5, '2018-12-01 09:27:05', '2018-12-01 09:28:26', NULL),
(90, 'OUTERWEAR', 81, 1, '2018-12-01 09:27:27', '2018-12-01 09:28:26', NULL),
(91, 'JACKETS', 81, 2, '2018-12-01 09:27:42', '2018-12-01 09:28:26', NULL),
(92, 'DRESSES', 81, 3, '2018-12-01 09:27:51', '2018-12-01 09:28:26', NULL),
(93, 'PANTS', 82, 1, '2018-12-01 09:28:13', '2018-12-01 09:28:26', NULL),
(94, 'T-SHIRTS', 82, 2, '2018-12-01 09:28:22', '2018-12-01 09:28:26', NULL),
(95, 'POLOS', 82, 3, '2018-12-01 09:28:35', '2018-12-01 09:28:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_banners`
--

DROP TABLE IF EXISTS `category_banners`;
CREATE TABLE IF NOT EXISTS `category_banners` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_banners`
--

INSERT INTO `category_banners` (`id`, `type`, `vendor_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2018-06-07 15:19:59', '2018-06-07 15:19:59'),
(2, 2, 2, 1, '2018-06-07 15:25:45', '2018-06-07 15:25:45'),
(3, 3, 1, 1, '2018-06-07 15:25:49', '2018-06-07 15:25:49'),
(6, 3, 2, 1, '2018-06-07 15:30:23', '2018-06-07 15:30:23'),
(8, 2, 1, 1, '2018-06-08 10:28:20', '2018-06-08 10:28:20'),
(7, 1, 2, 1, '2018-06-08 09:09:28', '2018-06-08 09:09:28'),
(10, 1, 1, 0, '2018-06-11 05:49:31', '2018-06-11 05:49:31'),
(11, 1, 2, 0, '2018-06-11 12:52:37', '2018-06-11 12:52:37'),
(12, 2, 1, 0, '2018-06-11 12:52:40', '2018-06-11 12:52:40'),
(13, 2, 2, 0, '2018-06-11 12:52:44', '2018-06-11 12:52:44'),
(14, 3, 1, 0, '2018-06-11 12:52:48', '2018-06-11 12:52:48'),
(15, 3, 2, 0, '2018-06-11 12:52:51', '2018-06-11 12:52:51');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `master_color_id` int(11) DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbs_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `status`, `master_color_id`, `image_path`, `thumbs_image_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BEIGE', 1, 14, NULL, NULL, '2018-05-11 09:11:15', '2018-06-25 07:55:30', NULL),
(5, 'BLACK MULTI', 1, 8, NULL, NULL, '2018-05-22 14:38:46', '2018-06-10 03:53:16', NULL),
(2, 'tet2', 1, 1, NULL, NULL, '2018-05-12 02:29:35', '2018-05-12 03:15:09', '2018-05-12 03:15:09'),
(3, 'DeepBlue2', 1, 4, NULL, NULL, '2018-05-17 10:36:52', '2018-05-17 10:36:59', '2018-05-17 10:36:59'),
(4, 'BLACK', 1, 8, NULL, NULL, '2018-05-18 10:55:03', '2018-06-10 03:34:39', NULL),
(6, 'BLUE', 1, 9, NULL, NULL, '2018-05-22 14:38:59', '2018-06-10 03:53:20', NULL),
(7, 'BLUE MULTI', 1, 9, NULL, NULL, '2018-05-22 14:39:12', '2018-06-10 03:53:25', NULL),
(8, 'BLUE/YELLOW', 1, 15, NULL, NULL, '2018-05-22 14:39:26', '2018-06-10 03:53:30', NULL),
(9, 'BLUSH', 1, 17, NULL, NULL, '2018-05-22 14:39:36', '2018-07-07 10:00:00', '2018-07-07 10:00:00'),
(10, 'BRICK', 1, 10, NULL, NULL, '2018-05-22 14:39:47', '2018-05-22 14:39:47', NULL),
(11, 'BROWN', 1, 10, NULL, NULL, '2018-05-22 14:40:01', '2018-06-10 03:53:39', NULL),
(12, 'CAMEL', 1, 15, NULL, NULL, '2018-05-22 14:40:09', '2018-06-10 03:53:43', NULL),
(13, 'CHARCOAL', 1, 8, NULL, NULL, '2018-05-22 14:40:18', '2018-05-22 14:41:13', NULL),
(14, 'CORAL', 1, 16, NULL, NULL, '2018-05-22 14:40:31', '2018-06-10 03:53:51', NULL),
(15, 'GREEN', 1, 13, NULL, NULL, '2018-05-23 04:14:51', '2018-06-10 03:53:56', NULL),
(16, 'RED', 1, 7, NULL, NULL, '2018-05-23 04:14:57', '2018-06-10 03:54:07', NULL),
(17, 'WHITE', 1, 21, NULL, NULL, '2018-05-23 04:15:04', '2018-05-23 04:15:04', NULL),
(18, 'PINK', 1, 17, NULL, NULL, '2018-05-23 04:24:10', '2018-06-10 03:54:02', NULL),
(19, 'JADE', 1, 14, NULL, NULL, '2018-05-23 15:23:45', '2018-05-23 15:23:45', NULL),
(20, 'WHITE', 1, 21, NULL, NULL, '2018-05-23 15:24:04', '2018-05-23 15:24:04', NULL),
(21, 'YELLOW', 1, 22, NULL, NULL, '2018-05-23 15:24:18', '2018-05-23 15:24:18', NULL),
(22, 'PINK', 1, 17, NULL, NULL, '2018-05-23 15:27:43', '2018-05-23 15:27:43', NULL),
(23, 'MINT', 1, 13, NULL, NULL, '2018-05-23 15:28:02', '2018-05-23 15:28:02', NULL),
(24, '122', 1, 9, NULL, NULL, '2018-05-26 09:47:53', '2018-05-26 09:53:27', '2018-05-26 09:53:27'),
(25, '3', 1, 10, NULL, NULL, '2018-05-26 09:52:58', '2018-05-26 09:53:24', '2018-05-26 09:53:24'),
(26, 'test', 1, 9, NULL, NULL, '2018-06-10 03:15:20', '2018-06-22 05:36:56', '2018-06-22 05:36:56'),
(27, 'tttt', 1, 8, '/images/vendors/1/colors/19af7a90-6f29-11e8-8d9e-99bd1237045b.jpg', '/images/vendors/1/colors/thumbs/19af7a90-6f29-11e8-8d9e-99bd1237045b.jpg', '2018-06-13 10:40:13', '2018-06-13 15:46:47', '2018-06-13 15:46:47'),
(28, 'fdf', 1, 8, NULL, NULL, '2018-06-22 05:37:02', '2018-06-22 05:37:10', '2018-06-22 05:37:10'),
(29, 'sdf', 1, 9, '/images/vendors/1/colors/9fb374f0-7610-11e8-9d3a-cf28bc1263a7.jpg', '/images/vendors/1/colors/thumbs/9fb374f0-7610-11e8-9d3a-cf28bc1263a7.jpg', '2018-06-22 05:37:20', '2018-06-22 05:37:24', '2018-06-22 05:37:24'),
(30, 'rt', 1, 8, NULL, NULL, '2018-06-25 07:56:25', '2018-06-25 07:56:28', '2018-06-25 07:56:28'),
(31, 'trt', 1, 8, NULL, NULL, '2018-06-25 10:52:40', '2018-06-25 10:52:43', '2018-06-25 10:52:43'),
(35, 'sadf', 1, 9, NULL, NULL, '2018-06-28 09:40:47', '2018-06-28 09:40:47', NULL),
(36, 'asdf', 1, 20, NULL, NULL, '2018-06-28 09:43:58', '2018-06-28 09:43:58', NULL),
(37, 'bsdf', 1, 19, NULL, NULL, '2018-06-28 09:55:47', '2018-06-28 09:55:47', NULL),
(38, 'v', 1, 20, NULL, NULL, '2018-06-28 10:42:02', '2018-06-28 10:42:02', NULL),
(39, 'fasd', 1, 8, NULL, NULL, '2018-06-28 12:21:52', '2018-06-28 12:21:52', NULL),
(40, 'bl', 1, 13, NULL, NULL, '2018-06-28 13:26:09', '2018-06-28 13:26:09', NULL),
(41, 'gfsad22', 1, 8, '/images/colors/38155b90-7bb6-11e8-b7ad-a176e0cfc740.jpg', '/images/colors/thumbs/38155b90-7bb6-11e8-b7ad-a176e0cfc740.jpg', '2018-06-29 10:05:08', '2018-06-29 10:05:44', '2018-06-29 10:05:44'),
(42, 'asdfdf', 1, 20, NULL, NULL, '2018-06-30 10:28:36', '2018-06-30 10:28:36', NULL),
(43, 'GOLD', 1, 11, NULL, NULL, '2018-07-09 07:50:58', '2018-07-09 07:50:58', NULL),
(44, 'SILVER', 1, 19, NULL, NULL, '2018-07-09 07:51:04', '2018-07-09 07:51:04', NULL),
(45, 'AMBER', 1, 8, NULL, NULL, '2018-07-09 07:53:03', '2018-07-09 07:53:03', NULL),
(46, 'GREY', 1, 12, NULL, NULL, '2018-07-09 07:53:14', '2018-07-09 07:53:14', NULL),
(47, 'GUNMETAL', 1, 19, NULL, NULL, '2018-07-09 07:53:22', '2018-07-09 07:53:22', NULL),
(48, 'Navy', 1, NULL, NULL, NULL, '2018-08-09 05:23:15', '2018-08-09 05:23:15', NULL),
(49, 'JHAKANAKA', 1, NULL, NULL, NULL, '2018-09-24 06:48:42', '2018-09-24 06:48:42', NULL),
(50, 'TAN', 1, 10, NULL, NULL, '2018-12-01 09:32:16', '2018-12-01 09:32:16', NULL),
(51, 'BURGUNDY', 1, 10, NULL, NULL, '2018-12-01 09:32:41', '2018-12-01 09:32:41', NULL),
(52, 'MID-CAMEL', 1, 11, NULL, NULL, '2018-12-01 09:42:53', '2018-12-01 09:42:53', NULL),
(53, 'CHEETAH', 1, NULL, NULL, NULL, '2018-12-13 02:43:45', '2018-12-13 02:43:45', NULL),
(54, 'asdfsdf', 1, 20, NULL, NULL, '2018-12-27 06:10:39', '2018-12-27 06:10:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `color_item`
--

DROP TABLE IF EXISTS `color_item`;
CREATE TABLE IF NOT EXISTS `color_item` (
  `item_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `color_item`
--

INSERT INTO `color_item` (`item_id`, `color_id`, `available`) VALUES
(2, 4, 1),
(3, 4, 1),
(3, 40, 1),
(4, 4, 1),
(5, 43, 1),
(5, 44, 1),
(6, 45, 1),
(6, 46, 1),
(6, 47, 1),
(7, 46, 1),
(7, 44, 1),
(8, 4, 1),
(9, 46, 1),
(9, 44, 1),
(10, 4, 1),
(10, 20, 1),
(12, 4, 0),
(12, 20, 0),
(13, 4, 1),
(13, 20, 0),
(14, 4, 1),
(14, 20, 0),
(14, 23, 0),
(15, 4, 0),
(15, 23, 1),
(15, 20, 1),
(16, 4, 0),
(16, 23, 1),
(16, 20, 1),
(17, 17, 1),
(17, 4, 1),
(18, 45, 1),
(18, 46, 1),
(18, 47, 1),
(19, 48, 1),
(19, 13, 1),
(19, 4, 1),
(20, 49, 1),
(20, 17, 1),
(20, 16, 1),
(20, 15, 1),
(21, 51, 0),
(21, 50, 1),
(22, 11, 1),
(23, 52, 1),
(24, 4, 1),
(25, 53, 1),
(26, 4, 1),
(27, 4, 1),
(28, 4, 1),
(29, 51, 0),
(29, 50, 1),
(29, 4, 0),
(30, 52, 1),
(31, 52, 1),
(34, 52, 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=243 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'US', 'United States'),
(2, 'CA', 'Canada'),
(3, 'AF', 'Afghanistan'),
(4, 'AL', 'Albania'),
(5, 'DZ', 'Algeria'),
(6, 'AS', 'American Samoa'),
(7, 'AD', 'Andorra'),
(8, 'AO', 'Angola'),
(9, 'AI', 'Anguilla'),
(10, 'AQ', 'Antarctica'),
(11, 'AG', 'Antigua and/or Barbuda'),
(12, 'AR', 'Argentina'),
(13, 'AM', 'Armenia'),
(14, 'AW', 'Aruba'),
(15, 'AU', 'Australia'),
(16, 'AT', 'Austria'),
(17, 'AZ', 'Azerbaijan'),
(18, 'BS', 'Bahamas'),
(19, 'BH', 'Bahrain'),
(20, 'BD', 'Bangladesh'),
(21, 'BB', 'Barbados'),
(22, 'BY', 'Belarus'),
(23, 'BE', 'Belgium'),
(24, 'BZ', 'Belize'),
(25, 'BJ', 'Benin'),
(26, 'BM', 'Bermuda'),
(27, 'BT', 'Bhutan'),
(28, 'BO', 'Bolivia'),
(29, 'BA', 'Bosnia and Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BV', 'Bouvet Island'),
(32, 'BR', 'Brazil'),
(33, 'IO', 'British lndian Ocean Territory'),
(34, 'BN', 'Brunei Darussalam'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CV', 'Cape Verde'),
(41, 'KY', 'Cayman Islands'),
(42, 'CF', 'Central African Republic'),
(43, 'TD', 'Chad'),
(44, 'CL', 'Chile'),
(45, 'CN', 'China'),
(46, 'CX', 'Christmas Island'),
(47, 'CC', 'Cocos (Keeling) Islands'),
(48, 'CO', 'Colombia'),
(49, 'KM', 'Comoros'),
(50, 'CG', 'Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'CD', 'Democratic Republic of Congo'),
(58, 'DK', 'Denmark'),
(59, 'DJ', 'Djibouti'),
(60, 'DM', 'Dominica'),
(61, 'DO', 'Dominican Republic'),
(62, 'TP', 'East Timor'),
(63, 'EC', 'Ecudaor'),
(64, 'EG', 'Egypt'),
(65, 'SV', 'El Salvador'),
(66, 'GQ', 'Equatorial Guinea'),
(67, 'ER', 'Eritrea'),
(68, 'EE', 'Estonia'),
(69, 'ET', 'Ethiopia'),
(70, 'FK', 'Falkland Islands (Malvinas)'),
(71, 'FO', 'Faroe Islands'),
(72, 'FJ', 'Fiji'),
(73, 'FI', 'Finland'),
(74, 'FR', 'France'),
(75, 'FX', 'France, Metropolitan'),
(76, 'GF', 'French Guiana'),
(77, 'PF', 'French Polynesia'),
(78, 'TF', 'French Southern Territories'),
(79, 'GA', 'Gabon'),
(80, 'GM', 'Gambia'),
(81, 'GE', 'Georgia'),
(82, 'DE', 'Germany'),
(83, 'GH', 'Ghana'),
(84, 'GI', 'Gibraltar'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Hungary'),
(99, 'IS', 'Iceland'),
(100, 'IN', 'India'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'JO', 'Jordan'),
(111, 'KZ', 'Kazakhstan'),
(112, 'KE', 'Kenya'),
(113, 'KI', 'Kiribati'),
(114, 'KP', 'Korea, Democratic People\'s Republic of'),
(115, 'KR', 'Korea, Republic of'),
(116, 'KW', 'Kuwait'),
(117, 'KG', 'Kyrgyzstan'),
(118, 'LA', 'Lao People\'s Democratic Republic'),
(119, 'LV', 'Latvia'),
(120, 'LB', 'Lebanon'),
(121, 'LS', 'Lesotho'),
(122, 'LR', 'Liberia'),
(123, 'LY', 'Libyan Arab Jamahiriya'),
(124, 'LI', 'Liechtenstein'),
(125, 'LT', 'Lithuania'),
(126, 'LU', 'Luxembourg'),
(127, 'MO', 'Macau'),
(128, 'MK', 'Macedonia'),
(129, 'MG', 'Madagascar'),
(130, 'MW', 'Malawi'),
(131, 'MY', 'Malaysia'),
(132, 'MV', 'Maldives'),
(133, 'ML', 'Mali'),
(134, 'MT', 'Malta'),
(135, 'MH', 'Marshall Islands'),
(136, 'MQ', 'Martinique'),
(137, 'MR', 'Mauritania'),
(138, 'MU', 'Mauritius'),
(139, 'TY', 'Mayotte'),
(140, 'MX', 'Mexico'),
(141, 'FM', 'Micronesia, Federated States of'),
(142, 'MD', 'Moldova, Republic of'),
(143, 'MC', 'Monaco'),
(144, 'MN', 'Mongolia'),
(145, 'MS', 'Montserrat'),
(146, 'MA', 'Morocco'),
(147, 'MZ', 'Mozambique'),
(148, 'MM', 'Myanmar'),
(149, 'NA', 'Namibia'),
(150, 'NR', 'Nauru'),
(151, 'NP', 'Nepal'),
(152, 'NL', 'Netherlands'),
(153, 'AN', 'Netherlands Antilles'),
(154, 'NC', 'New Caledonia'),
(155, 'NZ', 'New Zealand'),
(156, 'NI', 'Nicaragua'),
(157, 'NE', 'Niger'),
(158, 'NG', 'Nigeria'),
(159, 'NU', 'Niue'),
(160, 'NF', 'Norfork Island'),
(161, 'MP', 'Northern Mariana Islands'),
(162, 'NO', 'Norway'),
(163, 'OM', 'Oman'),
(164, 'PK', 'Pakistan'),
(165, 'PW', 'Palau'),
(166, 'PA', 'Panama'),
(167, 'PG', 'Papua New Guinea'),
(168, 'PY', 'Paraguay'),
(169, 'PE', 'Peru'),
(170, 'PH', 'Philippines'),
(171, 'PN', 'Pitcairn'),
(172, 'PL', 'Poland'),
(173, 'PT', 'Portugal'),
(174, 'PR', 'Puerto Rico'),
(175, 'QA', 'Qatar'),
(176, 'SS', 'Republic of South Sudan'),
(177, 'RE', 'Reunion'),
(178, 'RO', 'Romania'),
(179, 'RU', 'Russian Federation'),
(180, 'RW', 'Rwanda'),
(181, 'KN', 'Saint Kitts and Nevis'),
(182, 'LC', 'Saint Lucia'),
(183, 'VC', 'Saint Vincent and the Grenadines'),
(184, 'WS', 'Samoa'),
(185, 'SM', 'San Marino'),
(186, 'ST', 'Sao Tome and Principe'),
(187, 'SA', 'Saudi Arabia'),
(188, 'SN', 'Senegal'),
(189, 'RS', 'Serbia'),
(190, 'SC', 'Seychelles'),
(191, 'SL', 'Sierra Leone'),
(192, 'SG', 'Singapore'),
(193, 'SK', 'Slovakia'),
(194, 'SI', 'Slovenia'),
(195, 'SB', 'Solomon Islands'),
(196, 'SO', 'Somalia'),
(197, 'ZA', 'South Africa'),
(198, 'GS', 'South Georgia South Sandwich Islands'),
(199, 'ES', 'Spain'),
(200, 'LK', 'Sri Lanka'),
(201, 'SH', 'St. Helena'),
(202, 'PM', 'St. Pierre and Miquelon'),
(203, 'SD', 'Sudan'),
(204, 'SR', 'Suriname'),
(205, 'SJ', 'Svalbarn and Jan Mayen Islands'),
(206, 'SZ', 'Swaziland'),
(207, 'SE', 'Sweden'),
(208, 'CH', 'Switzerland'),
(209, 'SY', 'Syrian Arab Republic'),
(210, 'TW', 'Taiwan'),
(211, 'TJ', 'Tajikistan'),
(212, 'TZ', 'Tanzania, United Republic of'),
(213, 'TH', 'Thailand'),
(214, 'TG', 'Togo'),
(215, 'TK', 'Tokelau'),
(216, 'TO', 'Tonga'),
(217, 'TT', 'Trinidad and Tobago'),
(218, 'TN', 'Tunisia'),
(219, 'TR', 'Turkey'),
(220, 'TM', 'Turkmenistan'),
(221, 'TC', 'Turks and Caicos Islands'),
(222, 'TV', 'Tuvalu'),
(223, 'UG', 'Uganda'),
(224, 'UA', 'Ukraine'),
(225, 'AE', 'United Arab Emirates'),
(226, 'GB', 'United Kingdom'),
(227, 'UM', 'United States minor outlying islands'),
(228, 'UY', 'Uruguay'),
(229, 'UZ', 'Uzbekistan'),
(230, 'VU', 'Vanuatu'),
(231, 'VA', 'Vatican City State'),
(232, 'VE', 'Venezuela'),
(233, 'VN', 'Vietnam'),
(234, 'VG', 'Virgin Islands (British)'),
(235, 'VI', 'Virgin Islands (U.S.)'),
(236, 'WF', 'Wallis and Futuna Islands'),
(237, 'EH', 'Western Sahara'),
(238, 'YE', 'Yemen'),
(239, 'YU', 'Yugoslavia'),
(240, 'ZR', 'Zaire'),
(241, 'ZM', 'Zambia'),
(242, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

DROP TABLE IF EXISTS `couriers`;
CREATE TABLE IF NOT EXISTS `couriers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `couriers`
--

INSERT INTO `couriers` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'UPS', '2018-05-31 13:43:21', '2018-05-31 13:46:34', NULL),
(2, 'tyty', '2018-05-31 13:48:22', '2018-05-31 13:48:25', '2018-05-31 13:48:25'),
(3, 'Others', '2018-06-01 03:38:03', '2018-07-02 09:20:12', '2018-07-02 09:20:12'),
(4, 'dfdf2', '2018-07-02 09:20:16', '2018-07-02 09:20:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fabrics`
--

DROP TABLE IF EXISTS `fabrics`;
CREATE TABLE IF NOT EXISTS `fabrics` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_fabric_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fabrics`
--

INSERT INTO `fabrics` (`id`, `name`, `master_fabric_id`, `status`, `default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, '100% COTTON', 5, 1, 0, '2018-05-12 11:02:30', '2018-06-30 09:57:35', NULL),
(10, 'er', 2, 1, 0, '2018-05-12 11:02:54', '2018-05-12 11:02:57', '2018-05-12 11:02:57'),
(8, '100% POLYAMIDE', 37, 1, 0, '2018-05-12 09:49:52', '2018-06-30 09:57:35', NULL),
(7, '53% COTTON 47% ACETATE', 5, 1, 0, '2018-05-12 09:49:28', '2018-06-30 09:57:35', NULL),
(6, '100% ACRYLIC', 43, 1, 0, '2018-05-12 09:48:02', '2018-06-30 09:57:35', NULL),
(11, 'dd2', 3, 1, 0, '2018-05-17 01:25:56', '2018-05-17 01:26:07', '2018-05-17 01:26:07'),
(12, '59% POLYESTER 35% POLYAMIDE 6% SPANDEX', 28, 1, 0, '2018-05-23 15:22:05', '2018-06-30 09:57:35', NULL),
(13, '60% COTTON 37% POLYAMIDE 3% SPANDEX', 3, 1, 0, '2018-05-23 15:22:16', '2018-06-30 09:57:35', NULL),
(14, '64% COTTON 36% POLYESTER', 5, 1, 0, '2018-05-23 15:22:25', '2018-06-30 09:57:35', NULL),
(15, '122', 3, 1, 1, '2018-05-26 11:42:54', '2018-05-26 12:03:45', '2018-05-26 12:03:45'),
(16, '3334', 43, 1, 1, '2018-06-30 09:55:59', '2018-06-30 09:57:39', '2018-06-30 09:57:39');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

DROP TABLE IF EXISTS `industries`;
CREATE TABLE IF NOT EXISTS `industries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'erd', '2018-05-14 04:26:39', '2018-05-14 04:28:21', '2018-05-14 04:28:21'),
(2, 'df', '2018-05-14 04:27:04', '2018-05-14 05:14:22', '2018-05-14 05:14:22'),
(3, 'Women\'s Clothing', '2018-05-14 05:14:32', '2018-05-14 05:14:32', NULL),
(4, 'Men\'s Clothing', '2018-05-14 05:14:44', '2018-05-14 05:14:44', NULL),
(5, 'Children\'s Clothings', '2018-05-14 05:14:59', '2018-05-14 05:14:59', NULL),
(6, 'Accessories', '2018-05-14 05:15:09', '2018-05-14 05:15:09', NULL),
(7, 'Shoes', '2018-05-14 05:15:19', '2018-05-14 05:15:19', NULL),
(8, 'Fixure', '2018-05-14 05:15:28', '2018-05-14 05:15:28', NULL),
(9, 'Handbag', '2018-05-14 05:15:38', '2018-05-14 05:15:38', NULL),
(10, 'Cosmetic', '2018-05-14 05:15:47', '2018-05-14 05:15:47', NULL),
(11, 'Lingerie', '2018-05-14 05:15:58', '2018-05-14 05:15:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `industry_user`
--

DROP TABLE IF EXISTS `industry_user`;
CREATE TABLE IF NOT EXISTS `industry_user` (
  `user_id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industry_user`
--

INSERT INTO `industry_user` (`user_id`, `industry_id`) VALUES
(1, 3),
(1, 5),
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `style_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) NOT NULL,
  `orig_price` double(8,2) DEFAULT NULL,
  `pack_id` int(11) NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_on` date DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_parent_category` int(11) NOT NULL,
  `default_second_category` int(11) DEFAULT NULL,
  `default_third_category` int(11) DEFAULT NULL,
  `min_qty` int(11) DEFAULT NULL,
  `fabric` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `made_in_id` int(11) DEFAULT NULL,
  `labeled` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `status`, `style_no`, `video`, `price`, `orig_price`, `pack_id`, `sorting`, `description`, `available_on`, `availability`, `name`, `default_parent_category`, `default_second_category`, `default_third_category`, `min_qty`, `fabric`, `made_in_id`, `labeled`, `memo`, `activated_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 0, 'TEST!2', NULL, 12.00, NULL, 4, 6, NULL, '2018-07-04', 1, 'rerer', 1, 10, 17, 6, NULL, 1, 'labeled', NULL, '2018-07-02 07:32:31', '2018-06-30 11:21:50', '2018-07-21 05:02:42', NULL),
(3, 0, 'TEST!2-Clone-delete-103831705', NULL, 12.00, NULL, 4, 5, NULL, '2018-07-04', NULL, 'rerer', 1, 10, 17, 6, NULL, 1, 'labeled', NULL, NULL, '2018-06-30 12:15:27', '2018-06-30 12:16:50', '2018-06-30 12:16:50'),
(4, 0, 'TEST!2-Clone-delete-2017475691', NULL, 12.00, NULL, 4, 10, NULL, '2018-07-04', 1, 'rerer', 62, NULL, NULL, 6, NULL, 1, 'labeled', NULL, '2018-07-02 07:32:31', '2018-06-30 12:17:00', '2018-08-09 05:43:30', '2018-08-09 05:43:30'),
(5, 0, 'N01', NULL, 12.00, 15.00, 1, NULL, NULL, NULL, 1, 'Flat Metallic Cut Disc Pendant Necklace', 1, 10, 12, 6, NULL, 1, 'labeled', NULL, '2018-11-21 08:38:46', '2018-07-09 07:51:06', '2018-12-01 09:34:11', NULL),
(6, 0, 'N343', NULL, 54.00, NULL, 3, NULL, 'nbm', NULL, 2, 'Open Raindrop Netted Bead Pendant Necklace', 1, 10, 12, NULL, NULL, NULL, NULL, NULL, '2018-11-21 08:38:46', '2018-07-09 07:53:38', '2018-12-01 09:34:11', NULL),
(7, 0, 'N734', NULL, 67.00, NULL, 1, 6, NULL, '2018-07-21', 2, 'Tiered Ring Triangle Rhinestone Charm Necklace', 1, 10, 13, NULL, NULL, NULL, NULL, NULL, '2018-07-09 07:56:59', '2018-07-09 07:56:56', '2018-12-01 09:34:11', NULL),
(8, 0, 'N67', NULL, 34.00, NULL, 2, 7, NULL, '2018-07-18', 2, 'Scorpio Pendant Necklace.', 1, 10, 13, NULL, NULL, NULL, NULL, NULL, '2018-07-09 07:58:51', '2018-07-09 07:58:48', '2018-12-01 09:34:11', NULL),
(9, 0, 'N546', NULL, 45.00, NULL, 1, NULL, NULL, NULL, 2, 'Layered Metallic Warped Circle Pendant Necklace', 1, 10, 17, NULL, NULL, NULL, NULL, NULL, '2018-11-21 08:38:46', '2018-07-09 08:00:33', '2018-12-01 09:34:11', NULL),
(14, 0, 'avai', NULL, 12.00, NULL, 1, 11, NULL, NULL, 2, NULL, 1, 10, 12, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-19 13:49:18', '2018-07-21 05:02:42', NULL),
(15, 0, 'avai-Clone', NULL, 12.00, NULL, 1, 5, NULL, NULL, 2, NULL, 1, 10, 12, NULL, NULL, NULL, NULL, NULL, '2018-07-19 14:04:44', '2018-07-19 14:00:45', '2018-12-01 09:34:11', NULL),
(16, 0, 'avai-Clone-Clone', NULL, 12.00, NULL, 1, 8, NULL, NULL, 2, NULL, 1, 10, 12, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-20 04:25:13', '2018-12-01 09:34:11', NULL),
(17, 0, 'Test', NULL, 10.99, 12.99, 1, 5, 'Test', '2017-09-08', 2, 'Test', 1, 10, 12, 6, '100% COTTON', 16, NULL, 'Test', NULL, '2018-07-20 06:18:12', '2018-08-07 06:01:56', NULL),
(18, 0, 'N343-Clone', NULL, 54.00, NULL, 1, NULL, NULL, NULL, 2, 'Open Raindrop Netted Bead Pendant Necklace', 1, 14, 15, NULL, NULL, NULL, NULL, NULL, '2018-11-21 08:38:46', '2018-07-23 05:35:51', '2018-12-01 09:34:11', NULL),
(19, 0, 'SFJ100X', NULL, 21.00, NULL, 11, NULL, 'Hippie Chic Plus Size Adjustable Strap Tank Top Jumpsuit with Pockets', NULL, 2, NULL, 78, 79, NULL, NULL, '95% Rayon 5% Spandex', NULL, NULL, NULL, NULL, '2018-08-09 05:23:15', '2018-08-09 05:25:31', NULL),
(20, 0, '9383 TEST', NULL, 15.50, 18.00, 1, NULL, 'desc220\r\n2\r\nadfasd', NULL, 2, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 10, 12, NULL, 'asdfad', 16, 'labeled', NULL, NULL, '2018-09-11 10:30:01', '2018-10-11 07:20:03', NULL),
(21, 1, 'SOFT FAUX FUR COAT', NULL, 49.99, NULL, 1, 2, 'Coat with lapel collar and long sleeves. Hidden side in-seam pockets. Hidden front snap button closure.', NULL, 2, 'SOFT FAUX FUR COAT', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 10:34:48', '2018-12-01 09:33:58', '2019-01-05 02:06:38', NULL),
(22, 1, 'FAUX FUR COAT', NULL, 169.00, NULL, 1, 4, 'Coat with lapel collar and long sleeves. Front welt pockets. Front metal hook closure.', NULL, 2, 'FAUX FUR COAT', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 10:34:48', '2018-12-01 09:37:41', '2018-12-17 08:45:28', NULL),
(23, 1, 'DOUBLE-BREASTED COAT', 'videos/79875510-f89e-11e8-8302-e7c6333957d2.mp4', 199.00, NULL, 1, 3, 'Coat with lapel collar and long sleeves. Front flap pockets and chest welt pockets. Front double-breasted button closure.', NULL, 2, 'DOUBLE-BREASTED COAT', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 10:34:48', '2018-12-01 09:43:20', '2018-12-20 09:03:00', NULL),
(24, 0, 'video', 'videos/5df471e0-f88f-11e8-9f8c-e785cafb532b.mp4', 34.00, NULL, 1, NULL, NULL, NULL, 2, 'video', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-05 07:14:44', '2018-12-04 11:25:03', '2018-12-05 09:00:02', NULL),
(25, 0, 'J17757H CHEET..', NULL, 12.00, NULL, 1, NULL, 'ANIMAL PRINT HI MULTI CHIFFON LONG LENGTH KIMONO\n\n100% POLYESTER. \nMADE IN USA.\n\nModel is wearing a Small \n\nModel\'s Profile: Height 5\'11\"  Bust 32\" Waist 23\" Hips 35\"', NULL, 2, 'ANIMAL PRINT HI MULTI CHIFFON LONG LENGTH KIMONO', 80, 85, NULL, NULL, '100% POLYESTER', 16, 'labeled', NULL, NULL, '2018-12-13 02:43:45', '2018-12-13 02:43:45', NULL),
(26, 0, 'gif-delete-141356303', NULL, 34.00, NULL, 1, NULL, NULL, NULL, 2, 'gif', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-13 09:35:51', '2018-12-13 09:35:33', '2018-12-13 09:37:36', '2018-12-13 09:37:36'),
(27, 0, 'gif-delete-332846427', NULL, 34.00, NULL, 1, NULL, NULL, NULL, 2, 'gif', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-13 12:07:29', '2018-12-13 12:08:07', '2018-12-13 12:08:07'),
(28, 1, 'gif', NULL, 23.00, NULL, 1, 1, NULL, NULL, 2, 'gif', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-13 12:20:30', '2018-12-13 12:10:02', '2018-12-17 08:45:28', NULL),
(29, 1, 'SOFT FAUX FUR COAT-Clone', NULL, 49.99, NULL, 1, 2, 'Coat with lapel collar and long sleeves. Hidden side in-seam pockets. Hidden front snap button closure.', NULL, 2, 'SOFT FAUX FUR COAT', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-27 05:34:21', '2019-01-08 03:36:39', NULL),
(30, 0, 'DOUBLE-BREASTED COAT-Clone2222-delete-1358431418', NULL, 199.00, NULL, 1, 3, 'Coat with lapel collar and long sleeves. Front flap pockets and chest welt pockets. Front double-breasted button closure.', NULL, 2, 'DOUBLE-BREASTED COAT', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-01 10:22:22', '2019-01-01 10:22:42', '2019-01-01 10:22:42'),
(31, 0, 'DOUBLE-BREASTED COAT-Clone6767-delete-480868701', NULL, 199.00, NULL, 1, 3, 'Coat with lapel collar and long sleeves. Front flap pockets and chest welt pockets. Front double-breasted button closure.', NULL, 2, 'DOUBLE-BREASTED COAT', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-01 10:31:37', '2019-01-01 10:34:30', '2019-01-01 10:34:30'),
(34, 0, 'DOUBLE-BREASTED COAT-Clone4545', 'videos/74d1c6b0-0de4-11e9-bc34-4bfd1211eb45.mp4', 199.00, NULL, 1, 3, 'Coat with lapel collar and long sleeves. Front flap pockets and chest welt pockets. Front double-breasted button closure.', NULL, 2, 'DOUBLE-BREASTED COAT', 80, 85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-01 10:44:07', '2019-01-01 10:44:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_images`
--

DROP TABLE IF EXISTS `item_images`;
CREATE TABLE IF NOT EXISTS `item_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `list_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbs_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=181 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_images`
--

INSERT INTO `item_images` (`id`, `item_id`, `color_id`, `sort`, `image_path`, `list_image_path`, `thumbs_image_path`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, '/images/item/6e54eed0-7c88-11e8-97da-8ff2bf00b99c.jpg', NULL, NULL, '2018-06-30 11:10:04', '2018-06-30 11:10:04'),
(2, 2, NULL, 1, 'images/item/original/134dc620-7c8a-11e8-a65a-bd1ab6a8cc80.jpg', 'images/item/list/134dc620-7c8a-11e8-a65a-bd1ab6a8cc80.jpg', 'images/item/thumbs/134dc620-7c8a-11e8-a65a-bd1ab6a8cc80.jpg', '2018-06-30 11:18:16', '2018-06-30 12:02:10'),
(3, NULL, NULL, NULL, '/images/item/31fda5b0-7c91-11e8-9394-19058d004339.jpg', NULL, NULL, '2018-06-30 12:12:48', '2018-06-30 12:12:48'),
(4, 4, NULL, 1, 'images/item/original/c847f460-7c91-11e8-9d46-e1ed8ac6a1cb.jpg', 'images/item/list/c847f460-7c91-11e8-9d46-e1ed8ac6a1cb.jpg', 'images/item/thumbs/c847f460-7c91-11e8-9d46-e1ed8ac6a1cb.jpg', '2018-06-30 12:16:59', '2018-08-04 07:52:56'),
(6, 5, 43, 2, 'images/item/original/20f589e0-837f-11e8-a7c5-87faaadd99b5.jpg', 'images/item/list/20f589e0-837f-11e8-a7c5-87faaadd99b5.jpg', 'images/item/thumbs/20f589e0-837f-11e8-a7c5-87faaadd99b5.jpg', '2018-07-09 07:49:24', '2018-07-24 05:04:52'),
(7, 5, 44, 3, 'images/item/original/21027570-837f-11e8-965c-db4640c07abe.jpg', 'images/item/list/21027570-837f-11e8-965c-db4640c07abe.jpg', 'images/item/thumbs/21027570-837f-11e8-965c-db4640c07abe.jpg', '2018-07-09 07:49:24', '2018-07-24 05:04:52'),
(8, 5, 43, 1, 'images/item/original/20cb0000-837f-11e8-a8d5-e748b2753061.jpg', 'images/item/list/20cb0000-837f-11e8-a8d5-e748b2753061.jpg', 'images/item/thumbs/20cb0000-837f-11e8-a8d5-e748b2753061.jpg', '2018-07-09 07:49:24', '2018-07-24 05:04:52'),
(9, 6, NULL, 1, 'images/item/original/7add8160-837f-11e8-b217-cf67dc56314c.jpg', 'images/item/list/7add8160-837f-11e8-b217-cf67dc56314c.jpg', 'images/item/thumbs/7add8160-837f-11e8-b217-cf67dc56314c.jpg', '2018-07-09 07:53:33', '2018-07-27 13:19:23'),
(10, 6, NULL, 3, 'images/item/original/7af96890-837f-11e8-b63c-9b3581e71c65.jpg', 'images/item/list/7af96890-837f-11e8-b63c-9b3581e71c65.jpg', 'images/item/thumbs/7af96890-837f-11e8-b63c-9b3581e71c65.jpg', '2018-07-09 07:53:33', '2018-07-27 13:19:23'),
(11, 6, NULL, 2, 'images/item/original/7aebcb90-837f-11e8-a3ec-1559201f0f12.jpg', 'images/item/list/7aebcb90-837f-11e8-a3ec-1559201f0f12.jpg', 'images/item/thumbs/7aebcb90-837f-11e8-a3ec-1559201f0f12.jpg', '2018-07-09 07:53:33', '2018-07-27 13:19:23'),
(12, 7, NULL, 1, 'images/item/original/f0e88620-837f-11e8-b2e3-cf28f7bef775.jpg', 'images/item/list/f0e88620-837f-11e8-b2e3-cf28f7bef775.jpg', 'images/item/thumbs/f0e88620-837f-11e8-b2e3-cf28f7bef775.jpg', '2018-07-09 07:56:40', '2018-07-09 07:56:56'),
(13, 7, NULL, 2, 'images/item/original/f0f679e0-837f-11e8-8db8-3fa4e76c86ce.jpg', 'images/item/list/f0f679e0-837f-11e8-8db8-3fa4e76c86ce.jpg', 'images/item/thumbs/f0f679e0-837f-11e8-8db8-3fa4e76c86ce.jpg', '2018-07-09 07:56:40', '2018-07-09 07:56:56'),
(14, 8, NULL, 2, 'images/item/original/33f764d0-8380-11e8-87d0-598a1ba9e8e5.JPG', 'images/item/list/33f764d0-8380-11e8-87d0-598a1ba9e8e5.JPG', 'images/item/thumbs/33f764d0-8380-11e8-87d0-598a1ba9e8e5.JPG', '2018-07-09 07:58:40', '2018-07-27 13:20:31'),
(15, 8, NULL, 1, 'images/item/original/33ebf580-8380-11e8-902e-ff4bf2c75022.jpg', 'images/item/list/33ebf580-8380-11e8-902e-ff4bf2c75022.jpg', 'images/item/thumbs/33ebf580-8380-11e8-902e-ff4bf2c75022.jpg', '2018-07-09 07:58:40', '2018-07-27 13:20:31'),
(16, 9, NULL, 3, 'images/item/original/72712d80-8380-11e8-a788-11435879e048.jpg', 'images/item/list/72712d80-8380-11e8-a788-11435879e048.jpg', 'images/item/thumbs/72712d80-8380-11e8-a788-11435879e048.jpg', '2018-07-09 08:00:14', '2018-07-14 12:16:43'),
(17, 9, NULL, 1, 'images/item/original/72564870-8380-11e8-8d4d-f1eb6da87927.jpg', 'images/item/list/72564870-8380-11e8-8d4d-f1eb6da87927.jpg', 'images/item/thumbs/72564870-8380-11e8-8d4d-f1eb6da87927.jpg', '2018-07-09 08:00:14', '2018-07-14 12:16:43'),
(18, 9, NULL, 2, 'images/item/original/7263c820-8380-11e8-b7fb-6bc07d4cdf0a.jpg', 'images/item/list/7263c820-8380-11e8-b7fb-6bc07d4cdf0a.jpg', 'images/item/thumbs/7263c820-8380-11e8-b7fb-6bc07d4cdf0a.jpg', '2018-07-09 08:00:14', '2018-07-14 12:16:43'),
(31, 17, 4, 1, 'images/item/original/aeb3d080-9a39-11e8-bf5d-8b3a612a9c9d.jpg', 'images/item/list/aeb3d080-9a39-11e8-bf5d-8b3a612a9c9d.jpg', 'images/item/thumbs/aeb3d080-9a39-11e8-bf5d-8b3a612a9c9d.jpg', '2018-08-07 06:02:00', '2018-08-07 06:02:00'),
(23, 18, NULL, 1, 'images/item/original/8da828b0-8e6c-11e8-b7bb-a786c72c6e56.jpg', 'images/item/list/8da828b0-8e6c-11e8-b7bb-a786c72c6e56.jpg', 'images/item/thumbs/8da828b0-8e6c-11e8-b7bb-a786c72c6e56.jpg', '2018-07-23 05:35:52', '2018-07-23 05:35:52'),
(24, 18, NULL, 2, 'images/item/original/8db83f00-8e6c-11e8-a855-e79f3dfc2924.jpg', 'images/item/list/8db83f00-8e6c-11e8-a855-e79f3dfc2924.jpg', 'images/item/thumbs/8db83f00-8e6c-11e8-a855-e79f3dfc2924.jpg', '2018-07-23 05:35:52', '2018-07-23 05:35:52'),
(25, 18, NULL, 3, 'images/item/original/8dc4e230-8e6c-11e8-8ba0-a796b05d5669.jpg', 'images/item/list/8dc4e230-8e6c-11e8-8ba0-a796b05d5669.jpg', 'images/item/thumbs/8dc4e230-8e6c-11e8-8ba0-a796b05d5669.jpg', '2018-07-23 05:35:52', '2018-07-23 05:35:52'),
(37, 19, 48, 3, 'images/item/original/fc1b4c00-9bc6-11e8-91b7-7b0f171f1b13.jpg', 'images/item/list/fc1b4c00-9bc6-11e8-91b7-7b0f171f1b13.jpg', 'images/item/thumbs/fc1b4c00-9bc6-11e8-91b7-7b0f171f1b13.jpg', '2018-08-09 05:26:05', '2018-08-09 05:26:05'),
(36, 19, 13, 2, 'images/item/original/f732f730-9bc6-11e8-b2c8-73fe25329a18.jpg', 'images/item/list/f732f730-9bc6-11e8-b2c8-73fe25329a18.jpg', 'images/item/thumbs/f732f730-9bc6-11e8-b2c8-73fe25329a18.jpg', '2018-08-09 05:25:57', '2018-08-09 05:25:57'),
(35, 19, 4, 1, 'images/item/original/ecca69c0-9bc6-11e8-9758-db4663ff6505.jpg', 'images/item/list/ecca69c0-9bc6-11e8-9758-db4663ff6505.jpg', 'images/item/thumbs/ecca69c0-9bc6-11e8-9758-db4663ff6505.jpg', '2018-08-09 05:25:48', '2018-08-09 05:25:48'),
(38, 19, NULL, 4, 'images/item/original/0118f0c0-9bc7-11e8-8a37-7fb710c3befd.jpg', 'images/item/list/0118f0c0-9bc7-11e8-8a37-7fb710c3befd.jpg', 'images/item/thumbs/0118f0c0-9bc7-11e8-8a37-7fb710c3befd.jpg', '2018-08-09 05:26:14', '2018-08-09 05:26:14'),
(39, 19, NULL, 5, 'images/item/original/06b68060-9bc7-11e8-8d56-2f399f95e63b.jpg', 'images/item/list/06b68060-9bc7-11e8-8d56-2f399f95e63b.jpg', 'images/item/thumbs/06b68060-9bc7-11e8-8d56-2f399f95e63b.jpg', '2018-08-09 05:26:25', '2018-08-09 05:26:25'),
(40, 19, NULL, 6, 'images/item/original/0d303780-9bc7-11e8-af80-b962922e7013.jpg', 'images/item/list/0d303780-9bc7-11e8-af80-b962922e7013.jpg', 'images/item/thumbs/0d303780-9bc7-11e8-af80-b962922e7013.jpg', '2018-08-09 05:26:36', '2018-08-09 05:26:36'),
(41, 19, NULL, 7, 'images/item/original/13d05350-9bc7-11e8-8b6f-4f02d821634a.jpg', 'images/item/list/13d05350-9bc7-11e8-8b6f-4f02d821634a.jpg', 'images/item/thumbs/13d05350-9bc7-11e8-8b6f-4f02d821634a.jpg', '2018-08-09 05:26:47', '2018-08-09 05:26:47'),
(42, 19, NULL, 8, 'images/item/original/1a38a2f0-9bc7-11e8-a07b-69a442e709c8.jpg', 'images/item/list/1a38a2f0-9bc7-11e8-a07b-69a442e709c8.jpg', 'images/item/thumbs/1a38a2f0-9bc7-11e8-a07b-69a442e709c8.jpg', '2018-08-09 05:26:59', '2018-08-09 05:26:59'),
(43, 19, NULL, 9, 'images/item/original/21719070-9bc7-11e8-9155-55522d500cb5.jpg', 'images/item/list/21719070-9bc7-11e8-9155-55522d500cb5.jpg', 'images/item/thumbs/21719070-9bc7-11e8-9155-55522d500cb5.jpg', '2018-08-09 05:27:11', '2018-08-09 05:27:11'),
(44, 19, NULL, 10, 'images/item/original/28c8c720-9bc7-11e8-964a-ebd7bfce7495.jpg', 'images/item/list/28c8c720-9bc7-11e8-964a-ebd7bfce7495.jpg', 'images/item/thumbs/28c8c720-9bc7-11e8-964a-ebd7bfce7495.jpg', '2018-08-09 05:27:23', '2018-08-09 05:27:23'),
(45, 19, NULL, 11, 'images/item/original/2ff03ba0-9bc7-11e8-b92a-ebae1139aa62.jpg', 'images/item/list/2ff03ba0-9bc7-11e8-b92a-ebae1139aa62.jpg', 'images/item/thumbs/2ff03ba0-9bc7-11e8-b92a-ebae1139aa62.jpg', '2018-08-09 05:27:35', '2018-08-09 05:27:35'),
(46, 19, NULL, 12, 'images/item/original/370084e0-9bc7-11e8-9598-67d46fb409d3.jpg', 'images/item/list/370084e0-9bc7-11e8-9598-67d46fb409d3.jpg', 'images/item/thumbs/370084e0-9bc7-11e8-9598-67d46fb409d3.jpg', '2018-08-09 05:27:48', '2018-08-09 05:27:48'),
(47, 19, NULL, 13, 'images/item/original/3e58d690-9bc7-11e8-ade1-57674d2e4d63.jpg', 'images/item/list/3e58d690-9bc7-11e8-ade1-57674d2e4d63.jpg', 'images/item/thumbs/3e58d690-9bc7-11e8-ade1-57674d2e4d63.jpg', '2018-08-09 05:28:01', '2018-08-09 05:28:01'),
(48, 19, NULL, 14, 'images/item/original/4633ead0-9bc7-11e8-b577-27a8fa67fbed.jpg', 'images/item/list/4633ead0-9bc7-11e8-b577-27a8fa67fbed.jpg', 'images/item/thumbs/4633ead0-9bc7-11e8-b577-27a8fa67fbed.jpg', '2018-08-09 05:28:16', '2018-08-09 05:28:16'),
(49, 19, NULL, 15, 'images/item/original/4f626a10-9bc7-11e8-996e-a5adf462f80f.jpg', 'images/item/list/4f626a10-9bc7-11e8-996e-a5adf462f80f.jpg', 'images/item/thumbs/4f626a10-9bc7-11e8-996e-a5adf462f80f.jpg', '2018-08-09 05:28:28', '2018-08-09 05:28:28'),
(50, 19, NULL, 16, 'images/item/original/568f9820-9bc7-11e8-b855-c170eb86ff9e.jpg', 'images/item/list/568f9820-9bc7-11e8-b855-c170eb86ff9e.jpg', 'images/item/thumbs/568f9820-9bc7-11e8-b855-c170eb86ff9e.jpg', '2018-08-09 05:28:41', '2018-08-09 05:28:41'),
(51, 19, NULL, 17, 'images/item/original/5de9a4b0-9bc7-11e8-b512-01837509a934.jpg', 'images/item/list/5de9a4b0-9bc7-11e8-b512-01837509a934.jpg', 'images/item/thumbs/5de9a4b0-9bc7-11e8-b512-01837509a934.jpg', '2018-08-09 05:28:51', '2018-08-09 05:28:51'),
(52, 19, NULL, 18, 'images/item/original/63d62df0-9bc7-11e8-8687-a13435f76e81.jpg', 'images/item/list/63d62df0-9bc7-11e8-8687-a13435f76e81.jpg', 'images/item/thumbs/63d62df0-9bc7-11e8-8687-a13435f76e81.jpg', '2018-08-09 05:28:58', '2018-08-09 05:28:58'),
(53, 19, NULL, 19, 'images/item/original/68387c00-9bc7-11e8-8c89-5d017b999827.jpg', 'images/item/list/68387c00-9bc7-11e8-8c89-5d017b999827.jpg', 'images/item/thumbs/68387c00-9bc7-11e8-8c89-5d017b999827.jpg', '2018-08-09 05:29:06', '2018-08-09 05:29:06'),
(54, 19, NULL, 20, 'images/item/original/6d1fd540-9bc7-11e8-91fd-49edb9f889d7.jpg', 'images/item/list/6d1fd540-9bc7-11e8-91fd-49edb9f889d7.jpg', 'images/item/thumbs/6d1fd540-9bc7-11e8-91fd-49edb9f889d7.jpg', '2018-08-09 05:29:13', '2018-08-09 05:29:13'),
(55, 19, NULL, 21, 'images/item/original/71363200-9bc7-11e8-95b7-ffcc20afd423.jpg', 'images/item/list/71363200-9bc7-11e8-95b7-ffcc20afd423.jpg', 'images/item/thumbs/71363200-9bc7-11e8-95b7-ffcc20afd423.jpg', '2018-08-09 05:29:20', '2018-08-09 05:29:20'),
(56, 19, NULL, 22, 'images/item/original/75683370-9bc7-11e8-b801-85012e4bc779.jpg', 'images/item/list/75683370-9bc7-11e8-b801-85012e4bc779.jpg', 'images/item/thumbs/75683370-9bc7-11e8-b801-85012e4bc779.jpg', '2018-08-09 05:29:31', '2018-08-09 05:29:31'),
(57, 19, NULL, 23, 'images/item/original/7c13b420-9bc7-11e8-a43d-0fa6b041a2e1.jpg', 'images/item/list/7c13b420-9bc7-11e8-a43d-0fa6b041a2e1.jpg', 'images/item/thumbs/7c13b420-9bc7-11e8-a43d-0fa6b041a2e1.jpg', '2018-08-09 05:29:42', '2018-08-09 05:29:42'),
(58, 19, NULL, 24, 'images/item/original/82b34380-9bc7-11e8-9d1c-dbf4b3d64aaf.jpg', 'images/item/list/82b34380-9bc7-11e8-9d1c-dbf4b3d64aaf.jpg', 'images/item/thumbs/82b34380-9bc7-11e8-9d1c-dbf4b3d64aaf.jpg', '2018-08-09 05:29:53', '2018-08-09 05:29:53'),
(59, 19, NULL, 25, 'images/item/original/88ef03b0-9bc7-11e8-bf22-0b76259070b3.jpg', 'images/item/list/88ef03b0-9bc7-11e8-bf22-0b76259070b3.jpg', 'images/item/thumbs/88ef03b0-9bc7-11e8-bf22-0b76259070b3.jpg', '2018-08-09 05:30:06', '2018-08-09 05:30:06'),
(107, 20, 49, 6, 'images/item/original/300dd730-bff8-11e8-bba9-cd0496536363.jpg', 'images/item/list/300dd730-bff8-11e8-bba9-cd0496536363.jpg', 'images/item/thumbs/300dd730-bff8-11e8-bba9-cd0496536363.jpg', '2018-09-24 06:48:51', '2018-09-24 06:48:51'),
(106, 20, 15, 5, 'images/item/original/3002eeb0-bff8-11e8-92e4-cf2bdc1c3450.jpg', 'images/item/list/3002eeb0-bff8-11e8-92e4-cf2bdc1c3450.jpg', 'images/item/thumbs/3002eeb0-bff8-11e8-92e4-cf2bdc1c3450.jpg', '2018-09-24 06:48:51', '2018-09-24 06:48:51'),
(105, 20, 17, 4, 'images/item/original/2ff7c870-bff8-11e8-9bd3-197834f7f481.jpg', 'images/item/list/2ff7c870-bff8-11e8-9bd3-197834f7f481.jpg', 'images/item/thumbs/2ff7c870-bff8-11e8-9bd3-197834f7f481.jpg', '2018-09-24 06:48:51', '2018-09-24 06:48:51'),
(104, 20, 17, 3, 'images/item/original/2fece7b0-bff8-11e8-a7f6-d9c985f6b98f.jpg', 'images/item/list/2fece7b0-bff8-11e8-a7f6-d9c985f6b98f.jpg', 'images/item/thumbs/2fece7b0-bff8-11e8-a7f6-d9c985f6b98f.jpg', '2018-09-24 06:48:51', '2018-09-24 06:48:51'),
(103, 20, 16, 2, 'images/item/original/2fe17160-bff8-11e8-adb3-0bb00558cec5.jpg', 'images/item/list/2fe17160-bff8-11e8-adb3-0bb00558cec5.jpg', 'images/item/thumbs/2fe17160-bff8-11e8-adb3-0bb00558cec5.jpg', '2018-09-24 06:48:51', '2018-09-24 06:48:51'),
(102, 20, 16, 1, 'images/item/original/2fd5b100-bff8-11e8-a6c3-83850ff3a2d6.jpg', 'images/item/list/2fd5b100-bff8-11e8-a6c3-83850ff3a2d6.jpg', 'images/item/thumbs/2fd5b100-bff8-11e8-a6c3-83850ff3a2d6.jpg', '2018-09-24 06:48:51', '2018-09-24 06:48:51'),
(108, 21, 50, 3, 'images/item/original/85ae6e70-f57e-11e8-86d8-9f4a0b9c6f97.jpg', 'images/item/list/85ae6e70-f57e-11e8-86d8-9f4a0b9c6f97.jpg', 'images/item/thumbs/85ae6e70-f57e-11e8-86d8-9f4a0b9c6f97.jpg', '2018-12-01 09:32:55', '2018-12-01 09:33:59'),
(109, 21, 50, 5, 'images/item/original/85f82d30-f57e-11e8-be90-b7ff6f06d71b.jpg', 'images/item/list/85f82d30-f57e-11e8-be90-b7ff6f06d71b.jpg', 'images/item/thumbs/85f82d30-f57e-11e8-be90-b7ff6f06d71b.jpg', '2018-12-01 09:32:55', '2018-12-01 09:33:59'),
(110, 21, 50, 2, 'images/item/original/8587ff80-f57e-11e8-8eee-459464613a9c.jpg', 'images/item/list/8587ff80-f57e-11e8-8eee-459464613a9c.jpg', 'images/item/thumbs/8587ff80-f57e-11e8-8eee-459464613a9c.jpg', '2018-12-01 09:32:55', '2018-12-01 09:33:59'),
(111, 21, 50, 1, 'images/item/original/8537eb40-f57e-11e8-a896-4b82a409cbe3.jpg', 'images/item/list/8537eb40-f57e-11e8-a896-4b82a409cbe3.jpg', 'images/item/thumbs/8537eb40-f57e-11e8-a896-4b82a409cbe3.jpg', '2018-12-01 09:32:55', '2018-12-01 09:33:58'),
(112, 21, 50, 4, 'images/item/original/85d301f0-f57e-11e8-a4f7-990bd56cd667.jpg', 'images/item/list/85d301f0-f57e-11e8-a4f7-990bd56cd667.jpg', 'images/item/thumbs/85d301f0-f57e-11e8-a4f7-990bd56cd667.jpg', '2018-12-01 09:32:55', '2018-12-01 09:33:59'),
(113, 21, 50, 6, 'images/item/original/861dd330-f57e-11e8-aec0-21a7f9acfb35.jpg', 'images/item/list/861dd330-f57e-11e8-aec0-21a7f9acfb35.jpg', 'images/item/thumbs/861dd330-f57e-11e8-aec0-21a7f9acfb35.jpg', '2018-12-01 09:32:55', '2018-12-01 09:34:00'),
(114, 21, 51, 7, 'images/item/original/864316a0-f57e-11e8-9b5d-7f059d90c336.jpg', 'images/item/list/864316a0-f57e-11e8-9b5d-7f059d90c336.jpg', 'images/item/thumbs/864316a0-f57e-11e8-9b5d-7f059d90c336.jpg', '2018-12-01 09:32:55', '2018-12-01 09:34:00'),
(115, 21, 50, 8, 'images/item/original/86673160-f57e-11e8-817f-e335f6631132.jpg', 'images/item/list/86673160-f57e-11e8-817f-e335f6631132.jpg', 'images/item/thumbs/86673160-f57e-11e8-817f-e335f6631132.jpg', '2018-12-01 09:32:55', '2018-12-01 09:34:00'),
(116, 21, 50, 9, 'images/item/original/868e4360-f57e-11e8-b259-bd768617cafc.jpg', 'images/item/list/868e4360-f57e-11e8-b259-bd768617cafc.jpg', 'images/item/thumbs/868e4360-f57e-11e8-b259-bd768617cafc.jpg', '2018-12-01 09:32:55', '2018-12-01 09:34:00'),
(117, 21, 50, 10, 'images/item/original/86b323e0-f57e-11e8-9dda-5fddbddc272b.jpg', 'images/item/list/86b323e0-f57e-11e8-9dda-5fddbddc272b.jpg', 'images/item/thumbs/86b323e0-f57e-11e8-9dda-5fddbddc272b.jpg', '2018-12-01 09:32:55', '2018-12-01 09:34:01'),
(118, 22, 11, 2, 'images/item/original/0a46ab00-f57f-11e8-9bc0-516b7cd7e1b2.jpg', 'images/item/list/0a46ab00-f57f-11e8-9bc0-516b7cd7e1b2.jpg', 'images/item/thumbs/0a46ab00-f57f-11e8-9bc0-516b7cd7e1b2.jpg', '2018-12-01 09:36:53', '2018-12-01 09:37:41'),
(119, 22, 11, 4, 'images/item/original/0a94ea40-f57f-11e8-92c9-07039660f588.jpg', 'images/item/list/0a94ea40-f57f-11e8-92c9-07039660f588.jpg', 'images/item/thumbs/0a94ea40-f57f-11e8-92c9-07039660f588.jpg', '2018-12-01 09:36:53', '2018-12-01 09:37:42'),
(120, 22, 11, 3, 'images/item/original/0a6c3210-f57f-11e8-993b-4b64901fd233.jpg', 'images/item/list/0a6c3210-f57f-11e8-993b-4b64901fd233.jpg', 'images/item/thumbs/0a6c3210-f57f-11e8-993b-4b64901fd233.jpg', '2018-12-01 09:36:53', '2018-12-01 09:37:42'),
(121, 22, 11, 1, 'images/item/original/0a1f8000-f57f-11e8-8972-15ae73ccb69a.jpg', 'images/item/list/0a1f8000-f57f-11e8-8972-15ae73ccb69a.jpg', 'images/item/thumbs/0a1f8000-f57f-11e8-8972-15ae73ccb69a.jpg', '2018-12-01 09:36:53', '2018-12-01 09:37:41'),
(122, 22, 11, 5, 'images/item/original/0abd0aa0-f57f-11e8-911a-4b8f9044ff51.jpg', 'images/item/list/0abd0aa0-f57f-11e8-911a-4b8f9044ff51.jpg', 'images/item/thumbs/0abd0aa0-f57f-11e8-911a-4b8f9044ff51.jpg', '2018-12-01 09:36:53', '2018-12-01 09:37:42'),
(123, 22, 11, 6, 'images/item/original/0ae3d190-f57f-11e8-949d-957f25a1bc13.jpg', 'images/item/list/0ae3d190-f57f-11e8-949d-957f25a1bc13.jpg', 'images/item/thumbs/0ae3d190-f57f-11e8-949d-957f25a1bc13.jpg', '2018-12-01 09:36:53', '2018-12-01 09:37:42'),
(124, 22, 11, 7, 'images/item/original/0b091ee0-f57f-11e8-b66b-81f39c3869ea.jpg', 'images/item/list/0b091ee0-f57f-11e8-b66b-81f39c3869ea.jpg', 'images/item/thumbs/0b091ee0-f57f-11e8-b66b-81f39c3869ea.jpg', '2018-12-01 09:36:53', '2018-12-01 09:37:43'),
(125, 23, 52, 2, 'images/item/original/d4701270-f57f-11e8-8e89-454923c12f22.jpg', 'images/item/list/d4701270-f57f-11e8-8e89-454923c12f22.jpg', 'images/item/thumbs/d4701270-f57f-11e8-8e89-454923c12f22.jpg', '2018-12-01 09:43:01', '2018-12-01 09:43:21'),
(126, 23, 52, 3, 'images/item/original/d4961790-f57f-11e8-969b-47d10bb7462d.jpg', 'images/item/list/d4961790-f57f-11e8-969b-47d10bb7462d.jpg', 'images/item/thumbs/d4961790-f57f-11e8-969b-47d10bb7462d.jpg', '2018-12-01 09:43:01', '2018-12-01 09:43:21'),
(127, 23, 52, 1, 'images/item/original/d4491ef0-f57f-11e8-8b73-9b9eb9a7b4ce.jpg', 'images/item/list/d4491ef0-f57f-11e8-8b73-9b9eb9a7b4ce.jpg', 'images/item/thumbs/d4491ef0-f57f-11e8-8b73-9b9eb9a7b4ce.jpg', '2018-12-01 09:43:01', '2018-12-01 09:43:20'),
(128, 23, 52, 5, 'images/item/original/d4e20a30-f57f-11e8-be20-6dfd192b3164.jpg', 'images/item/list/d4e20a30-f57f-11e8-be20-6dfd192b3164.jpg', 'images/item/thumbs/d4e20a30-f57f-11e8-be20-6dfd192b3164.jpg', '2018-12-01 09:43:01', '2018-12-01 09:43:21'),
(129, 23, 52, 6, 'images/item/original/d50713e0-f57f-11e8-928b-77b34947ef73.jpg', 'images/item/list/d50713e0-f57f-11e8-928b-77b34947ef73.jpg', 'images/item/thumbs/d50713e0-f57f-11e8-928b-77b34947ef73.jpg', '2018-12-01 09:43:01', '2018-12-01 09:43:22'),
(130, 23, 52, 4, 'images/item/original/d4bdbf50-f57f-11e8-9985-271a992a609b.jpg', 'images/item/list/d4bdbf50-f57f-11e8-9985-271a992a609b.jpg', 'images/item/thumbs/d4bdbf50-f57f-11e8-9985-271a992a609b.jpg', '2018-12-01 09:43:01', '2018-12-01 09:43:21'),
(131, 23, 52, 7, 'images/item/original/d52c5060-f57f-11e8-88d9-2fda5e5df421.jpg', 'images/item/list/d52c5060-f57f-11e8-88d9-2fda5e5df421.jpg', 'images/item/thumbs/d52c5060-f57f-11e8-88d9-2fda5e5df421.jpg', '2018-12-01 09:43:01', '2018-12-01 09:43:22'),
(132, NULL, NULL, NULL, '/images/item/0fb2bab0-f7e9-11e8-9cb7-d5f0e7723cb0.jpg', NULL, NULL, '2018-12-04 11:21:39', '2018-12-04 11:21:39'),
(133, 24, NULL, 1, 'images/item/original/8916f7f0-f7e9-11e8-9973-abbd68e5162a.jpg', 'images/item/list/8916f7f0-f7e9-11e8-9973-abbd68e5162a.jpg', 'images/item/thumbs/8916f7f0-f7e9-11e8-9973-abbd68e5162a.jpg', '2018-12-04 11:24:54', '2018-12-04 11:25:03'),
(134, 25, 53, 1, 'images/item/original/33b507e0-feb3-11e8-be59-6fb7adf0b596.jpg', 'images/item/list/33b507e0-feb3-11e8-be59-6fb7adf0b596.jpg', 'images/item/thumbs/33b507e0-feb3-11e8-be59-6fb7adf0b596.jpg', '2018-12-13 02:43:45', '2018-12-13 02:43:45'),
(135, 25, 53, 2, 'images/item/original/33e90f20-feb3-11e8-ab94-7fdc740c4923.jpg', 'images/item/list/33e90f20-feb3-11e8-ab94-7fdc740c4923.jpg', 'images/item/thumbs/33e90f20-feb3-11e8-ab94-7fdc740c4923.jpg', '2018-12-13 02:43:45', '2018-12-13 02:43:45'),
(136, 25, 53, 3, 'images/item/original/33fab400-feb3-11e8-bf62-e70b2402bc84.jpg', 'images/item/list/33fab400-feb3-11e8-bf62-e70b2402bc84.jpg', 'images/item/thumbs/33fab400-feb3-11e8-bf62-e70b2402bc84.jpg', '2018-12-13 02:43:46', '2018-12-13 02:43:46'),
(137, 25, 53, 4, 'images/item/original/340cbf60-feb3-11e8-b7d5-25e9b30668f4.jpg', 'images/item/list/340cbf60-feb3-11e8-b7d5-25e9b30668f4.jpg', 'images/item/thumbs/340cbf60-feb3-11e8-b7d5-25e9b30668f4.jpg', '2018-12-13 02:43:46', '2018-12-13 02:43:46'),
(138, 25, 53, 5, 'images/item/original/341c69b0-feb3-11e8-ac4f-6db648bbe546.jpg', 'images/item/list/341c69b0-feb3-11e8-ac4f-6db648bbe546.jpg', 'images/item/thumbs/341c69b0-feb3-11e8-ac4f-6db648bbe546.jpg', '2018-12-13 02:43:46', '2018-12-13 02:43:46'),
(139, 25, 53, 6, 'images/item/original/342b1cd0-feb3-11e8-8f08-d5d38f5c0379.jpg', 'images/item/list/342b1cd0-feb3-11e8-8f08-d5d38f5c0379.jpg', 'images/item/thumbs/342b1cd0-feb3-11e8-8f08-d5d38f5c0379.jpg', '2018-12-13 02:43:46', '2018-12-13 02:43:46'),
(140, 25, 53, 7, 'images/item/original/34396ef0-feb3-11e8-8d83-b71ea3531f55.jpg', 'images/item/list/34396ef0-feb3-11e8-8d83-b71ea3531f55.jpg', 'images/item/thumbs/34396ef0-feb3-11e8-8d83-b71ea3531f55.jpg', '2018-12-13 02:43:46', '2018-12-13 02:43:46'),
(141, 25, 53, 8, 'images/item/original/344aabc0-feb3-11e8-88c9-5dbd5c56fd52.jpg', 'images/item/list/344aabc0-feb3-11e8-88c9-5dbd5c56fd52.jpg', 'images/item/thumbs/344aabc0-feb3-11e8-88c9-5dbd5c56fd52.jpg', '2018-12-13 02:43:46', '2018-12-13 02:43:46'),
(142, 25, 53, 9, 'images/item/original/345bea10-feb3-11e8-848c-f97a833b7970.jpg', 'images/item/list/345bea10-feb3-11e8-848c-f97a833b7970.jpg', 'images/item/thumbs/345bea10-feb3-11e8-848c-f97a833b7970.jpg', '2018-12-13 02:43:46', '2018-12-13 02:43:46'),
(143, 25, 53, 10, 'images/item/original/346df4a0-feb3-11e8-a882-6d986444c596.jpg', 'images/item/list/346df4a0-feb3-11e8-a882-6d986444c596.jpg', 'images/item/thumbs/346df4a0-feb3-11e8-a882-6d986444c596.jpg', '2018-12-13 02:43:46', '2018-12-13 02:43:46'),
(144, 25, 53, 11, 'images/item/original/347f1d40-feb3-11e8-860a-b7d30e7bb350.jpg', 'images/item/list/347f1d40-feb3-11e8-860a-b7d30e7bb350.jpg', 'images/item/thumbs/347f1d40-feb3-11e8-860a-b7d30e7bb350.jpg', '2018-12-13 02:43:46', '2018-12-13 02:43:46'),
(145, NULL, NULL, NULL, '/images/item/5231dbb0-feec-11e8-be13-7d00736a7da1.gif', NULL, NULL, '2018-12-13 09:32:37', '2018-12-13 09:32:37'),
(146, NULL, NULL, NULL, '/images/item/90e01570-feec-11e8-b0f9-ef077fd52639.gif', NULL, NULL, '2018-12-13 09:34:23', '2018-12-13 09:34:23'),
(148, NULL, NULL, NULL, '/images/item/f276dd80-ff01-11e8-8ed5-7bf766ee5e67.gif', NULL, NULL, '2018-12-13 12:07:26', '2018-12-13 12:07:26'),
(149, 28, NULL, 1, 'images/item/original/4f931ce0-ff02-11e8-8638-53c8a994e086.gif', 'images/item/list/4f931ce0-ff02-11e8-8638-53c8a994e086.gif', 'images/item/thumbs/4f931ce0-ff02-11e8-8638-53c8a994e086.gif', '2018-12-13 12:10:00', '2018-12-13 12:10:02'),
(150, 29, 50, 1, 'images/item/original/5a797f90-09cb-11e9-833c-395e3a1aea82.jpg', 'images/item/list/5a797f90-09cb-11e9-833c-395e3a1aea82.jpg', 'images/item/thumbs/5a797f90-09cb-11e9-833c-395e3a1aea82.jpg', '2018-12-27 05:34:21', '2018-12-27 05:34:21'),
(151, 29, 50, 2, 'images/item/original/5aded580-09cb-11e9-9ec5-bf522cd2cc9a.jpg', 'images/item/list/5aded580-09cb-11e9-9ec5-bf522cd2cc9a.jpg', 'images/item/thumbs/5aded580-09cb-11e9-9ec5-bf522cd2cc9a.jpg', '2018-12-27 05:34:22', '2018-12-27 05:34:22'),
(152, 29, 50, 3, 'images/item/original/5b1eef50-09cb-11e9-8c5f-9345ad7a45dc.jpg', 'images/item/list/5b1eef50-09cb-11e9-8c5f-9345ad7a45dc.jpg', 'images/item/thumbs/5b1eef50-09cb-11e9-8c5f-9345ad7a45dc.jpg', '2018-12-27 05:34:22', '2018-12-27 05:34:22'),
(153, 29, 50, 4, 'images/item/original/5b5f3670-09cb-11e9-abd7-fd327589d9f5.jpg', 'images/item/list/5b5f3670-09cb-11e9-abd7-fd327589d9f5.jpg', 'images/item/thumbs/5b5f3670-09cb-11e9-abd7-fd327589d9f5.jpg', '2018-12-27 05:34:23', '2018-12-27 05:34:23'),
(154, 29, 50, 5, 'images/item/original/5b9e7db0-09cb-11e9-9836-7535bd87e3f3.jpg', 'images/item/list/5b9e7db0-09cb-11e9-9836-7535bd87e3f3.jpg', 'images/item/thumbs/5b9e7db0-09cb-11e9-9836-7535bd87e3f3.jpg', '2018-12-27 05:34:23', '2018-12-27 05:34:23'),
(155, 29, 50, 6, 'images/item/original/5bdf6f30-09cb-11e9-ad68-a1105222609e.jpg', 'images/item/list/5bdf6f30-09cb-11e9-ad68-a1105222609e.jpg', 'images/item/thumbs/5bdf6f30-09cb-11e9-ad68-a1105222609e.jpg', '2018-12-27 05:34:24', '2018-12-27 05:34:24'),
(156, 29, 51, 7, 'images/item/original/5c21d430-09cb-11e9-9bba-b52113eb4405.jpg', 'images/item/list/5c21d430-09cb-11e9-9bba-b52113eb4405.jpg', 'images/item/thumbs/5c21d430-09cb-11e9-9bba-b52113eb4405.jpg', '2018-12-27 05:34:24', '2018-12-27 05:34:24'),
(157, 29, 50, 8, 'images/item/original/5c616a60-09cb-11e9-a979-61532cf7c0d6.jpg', 'images/item/list/5c616a60-09cb-11e9-a979-61532cf7c0d6.jpg', 'images/item/thumbs/5c616a60-09cb-11e9-a979-61532cf7c0d6.jpg', '2018-12-27 05:34:24', '2018-12-27 05:34:24'),
(158, 29, 50, 9, 'images/item/original/5ca5b9a0-09cb-11e9-8e02-e1e51ca31da6.jpg', 'images/item/list/5ca5b9a0-09cb-11e9-8e02-e1e51ca31da6.jpg', 'images/item/thumbs/5ca5b9a0-09cb-11e9-8e02-e1e51ca31da6.jpg', '2018-12-27 05:34:25', '2018-12-27 05:34:25'),
(159, 29, 50, 10, 'images/item/original/5ce5a800-09cb-11e9-88ec-9de14aa18d47.jpg', 'images/item/list/5ce5a800-09cb-11e9-88ec-9de14aa18d47.jpg', 'images/item/thumbs/5ce5a800-09cb-11e9-88ec-9de14aa18d47.jpg', '2018-12-27 05:34:25', '2018-12-27 05:34:25'),
(174, 34, 52, 1, 'images/item/original/74d4afe0-0de4-11e9-b22a-078f01f28ff5.jpg', 'images/item/list/74d4afe0-0de4-11e9-b22a-078f01f28ff5.jpg', 'images/item/thumbs/74d4afe0-0de4-11e9-b22a-078f01f28ff5.jpg', '2019-01-01 10:44:07', '2019-01-01 10:44:07'),
(175, 34, 52, 2, 'images/item/original/751658c0-0de4-11e9-99aa-bda83a1e15ee.jpg', 'images/item/list/751658c0-0de4-11e9-99aa-bda83a1e15ee.jpg', 'images/item/thumbs/751658c0-0de4-11e9-99aa-bda83a1e15ee.jpg', '2019-01-01 10:44:08', '2019-01-01 10:44:08'),
(176, 34, 52, 3, 'images/item/original/755623d0-0de4-11e9-8adf-d786c693b24e.jpg', 'images/item/list/755623d0-0de4-11e9-8adf-d786c693b24e.jpg', 'images/item/thumbs/755623d0-0de4-11e9-8adf-d786c693b24e.jpg', '2019-01-01 10:44:08', '2019-01-01 10:44:08'),
(177, 34, 52, 4, 'images/item/original/759cd130-0de4-11e9-b056-cbbe0e75f405.jpg', 'images/item/list/759cd130-0de4-11e9-b056-cbbe0e75f405.jpg', 'images/item/thumbs/759cd130-0de4-11e9-b056-cbbe0e75f405.jpg', '2019-01-01 10:44:09', '2019-01-01 10:44:09'),
(178, 34, 52, 5, 'images/item/original/75daa340-0de4-11e9-8cab-8b5517266b5c.jpg', 'images/item/list/75daa340-0de4-11e9-8cab-8b5517266b5c.jpg', 'images/item/thumbs/75daa340-0de4-11e9-8cab-8b5517266b5c.jpg', '2019-01-01 10:44:09', '2019-01-01 10:44:09'),
(179, 34, 52, 6, 'images/item/original/761a4450-0de4-11e9-8c90-b1ab58558502.jpg', 'images/item/list/761a4450-0de4-11e9-8c90-b1ab58558502.jpg', 'images/item/thumbs/761a4450-0de4-11e9-8c90-b1ab58558502.jpg', '2019-01-01 10:44:10', '2019-01-01 10:44:10'),
(180, 34, 52, 7, 'images/item/original/765a71b0-0de4-11e9-994b-abe38c66f5ad.jpg', 'images/item/list/765a71b0-0de4-11e9-994b-abe38c66f5ad.jpg', 'images/item/thumbs/765a71b0-0de4-11e9-994b-abe38c66f5ad.jpg', '2019-01-01 10:44:10', '2019-01-01 10:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `item_views`
--

DROP TABLE IF EXISTS `item_views`;
CREATE TABLE IF NOT EXISTS `item_views` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=295 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_views`
--

INSERT INTO `item_views` (`id`, `item_id`, `user_id`, `created_at`, `updated_at`) VALUES
(149, 18, 14, '2018-10-11 02:55:57', '2018-10-11 02:55:57'),
(39, 16, 14, '2018-07-26 12:38:12', '2018-07-26 12:38:12'),
(150, 8, 14, '2018-10-11 03:04:14', '2018-10-11 03:04:14'),
(218, 9, 14, '2018-12-01 09:11:09', '2018-12-01 09:11:09'),
(217, 5, 14, '2018-12-01 08:42:00', '2018-12-01 08:42:00'),
(203, 6, 14, '2018-12-01 07:49:55', '2018-12-01 07:49:55'),
(140, 7, 14, '2018-09-25 06:00:02', '2018-09-25 06:00:02'),
(106, 15, 14, '2018-07-31 11:25:13', '2018-07-31 11:25:13'),
(136, 18, 1, '2018-09-20 09:47:22', '2018-09-20 09:47:22'),
(118, 9, 1, '2018-08-03 10:43:33', '2018-08-03 10:43:33'),
(160, 6, 1, '2018-11-21 09:27:22', '2018-11-21 09:27:22'),
(154, 5, 1, '2018-11-21 08:52:37', '2018-11-21 08:52:37'),
(286, 21, 1, '2019-01-04 03:35:20', '2019-01-04 03:35:20'),
(282, 22, 1, '2019-01-01 09:07:35', '2019-01-01 09:07:35'),
(262, 22, 14, '2018-12-03 07:56:47', '2018-12-03 07:56:47'),
(294, 21, 14, '2019-01-05 02:52:04', '2019-01-05 02:52:04'),
(277, 23, 14, '2018-12-05 11:12:34', '2018-12-05 11:12:34'),
(274, 24, 1, '2018-12-05 07:43:01', '2018-12-05 07:43:01'),
(281, 23, 1, '2019-01-01 09:02:40', '2019-01-01 09:02:40'),
(287, 29, 14, '2019-01-05 02:08:05', '2019-01-05 02:08:05');

-- --------------------------------------------------------

--
-- Table structure for table `lengths`
--

DROP TABLE IF EXISTS `lengths`;
CREATE TABLE IF NOT EXISTS `lengths` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lengths`
--

INSERT INTO `lengths` (`id`, `name`, `sub_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'sdf2', 8, '2018-05-17 07:02:23', '2018-05-17 07:02:52', '2018-05-17 07:02:52'),
(2, '1/2 Sleeve', 10, '2018-05-17 07:02:50', '2018-05-22 14:23:02', NULL),
(3, '3/4 Sleeve', 10, '2018-05-22 14:23:10', '2018-05-22 14:23:10', NULL),
(4, 'Asymmetrical', 10, '2018-05-22 14:23:18', '2018-05-22 14:23:18', NULL),
(5, 'Bell Sleeves', 10, '2018-05-22 14:23:24', '2018-05-22 14:23:24', NULL),
(6, 'Bubble Sleeve', 10, '2018-05-22 14:23:32', '2018-05-22 14:23:32', NULL),
(7, 'Cap Sleeve', 10, '2018-05-22 14:23:42', '2018-05-22 14:23:42', NULL),
(8, 'Dolman Sleeve', 10, '2018-05-22 14:23:49', '2018-05-22 14:23:49', NULL),
(9, 'Flyaway Sleeve', 10, '2018-05-22 14:23:57', '2018-05-22 14:23:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_history`
--

DROP TABLE IF EXISTS `login_history`;
CREATE TABLE IF NOT EXISTS `login_history` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=191 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `login_history`
--

INSERT INTO `login_history` (`id`, `user_id`, `ip`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2018-07-26 02:37:44', '2018-07-26 02:37:44'),
(2, 14, '::1', '2018-07-26 07:58:34', '2018-07-26 07:58:34'),
(3, 1, '::1', '2018-07-26 08:07:24', '2018-07-26 08:07:24'),
(4, 1, '::1', '2018-07-26 08:07:33', '2018-07-26 08:07:33'),
(5, 1, '::1', '2018-07-26 08:37:44', '2018-07-26 08:37:44'),
(6, 14, '::1', '2018-07-26 12:13:40', '2018-07-26 12:13:40'),
(7, 14, '::1', '2018-07-26 12:28:12', '2018-07-26 12:28:12'),
(8, 14, '::1', '2018-07-26 12:38:18', '2018-07-26 12:38:18'),
(9, 14, '::1', '2018-07-26 12:40:29', '2018-07-26 12:40:29'),
(10, 14, '::1', '2018-07-27 02:23:18', '2018-07-27 02:23:18'),
(11, 14, '::1', '2018-07-27 09:19:07', '2018-07-27 09:19:07'),
(12, 14, '::1', '2018-07-27 09:24:21', '2018-07-27 09:24:21'),
(13, 14, '::1', '2018-07-27 09:41:17', '2018-07-27 09:41:17'),
(14, 14, '::1', '2018-07-27 09:56:03', '2018-07-27 09:56:03'),
(15, 14, '::1', '2018-07-27 10:14:53', '2018-07-27 10:14:53'),
(16, 1, '::1', '2018-07-27 13:18:56', '2018-07-27 13:18:56'),
(17, 14, '::1', '2018-07-28 05:38:13', '2018-07-28 05:38:13'),
(18, 14, '::1', '2018-07-28 11:14:46', '2018-07-28 11:14:46'),
(19, 1, '::1', '2018-07-30 08:55:10', '2018-07-30 08:55:10'),
(20, 14, '::1', '2018-07-30 10:59:43', '2018-07-30 10:59:43'),
(21, 14, '::1', '2018-07-30 11:01:25', '2018-07-30 11:01:25'),
(22, 14, '::1', '2018-07-31 05:23:43', '2018-07-31 05:23:43'),
(23, 1, '::1', '2018-07-31 05:37:01', '2018-07-31 05:37:01'),
(24, 1, '::1', '2018-07-31 05:37:46', '2018-07-31 05:37:46'),
(25, 1, '::1', '2018-07-31 05:57:04', '2018-07-31 05:57:04'),
(26, 14, '::1', '2018-07-31 11:15:15', '2018-07-31 11:15:15'),
(27, 1, '::1', '2018-07-31 11:16:13', '2018-07-31 11:16:13'),
(28, 1, '::1', '2018-07-31 11:16:37', '2018-07-31 11:16:37'),
(29, 14, '::1', '2018-07-31 11:25:01', '2018-07-31 11:25:01'),
(30, 1, '::1', '2018-07-31 11:26:41', '2018-07-31 11:26:41'),
(31, 1, '::1', '2018-08-01 04:23:12', '2018-08-01 04:23:12'),
(32, 1, '::1', '2018-08-01 10:03:52', '2018-08-01 10:03:52'),
(33, 14, '::1', '2018-08-01 12:18:30', '2018-08-01 12:18:30'),
(34, 1, '::1', '2018-08-01 12:23:34', '2018-08-01 12:23:34'),
(35, 1, '::1', '2018-08-01 12:42:38', '2018-08-01 12:42:38'),
(36, 1, '::1', '2018-08-01 12:47:07', '2018-08-01 12:47:07'),
(37, 14, '::1', '2018-08-02 03:25:52', '2018-08-02 03:25:52'),
(38, 1, '::1', '2018-08-03 02:24:00', '2018-08-03 02:24:00'),
(39, 1, '::1', '2018-08-03 10:02:12', '2018-08-03 10:02:12'),
(40, 1, '::1', '2018-08-03 10:44:19', '2018-08-03 10:44:19'),
(41, 1, '::1', '2018-08-04 05:49:09', '2018-08-04 05:49:09'),
(42, 1, '::1', '2018-08-04 07:49:38', '2018-08-04 07:49:38'),
(43, 1, '::1', '2018-08-07 05:19:33', '2018-08-07 05:19:33'),
(44, 1, '::1', '2018-08-09 05:21:15', '2018-08-09 05:21:15'),
(45, 1, '::1', '2018-08-09 05:43:23', '2018-08-09 05:43:23'),
(46, 14, '::1', '2018-08-09 10:14:47', '2018-08-09 10:14:47'),
(47, 1, '::1', '2018-08-10 05:13:09', '2018-08-10 05:13:09'),
(48, 1, '::1', '2018-08-13 08:40:49', '2018-08-13 08:40:49'),
(49, 14, '::1', '2018-08-13 09:10:18', '2018-08-13 09:10:18'),
(50, 1, '::1', '2018-08-13 10:49:09', '2018-08-13 10:49:09'),
(51, 14, '::1', '2018-08-13 11:04:59', '2018-08-13 11:04:59'),
(52, 1, '::1', '2018-08-14 02:16:41', '2018-08-14 02:16:41'),
(53, 1, '::1', '2018-08-14 02:32:27', '2018-08-14 02:32:27'),
(54, 14, '::1', '2018-08-16 02:19:37', '2018-08-16 02:19:37'),
(55, 14, '::1', '2018-08-16 05:59:29', '2018-08-16 05:59:29'),
(56, 1, '::1', '2018-08-16 06:45:57', '2018-08-16 06:45:57'),
(57, 1, '::1', '2018-08-16 06:47:48', '2018-08-16 06:47:48'),
(58, 14, '::1', '2018-08-16 11:20:15', '2018-08-16 11:20:15'),
(59, 1, '::1', '2018-08-17 05:08:37', '2018-08-17 05:08:37'),
(60, 14, '::1', '2018-08-17 05:21:14', '2018-08-17 05:21:14'),
(61, 1, '::1', '2018-08-17 05:28:17', '2018-08-17 05:28:17'),
(62, 14, '::1', '2018-08-17 09:46:34', '2018-08-17 09:46:34'),
(63, 1, '::1', '2018-08-17 11:05:37', '2018-08-17 11:05:37'),
(64, 14, '::1', '2018-08-17 11:09:30', '2018-08-17 11:09:30'),
(65, 14, '::1', '2018-08-18 03:34:49', '2018-08-18 03:34:49'),
(66, 1, '::1', '2018-08-18 03:38:20', '2018-08-18 03:38:20'),
(67, 14, '::1', '2018-08-18 04:07:52', '2018-08-18 04:07:52'),
(68, 1, '::1', '2018-09-06 04:23:00', '2018-09-06 04:23:00'),
(69, 1, '::1', '2018-09-06 07:26:46', '2018-09-06 07:26:46'),
(70, 1, '::1', '2018-09-11 08:57:45', '2018-09-11 08:57:45'),
(71, 1, '::1', '2018-09-11 09:42:08', '2018-09-11 09:42:08'),
(72, 1, '::1', '2018-09-16 05:37:28', '2018-09-16 05:37:28'),
(73, 14, '::1', '2018-09-17 07:07:23', '2018-09-17 07:07:23'),
(74, 1, '::1', '2018-09-17 07:23:35', '2018-09-17 07:23:35'),
(75, 1, '::1', '2018-09-17 07:55:00', '2018-09-17 07:55:00'),
(76, 14, '::1', '2018-09-17 08:01:06', '2018-09-17 08:01:06'),
(77, 1, '::1', '2018-09-17 08:01:40', '2018-09-17 08:01:40'),
(78, 1, '::1', '2018-09-18 02:06:31', '2018-09-18 02:06:31'),
(79, 14, '::1', '2018-09-18 08:17:38', '2018-09-18 08:17:38'),
(80, 1, '::1', '2018-09-18 10:22:01', '2018-09-18 10:22:01'),
(81, 1, '::1', '2018-09-19 05:59:59', '2018-09-19 05:59:59'),
(82, 14, '::1', '2018-09-19 09:26:44', '2018-09-19 09:26:44'),
(83, 1, '::1', '2018-09-20 09:18:21', '2018-09-20 09:18:21'),
(84, 1, '::1', '2018-09-20 13:07:47', '2018-09-20 13:07:47'),
(85, 1, '::1', '2018-09-24 02:04:27', '2018-09-24 02:04:27'),
(86, 14, '::1', '2018-09-24 02:11:57', '2018-09-24 02:11:57'),
(87, 14, '::1', '2018-09-24 04:58:25', '2018-09-24 04:58:25'),
(88, 1, '::1', '2018-09-24 06:00:01', '2018-09-24 06:00:01'),
(89, 1, '::1', '2018-09-24 06:48:22', '2018-09-24 06:48:22'),
(90, 14, '::1', '2018-09-24 08:48:28', '2018-09-24 08:48:28'),
(91, 1, '::1', '2018-09-24 08:48:44', '2018-09-24 08:48:44'),
(92, 1, '::1', '2018-09-25 02:02:41', '2018-09-25 02:02:41'),
(93, 1, '::1', '2018-09-25 04:19:27', '2018-09-25 04:19:27'),
(94, 14, '::1', '2018-09-25 04:31:25', '2018-09-25 04:31:25'),
(95, 1, '::1', '2018-09-25 07:50:54', '2018-09-25 07:50:54'),
(96, 14, '::1', '2018-09-25 08:03:38', '2018-09-25 08:03:38'),
(97, 1, '::1', '2018-09-25 08:05:39', '2018-09-25 08:05:39'),
(98, 14, '::1', '2018-09-25 08:20:33', '2018-09-25 08:20:33'),
(99, 1, '::1', '2018-09-25 08:33:40', '2018-09-25 08:33:40'),
(100, 14, '::1', '2018-09-25 09:08:31', '2018-09-25 09:08:31'),
(101, 14, '::1', '2018-09-25 09:33:15', '2018-09-25 09:33:15'),
(102, 14, '::1', '2018-09-25 09:48:48', '2018-09-25 09:48:48'),
(103, 14, '::1', '2018-09-25 10:49:10', '2018-09-25 10:49:10'),
(104, 1, '::1', '2018-09-26 05:32:25', '2018-09-26 05:32:25'),
(105, 14, '::1', '2018-09-26 07:13:45', '2018-09-26 07:13:45'),
(106, 1, '::1', '2018-09-26 07:34:48', '2018-09-26 07:34:48'),
(107, 14, '::1', '2018-09-28 04:12:45', '2018-09-28 04:12:45'),
(108, 1, '::1', '2018-09-28 04:36:51', '2018-09-28 04:36:51'),
(109, 1, '::1', '2018-10-03 02:10:55', '2018-10-03 02:10:55'),
(110, 1, '::1', '2018-10-09 03:27:38', '2018-10-09 03:27:38'),
(111, 1, '::1', '2018-10-09 03:45:01', '2018-10-09 03:45:01'),
(112, 1, '::1', '2018-10-09 08:51:21', '2018-10-09 08:51:21'),
(113, 1, '::1', '2018-10-09 08:51:42', '2018-10-09 08:51:42'),
(114, 1, '::1', '2018-10-10 02:41:17', '2018-10-10 02:41:17'),
(115, 14, '::1', '2018-10-11 02:27:00', '2018-10-11 02:27:00'),
(116, 1, '::1', '2018-10-11 07:19:41', '2018-10-11 07:19:41'),
(117, 1, '::1', '2018-10-30 08:24:36', '2018-10-30 08:24:36'),
(118, 1, '::1', '2018-11-21 04:31:23', '2018-11-21 04:31:23'),
(119, 1, '::1', '2018-11-21 07:20:43', '2018-11-21 07:20:43'),
(120, 14, '::1', '2018-11-21 09:00:44', '2018-11-21 09:00:44'),
(121, 1, '::1', '2018-11-21 09:09:41', '2018-11-21 09:09:41'),
(122, 14, '::1', '2018-11-21 09:27:39', '2018-11-21 09:27:39'),
(123, 1, '::1', '2018-11-24 10:59:04', '2018-11-24 10:59:04'),
(124, 14, '::1', '2018-11-30 08:09:58', '2018-11-30 08:09:58'),
(125, 14, '::1', '2018-11-30 08:17:38', '2018-11-30 08:17:38'),
(126, 14, '::1', '2018-11-30 12:34:32', '2018-11-30 12:34:32'),
(127, 14, '::1', '2018-12-01 02:05:51', '2018-12-01 02:05:51'),
(128, 14, '::1', '2018-12-01 04:26:38', '2018-12-01 04:26:38'),
(129, 14, '::1', '2018-12-01 04:44:08', '2018-12-01 04:44:08'),
(130, 1, '::1', '2018-12-01 07:55:54', '2018-12-01 07:55:54'),
(131, 14, '::1', '2018-12-01 08:03:05', '2018-12-01 08:03:05'),
(132, 1, '::1', '2018-12-01 09:14:20', '2018-12-01 09:14:20'),
(133, 1, '::1', '2018-12-01 10:34:29', '2018-12-01 10:34:29'),
(134, 14, '::1', '2018-12-03 03:24:11', '2018-12-03 03:24:11'),
(135, 1, '::1', '2018-12-03 07:11:35', '2018-12-03 07:11:35'),
(136, 14, '::1', '2018-12-03 07:34:14', '2018-12-03 07:34:14'),
(137, 1, '::1', '2018-12-03 07:54:24', '2018-12-03 07:54:24'),
(138, 1, '::1', '2018-12-04 04:19:52', '2018-12-04 04:19:52'),
(139, 1, '::1', '2018-12-04 07:30:39', '2018-12-04 07:30:39'),
(140, 1, '::1', '2018-12-04 08:49:28', '2018-12-04 08:49:28'),
(141, 1, '::1', '2018-12-04 11:00:20', '2018-12-04 11:00:20'),
(142, 1, '::1', '2018-12-05 07:11:04', '2018-12-05 07:11:04'),
(143, 1, '::1', '2018-12-05 08:59:44', '2018-12-05 08:59:44'),
(144, 14, '::1', '2018-12-05 10:32:11', '2018-12-05 10:32:11'),
(145, 1, '::1', '2018-12-13 02:18:36', '2018-12-13 02:18:36'),
(146, 1, '::1', '2018-12-13 09:23:51', '2018-12-13 09:23:51'),
(147, 1, '::1', '2018-12-13 09:37:20', '2018-12-13 09:37:20'),
(148, 1, '::1', '2018-12-13 12:02:29', '2018-12-13 12:02:29'),
(149, 1, '::1', '2018-12-17 05:00:16', '2018-12-17 05:00:16'),
(150, 1, '::1', '2018-12-17 05:16:45', '2018-12-17 05:16:45'),
(151, 1, '::1', '2018-12-17 05:25:30', '2018-12-17 05:25:30'),
(152, 1, '::1', '2018-12-17 09:44:13', '2018-12-17 09:44:13'),
(153, 1, '::1', '2018-12-17 22:07:27', '2018-12-17 22:07:27'),
(154, 1, '::1', '2018-12-17 22:13:32', '2018-12-17 22:13:32'),
(155, 14, '::1', '2018-12-18 00:05:02', '2018-12-18 00:05:02'),
(156, 1, '::1', '2018-12-18 00:06:15', '2018-12-18 00:06:15'),
(157, 1, '::1', '2018-12-18 03:37:05', '2018-12-18 03:37:05'),
(158, 1, '::1', '2018-12-19 03:07:00', '2018-12-19 03:07:00'),
(159, 1, '::1', '2018-12-19 04:27:21', '2018-12-19 04:27:21'),
(160, 1, '::1', '2018-12-19 04:55:36', '2018-12-19 04:55:36'),
(161, 1, '::1', '2018-12-19 05:51:57', '2018-12-19 05:51:57'),
(162, 1, '::1', '2018-12-19 06:44:36', '2018-12-19 06:44:36'),
(163, 1, '::1', '2018-12-19 07:54:36', '2018-12-19 07:54:36'),
(164, 1, '::1', '2018-12-19 09:56:03', '2018-12-19 09:56:03'),
(165, 1, '::1', '2018-12-20 06:52:10', '2018-12-20 06:52:10'),
(166, 1, '::1', '2018-12-27 02:15:45', '2018-12-27 02:15:45'),
(167, 1, '::1', '2018-12-27 05:24:14', '2018-12-27 05:24:14'),
(168, 1, '::1', '2018-12-27 06:27:12', '2018-12-27 06:27:12'),
(169, 1, '::1', '2018-12-28 02:35:30', '2018-12-28 02:35:30'),
(170, 1, '::1', '2018-12-29 02:03:33', '2018-12-29 02:03:33'),
(171, 1, '::1', '2019-01-01 04:01:29', '2019-01-01 04:01:29'),
(172, 1, '::1', '2019-01-01 10:17:40', '2019-01-01 10:17:40'),
(173, 1, '::1', '2019-01-02 02:07:14', '2019-01-02 02:07:14'),
(174, 1, '::1', '2019-01-02 02:41:57', '2019-01-02 02:41:57'),
(175, 14, '::1', '2019-01-02 10:17:16', '2019-01-02 10:17:16'),
(176, 14, '::1', '2019-01-02 11:01:22', '2019-01-02 11:01:22'),
(177, 1, '::1', '2019-01-03 08:42:26', '2019-01-03 08:42:26'),
(178, 14, '::1', '2019-01-04 02:04:27', '2019-01-04 02:04:27'),
(179, 1, '::1', '2019-01-04 02:18:32', '2019-01-04 02:18:32'),
(180, 1, '::1', '2019-01-04 03:17:22', '2019-01-04 03:17:22'),
(181, 1, '::1', '2019-01-05 02:04:57', '2019-01-05 02:04:57'),
(182, 14, '::1', '2019-01-05 02:06:57', '2019-01-05 02:06:57'),
(183, 1, '::1', '2019-01-05 03:09:03', '2019-01-05 03:09:03'),
(184, 14, '::1', '2019-01-05 05:24:56', '2019-01-05 05:24:56'),
(185, 14, '::1', '2019-01-05 05:34:54', '2019-01-05 05:34:54'),
(186, 14, '::1', '2019-01-05 05:43:57', '2019-01-05 05:43:57'),
(187, 1, '::1', '2019-01-07 02:02:22', '2019-01-07 02:02:22'),
(188, 1, '::1', '2019-01-08 03:31:33', '2019-01-08 03:31:33'),
(189, 1, '::1', '2019-01-08 07:02:01', '2019-01-08 07:02:01'),
(190, 1, '::1', '2019-01-08 07:29:23', '2019-01-08 07:29:23');

-- --------------------------------------------------------

--
-- Table structure for table `made_in_countries`
--

DROP TABLE IF EXISTS `made_in_countries`;
CREATE TABLE IF NOT EXISTS `made_in_countries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `made_in_countries`
--

INSERT INTO `made_in_countries` (`id`, `name`, `status`, `default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'IMPORT', 1, 1, '2018-05-12 05:03:14', '2018-06-30 09:57:51', NULL),
(11, 'ee', 1, 0, '2018-05-12 06:08:15', '2018-05-12 06:28:46', '2018-05-12 06:28:46'),
(12, 'd', 0, 0, '2018-05-12 06:20:36', '2018-05-22 14:43:53', '2018-05-22 14:43:53'),
(13, '2', 0, 0, '2018-05-12 06:43:58', '2018-05-12 06:44:11', '2018-05-12 06:44:11'),
(8, '4', 1, 0, '2018-05-12 05:42:22', '2018-05-22 14:43:49', '2018-05-22 14:43:49'),
(10, 'aa', 1, 0, '2018-05-12 05:46:40', '2018-05-22 14:43:51', '2018-05-22 14:43:51'),
(14, 'dfd', 1, 0, '2018-05-12 09:27:19', '2018-05-22 14:43:54', '2018-05-22 14:43:54'),
(15, 'dda3', 0, 0, '2018-05-17 01:30:11', '2018-05-17 01:30:28', '2018-05-17 01:30:28'),
(16, 'USA', 1, 0, '2018-05-22 14:44:10', '2018-06-30 09:57:51', NULL),
(17, 'IMPORT', 1, 0, '2018-05-23 15:22:38', '2018-06-30 09:57:51', NULL),
(18, 'USA', 1, 0, '2018-05-23 15:22:47', '2018-06-30 09:57:51', NULL),
(19, 'd', 1, 0, '2018-06-25 08:01:29', '2018-06-25 08:01:33', '2018-06-25 08:01:33'),
(20, 'erer', 0, 0, '2018-06-30 09:57:44', '2018-06-30 09:57:56', '2018-06-30 09:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `master_colors`
--

DROP TABLE IF EXISTS `master_colors`;
CREATE TABLE IF NOT EXISTS `master_colors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_colors`
--

INSERT INTO `master_colors` (`id`, `name`, `image_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 'Black', '/images/master_color/4e77ad10-5df2-11e8-9866-35f0a9aad927.jpg', '2018-05-22 12:59:51', '2018-05-22 12:59:51', NULL),
(7, 'Red', '/images/master_color/3586a0b0-5df2-11e8-8423-fbb0e7848eaf.jpg', '2018-05-22 12:59:09', '2018-05-22 12:59:09', NULL),
(9, 'Blue', '/images/master_color/5e75f940-5df2-11e8-8b31-53e1d9dda377.jpg', '2018-05-22 13:00:18', '2018-05-22 13:00:18', NULL),
(10, 'Brown', '/images/master_color/791e7f20-5df2-11e8-b6ea-f9806acbee22.jpg', '2018-05-22 13:01:03', '2018-05-22 13:01:03', NULL),
(11, 'Gold', '/images/master_color/871c7b10-5df2-11e8-bd06-5d3d96e22977.jpg', '2018-05-22 13:01:26', '2018-05-22 13:01:26', NULL),
(12, 'Gray', '/images/master_color/97251680-5df2-11e8-80d0-0b45ed04fbf9.jpg', '2018-05-22 13:01:53', '2018-05-22 13:01:53', NULL),
(13, 'Green', '/images/master_color/ad0764b0-5df2-11e8-bf69-49778d6fff9a.jpg', '2018-05-22 13:02:30', '2018-05-22 13:02:30', NULL),
(14, 'Ivory', '/images/master_color/c1f2e920-5df2-11e8-8da5-5b7f0944a348.jpg', '2018-05-22 13:03:05', '2018-05-22 13:03:05', NULL),
(15, 'Multi', '/images/master_color/d3727d00-5df2-11e8-8efd-e3a8ede6be2e.jpg', '2018-05-22 13:03:34', '2018-05-22 13:03:34', NULL),
(16, 'Orange', '/images/master_color/e5b44320-5df2-11e8-8331-275f5efbdb75.jpg', '2018-05-22 13:04:05', '2018-05-22 13:04:05', NULL),
(17, 'Pink', '/images/master_color/f9303bb0-5df2-11e8-a50f-230282c65397.jpg', '2018-05-22 13:04:37', '2018-05-22 13:04:37', NULL),
(18, 'Purple', '/images/master_color/12f03b80-5df3-11e8-a134-07210ad6c7cb.jpg', '2018-05-22 13:05:21', '2018-05-22 13:05:21', NULL),
(19, 'Silver', '/images/master_color/283ad410-5df3-11e8-878c-15abac44d755.jpg', '2018-05-22 13:05:56', '2018-05-22 13:05:56', NULL),
(20, 'Beige', '/images/master_color/3bbf2f50-5df3-11e8-861a-ff67af98860e.jpg', '2018-05-22 13:06:29', '2018-05-22 13:06:29', NULL),
(21, 'White', '/images/master_color/4b694870-5df3-11e8-a72e-7702ab347774.jpg', '2018-05-22 13:06:55', '2018-05-22 13:06:55', NULL),
(22, 'Yellow', '/images/master_color/5b5a3520-5df3-11e8-a6bc-bd02725fcab8.jpg', '2018-05-22 13:07:22', '2018-06-29 09:43:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_fabrics`
--

DROP TABLE IF EXISTS `master_fabrics`;
CREATE TABLE IF NOT EXISTS `master_fabrics` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_fabrics`
--

INSERT INTO `master_fabrics` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Canvas', '2018-05-12 08:04:56', '2018-05-22 13:10:03', NULL),
(2, 'Chiffon', '2018-05-12 08:05:13', '2018-05-22 13:10:12', NULL),
(3, 'Acid Wash', '2018-05-12 08:08:01', '2018-05-22 13:09:48', NULL),
(4, 'Corduroy', '2018-05-22 13:10:23', '2018-05-22 13:10:23', NULL),
(5, 'Cotton', '2018-05-22 13:10:32', '2018-05-22 13:10:32', NULL),
(6, 'Crystal', '2018-05-22 13:10:37', '2018-05-22 13:10:37', NULL),
(7, 'Dark Wash', '2018-05-22 13:10:49', '2018-05-22 13:10:49', NULL),
(8, 'Denim', '2018-05-22 13:11:21', '2018-05-22 13:11:21', NULL),
(9, 'Faux Leather', '2018-05-22 13:11:27', '2018-05-22 13:11:27', NULL),
(10, 'Faux Pearl', '2018-05-22 13:11:32', '2018-05-22 13:11:32', NULL),
(11, 'Genuine Stones', '2018-05-22 13:11:38', '2018-05-22 13:11:38', NULL),
(12, 'Jersey', '2018-05-22 13:11:43', '2018-05-22 13:11:43', NULL),
(13, 'Lace', '2018-05-22 13:11:49', '2018-05-22 13:11:49', NULL),
(14, 'Leather', '2018-05-22 13:12:06', '2018-05-22 13:12:06', NULL),
(15, 'Leather/Pleather', '2018-05-22 13:12:11', '2018-05-22 13:12:11', NULL),
(16, 'Light Wash', '2018-05-22 13:12:18', '2018-05-22 13:12:18', NULL),
(17, 'Linen', '2018-05-22 13:12:23', '2018-05-22 13:12:23', NULL),
(18, 'Mesh', '2018-05-22 13:12:28', '2018-05-22 13:12:28', NULL),
(19, 'Metal', '2018-05-22 13:12:34', '2018-05-22 13:12:34', NULL),
(20, 'Patent Leather', '2018-05-22 13:12:38', '2018-05-22 13:12:38', NULL),
(21, 'Pearl', '2018-05-22 13:12:44', '2018-05-22 13:12:44', NULL),
(22, 'Plastic', '2018-05-22 13:12:51', '2018-05-22 13:12:51', NULL),
(23, 'Pleather', '2018-05-22 13:12:55', '2018-05-22 13:12:55', NULL),
(24, 'Polyester', '2018-05-22 13:12:59', '2018-05-22 13:12:59', NULL),
(25, 'Rhinestones', '2018-05-22 13:13:04', '2018-05-22 13:13:04', NULL),
(26, 'Satin', '2018-05-22 13:13:08', '2018-05-22 13:13:08', NULL),
(27, 'Silk', '2018-05-22 13:13:15', '2018-05-22 13:13:15', NULL),
(28, 'Spandex', '2018-05-22 13:13:23', '2018-05-22 13:13:23', NULL),
(29, 'Stones', '2018-05-22 13:13:28', '2018-05-22 13:13:28', NULL),
(30, 'Straw', '2018-05-22 13:13:33', '2018-05-22 13:13:33', NULL),
(31, 'Suede', '2018-05-22 13:13:37', '2018-05-22 13:13:37', NULL),
(32, 'Taffeta', '2018-05-22 13:13:43', '2018-05-22 13:13:43', NULL),
(33, 'Terry/Velour', '2018-05-22 13:13:47', '2018-05-22 13:13:47', NULL),
(34, 'Wood', '2018-05-22 13:13:55', '2018-05-22 13:13:55', NULL),
(35, 'Wool', '2018-05-22 13:14:01', '2018-05-22 13:14:01', NULL),
(36, 'Rayon', '2018-05-22 13:14:06', '2018-05-22 13:14:06', NULL),
(37, 'Cotton/Poly', '2018-05-22 13:14:10', '2018-05-22 13:14:10', NULL),
(38, 'Viscose', '2018-05-22 13:14:17', '2018-05-22 13:14:17', NULL),
(39, 'Nylon', '2018-05-22 13:14:22', '2018-05-22 13:14:22', NULL),
(40, 'Medium Wash', '2018-05-22 13:14:27', '2018-05-22 13:14:27', NULL),
(41, 'White Denim', '2018-05-22 13:14:31', '2018-05-22 13:14:31', NULL),
(42, 'Black and Grey', '2018-05-22 13:14:36', '2018-05-22 13:14:36', NULL),
(43, 'Acrylic', '2018-05-22 13:14:42', '2018-05-22 13:14:42', NULL),
(44, 'Tencel', '2018-05-22 13:14:53', '2018-05-22 13:14:53', NULL),
(45, 'Cashmere', '2018-05-22 13:14:57', '2018-05-22 13:14:57', NULL),
(46, 'Modal', '2018-05-22 13:15:01', '2018-05-22 13:15:01', NULL),
(47, 'Velvet', '2018-05-22 13:15:06', '2018-05-22 13:15:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `metas`
--

DROP TABLE IF EXISTS `metas`;
CREATE TABLE IF NOT EXISTS `metas` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `page` int(11) NOT NULL,
  `category` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `metas`
--

INSERT INTO `metas` (`id`, `page`, `category`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 6, NULL, 'Welcome CQBYCQ', NULL, '2018-07-31 05:39:19', '2018-12-01 09:22:25'),
(2, 7, 1, NULL, NULL, '2018-07-31 11:16:18', '2018-07-31 11:16:18'),
(3, 4, NULL, NULL, NULL, '2018-10-09 09:09:06', '2018-10-09 09:09:06'),
(4, 8, NULL, NULL, NULL, '2018-10-09 09:09:09', '2018-10-09 09:09:09');

-- --------------------------------------------------------

--
-- Table structure for table `meta_buyers`
--

DROP TABLE IF EXISTS `meta_buyers`;
CREATE TABLE IF NOT EXISTS `meta_buyers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `verified` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `block` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primary_customer_market` int(11) NOT NULL,
  `seller_permit_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sell_online` tinyint(1) DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attention` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country_id` int(11) NOT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_commercial` tinyint(1) DEFAULT NULL,
  `hear_about_us` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hear_about_us_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_offers` tinyint(1) NOT NULL,
  `sales2_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales1_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_order` tinyint(1) NOT NULL DEFAULT '1',
  `ein_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_buyers`
--

INSERT INTO `meta_buyers` (`id`, `verified`, `active`, `block`, `user_id`, `company_name`, `primary_customer_market`, `seller_permit_number`, `sell_online`, `website`, `attention`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state_id`, `billing_state`, `billing_zip`, `billing_country_id`, `billing_phone`, `billing_fax`, `billing_commercial`, `hear_about_us`, `hear_about_us_other`, `receive_offers`, `sales2_path`, `sales1_path`, `min_order`, `ein_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 1, 1, 0, 14, 'Buyer LTD', 1, '54654654', NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 6, NULL, '5656', 1, '4576767', '4545', 0, 'bing', NULL, 1, NULL, NULL, 1, NULL, '2018-05-24 10:22:34', '2018-07-28 05:54:46', NULL),
(4, 1, 1, 0, 19, 'newer', 2, '343434', 0, NULL, '53453', 'US', '45345', NULL, '45', 8, NULL, '34535', 1, '4545', NULL, 0, 'yahoo', NULL, 1, NULL, NULL, 1, NULL, '2018-06-22 07:18:27', '2018-07-24 11:54:45', NULL),
(9, 0, 0, 0, 23, 'sdffffffff', 1, 'werqwr', NULL, NULL, 'qwer', 'US', 'rqwer', NULL, 'qwerqe', 3, NULL, '234324', 1, 'rqwerq', NULL, 0, 'google', NULL, 1, '/files/buyer/45851940-c0d4-11e8-8ded-2d01b888fc39.jpg', '/files/buyer/4584e7e0-c0d4-11e8-91aa-453d61002a0f.jpg', 1, '/files/buyer/45848ff0-c0d4-11e8-a55a-4149f7d491a4.jpg', '2018-09-25 09:04:16', '2018-09-25 09:04:16', NULL),
(10, 1, 0, 0, 24, 'asdfasdf', 1, 'asdfadf', 0, NULL, 'asdf', 'US', 'sadf', NULL, 'asdf', 3, NULL, 'asdf', 1, 'asdfa', 'asdf', 0, 'google', NULL, 1, '/files/buyer/41092580-0e77-11e9-9931-f95eb53abf89.jpg', NULL, 1, '/files/buyer/410906c0-0e77-11e9-bc0a-b96518595b7c.jpg', '2019-01-02 04:14:56', '2019-01-02 04:14:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meta_vendors`
--

DROP TABLE IF EXISTS `meta_vendors`;
CREATE TABLE IF NOT EXISTS `meta_vendors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `verified` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country_id` int(11) NOT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_alternate_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_commercial` tinyint(1) NOT NULL,
  `factory_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_state_id` int(11) DEFAULT NULL,
  `factory_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_country_id` int(11) NOT NULL,
  `factory_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_alternate_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_evening_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_commercial` tinyint(1) DEFAULT NULL,
  `company_info` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hear_about_us` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hear_about_us_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_notice` varchar(3000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_chart` varchar(3000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_offers` tinyint(1) NOT NULL,
  `setting_estimated_shipping_charge` tinyint(1) NOT NULL DEFAULT '0',
  `setting_consolidation` tinyint(1) NOT NULL DEFAULT '0',
  `setting_sort_activation_date` tinyint(1) NOT NULL DEFAULT '0',
  `setting_unverified_checkout` tinyint(1) NOT NULL DEFAULT '0',
  `setting_unverified_user` tinyint(1) NOT NULL DEFAULT '0',
  `setting_not_logged` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_main_slider` tinyint(1) NOT NULL DEFAULT '0',
  `sp_vendor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp_password` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp_default_category` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_order` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_vendors`
--

INSERT INTO `meta_vendors` (`id`, `verified`, `active`, `company_name`, `business_name`, `website`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state_id`, `billing_state`, `billing_zip`, `billing_country_id`, `billing_phone`, `billing_alternate_phone`, `billing_fax`, `billing_commercial`, `factory_location`, `factory_address`, `factory_unit`, `factory_city`, `factory_state_id`, `factory_state`, `factory_zip`, `factory_country_id`, `factory_phone`, `factory_alternate_phone`, `factory_evening_phone`, `factory_fax`, `factory_commercial`, `company_info`, `hear_about_us`, `hear_about_us_other`, `order_notice`, `size_chart`, `receive_offers`, `setting_estimated_shipping_charge`, `setting_consolidation`, `setting_sort_activation_date`, `setting_unverified_checkout`, `setting_unverified_user`, `setting_not_logged`, `show_in_main_slider`, `sp_vendor`, `sp_password`, `sp_category`, `sp_default_category`, `min_order`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Binjar', 'Binjar Inc.', 'www.binjar.com', 'CA', 'Banashree', '2', 'Dhaka', 68, 'Rampura', '1219', 2, '01670985422', NULL, '123456', 1, 'US', 'cuet', '2', 'Chittagong', 11, 'Raozan', '3152', 1, '123456789', '45664', NULL, '4555', NULL, 'Company Info in details2', 'other', 'Bola jabena', '<p>&nbsp;asdf<strong> asd ORDER6</strong></p>\n\n<p><strong>asdfasdf</strong></p>', '<p>&nbsp;asdfa<em> ds2</em></p>', 1, 0, 0, 1, 0, 0, 0, 1, 'vendor', '123456', 'SALES', 'df', 500, '2018-05-08 09:03:20', '2019-01-05 03:38:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=159 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(46, '2014_10_12_000000_create_users_table', 1),
(47, '2014_10_12_100000_create_password_resets_table', 1),
(48, '2018_05_05_133752_create_default_categories_table', 1),
(49, '2018_05_08_080401_create_countries_table', 1),
(50, '2018_05_08_083857_create_states_table', 1),
(51, '2018_05_08_134438_create_meta_vendors_table', 1),
(52, '2018_05_09_103818_create_sizes_table', 2),
(53, '2018_05_11_105225_create_packs_table', 3),
(54, '2018_05_11_120421_create_master_colors_table', 4),
(56, '2018_05_11_135750_create_colors_table', 5),
(57, '2018_05_12_103732_create_made_in_countries_table', 6),
(58, '2018_05_12_134524_create_master_fabrics_table', 7),
(59, '2018_05_12_151609_create_fabrics_table', 8),
(60, '2018_05_14_101752_create_industries_table', 9),
(62, '2018_05_14_122651_add_year_establish_in_meta_vendors_table', 10),
(63, '2018_05_14_133652_create_industry_user_table', 11),
(64, '2018_05_14_152938_add_size_chart_column_in_meta_vendors_table', 12),
(65, '2018_05_15_092930_add_sort_column_in_default_categories_table', 13),
(66, '2018_05_15_110154_add_user_id_column_in_users_table', 14),
(67, '2018_05_15_110903_remove_user_id_column_in_meta_vendors_table', 15),
(68, '2018_05_15_125606_add_status_column_in_users_table', 16),
(69, '2018_05_15_131551_add_last_login_column_in_users_table', 17),
(70, '2018_05_15_135817_remove_verified_column_in_users_table', 18),
(71, '2018_05_15_135945_add_verified_column_in_meta_vendors_table', 19),
(72, '2018_05_15_164157_create_user_permission_table', 20),
(73, '2018_05_16_081643_add_settings_columns_in_meta_vendors_table', 21),
(74, '2018_05_16_091300_create_login_history_table', 22),
(79, '2018_05_16_121659_create_categories_table', 23),
(80, '2018_05_16_173514_change_user_id_to_meta_vendors_in_packs_table', 24),
(81, '2018_05_17_072246_change_user_id_to_meta_vendor_id_in_fabrics_table', 25),
(82, '2018_05_17_072801_change_user_id_to_meta_vendor_id_in_made_in_countries_table', 26),
(83, '2018_05_17_084204_create_body_sizes_table', 27),
(84, '2018_05_17_113905_create_patterns_table', 28),
(85, '2018_05_17_124941_create_lengths_table', 29),
(86, '2018_05_17_133027_create_styles_table', 30),
(87, '2018_05_17_163110_change_user_id_to_meta_vendor_id_in_made_in_colors_table', 31),
(88, '2018_05_18_142050_create_item_images_table', 32),
(89, '2018_05_18_180811_create_items_table', 33),
(90, '2018_05_18_182509_create_color_item_table', 34),
(91, '2018_05_18_194317_add_color_id_column_in_item_images_table', 35),
(92, '2018_05_19_150357_add_activation_at_column_in_items_table', 36),
(93, '2018_05_21_085041_add_meta_vendor_id_in_items_column', 37),
(94, '2018_05_21_104611_change_descrtiption_to_nullable_in_items_table', 38),
(95, '2018_05_21_181848_add_sort_column_in_item_images_table', 39),
(96, '2018_05_22_183025_add_image_path_column_in_master_colors_table', 40),
(97, '2018_05_24_155751_create_meta_buyers_table', 41),
(99, '2018_05_25_052549_create_cart_items_table', 42),
(100, '2018_05_25_150654_add_active_column_in_meta_vendors_table', 43),
(101, '2018_05_26_115243_add_location_columns_in_meta_vendors_table', 44),
(103, '2018_05_28_135447_create_vendor_images_table', 45),
(105, '2018_05_28_182550_create_orders_table', 46),
(106, '2018_05_28_200922_add_vendor_meta_id_column_in_cart_items_table', 47),
(109, '2018_05_29_085525_create_order_items_table', 48),
(111, '2018_05_29_095042_add_order_number_column_in_orders_table', 49),
(112, '2018_05_29_164751_add_per_unit_price_column_in_order_items_table', 50),
(113, '2018_05_30_083752_create_notifications_table', 51),
(114, '2018_05_30_181718_add_verify_active_column_in_meta_buyers_table', 52),
(115, '2018_05_30_183944_add_block_column_in_meta_buyers_table', 53),
(116, '2018_05_31_055248_add_shipping_location_column_in_meta_buyers_table', 54),
(118, '2018_05_31_060140_add_store_no_column_in_meta_buyers_table', 55),
(119, '2018_05_31_192012_create_couriers_table', 56),
(120, '2018_06_01_083044_create_admin_ship_methods_table', 57),
(121, '2018_06_01_094139_create_shipping_methods_table', 58),
(122, '2018_06_01_191253_add_shipping_method_id_in_orders_table', 59),
(123, '2018_06_04_090630_add_show_main_slider_column_in_meta_vendors_table', 60),
(124, '2018_06_05_090127_create_visitors_table', 61),
(125, '2018_06_07_204324_create_category_banners_table', 62),
(126, '2018_06_08_101221_add_main_slider_column_in_items_table', 63),
(127, '2018_06_08_180010_add_soft_delete_in_orders_table', 64),
(128, '2018_06_09_153920_add_shipping_location_column_in_orders_table', 65),
(129, '2018_06_09_155854_add_shipping_country_id_column_in_orders_table', 66),
(130, '2018_06_10_085852_add_code_column_in_colors_table', 67),
(131, '2018_06_10_111156_add_tracking_number_column_in_orders_table', 68),
(133, '2018_06_11_090147_add_user_id_column_in_meta_buyers_table', 69),
(134, '2018_06_11_154425_change_card_number_length_in_orders_table', 70),
(135, '2018_06_11_190820_add_new_top_slider_column_in_items_table', 71),
(136, '2018_06_12_151328_create_wish_list_items_table', 72),
(138, '2018_06_12_181955_add_list_image_path_in_item_images_table', 73),
(139, '2018_06_12_190655_add_thumbs_image_path_in_item_images_table', 74),
(140, '2018_06_13_090427_add_style_no_column_in_order_items_table', 75),
(141, '2018_06_13_093013_add_company_name_column_in_orders_table', 76),
(142, '2018_06_13_094203_add_name_column_in_orders_table', 77),
(143, '2018_06_13_094740_add_shipping_column_in_orders_table', 78),
(144, '2018_06_13_105024_add_notes_column_in_orders_table', 79),
(145, '2018_06_13_160933_add_image_path_column_in_colors_table', 80),
(146, '2018_06_13_180012_create_slider_items_table', 81),
(147, '2018_06_22_090108_remove_sub_category_from_body_sizes_table', 82),
(148, '2018_06_22_090425_add_parent_category_id_in_body_sizes_table', 83),
(149, '2018_06_22_101056_change_sub_category_id_to_parent_category_id_in_patterns_table', 84),
(150, '2018_06_22_102143_change_sub_category_id_to_parent_category_id_in_styles_table', 85),
(152, '2018_06_22_124629_create_buyer_shipping_addresses_table', 86),
(153, '2018_06_22_130248_remove_shipping_address_from_meta_buyers_table', 87),
(154, '2018_06_22_141254_rename_state_to_state_text_in_buyer_shipping_address_table', 88),
(155, '2018_06_22_175526_add_shipping_address_id_in_orders_table', 89),
(156, '2018_06_26_095141_add_user_id_column_in_visitors_table', 90),
(157, '2018_06_26_103908_create_block_users_table', 91),
(158, '2018_06_28_164609_add_fabric_column_in_items_table', 92);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `order_id`, `text`, `link`, `view`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, NULL, 'G9QUFXMHTMLG0 need approval.', 'http://localhost/fourgirls/public/order/34', 0, '2018-09-25 08:01:29', '2018-09-25 08:01:29', NULL),
(2, 14, NULL, 'G9QUFXMHTMLG0 need approval.', 'http://localhost/fourgirls/public/order/34', 0, '2018-09-25 08:02:10', '2018-09-25 08:02:10', NULL),
(3, 14, NULL, 'C1CM9B506UFZR-BO2 need approval.', 'http://localhost/fourgirls/public/order/36', 0, '2018-09-25 08:33:47', '2018-09-25 08:33:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `rejected` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address_id` int(11) DEFAULT NULL,
  `shipping_method_id` int(11) DEFAULT NULL,
  `shipping_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_id` int(11) DEFAULT NULL,
  `shipping_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country_id` int(11) DEFAULT NULL,
  `shipping_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country_id` int(11) DEFAULT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_full_name` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_expire` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_cvc` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtotal` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `store_credit` float NOT NULL DEFAULT '0',
  `shipping_cost` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `note` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `can_call` tinyint(1) NOT NULL DEFAULT '0',
  `authorize_info` text COLLATE utf8mb4_unicode_ci,
  `paypal_payment_id` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_token` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_payer_id` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirm_at` timestamp NULL DEFAULT NULL,
  `partially_shipped_at` timestamp NULL DEFAULT NULL,
  `fully_shipped_at` timestamp NULL DEFAULT NULL,
  `back_ordered_at` timestamp NULL DEFAULT NULL,
  `cancel_at` timestamp NULL DEFAULT NULL,
  `return_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `status`, `rejected`, `user_id`, `order_number`, `invoice_number`, `tracking_number`, `name`, `email`, `company_name`, `shipping`, `shipping_address_id`, `shipping_method_id`, `shipping_location`, `shipping_address`, `shipping_unit`, `shipping_city`, `shipping_state`, `shipping_state_text`, `shipping_state_id`, `shipping_zip`, `shipping_country`, `shipping_country_id`, `shipping_phone`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state`, `billing_state_text`, `billing_state_id`, `billing_zip`, `billing_country`, `billing_country_id`, `billing_phone`, `card_number`, `card_full_name`, `card_expire`, `card_cvc`, `payment_type`, `subtotal`, `discount`, `store_credit`, `shipping_cost`, `total`, `note`, `can_call`, `authorize_info`, `paypal_payment_id`, `paypal_token`, `paypal_payer_id`, `confirm_at`, `partially_shipped_at`, `fully_shipped_at`, `back_ordered_at`, `cancel_at`, `return_at`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 0, 14, 'INYRWTHZJU3MZ', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 144.00, 0.00, 0, 0.00, 144.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-26 13:10:50', '2018-07-26 13:10:50', NULL),
(2, 1, 0, 14, 'Y1G8MJFXREU2Q', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 1116.00, 0.00, 0, 0.00, 1116.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 02:24:31', '2018-07-27 02:24:31', NULL),
(3, 1, 0, 14, 'T1B5BDI50GXA5', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 1116.00, 0.00, 0, 0.00, 1116.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 02:47:54', '2018-07-27 02:47:54', NULL),
(4, 2, 0, 14, 'UHZXTTWCKBBSC', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'with fee', 6, 7, 'US', 'asdf', NULL, 'dfadf', 'Alaska', NULL, 1, NULL, 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', 'eyJpdiI6IlZqUkZrcEJSV0w5RHhIMnlLemFocGc9PSIsInZhbHVlIjoiaUdkeTRDK3FvZUFPMlZ2NVNzQ0lweDNcL3RWMFBNRGY3bW5cL2RhRUhzb0RnPSIsIm1hYyI6IjQ0OWNiNTY0ZjU3Y2U5MzNkMmIwYmJlMmMzNDZmYTNmMWEzODFhNzU4YTQ5NjBmOWYyMmE1MDVmZGJjMWYyY2MifQ==', 'eyJpdiI6ImIxcEZhYk5MSTRvQlhlTmp4Y043XC9BPT0iLCJ2YWx1ZSI6InhVM3IwNTVZa0o1QjRcL3E2dkFwZzEyU0p2Yk9SQVJKeWh0Y3BhRERXTDh3PSIsIm1hYyI6ImY1ZjE3YmQzMmYyYzI2MGNlMmZhN2M5MWFmOGFmOTM4MzNhMWFhZGM3ODYxNTkyOTAzM2ZlMTQxZWM0OGJkODAifQ==', 'eyJpdiI6IkVHc2NEZ1RyMGRNT1NIazFOQ2RZSWc9PSIsInZhbHVlIjoiUmUyRG1pZEROV0RLbFA5ekgrcklVZz09IiwibWFjIjoiMjk4MDcyNzYwN2NkZTUzYTkzNGU1MGQzOWEwNDNjZmMwYzEyYmQ5NTQxOTczZWNiMjJlNmM4NDE1ZTY0Y2MxNCJ9', 'eyJpdiI6IlBUOUEzdnJKVUMxNW5paEFGRzZsSGc9PSIsInZhbHVlIjoieHUxNHdsQTIxK0J6dVA2R1NuMEZqQT09IiwibWFjIjoiYzQ1NDU3NGY4YzBmNjdjODVkZTcwOGE0MmRlZmVhYzM0NDg3NzM4MjQ4MDMyZjM5Mjc3MTMxMDQyNDgwZjlkMCJ9', 'amex', 1116.00, 0.00, 0, 35.00, 1151.00, 'test', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 03:03:06', '2018-07-27 04:24:57', NULL),
(5, 2, 0, 14, '7C5P8DYUAU76J', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'with fee', 6, 7, 'US', 'asdf', NULL, 'dfadf', 'Alaska', NULL, 1, NULL, 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 324.00, 0.00, 0, 35.00, 359.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 04:26:08', '2018-07-27 04:26:15', NULL),
(6, 1, 0, 14, 'KP0UFHAVO6507', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 972.00, 0.00, 0, 0.00, 972.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 04:26:47', '2018-07-27 04:26:47', NULL),
(7, 1, 0, 14, 'AARF9BRY28S5Z', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 972.00, 0.00, 0, 0.00, 972.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 04:30:28', '2018-07-27 04:30:28', NULL),
(8, 1, 0, 14, 'VFUH3BZ5SHML8', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 972.00, 0.00, 0, 0.00, 972.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 09:12:18', '2018-07-27 09:12:18', NULL),
(9, 1, 0, 14, '22FI6TEXIW0Q4', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 2592.00, 0.00, 0, 0.00, 2592.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 10:15:35', '2018-07-27 10:15:35', NULL),
(10, 1, 0, 14, 'U9SYGO3JVU0AK', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 2592.00, 0.00, 0, 0.00, 2592.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 10:16:07', '2018-07-27 10:16:07', NULL),
(11, 1, 0, 14, 'CO3K32MU4GMZ9', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 2592.00, 0.00, 0, 0.00, 2592.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 10:20:23', '2018-07-27 10:20:23', NULL),
(12, 1, 0, 14, 'O5S3B18WTKT80', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 2592.00, 0.00, 0, 0.00, 2592.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 11:21:53', '2018-07-27 11:21:53', NULL),
(13, 1, 0, 14, 'AN705SSIOOKEM', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 2592.00, 0.00, 0, 0.00, 2592.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 11:52:06', '2018-07-27 11:52:06', NULL),
(14, 1, 0, 14, 'PS392495EGI88', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 2592.00, 0.00, 0, 0.00, 2592.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 12:28:18', '2018-07-27 12:28:18', NULL),
(15, 1, 0, 14, 'DIMN55KNMBJAE', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 2592.00, 0.00, 0, 0.00, 2592.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 12:33:14', '2018-07-27 12:33:14', NULL),
(16, 1, 0, 14, 'X7RL48SRT0W9G', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 2592.00, 0.00, 0, 0.00, 2592.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 13:15:28', '2018-07-27 13:15:28', NULL),
(17, 1, 0, 14, 'JR6AM557WG9GE', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 3000.00, 0.00, 0, 0.00, 3000.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 13:40:16', '2018-07-27 13:40:16', NULL),
(18, 2, 0, 14, 'Y380HV5EZW0LU', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'with fee', 6, 7, 'US', 'asdf', '2', 'dfadf', 'Alaska', NULL, 1, NULL, 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 3000.00, 0.00, 0, 35.00, 3035.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-28 05:39:16', '2018-07-28 05:39:22', NULL),
(19, 2, 0, 14, 'WVJX3IJUWU8GF', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS 23', 6, 2, 'US', 'asdf', '2', 'dfadf', 'Alaska', NULL, 1, NULL, 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 972.00, 0.00, 0, 0.00, 972.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-31 05:24:41', '2018-07-31 05:24:47', NULL),
(20, 2, 0, 14, 'QEIHCVOMT4104', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS 3', 6, 3, 'US', 'asdf', '2', 'dfadf', 'Alaska', NULL, 1, NULL, 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', 'eyJpdiI6IkJJeEcrT3pzSXk2MGEwbEkzNXNuZmc9PSIsInZhbHVlIjoicjdoa3dzaHVBeHIyenJDN2xVRVwvOWZlUDVVcGgrV0d1U1ZoRFEwVVRBOXM9IiwibWFjIjoiM2ZmM2VjY2E0NmE2N2I0YWIyYjc1ZDNjZjVhZTJmODk3NDQ0YzEzNTBlNmVlMGI3ZjI5MGM1NmYzM2MyZTQ2MyJ9', 'eyJpdiI6InordTh3SWRoc1BkS2dnTVdYXC85aDdBPT0iLCJ2YWx1ZSI6ImVuWVRTWUs3TmF6T0o3cWVvcGZuMlE9PSIsIm1hYyI6IjJlNDUxZTQ3MGRjZWUyZjJiODU0YzRhYzVhN2YxOGNjZmYxOWFiNmQyOWRkNmFiMDEzMzg0NmYyZTY4ZTQ0ZDUifQ==', 'eyJpdiI6ImRwMk4rOW1mS014Y0VwOUxwRmVxMUE9PSIsInZhbHVlIjoidE5VMVZSUmZPbllmOGlOUjFKbU4wZz09IiwibWFjIjoiMTJkNTdlMDVlNWY3OGNiMzgzYTc5YjFhYTk2NGM4NTk3YzRjMWNhMzg4ODY2M2I0Yjc1MWI5MWFkOTIwMTExYiJ9', 'eyJpdiI6IlRhdEVTemJocTZkbWN6VDVqM25FalE9PSIsInZhbHVlIjoiZW1PcE1aRjBKOE9NN2xmXC9WckxSMkE9PSIsIm1hYyI6Ijk0MGQ4OTUyZDE5N2Y4Y2QzMjJmYzAzN2YyYWRhMzdhMmJjNDFiYmU0N2YzNTkyYjk5ZWExZDY0NzgzNGVmNDEifQ==', 'amex', 270.00, 0.00, 0, 0.00, 270.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-31 05:24:58', '2018-07-31 05:25:12', NULL),
(21, 2, 0, 14, '59EEJUCHSWKB4', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'Pore', 6, 8, 'US', 'asdf', '2', 'dfadf', 'Alaska', NULL, 1, NULL, 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 972.00, 0.00, 0, 0.00, 972.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-01 12:18:56', '2018-08-01 12:23:22', NULL),
(22, 1, 0, 14, 'GE44F8LLFWCRD', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 540.00, 0.00, 0, 0.00, 540.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-13 11:08:57', '2018-08-13 11:08:57', NULL),
(23, 1, 0, 14, 'ATLAC8C7VHNI4', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 540.00, 0.00, 0, 0.00, 540.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-17 05:22:03', '2018-08-17 05:22:03', NULL),
(24, 1, 0, 14, 'JQSSH7RLLNHEG', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'California', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 540.00, 0.00, 0, 0.00, 540.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-17 05:26:44', '2018-08-17 05:26:44', NULL),
(25, 6, 1, 14, 'AAGA0AOKDBE3P', 'aer', NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 2056.00, 0.00, 0, 0.00, 2056.00, NULL, 0, NULL, NULL, NULL, NULL, '2018-09-24 09:45:13', NULL, NULL, '2018-09-25 04:26:32', NULL, NULL, 1, '2018-09-17 08:01:20', '2018-09-25 05:08:18', NULL),
(26, 1, 0, 14, 'RGI4VPMGYKU2E', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 216.00, 0.00, 0, 0.00, 216.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-24 02:12:14', '2018-09-24 02:12:14', NULL),
(27, 1, 0, 14, 'TN9F5FWYJOXOZ', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 216.00, 0.00, 0, 0.00, 216.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-24 05:04:27', '2018-09-24 05:04:27', NULL),
(28, 1, 0, 14, 'UI5PP0VI4P9ZF', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 216.00, 0.00, 0, 0.00, 216.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-24 05:30:16', '2018-09-24 05:30:16', NULL),
(29, 1, 0, 14, 'LB2QC9GG3ZMPI', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 216.00, 0.00, 0, 0.00, 216.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-24 05:59:37', '2018-09-24 05:59:37', NULL),
(30, 1, 0, 14, 'IW7K30YQWRQS9', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 216.00, 0.00, 0, 0.00, 216.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-24 08:48:32', '2018-09-24 08:48:32', NULL),
(31, 1, 0, 14, 'WXSKZAZOQI9ZC', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 864.00, 0.00, 0, 0.00, 864.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-25 06:00:51', '2018-09-25 06:00:51', NULL),
(32, 2, 0, 14, 'C1CM9B506UFZR', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', 'eyJpdiI6IjBydHdqOVdTZDhEMjJkdE1FVStjUFE9PSIsInZhbHVlIjoiUUJlZkY3OVhJQVBFS1FRRFUySURpdW83bHZHK2ErM0locVk2U1FHT2RPST0iLCJtYWMiOiJkNTgyYWY2Yzg3NTNmYzljNzg4NTYzZDdmZTY0MjJmOTUxZGNhNGU1NDM0NjllYjhhMThkNGQ5ZWVlNTJiODhjIn0=', 'eyJpdiI6IkpsOXlYWDI1QmpkSEpOQ3ZlUlh5SkE9PSIsInZhbHVlIjoiazh5SjFXTnprN2JFNTFUa1hjTUszUT09IiwibWFjIjoiM2EzZDBiZjU4ZWE5YzdmYTQ1MjA4MjIxYWM4M2U1MTMwNjEzMDZhNTc2ODU4NGE3ZDU4ZTc0NDcyMTEwNjdiOSJ9', 'eyJpdiI6IlkyRlpCeURBSUIzSG1VY3Z5V1ByWGc9PSIsInZhbHVlIjoiR25jMnlWeVFCTGdQUUh0Y3Z5eXFQZz09IiwibWFjIjoiYmNkZDFmZTY5Mjk1MmRmNGY0ZWI4MTFjNDk3YzFiZGUyYjA3OWFlMWM3YzBhNDg3MGM3Y2RjNzYxNWYwYzBmYSJ9', 'eyJpdiI6IlRqWERGMDAyellSQXY1N2JrNit1SFE9PSIsInZhbHVlIjoiU2dra1BJVzJsVU1GNEh3ZndmTEpVUT09IiwibWFjIjoiMzhiMWE1NjgxNGUzMThkOGIxM2IzN2M4YjczZmIyZDhiMTUxYjViMDcwZWMwY2I5OTIzNjY2M2Q1MzgwNTViYiJ9', 'amex', 72.00, 0.00, 0, 0.00, 72.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-25 07:32:15', '2018-09-25 08:33:47', NULL),
(33, 2, 0, 14, '4OGTCZEDNJ5XK', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', 'eyJpdiI6IjhmYjhuWG9iSDZXbWFmVFFmSE40MlE9PSIsInZhbHVlIjoiN2NxRlNwaWJkd0Flb0p5M0xsdGlISnJvTFQ2Q3FMMjhja0V6b2xyTDZTMD0iLCJtYWMiOiJmYTNiM2FhOTM5NTllYWVlMDg2OGJhMjY2YjBiMzY5NTJkNmU2MTQzMjMxYmJiMTYyZWMyY2E4YjQ5OTQ4ZTVmIn0=', 'eyJpdiI6IkhibEhJMDl2U2tEY0xWVmluV3d4eEE9PSIsInZhbHVlIjoiQVdvNjRUOFBDeUUyMTFIR0p4U0h3dz09IiwibWFjIjoiMzgwODhmZDgxMGM5NmU5MWJlNmJjYjI0M2VhYTJjNzIwZjQyYjRkMWVjYWNjZjE5MzI5NTJkYWQ2NTJkMTQzZSJ9', 'eyJpdiI6Im01UFQzUCtOMUVjS3FMRzZNWUtneGc9PSIsInZhbHVlIjoiWkgyWnJiMnVORDRRRHNVZjFUMEs2Zz09IiwibWFjIjoiMzk1ZjYzNDI4MTgwOTg5OWU2ZGUxYWNhYzhmZmIzODkzOTdmYmRmY2JmOGMyNzdkOGVmZTRiYjIyZTM5MWIyMSJ9', 'eyJpdiI6Ik9HQU42TE1uZ1pETUR4NDhUc0ZmM3c9PSIsInZhbHVlIjoid25xMExUUVByc2VUenVNTEQrOFhMZz09IiwibWFjIjoiZjdlYTBkYjM2ZWVmN2EwYTE0NTZkMjFhNjg5NTQyZTE1MGZhNjk0Mzg2YWYxYWRmZDViYzkwMzFlMTA1ODk2ZCJ9', 'amex', 216.00, 0.00, 0, 0.00, 216.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-25 07:41:39', '2018-09-25 07:41:57', NULL),
(34, 2, 0, 14, 'G9QUFXMHTMLG0', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', 'eyJpdiI6IlNDWjZZa1wvb1BjREU3ZHZCYmRxOEJ3PT0iLCJ2YWx1ZSI6IlwvamNSXC9TVHlQRHIyYmFZMkdXdDBXd0RZXC9pc2pWXC9uWkdvRkJWc054dkJ3PSIsIm1hYyI6IjlmMzJiNGNmOTZhYzYyYTM1NWM5ZjZjNGU2NTE2N2Q3MjQzZDNhMWVkMzhkMGNmZmI5ZTQ1MmI4ZTA2MTRiNDYifQ==', 'eyJpdiI6IjRFSmcwckVwZFM3eFJQOHk3QmhhTXc9PSIsInZhbHVlIjoiSk5naEZiMW9XWWc5b0E0TVg1V3ZtUT09IiwibWFjIjoiZDNhZjc1YzdmODgzMmI1ZTMyOTEzMjg0MGQzYzc5ZjkyOGZlOWUzZmFlZDg1MTk4NWYyODYyMjM5MTlkNThmYiJ9', 'eyJpdiI6IkVmK0lCd0hGeW94ZXFXenJpN01aU2c9PSIsInZhbHVlIjoiZDIyc2xKMUs0eW01alpBck1FalV3QT09IiwibWFjIjoiMWYyZDk0MjkyNzllM2FhNDgyMjk1ODBhMTE4NjQ5ZDZmN2Q1OWNmMGIxMGY0ZmRkZGFlNjhjNDNlNjJhNWQwYiJ9', 'eyJpdiI6IlwvbTFYU3lMN2VPYkR5Z2tZdXFDOEdnPT0iLCJ2YWx1ZSI6IjJKYlZ0dEw1aW91dlpPY0laOURWZlE9PSIsIm1hYyI6IjFkYzM3NmQ3MWYyODdlZjJkOTgxY2ZhZTM1ZWUwODNjZmNkM2QzYzA1NDJjOTg0MTg0N2I3Y2Y2YTkyNDEyNTcifQ==', 'amex', 288.00, 0.00, 0, 0.00, 288.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-25 08:02:10', NULL, NULL, 1, '2018-09-25 07:42:40', '2018-09-25 08:02:48', NULL),
(35, 6, 2, 14, 'C1CM9B506UFZR-BO1', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, NULL, 'asdf', NULL, 'dfadf', 'AK', NULL, NULL, '3er', 'United States', NULL, '3434', NULL, 'Raj', NULL, 'gsdfgfga', 'CA', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6IjBydHdqOVdTZDhEMjJkdE1FVStjUFE9PSIsInZhbHVlIjoiUUJlZkY3OVhJQVBFS1FRRFUySURpdW83bHZHK2ErM0locVk2U1FHT2RPST0iLCJtYWMiOiJkNTgyYWY2Yzg3NTNmYzljNzg4NTYzZDdmZTY0MjJmOTUxZGNhNGU1NDM0NjllYjhhMThkNGQ5ZWVlNTJiODhjIn0=', 'eyJpdiI6IkpsOXlYWDI1QmpkSEpOQ3ZlUlh5SkE9PSIsInZhbHVlIjoiazh5SjFXTnprN2JFNTFUa1hjTUszUT09IiwibWFjIjoiM2EzZDBiZjU4ZWE5YzdmYTQ1MjA4MjIxYWM4M2U1MTMwNjEzMDZhNTc2ODU4NGE3ZDU4ZTc0NDcyMTEwNjdiOSJ9', 'eyJpdiI6IlkyRlpCeURBSUIzSG1VY3Z5V1ByWGc9PSIsInZhbHVlIjoiR25jMnlWeVFCTGdQUUh0Y3Z5eXFQZz09IiwibWFjIjoiYmNkZDFmZTY5Mjk1MmRmNGY0ZWI4MTFjNDk3YzFiZGUyYjA3OWFlMWM3YzBhNDg3MGM3Y2RjNzYxNWYwYzBmYSJ9', 'eyJpdiI6IlRqWERGMDAyellSQXY1N2JrNit1SFE9PSIsInZhbHVlIjoiU2dra1BJVzJsVU1GNEh3ZndmTEpVUT09IiwibWFjIjoiMzhiMWE1NjgxNGUzMThkOGIxM2IzN2M4YjczZmIyZDhiMTUxYjViMDcwZWMwY2I5OTIzNjY2M2Q1MzgwNTViYiJ9', 'amex', 648.00, 0.00, 0, 0.00, 648.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-25 08:05:47', '2018-09-25 08:20:49', NULL),
(36, 6, 0, 14, 'C1CM9B506UFZR-BO2', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, NULL, 'asdf', NULL, 'dfadf', 'AK', NULL, NULL, '3er', 'United States', NULL, '3434', NULL, 'Raj', NULL, 'gsdfgfga', 'CA', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6IjBydHdqOVdTZDhEMjJkdE1FVStjUFE9PSIsInZhbHVlIjoiUUJlZkY3OVhJQVBFS1FRRFUySURpdW83bHZHK2ErM0locVk2U1FHT2RPST0iLCJtYWMiOiJkNTgyYWY2Yzg3NTNmYzljNzg4NTYzZDdmZTY0MjJmOTUxZGNhNGU1NDM0NjllYjhhMThkNGQ5ZWVlNTJiODhjIn0=', 'eyJpdiI6IkpsOXlYWDI1QmpkSEpOQ3ZlUlh5SkE9PSIsInZhbHVlIjoiazh5SjFXTnprN2JFNTFUa1hjTUszUT09IiwibWFjIjoiM2EzZDBiZjU4ZWE5YzdmYTQ1MjA4MjIxYWM4M2U1MTMwNjEzMDZhNTc2ODU4NGE3ZDU4ZTc0NDcyMTEwNjdiOSJ9', 'eyJpdiI6IlkyRlpCeURBSUIzSG1VY3Z5V1ByWGc9PSIsInZhbHVlIjoiR25jMnlWeVFCTGdQUUh0Y3Z5eXFQZz09IiwibWFjIjoiYmNkZDFmZTY5Mjk1MmRmNGY0ZWI4MTFjNDk3YzFiZGUyYjA3OWFlMWM3YzBhNDg3MGM3Y2RjNzYxNWYwYzBmYSJ9', 'eyJpdiI6IlRqWERGMDAyellSQXY1N2JrNit1SFE9PSIsInZhbHVlIjoiU2dra1BJVzJsVU1GNEh3ZndmTEpVUT09IiwibWFjIjoiMzhiMWE1NjgxNGUzMThkOGIxM2IzN2M4YjczZmIyZDhiMTUxYjViMDcwZWMwY2I5OTIzNjY2M2Q1MzgwNTViYiJ9', 'amex', 144.00, 0.00, 0, 0.00, 144.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-25 08:33:47', '2018-09-25 08:33:47', NULL),
(37, 2, 0, 14, 'IL1ZENO9NI6O8', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', 'eyJpdiI6Im5IMlBaUXlZXC9WQWwrS2w2RlBiclFBPT0iLCJ2YWx1ZSI6IjZBRlA0OVBrbXM1ZkZUTkRpR2I3cVZwTU5aRnNwcmdmUklXVVZrbVZjZmc9IiwibWFjIjoiYzQ1MGJjMjg0OWU1OTA5MTVmNWJkZmRhYzFmMDcxZTQ1ZDU1MWQ5ZDk3Mjk0OTkyYzQ1M2M0ZDZlNTEzNTAyMSJ9', 'eyJpdiI6ImhKVGwxOWFvMTZSbVlza2o1YmIzMkE9PSIsInZhbHVlIjoiYXJaZUNKV0c0MSs1UzlKU1FQRXJTUT09IiwibWFjIjoiZDAxNTViYmE2YzY4Mjg5Y2I1NDY0ZjUwNjRmZjdjNGMzNjhiNDQwZTAwNzJlNmM3YWFkN2JkYzI1M2FkMTFhYyJ9', 'eyJpdiI6IkVNenNhM2hVSlNTSVNrVHMzaWNKVnc9PSIsInZhbHVlIjoiaTREYmRwYXdyWGdzS1B6Y2VZbkxKQT09IiwibWFjIjoiMDI5NWQxYmYwMzMxMGE5MDUyMzJjNzc2OTUzMzZhMjQyYjFmMjZkOWI4NzIyNjRiYWZiZTYyYTlkZTU4MjRmNyJ9', 'eyJpdiI6IkNPdTBsN0txV1dGTVwvbGJMOTRkQlBnPT0iLCJ2YWx1ZSI6IldkS2FLU3M0eERDOGtiTTVvNHJPRnc9PSIsIm1hYyI6IjVkNzc2ZDU5N2I0ZDBlMzMyZmNjZjllM2U1NWRkYmI3MDg1NDc5NDM2ZmQ1MmMxZTIxZGI2Mzg4OGI4YjFkODYifQ==', 'amex', 216.00, 0.00, 0, 0.00, 216.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-26 07:29:51', '2018-09-26 07:33:35', NULL),
(38, 1, 0, 14, NULL, NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 06:45:06', '2018-12-01 06:45:06', NULL),
(39, 1, 0, 14, NULL, NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 06:45:14', '2018-12-01 06:45:14', NULL),
(40, 1, 0, 14, NULL, NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 06:46:31', '2018-12-01 06:46:31', NULL),
(41, 1, 0, 14, NULL, NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 06:46:31', '2018-12-01 06:46:31', NULL),
(42, 1, 0, 14, NULL, NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 06:46:32', '2018-12-01 06:46:32', NULL),
(43, 1, 0, 14, NULL, NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 06:47:16', '2018-12-01 06:47:16', NULL),
(44, 1, 0, 14, 'IVY2Z1E9XK2PM', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, 84.00, 0.00, 0, 0.00, 84.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 06:51:16', '2018-12-01 06:51:16', NULL),
(45, 2, 0, 14, 'FM10001', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 216.00, 0.00, 0, 0.00, 216.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 07:00:33', '2018-12-01 07:35:42', NULL),
(46, 2, 0, 14, 'FM10002', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 216.00, 0.00, 0, 0.00, 216.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 07:38:48', '2018-12-01 07:38:56', NULL),
(47, 2, 0, 14, 'FM10003', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 54.00, 0.00, 0, 0.00, 54.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01 07:49:59', '2018-12-01 07:50:06', NULL),
(48, 2, 0, 14, 'FM10004', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 99.00, 0.00, 0, 0.00, 99.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-03 06:48:42', '2018-12-03 06:48:48', NULL),
(49, 2, 0, 14, 'CQ10001', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 845.00, 0.00, 0, 0.00, 845.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-03 07:03:02', '2018-12-03 07:03:15', NULL),
(50, 3, 0, 14, 'CQ10002', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 299.00, 0.00, 0, 0.00, 299.00, NULL, 0, NULL, NULL, NULL, NULL, '2018-12-17 22:08:30', NULL, NULL, NULL, NULL, NULL, 1, '2018-12-03 07:06:36', '2018-12-17 22:08:30', NULL),
(51, 3, 0, 14, 'CQ10003', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 1014.00, 0.00, 0, 0.00, 1014.00, NULL, 0, NULL, NULL, NULL, NULL, '2018-12-17 22:07:58', NULL, NULL, NULL, NULL, NULL, 1, '2018-12-03 07:56:51', '2018-12-17 22:07:58', NULL),
(52, 2, 0, 14, 'CQ10004', '344', 'dsf', 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 6, 1, 'US', 'asdf', '2', 'dfadf', 'AK', NULL, 1, '3er', 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'CA', NULL, 6, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 'Wire Transfer', 899.00, 0.00, 0, 0.00, 899.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-12-18 00:05:48', '2019-01-02 03:15:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `style_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_per_pack` int(11) NOT NULL,
  `pack` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `total_qty` int(11) NOT NULL,
  `dispatch` int(11) NOT NULL DEFAULT '0',
  `per_unit_price` double(8,2) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `item_id`, `style_no`, `color`, `size`, `item_per_pack`, `pack`, `qty`, `total_qty`, `dispatch`, `per_unit_price`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-07-26 13:10:50', '2018-07-26 13:10:50'),
(2, 2, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-07-27 02:24:31', '2018-07-27 02:24:31'),
(3, 2, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 02:24:31', '2018-07-27 02:24:31'),
(4, 2, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 02:24:31', '2018-07-27 02:24:31'),
(5, 3, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-07-27 02:47:54', '2018-07-27 02:47:54'),
(6, 3, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 02:47:54', '2018-07-27 02:47:54'),
(7, 3, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 02:47:54', '2018-07-27 02:47:54'),
(8, 4, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-07-27 03:03:06', '2018-07-27 03:03:06'),
(9, 4, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 03:03:06', '2018-07-27 03:03:06'),
(10, 4, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 03:03:06', '2018-07-27 03:03:06'),
(11, 5, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 04:26:08', '2018-07-27 04:26:08'),
(12, 6, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 04:26:47', '2018-07-27 04:26:47'),
(13, 6, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 04:26:47', '2018-07-27 04:26:47'),
(14, 7, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 04:30:28', '2018-07-27 04:30:28'),
(15, 7, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 04:30:28', '2018-07-27 04:30:28'),
(16, 8, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 09:12:18', '2018-07-27 09:12:18'),
(17, 8, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 09:12:18', '2018-07-27 09:12:18'),
(18, 9, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 10:15:35', '2018-07-27 10:15:35'),
(19, 9, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 10:15:35', '2018-07-27 10:15:35'),
(20, 9, 6, 'N343', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 10:15:35', '2018-07-27 10:15:35'),
(21, 9, 6, 'N343', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 10:15:35', '2018-07-27 10:15:35'),
(22, 9, 6, 'N343', 'GUNMETAL', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 10:15:35', '2018-07-27 10:15:35'),
(23, 10, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 10:16:07', '2018-07-27 10:16:07'),
(24, 10, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 10:16:07', '2018-07-27 10:16:07'),
(25, 10, 6, 'N343', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 10:16:07', '2018-07-27 10:16:07'),
(26, 10, 6, 'N343', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 10:16:07', '2018-07-27 10:16:07'),
(27, 10, 6, 'N343', 'GUNMETAL', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 10:16:07', '2018-07-27 10:16:07'),
(28, 11, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 10:20:23', '2018-07-27 10:20:23'),
(29, 11, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 10:20:23', '2018-07-27 10:20:23'),
(30, 11, 6, 'N343', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 10:20:23', '2018-07-27 10:20:23'),
(31, 11, 6, 'N343', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 10:20:23', '2018-07-27 10:20:23'),
(32, 11, 6, 'N343', 'GUNMETAL', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 10:20:23', '2018-07-27 10:20:23'),
(33, 12, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 11:21:53', '2018-07-27 11:21:53'),
(34, 12, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 11:21:53', '2018-07-27 11:21:53'),
(35, 12, 6, 'N343', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 11:21:53', '2018-07-27 11:21:53'),
(36, 12, 6, 'N343', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 11:21:53', '2018-07-27 11:21:53'),
(37, 12, 6, 'N343', 'GUNMETAL', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 11:21:53', '2018-07-27 11:21:53'),
(38, 13, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 11:52:06', '2018-07-27 11:52:06'),
(39, 13, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 11:52:06', '2018-07-27 11:52:06'),
(40, 13, 6, 'N343', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 11:52:06', '2018-07-27 11:52:06'),
(41, 13, 6, 'N343', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 11:52:06', '2018-07-27 11:52:06'),
(42, 13, 6, 'N343', 'GUNMETAL', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 11:52:06', '2018-07-27 11:52:06'),
(43, 14, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 12:28:18', '2018-07-27 12:28:18'),
(44, 14, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 12:28:18', '2018-07-27 12:28:18'),
(45, 14, 6, 'N343', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 12:28:18', '2018-07-27 12:28:18'),
(46, 14, 6, 'N343', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 12:28:18', '2018-07-27 12:28:18'),
(47, 14, 6, 'N343', 'GUNMETAL', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 12:28:18', '2018-07-27 12:28:18'),
(48, 15, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 12:33:14', '2018-07-27 12:33:14'),
(49, 15, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 12:33:14', '2018-07-27 12:33:14'),
(50, 15, 6, 'N343', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 12:33:14', '2018-07-27 12:33:14'),
(51, 15, 6, 'N343', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 12:33:14', '2018-07-27 12:33:14'),
(52, 15, 6, 'N343', 'GUNMETAL', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 12:33:14', '2018-07-27 12:33:14'),
(53, 16, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 13:15:28', '2018-07-27 13:15:28'),
(54, 16, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 13:15:28', '2018-07-27 13:15:28'),
(55, 16, 6, 'N343', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-07-27 13:15:28', '2018-07-27 13:15:28'),
(56, 16, 6, 'N343', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 13:15:28', '2018-07-27 13:15:28'),
(57, 16, 6, 'N343', 'GUNMETAL', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 13:15:28', '2018-07-27 13:15:28'),
(58, 17, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 13:40:16', '2018-07-27 13:40:16'),
(59, 17, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-27 13:40:16', '2018-07-27 13:40:16'),
(60, 17, 6, 'N343', 'GREY', 'ONE SIZE', 6, '6', 2, 12, 0, 54.00, 648.00, '2018-07-27 13:40:16', '2018-07-27 13:40:16'),
(61, 17, 6, 'N343', 'GUNMETAL', 'ONE SIZE', 6, '6', 2, 12, 0, 54.00, 648.00, '2018-07-27 13:40:16', '2018-07-27 13:40:16'),
(62, 17, 8, 'N67', 'BLACK', '3-3', 4, '2-2', 3, 12, 0, 34.00, 408.00, '2018-07-27 13:40:16', '2018-07-27 13:40:16'),
(63, 18, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-28 05:39:16', '2018-07-28 05:39:16'),
(64, 18, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-07-28 05:39:16', '2018-07-28 05:39:16'),
(65, 18, 6, 'N343', 'GREY', 'ONE SIZE', 6, '6', 2, 12, 0, 54.00, 648.00, '2018-07-28 05:39:16', '2018-07-28 05:39:16'),
(66, 18, 6, 'N343', 'GUNMETAL', 'ONE SIZE', 6, '6', 2, 12, 0, 54.00, 648.00, '2018-07-28 05:39:16', '2018-07-28 05:39:16'),
(67, 18, 8, 'N67', 'BLACK', '3-3', 4, '2-2', 3, 12, 0, 34.00, 408.00, '2018-07-28 05:39:16', '2018-07-28 05:39:16'),
(68, 19, 6, 'N343', 'AMBER', 'ONE SIZE', 6, '6', 1, 6, 0, 54.00, 324.00, '2018-07-31 05:24:41', '2018-07-31 05:24:41'),
(69, 19, 6, 'N343', 'GREY', 'ONE SIZE', 6, '6', 2, 12, 0, 54.00, 648.00, '2018-07-31 05:24:41', '2018-07-31 05:24:41'),
(70, 20, 9, 'N546', 'GREY', 'S-M-L', 6, '2-2-2', 1, 6, 0, 45.00, 270.00, '2018-07-31 05:24:58', '2018-07-31 05:24:58'),
(71, 21, 6, 'N343', 'AMBER', 'ONE SIZE', 6, '6', 1, 6, 0, 54.00, 324.00, '2018-08-01 12:18:56', '2018-08-01 12:18:56'),
(72, 21, 6, 'N343', 'GREY', 'ONE SIZE', 6, '6', 2, 12, 0, 54.00, 648.00, '2018-08-01 12:18:56', '2018-08-01 12:18:56'),
(73, 22, 9, 'N546', 'GREY', 'S-M-L', 6, '2-2-2', 1, 6, 0, 45.00, 270.00, '2018-08-13 11:08:57', '2018-08-13 11:08:57'),
(74, 22, 9, 'N546', 'SILVER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 45.00, 270.00, '2018-08-13 11:08:57', '2018-08-13 11:08:57'),
(75, 23, 9, 'N546', 'GREY', 'S-M-L', 6, '2-2-2', 1, 6, 0, 45.00, 270.00, '2018-08-17 05:22:03', '2018-08-17 05:22:03'),
(76, 23, 9, 'N546', 'SILVER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 45.00, 270.00, '2018-08-17 05:22:03', '2018-08-17 05:22:03'),
(77, 24, 9, 'N546', 'GREY', 'S-M-L', 6, '2-2-2', 1, 6, 0, 45.00, 270.00, '2018-08-17 05:26:44', '2018-08-17 05:26:44'),
(78, 24, 9, 'N546', 'SILVER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 45.00, 270.00, '2018-08-17 05:26:44', '2018-08-17 05:26:44'),
(79, 25, 9, 'N546', 'GREY', 'S-M-L', 6, '2-2-2', 1, 6, 0, 45.00, 270.00, '2018-09-17 08:01:20', '2018-09-24 09:45:13'),
(80, 25, 9, 'N546', 'SILVER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 45.00, 270.00, '2018-09-17 08:01:20', '2018-09-24 09:45:13'),
(81, 25, 18, 'N343-Clone', 'AMBER', 'S-M-L', 6, '2-2-2', 1, 6, 0, 54.00, 324.00, '2018-09-17 08:01:20', '2018-09-24 09:45:13'),
(82, 25, 18, 'N343-Clone', 'GREY', 'S-M-L', 6, '2-2-2', 2, 12, 0, 54.00, 648.00, '2018-09-17 08:01:20', '2018-09-24 09:45:13'),
(83, 26, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 1, 6, 0, 12.00, 72.00, '2018-09-24 02:12:14', '2018-09-24 02:12:14'),
(84, 26, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-24 02:12:14', '2018-09-24 02:12:14'),
(85, 27, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 1, 6, 0, 12.00, 72.00, '2018-09-24 05:04:27', '2018-09-24 05:04:27'),
(86, 27, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-24 05:04:27', '2018-09-24 05:04:27'),
(87, 28, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 1, 6, 0, 12.00, 72.00, '2018-09-24 05:30:16', '2018-09-24 05:30:16'),
(88, 28, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-24 05:30:16', '2018-09-24 05:30:16'),
(89, 29, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 1, 6, 0, 12.00, 72.00, '2018-09-24 05:59:37', '2018-09-24 05:59:37'),
(90, 29, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-24 05:59:37', '2018-09-24 05:59:37'),
(91, 25, 8, 'N67', 'BLACK', '3-3', 4, '2-2', 4, 16, 0, 34.00, 544.00, '2018-09-24 06:06:46', '2018-09-24 09:45:13'),
(92, 30, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 1, 6, 0, 12.00, 72.00, '2018-09-24 08:48:32', '2018-09-24 08:48:32'),
(93, 30, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-24 08:48:32', '2018-09-24 08:48:32'),
(94, 31, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 1, 6, 0, 12.00, 72.00, '2018-09-25 06:00:51', '2018-09-25 06:00:51'),
(95, 31, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-25 06:00:51', '2018-09-25 06:00:51'),
(96, 31, 6, 'N343', 'AMBER', 'ONE SIZE', 6, '6', 2, 12, 0, 54.00, 648.00, '2018-09-25 06:00:51', '2018-09-25 06:00:51'),
(97, 32, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 1, 6, 0, 12.00, 72.00, '2018-09-25 07:32:15', '2018-09-25 07:32:15'),
(98, 36, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-25 07:32:15', '2018-09-25 08:33:47'),
(99, 35, 6, 'N343', 'AMBER', 'ONE SIZE', 6, '6', 2, 12, 0, 54.00, 648.00, '2018-09-25 07:32:15', '2018-09-25 08:05:47'),
(100, 33, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 1, 6, 0, 12.00, 72.00, '2018-09-25 07:41:40', '2018-09-25 07:41:40'),
(101, 33, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-25 07:41:40', '2018-09-25 07:41:40'),
(102, 34, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-25 07:42:40', '2018-09-25 07:42:40'),
(103, 34, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-25 07:42:40', '2018-09-25 07:42:40'),
(104, 37, 5, 'N01', 'GOLD', 'S-M-L', 6, '2-2-2', 1, 6, 0, 12.00, 72.00, '2018-09-26 07:29:51', '2018-09-26 07:29:51'),
(105, 37, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 2, 12, 0, 12.00, 144.00, '2018-09-26 07:29:51', '2018-09-26 07:29:51'),
(106, 44, 5, 'N01', 'GOLD', '1', 1, '1', 7, 7, 7, 12.00, 84.00, '2018-12-01 06:51:16', '2018-12-01 06:51:16'),
(107, 45, 6, 'N343', 'AMBER', '1', 1, '1', 1, 1, 1, 54.00, 54.00, '2018-12-01 07:00:33', '2018-12-01 07:00:33'),
(108, 45, 6, 'N343', 'GREY', '1', 1, '1', 2, 2, 2, 54.00, 108.00, '2018-12-01 07:00:33', '2018-12-01 07:00:33'),
(109, 45, 6, 'N343', 'GUNMETAL', '1', 1, '1', 1, 1, 1, 54.00, 54.00, '2018-12-01 07:00:33', '2018-12-01 07:00:33'),
(110, 46, 6, 'N343', 'AMBER', '1', 1, '1', 1, 1, 1, 54.00, 54.00, '2018-12-01 07:38:48', '2018-12-01 07:38:48'),
(111, 46, 6, 'N343', 'GREY', '1', 1, '1', 2, 2, 2, 54.00, 108.00, '2018-12-01 07:38:48', '2018-12-01 07:38:48'),
(112, 46, 6, 'N343', 'GUNMETAL', '1', 1, '1', 1, 1, 1, 54.00, 54.00, '2018-12-01 07:38:48', '2018-12-01 07:38:48'),
(113, 47, 6, 'N343', 'AMBER', '1', 1, '1', 1, 1, 1, 54.00, 54.00, '2018-12-01 07:49:59', '2018-12-01 07:49:59'),
(114, 48, 21, 'SOFT FAUX FUR COAT', 'BURGUNDY', '1', 1, '1', 2, 2, 2, 49.99, 99.00, '2018-12-03 06:48:42', '2018-12-03 06:48:42'),
(115, 49, 22, 'FAUX FUR COAT', 'BROWN', '1', 1, '1', 5, 5, 5, 169.00, 845.00, '2018-12-03 07:03:02', '2018-12-03 07:03:02'),
(116, 50, 21, 'SOFT FAUX FUR COAT', 'BURGUNDY', '1', 1, '1', 6, 6, 6, 49.99, 299.00, '2018-12-03 07:06:36', '2018-12-03 07:11:47'),
(117, 51, 22, 'FAUX FUR COAT', 'BROWN', 'S-M-L', 6, '2-2-2', 1, 6, 0, 169.00, 1014.00, '2018-12-03 07:56:51', '2018-12-03 07:56:51'),
(118, 52, 21, 'SOFT FAUX FUR COAT', 'BURGUNDY', 'S-M-L', 6, '2-2-2', 3, 18, 0, 49.99, 899.00, '2018-12-18 00:05:48', '2019-01-02 03:15:25');

-- --------------------------------------------------------

--
-- Table structure for table `packs`
--

DROP TABLE IF EXISTS `packs`;
CREATE TABLE IF NOT EXISTS `packs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `pack1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pack2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packs`
--

INSERT INTO `packs` (`id`, `name`, `status`, `default`, `pack1`, `pack2`, `pack3`, `pack4`, `pack5`, `pack6`, `pack7`, `pack8`, `pack9`, `pack10`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'S-M-L', 1, 0, '2', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-11 05:16:28', '2018-06-29 10:20:48', NULL),
(2, '3-3', 1, 0, '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-11 05:30:54', '2018-07-27 13:31:37', NULL),
(3, 'ONE SIZE', 1, 0, '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-16 11:39:34', '2018-06-29 10:20:48', NULL),
(4, '2/2/2', 1, 0, '2', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:18', '2018-06-29 10:20:48', NULL),
(5, '3/3', 1, 0, '3', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:28', '2018-06-29 10:20:48', NULL),
(6, 'ONE SIZE', 1, 0, '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:38', '2018-06-29 10:20:48', NULL),
(7, 'dsfd', 1, 1, '1', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-26 08:59:37', '2018-06-29 10:20:48', NULL),
(8, '123', 1, 0, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-26 10:34:42', '2018-05-26 10:42:56', '2018-05-26 10:42:56'),
(9, 'a-u', 0, 0, '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-29 10:19:56', '2018-06-29 10:20:52', '2018-06-29 10:20:52'),
(10, '2-3-e-5', 1, 0, '34', '54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-30 09:20:07', '2018-06-30 09:27:20', NULL),
(11, '1X-2X-3X', 1, 0, '3', '4', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-09 05:22:42', '2018-08-09 05:22:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `content` varchar(10000) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, '<p>Details abouts us</p>', '2018-07-26 08:37:48', '2018-12-03 09:13:16'),
(2, 4, NULL, '2018-08-13 10:53:55', '2018-08-13 10:53:55'),
(3, 12, '<p>asdfa</p>', '2018-09-26 05:34:58', '2018-09-26 05:35:03'),
(4, 2, '<div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n                <div class=\"maps margin-bottom-1x\"><iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3306.34845552062!2d-118.2522470847853!3d34.03493161239942!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2c62d763ab777%3A0x43dd7511a9e96140!2s948+Crocker+St+%236%2C+Los+Angeles%2C+CA+90021%2C+USA!5e0!3m2!1sen!2sbd!4v1534517572031\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe></div>\r\n                <h2>Contact Us</h2>\r\n                <b>Phone:</b><br>\r\n                213-935-8771 <br>\r\n                <b>Business Hours:</b><br>\r\n                Mon-Fri 8:00 AM - 6:00 PM PST <br>\r\n                Closed on Sat & Sun <br>\r\n                <b>Web Order Inquiries</b><br>\r\n                support@fameaccessories.com <br>\r\n                <b>Web order return & exchange inquiries:</b><br>\r\n                returns@fameaccessories.com <br>\r\n                <b>All in-store inquiries:</b><br>\r\n                fame@fameaccessories.com <br>\r\n                <b>Buying department:</b><br>\r\n                buying@fameaccessories.com <br>\r\n                <b>Suggestions & Management:</b><br>\r\n                fameaccmgmt@gmail.com <br>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <div class=\"contact-title\">\r\n                    <h2>Questions or Concerns?</h2></div>\r\n                <form action=\"contact_us\" method=\"POST\">\r\n                    <div class=\"form-group{{ $errors->has(\'name\') ? \' has-danger\' : \'\' }}\">\r\n                        <label class=\"col-form-label\">Name *</label>\r\n                        <input class=\"form-control\" type=\"text\" name=\"name\">\r\n                    </div>\r\n\r\n                    <div class=\"form-group{{ $errors->has(\'email\') ? \' has-danger\' : \'\' }}\">\r\n                        <label class=\"col-form-label\">Email Address *</label>\r\n                        <input class=\"form-control\" type=\"text\" name=\"email\">\r\n                    </div>\r\n\r\n                    <div class=\"form-group{{ $errors->has(\'phone\') ? \' has-danger\' : \'\' }}\">\r\n                        <label class=\"col-form-label\">Phone</label>\r\n                        <input class=\"form-control\" type=\"text\" name=\"phone\">\r\n                    </div>\r\n\r\n                    <div class=\"form-group{{ $errors->has(\'comment\') ? \' has-danger\' : \'\' }}\">\r\n                        <label class=\"col-form-label\">Comment *</label>\r\n                        <textarea class=\"form-control\" name=\"comment\"></textarea>\r\n                    </div>\r\n\r\n                    <div class=\"form-group\">\r\n                        <div class=\"text-right\">\r\n                            <input type=\"submit\" class=\"btn btn-primary\" value=\"SEND\">\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n        </div>', '2018-10-03 02:10:58', '2018-12-03 09:11:44'),
(5, 3, NULL, '2018-10-09 09:09:19', '2018-10-09 09:09:19');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patterns`
--

DROP TABLE IF EXISTS `patterns`;
CREATE TABLE IF NOT EXISTS `patterns` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patterns`
--

INSERT INTO `patterns` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 1, '2018-05-17 06:44:46', '2018-05-17 06:44:52', '2018-05-17 06:44:52'),
(2, 'Burnout', 1, '2018-05-17 06:44:57', '2018-05-22 14:19:31', NULL),
(3, 'Check', 1, '2018-05-22 14:19:38', '2018-05-22 14:19:38', NULL),
(4, 'Color block', 1, '2018-05-22 14:19:46', '2018-05-22 14:19:46', NULL),
(5, 'Crochet', 1, '2018-05-22 14:19:55', '2018-05-22 14:19:55', NULL),
(6, 'Digital Print', 1, '2018-05-22 14:20:02', '2018-05-22 14:20:02', NULL),
(7, 'Embroidered', 1, '2018-05-22 14:20:11', '2018-05-22 14:20:11', NULL),
(8, 'Glitter/Sequins', 1, '2018-05-22 14:20:17', '2018-05-22 14:20:17', NULL),
(9, 'Lace', 1, '2018-05-22 14:20:27', '2018-05-22 14:20:27', NULL),
(10, 'Lettering', 1, '2018-05-22 14:20:33', '2018-05-22 14:20:33', NULL),
(11, 'Metallic', 1, '2018-05-22 14:20:41', '2018-05-22 14:20:41', NULL),
(12, 'Multi-Color', 1, '2018-05-22 14:20:47', '2018-05-22 14:20:47', NULL),
(13, 'Multicolor', 1, '2018-05-22 14:20:54', '2018-05-22 14:20:54', NULL),
(14, 'Ombre', 1, '2018-05-22 14:21:02', '2018-05-22 14:21:02', NULL),
(15, 'Plaid', 1, '2018-05-22 14:21:10', '2018-05-22 14:21:10', NULL),
(16, 'Polka Dot', 1, '2018-05-22 14:21:17', '2018-05-22 14:21:17', NULL),
(17, 'Print Screen', 1, '2018-05-22 14:21:24', '2018-05-22 14:21:24', NULL),
(18, 'Print Sublimation', 1, '2018-05-22 14:21:31', '2018-05-22 14:21:31', NULL),
(19, 'Print-Animal-Floral-Skull-Butterfly', 1, '2018-05-22 14:21:37', '2018-05-22 14:21:37', NULL),
(20, 'Rhinestones', 1, '2018-05-22 14:21:44', '2018-05-22 14:21:44', NULL),
(21, 'Ribbon/Bows', 1, '2018-05-22 14:21:51', '2018-05-22 14:21:51', NULL),
(22, 'Solid', 1, '2018-05-22 14:21:59', '2018-05-22 14:21:59', NULL),
(23, 'Striped', 1, '2018-05-22 14:22:07', '2018-05-22 14:22:07', NULL),
(24, 'Striped Sublimation', 1, '2018-05-22 14:22:15', '2018-05-22 14:22:15', NULL),
(27, 'tt', 5, '2018-06-22 04:18:28', '2018-06-22 04:18:41', '2018-06-22 04:18:41');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `star` int(11) NOT NULL DEFAULT '0',
  `review` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `order_id`, `user_id`, `star`, `review`, `reply`, `created_at`, `updated_at`) VALUES
(8, 6, 14, 0, 'werwr', 'd', '2018-07-17 11:09:16', '2018-07-17 11:09:24');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(5, 'welcome_notification', '<p>Welcome to CQBYCQ</p>', '2018-07-16 11:19:12', '2018-12-03 08:59:46'),
(4, 'buyer_home', '<p>WELCOME TO CQBYCQ</p>\r\n\r\n<p>&nbsp;</p>', '2018-07-16 11:19:04', '2018-12-03 09:00:37'),
(6, 'logo-white', 'images/logo/429a09c0-f7d8-11e8-a86a-5527d6cc1569.svg', NULL, NULL),
(7, 'logo-black', 'images/logo/447ac240-f7d8-11e8-a65d-f5b5457304ec.svg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_methods`
--

DROP TABLE IF EXISTS `shipping_methods`;
CREATE TABLE IF NOT EXISTS `shipping_methods` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `ship_method_id` int(11) NOT NULL,
  `list_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_methods`
--

INSERT INTO `shipping_methods` (`id`, `status`, `default`, `courier_id`, `ship_method_id`, `list_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 1, 1, '2018-06-01 04:41:24', '2018-06-25 10:20:16', NULL),
(2, 1, 0, 1, 3, 2, '2018-06-01 04:41:42', '2018-06-25 10:20:16', NULL),
(3, 1, 0, 3, 5, 1, '2018-06-04 09:22:27', '2018-07-02 09:20:12', '2018-07-02 09:20:12'),
(4, 1, 0, 1, 1, 3, '2018-07-02 11:08:02', '2018-07-02 11:08:12', '2018-07-02 11:08:12');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

DROP TABLE IF EXISTS `sizes`;
CREATE TABLE IF NOT EXISTS `sizes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `size1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `status`, `default`, `user_id`, `size1`, `size2`, `size3`, `size4`, `size5`, `size6`, `size7`, `size8`, `size9`, `size10`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test Size', 1, 0, 1, 'S', 'M', 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-09 06:42:40', '2018-05-11 08:16:24', NULL),
(2, 'Test Size 2', 0, 1, 1, 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-10 03:25:13', '2018-05-11 08:16:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider_items`
--

DROP TABLE IF EXISTS `slider_items`;
CREATE TABLE IF NOT EXISTS `slider_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_items`
--

INSERT INTO `slider_items` (`id`, `item_id`, `sort`, `type`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, '2018-07-02 07:34:35', '2018-07-02 07:34:40'),
(2, 4, 2, 1, '2018-07-02 07:34:37', '2018-07-02 07:34:40'),
(3, 2, 1, 4, '2018-07-02 07:34:46', '2018-07-02 07:34:46');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `country_id`, `name`, `code`) VALUES
(1, 1, 'Alaska', 'AK'),
(2, 1, 'Alabama', 'AL'),
(3, 1, 'American Samoa', 'AS'),
(4, 1, 'Arizona', 'AZ'),
(5, 1, 'Arkansas', 'AR'),
(6, 1, 'California', 'CA'),
(7, 1, 'Colorado', 'CO'),
(8, 1, 'Connecticut', 'CT'),
(9, 1, 'Delaware', 'DE'),
(10, 1, 'District of Columbia', 'DC'),
(11, 1, 'Federated States of Micronesia', 'FM'),
(12, 1, 'Florida', 'FL'),
(13, 1, 'Georgia', 'GA'),
(14, 1, 'Guam', 'GU'),
(15, 1, 'Hawaii', 'HI'),
(16, 1, 'Idaho', 'ID'),
(17, 1, 'Illinois', 'IL'),
(18, 1, 'Indiana', 'IN'),
(19, 1, 'Iowa', 'IA'),
(20, 1, 'Kansas', 'KS'),
(21, 1, 'Kentucky', 'KY'),
(22, 1, 'Louisiana', 'LA'),
(23, 1, 'Maine', 'ME'),
(24, 1, 'Marshall Islands', 'MH'),
(25, 1, 'Maryland', 'MD'),
(26, 1, 'Massachusetts', 'MA'),
(27, 1, 'Michigan', 'MI'),
(28, 1, 'Minnesota', 'MN'),
(29, 1, 'Mississippi', 'MS'),
(30, 1, 'Missouri', 'MO'),
(31, 1, 'Montana', 'MT'),
(32, 1, 'Nebraska', 'NE'),
(33, 1, 'Nevada', 'NV'),
(34, 1, 'New Hampshire', 'NH'),
(35, 1, 'New Jersey', 'NJ'),
(36, 1, 'New Mexico', 'NM'),
(37, 1, 'New York', 'NY'),
(38, 1, 'North Carolina', 'NC'),
(39, 1, 'North Dakota', 'ND'),
(40, 1, 'Northern Mariana Islands', 'MP'),
(41, 1, 'Ohio', 'OH'),
(42, 1, 'Oklahoma', 'OK'),
(43, 1, 'Oregon', 'OR'),
(44, 1, 'Palau', 'PW'),
(45, 1, 'Pennsylvania', 'PA'),
(46, 1, 'Puerto Rico', 'PR'),
(47, 1, 'Rhode Island', 'RI'),
(48, 1, 'South Carolina', 'SC'),
(49, 1, 'South Dakota', 'SD'),
(50, 1, 'Tennessee', 'TN'),
(51, 1, 'Texas', 'TX'),
(52, 1, 'Utah', 'UT'),
(53, 1, 'Vermont', 'VT'),
(54, 1, 'Virgin Islands', 'VI'),
(55, 1, 'Virginia', 'VA'),
(56, 1, 'Washington', 'WA'),
(57, 1, 'West Virginia', 'WV'),
(58, 1, 'Wisconsin', 'WI'),
(59, 1, 'Wyoming', 'WY'),
(60, 1, 'Armed Forces Africa', 'AE'),
(61, 1, 'Armed Forces Americas (except Canada)', 'AA'),
(62, 1, 'Armed Forces Canada', 'AE'),
(63, 1, 'Armed Forces Europe', 'AE'),
(64, 1, 'Armed Forces Middle East', 'AE'),
(65, 1, 'Armed Forces Pacific', 'AP'),
(66, 2, 'Alberta', 'AB'),
(67, 2, 'British Columbia', 'BC'),
(68, 2, 'Manitoba', 'MB'),
(69, 2, 'New Brunswick', 'NB'),
(70, 2, 'Newfoundland and Labrador', 'NL'),
(71, 2, 'Northwest Territories', 'NT'),
(72, 2, 'Nova Scotia', 'NS'),
(73, 2, 'Nunavut', 'NU'),
(74, 2, 'Ontario', 'ON'),
(75, 2, 'Prince Edward Island', 'PE'),
(76, 2, 'Quebec', 'QC'),
(77, 2, 'Saskatchewan', 'SK'),
(78, 2, 'Yukon', 'YT');

-- --------------------------------------------------------

--
-- Table structure for table `store_credits`
--

DROP TABLE IF EXISTS `store_credits`;
CREATE TABLE IF NOT EXISTS `store_credits` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_credits`
--

INSERT INTO `store_credits` (`id`, `user_id`, `amount`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 512.00, NULL, '2019-01-02 03:15:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store_credit_transections`
--

DROP TABLE IF EXISTS `store_credit_transections`;
CREATE TABLE IF NOT EXISTS `store_credit_transections` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_credit_transections`
--

INSERT INTO `store_credit_transections` (`id`, `user_id`, `order_id`, `reason`, `amount`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 20, 'qweqwe', 12.00, '2018-07-31 11:27:04', '2018-07-31 11:27:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
CREATE TABLE IF NOT EXISTS `styles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'A-line', 1, '2018-05-17 07:34:48', '2018-05-22 14:24:20', NULL),
(2, 'asdfa', 1, '2018-05-17 07:34:57', '2018-05-17 07:35:00', '2018-05-17 07:35:00'),
(3, 'Asymmetrical', 1, '2018-05-22 14:24:27', '2018-05-22 14:24:27', NULL),
(4, 'Babydoll', 1, '2018-05-22 14:24:33', '2018-05-22 14:24:33', NULL),
(5, 'Beaded', 1, '2018-05-22 14:24:40', '2018-05-22 14:24:40', NULL),
(6, 'Belted', 1, '2018-05-22 14:24:48', '2018-05-22 14:24:48', NULL),
(7, 'Blouse', 1, '2018-05-22 14:24:56', '2018-05-22 14:24:56', NULL),
(8, 'fd', 3, '2018-06-22 04:36:09', '2018-06-22 04:36:20', '2018-06-22 04:36:20');

-- --------------------------------------------------------

--
-- Table structure for table `top_banners`
--

DROP TABLE IF EXISTS `top_banners`;
CREATE TABLE IF NOT EXISTS `top_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `image_path` varchar(500) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `top_banners`
--

INSERT INTO `top_banners` (`id`, `page`, `category_id`, `url`, `image_path`, `created_at`, `updated_at`) VALUES
(8, 10, NULL, 'ty', '/images/banner/b6f98e60-95ba-11e8-b58d-b7d25d4f29a0.jpg', '2018-08-01 12:43:00', '2018-08-01 12:43:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `buyer_meta_id` int(11) DEFAULT NULL,
  `vendor_meta_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_token` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `order_count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `user_id`, `password`, `role`, `active`, `buyer_meta_id`, `vendor_meta_id`, `remember_token`, `reset_token`, `last_login`, `order_count`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mr.', 'ss', 'vendor@gmail.com', 'vendor', '$2y$10$Uumgr3En9yFT5fEUBw4OFeulreuynvKWNP2SnuYI2ic2vU8WpUFaG', 2, 1, NULL, 1, 'iOzqoxajQzKD6p129wCcnw9JldjHVMfHiALRYUx58msWBvyIwIz0MqFVA0fc', NULL, '2019-01-08 07:29:23', 0, '2018-05-08 09:03:20', '2019-01-08 07:29:23', NULL),
(2, 'Super', 'Admin', 'admin@gmail.com', NULL, '$2y$10$Q1pYXTCr.5Dxasr4byZR5uTxs3x8aFXDpPL7CX/r.6mHC1muwBw9m', 1, 1, NULL, NULL, 'kIjugAmxEaazEdIDfJg2zRKJ5ioAl6fcZMOkxKgc7Ukj9OQZ3faLHqc4AiP0', NULL, NULL, 0, '2018-05-10 05:41:54', '2018-05-10 05:41:54', NULL),
(3, 'dd3ye', 'dder', NULL, '4352', '$2y$10$XDxkIkQ8yMarz5yjGxqIXOWzBx540aGXl10BOsViq9reiQw0Bq7iO', 4, 1, NULL, NULL, NULL, NULL, NULL, 0, '2018-05-15 08:10:43', '2018-06-28 13:01:12', NULL),
(11, 'asdf', 'cvcc', NULL, 'sdf', '$2y$10$K2uvm3GOUItDXNBXoQxYNugJA2XpRtSKilZ.reKSia8BkYAuFNW92', 4, 1, NULL, NULL, NULL, NULL, NULL, 0, '2018-05-15 11:15:15', '2018-06-25 08:30:46', NULL),
(14, 'First', 'Customer', 'shantotrs@gmail.com', NULL, '$2y$10$JXoRQJYy/cufBYi44Avkt.BOBWUixcdqQUa/JkbgGrlsw4wR/eScG', 3, 1, 3, NULL, 'Hy2J5Kf53ytx5WWiU5BWMAXMrZc8Ko7XoprOmNG6V67CxVP0mKvQhsFhzIUk', NULL, '2019-01-05 05:43:57', 7, '2018-05-24 10:22:35', '2019-01-05 05:43:57', NULL),
(20, 'Employee', 'Test', NULL, 'employee', '$2y$10$1oHUJ89WqwtBEeJJ43i.T.spUPim.uQezevTCVsvXheFFyERiKjMi', 2, 1, NULL, 1, NULL, NULL, NULL, 0, '2018-07-02 09:05:10', '2018-07-02 09:05:10', NULL),
(19, 'New', 'buyer', 'newbuyer@gmail.com', NULL, '$2y$10$2YyXpCQy32JDLcWWEzV1NeqZjGGgnwHTwx24teL53ipi1N13uZlvS', 3, 1, 4, NULL, NULL, NULL, NULL, 0, '2018-06-22 07:18:27', '2018-06-22 07:18:27', NULL),
(23, 'sdfa', 'fasdf', 'shanto.razer@gmail.com', NULL, '$2y$10$IqPj2bFGzHdAKLv599k0uexuxvrkEbEGcnhMrxLP7TaY.Jbz74u7e', 3, 1, 9, NULL, NULL, NULL, NULL, 0, '2018-09-25 09:04:16', '2018-09-25 10:31:48', NULL),
(24, 'asdfasdf', 'asdf', 'asdfaasdf@afds.com', NULL, '$2y$10$noTrWedGcTYaDh2NlsBw9e0v0.JtNTwdYdDehP0YIZeiUIFhI2Zu2', 3, 1, 10, NULL, NULL, NULL, NULL, 0, '2019-01-02 04:14:56', '2019-01-02 04:14:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_permission`
--

DROP TABLE IF EXISTS `user_permission`;
CREATE TABLE IF NOT EXISTS `user_permission` (
  `user_id` int(11) NOT NULL,
  `permission` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_permission`
--

INSERT INTO `user_permission` (`user_id`, `permission`) VALUES
(9, 3),
(9, 4),
(10, 2),
(10, 6),
(11, 10),
(11, 4),
(11, 3),
(20, 3),
(20, 4);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_images`
--

DROP TABLE IF EXISTS `vendor_images`;
CREATE TABLE IF NOT EXISTS `vendor_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendor_images`
--

INSERT INTO `vendor_images` (`id`, `type`, `status`, `color`, `image_path`, `sort`, `url`, `created_at`, `updated_at`) VALUES
(23, 7, 1, 'black', '/images/banner/d7fe60c0-f57b-11e8-b2e0-a368279e9743.jpg', 3, 'sdf', '2018-12-01 09:14:48', '2018-12-18 01:29:19'),
(13, 8, 1, NULL, '/images/banner/cd322e20-83a2-11e8-a6e3-bb1ef1eb06af.jpg', 3, '4545', '2018-07-09 12:06:28', '2018-07-30 08:57:27'),
(14, 8, 1, NULL, '/images/banner/d03fbc20-83a2-11e8-a3de-e31afaa0ced3.jpg', 1, NULL, '2018-07-09 12:06:33', '2018-07-09 12:09:36'),
(15, 8, 1, NULL, '/images/banner/d2c10540-83a2-11e8-a969-9d7108a652e3.jpg', 2, NULL, '2018-07-09 12:06:37', '2018-07-09 12:09:36'),
(22, 7, 1, 'white', '/images/banner/daf46740-eda0-11e8-8133-d9d0b60c3280.mp4', 1, 'tyrt', '2018-11-21 09:19:35', '2018-12-18 01:29:19'),
(24, 1, 1, NULL, '/images/banner/5b42d7b0-f57c-11e8-a12b-ff9f73d5eee4.png', NULL, NULL, '2018-12-01 09:18:29', '2018-12-01 09:18:31'),
(25, 7, 1, 'white', '/images/banner/3a5ee960-f7ca-11e8-94c7-3398819bd6f2.jpg', 2, 'wertwert', '2018-12-04 07:40:56', '2018-12-18 01:29:19');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

DROP TABLE IF EXISTS `visitors`;
CREATE TABLE IF NOT EXISTS `visitors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1184 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `user_id`, `url`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 02:37:40', '2018-07-26 02:37:40'),
(2, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 02:39:54', '2018-07-26 02:39:54'),
(3, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 07:58:17', '2018-07-26 07:58:17'),
(4, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:07:40', '2018-07-26 08:07:40'),
(5, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:13:24', '2018-07-26 08:13:24'),
(6, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:13:46', '2018-07-26 08:13:46'),
(7, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:14:16', '2018-07-26 08:14:16'),
(8, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:14:41', '2018-07-26 08:14:41'),
(9, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:15:00', '2018-07-26 08:15:00'),
(10, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:15:51', '2018-07-26 08:15:51'),
(11, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:26:05', '2018-07-26 08:26:05'),
(12, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:27:23', '2018-07-26 08:27:23'),
(13, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:27:43', '2018-07-26 08:27:43'),
(14, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:28:43', '2018-07-26 08:28:43'),
(15, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:29:22', '2018-07-26 08:29:22'),
(16, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:39:15', '2018-07-26 08:39:15'),
(17, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:39:27', '2018-07-26 08:39:27'),
(18, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:39:31', '2018-07-26 08:39:31'),
(19, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 08:44:05', '2018-07-26 08:44:05'),
(20, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 09:07:20', '2018-07-26 09:07:20'),
(21, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 09:15:11', '2018-07-26 09:15:11'),
(22, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:23:21', '2018-07-26 11:23:21'),
(23, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:29:00', '2018-07-26 11:29:00'),
(24, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:34:35', '2018-07-26 11:34:35'),
(25, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:34:37', '2018-07-26 11:34:37'),
(26, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:34:56', '2018-07-26 11:34:56'),
(27, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:35:36', '2018-07-26 11:35:36'),
(28, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:35:37', '2018-07-26 11:35:37'),
(29, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:03', '2018-07-26 11:36:03'),
(30, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:06', '2018-07-26 11:36:06'),
(31, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:07', '2018-07-26 11:36:07'),
(32, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:08', '2018-07-26 11:36:08'),
(33, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:09', '2018-07-26 11:36:09'),
(34, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:15', '2018-07-26 11:36:15'),
(35, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:16', '2018-07-26 11:36:16'),
(36, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:16', '2018-07-26 11:36:16'),
(37, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:36', '2018-07-26 11:36:36'),
(38, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:37', '2018-07-26 11:36:37'),
(39, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:39', '2018-07-26 11:36:39'),
(40, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:36:39', '2018-07-26 11:36:39'),
(41, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:37:50', '2018-07-26 11:37:50'),
(42, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:37:51', '2018-07-26 11:37:51'),
(43, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:37:52', '2018-07-26 11:37:52'),
(44, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:37', '2018-07-26 11:40:37'),
(45, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:38', '2018-07-26 11:40:38'),
(46, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:38', '2018-07-26 11:40:38'),
(47, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:38', '2018-07-26 11:40:38'),
(48, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:39', '2018-07-26 11:40:39'),
(49, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:39', '2018-07-26 11:40:39'),
(50, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:39', '2018-07-26 11:40:39'),
(51, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:40', '2018-07-26 11:40:40'),
(52, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:40:40', '2018-07-26 11:40:40'),
(53, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:44:17', '2018-07-26 11:44:17'),
(54, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:11', '2018-07-26 11:47:11'),
(55, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:13', '2018-07-26 11:47:13'),
(56, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:14', '2018-07-26 11:47:14'),
(57, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:14', '2018-07-26 11:47:14'),
(58, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:26', '2018-07-26 11:47:26'),
(59, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:27', '2018-07-26 11:47:27'),
(60, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:27', '2018-07-26 11:47:27'),
(61, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:47:27', '2018-07-26 11:47:27'),
(62, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:48:44', '2018-07-26 11:48:44'),
(63, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:21', '2018-07-26 11:49:21'),
(64, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:22', '2018-07-26 11:49:22'),
(65, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:22', '2018-07-26 11:49:22'),
(66, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:23', '2018-07-26 11:49:23'),
(67, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:23', '2018-07-26 11:49:23'),
(68, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:49:27', '2018-07-26 11:49:27'),
(69, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:51:37', '2018-07-26 11:51:37'),
(70, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:08', '2018-07-26 11:54:08'),
(71, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:29', '2018-07-26 11:54:29'),
(72, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:30', '2018-07-26 11:54:30'),
(73, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:31', '2018-07-26 11:54:31'),
(74, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:31', '2018-07-26 11:54:31'),
(75, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:32', '2018-07-26 11:54:32'),
(76, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:32', '2018-07-26 11:54:32'),
(77, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:54:49', '2018-07-26 11:54:49'),
(78, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:55:22', '2018-07-26 11:55:22'),
(79, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:56:12', '2018-07-26 11:56:12'),
(80, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:57:01', '2018-07-26 11:57:01'),
(81, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:57:04', '2018-07-26 11:57:04'),
(82, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 11:57:49', '2018-07-26 11:57:49'),
(83, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:01:20', '2018-07-26 12:01:20'),
(84, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:07:39', '2018-07-26 12:07:39'),
(85, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:11:13', '2018-07-26 12:11:13'),
(86, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:11:42', '2018-07-26 12:11:42'),
(87, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:11:53', '2018-07-26 12:11:53'),
(88, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:11:58', '2018-07-26 12:11:58'),
(89, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:12:00', '2018-07-26 12:12:00'),
(90, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:12:03', '2018-07-26 12:12:03'),
(91, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:12:19', '2018-07-26 12:12:19'),
(92, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:12:39', '2018-07-26 12:12:39'),
(93, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:12:51', '2018-07-26 12:12:51'),
(94, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:13:04', '2018-07-26 12:13:04'),
(95, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:13:44', '2018-07-26 12:13:44'),
(96, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:28:08', '2018-07-26 12:28:08'),
(97, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:30:04', '2018-07-26 12:30:04'),
(98, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:37:13', '2018-07-26 12:37:13'),
(99, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:38:15', '2018-07-26 12:38:15'),
(100, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:38:27', '2018-07-26 12:38:27'),
(101, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:43:31', '2018-07-26 12:43:31'),
(102, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 12:57:19', '2018-07-26 12:57:19'),
(103, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 13:07:26', '2018-07-26 13:07:26'),
(104, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 13:13:43', '2018-07-26 13:13:43'),
(105, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 13:14:06', '2018-07-26 13:14:06'),
(106, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 13:22:54', '2018-07-26 13:22:54'),
(107, 14, 'http://localhost/fourgirls/public', '3', '2018-07-26 13:39:44', '2018-07-26 13:39:44'),
(108, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 02:22:15', '2018-07-27 02:22:15'),
(109, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 02:47:25', '2018-07-27 02:47:25'),
(110, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 02:47:45', '2018-07-27 02:47:45'),
(111, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 02:48:40', '2018-07-27 02:48:40'),
(112, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 03:02:59', '2018-07-27 03:02:59'),
(113, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 04:26:35', '2018-07-27 04:26:35'),
(114, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:13:22', '2018-07-27 06:13:22'),
(115, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:19:02', '2018-07-27 06:19:02'),
(116, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:20:33', '2018-07-27 06:20:33'),
(117, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:20:52', '2018-07-27 06:20:52'),
(118, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:20:53', '2018-07-27 06:20:53'),
(119, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:20:54', '2018-07-27 06:20:54'),
(120, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:21:20', '2018-07-27 06:21:20'),
(121, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:22:02', '2018-07-27 06:22:02'),
(122, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:23:14', '2018-07-27 06:23:14'),
(123, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:23:42', '2018-07-27 06:23:42'),
(124, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:25:30', '2018-07-27 06:25:30'),
(125, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:28:09', '2018-07-27 06:28:09'),
(126, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:28:48', '2018-07-27 06:28:48'),
(127, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:30:01', '2018-07-27 06:30:01'),
(128, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:30:18', '2018-07-27 06:30:18'),
(129, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:31:14', '2018-07-27 06:31:14'),
(130, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:31:27', '2018-07-27 06:31:27'),
(131, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:32:29', '2018-07-27 06:32:29'),
(132, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:32:30', '2018-07-27 06:32:30'),
(133, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:32:33', '2018-07-27 06:32:33'),
(134, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:32:38', '2018-07-27 06:32:38'),
(135, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:32:55', '2018-07-27 06:32:55'),
(136, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:33:58', '2018-07-27 06:33:58'),
(137, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:37:58', '2018-07-27 06:37:58'),
(138, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:38:40', '2018-07-27 06:38:40'),
(139, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:39:49', '2018-07-27 06:39:49'),
(140, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:41:06', '2018-07-27 06:41:06'),
(141, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:41:07', '2018-07-27 06:41:07'),
(142, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:41:09', '2018-07-27 06:41:09'),
(143, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:41:41', '2018-07-27 06:41:41'),
(144, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:42:47', '2018-07-27 06:42:47'),
(145, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:43:18', '2018-07-27 06:43:18'),
(146, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:45:14', '2018-07-27 06:45:14'),
(147, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:46:40', '2018-07-27 06:46:40'),
(148, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:47:10', '2018-07-27 06:47:10'),
(149, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:47:20', '2018-07-27 06:47:20'),
(150, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:48:12', '2018-07-27 06:48:12'),
(151, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:48:34', '2018-07-27 06:48:34'),
(152, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:48:57', '2018-07-27 06:48:57'),
(153, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:50:50', '2018-07-27 06:50:50'),
(154, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:52:03', '2018-07-27 06:52:03'),
(155, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:52:11', '2018-07-27 06:52:11'),
(156, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:53:19', '2018-07-27 06:53:19'),
(157, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:53:20', '2018-07-27 06:53:20'),
(158, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:54:23', '2018-07-27 06:54:23'),
(159, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:54:50', '2018-07-27 06:54:50'),
(160, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:55:09', '2018-07-27 06:55:09'),
(161, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:55:25', '2018-07-27 06:55:25'),
(162, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:55:25', '2018-07-27 06:55:25'),
(163, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:55:26', '2018-07-27 06:55:26'),
(164, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:55:53', '2018-07-27 06:55:53'),
(165, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:56:55', '2018-07-27 06:56:55'),
(166, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:57:43', '2018-07-27 06:57:43'),
(167, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:57:53', '2018-07-27 06:57:53'),
(168, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:58:02', '2018-07-27 06:58:02'),
(169, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:58:07', '2018-07-27 06:58:07'),
(170, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:58:11', '2018-07-27 06:58:11'),
(171, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:58:16', '2018-07-27 06:58:16'),
(172, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 06:59:16', '2018-07-27 06:59:16'),
(173, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:24', '2018-07-27 07:02:24'),
(174, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:26', '2018-07-27 07:02:26'),
(175, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:26', '2018-07-27 07:02:26'),
(176, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:27', '2018-07-27 07:02:27'),
(177, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:27', '2018-07-27 07:02:27'),
(178, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:27', '2018-07-27 07:02:27'),
(179, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:28', '2018-07-27 07:02:28'),
(180, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:28', '2018-07-27 07:02:28'),
(181, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:28', '2018-07-27 07:02:28'),
(182, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:29', '2018-07-27 07:02:29'),
(183, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:29', '2018-07-27 07:02:29'),
(184, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:29', '2018-07-27 07:02:29'),
(185, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:52', '2018-07-27 07:02:52'),
(186, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:52', '2018-07-27 07:02:52'),
(187, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:02:53', '2018-07-27 07:02:53'),
(188, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:06:43', '2018-07-27 07:06:43'),
(189, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:06:44', '2018-07-27 07:06:44'),
(190, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:06:45', '2018-07-27 07:06:45'),
(191, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:07:14', '2018-07-27 07:07:14'),
(192, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:07:15', '2018-07-27 07:07:15'),
(193, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:07:15', '2018-07-27 07:07:15'),
(194, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:07:15', '2018-07-27 07:07:15'),
(195, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:07:16', '2018-07-27 07:07:16'),
(196, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:07', '2018-07-27 07:08:07'),
(197, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:08', '2018-07-27 07:08:08'),
(198, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:08', '2018-07-27 07:08:08'),
(199, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:10', '2018-07-27 07:08:10'),
(200, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:10', '2018-07-27 07:08:10'),
(201, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:39', '2018-07-27 07:08:39'),
(202, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:42', '2018-07-27 07:08:42'),
(203, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:42', '2018-07-27 07:08:42'),
(204, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:50', '2018-07-27 07:08:50'),
(205, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:08:51', '2018-07-27 07:08:51'),
(206, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:09:16', '2018-07-27 07:09:16'),
(207, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:09:33', '2018-07-27 07:09:33'),
(208, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:09:45', '2018-07-27 07:09:45'),
(209, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:10:14', '2018-07-27 07:10:14'),
(210, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:12:57', '2018-07-27 07:12:57'),
(211, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:13:46', '2018-07-27 07:13:46'),
(212, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:14:40', '2018-07-27 07:14:40'),
(213, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:14:41', '2018-07-27 07:14:41'),
(214, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:14:42', '2018-07-27 07:14:42'),
(215, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:15:29', '2018-07-27 07:15:29'),
(216, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:15:44', '2018-07-27 07:15:44'),
(217, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:16:07', '2018-07-27 07:16:07'),
(218, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:19:44', '2018-07-27 07:19:44'),
(219, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:20:26', '2018-07-27 07:20:26'),
(220, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:22:02', '2018-07-27 07:22:02'),
(221, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:22:03', '2018-07-27 07:22:03'),
(222, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 07:23:45', '2018-07-27 07:23:45'),
(223, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 08:59:36', '2018-07-27 08:59:36'),
(224, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:00:38', '2018-07-27 09:00:38'),
(225, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:13:24', '2018-07-27 09:13:24'),
(226, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:13:31', '2018-07-27 09:13:31'),
(227, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:16:25', '2018-07-27 09:16:25'),
(228, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:16:26', '2018-07-27 09:16:26'),
(229, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:20:38', '2018-07-27 09:20:38'),
(230, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:24:19', '2018-07-27 09:24:19'),
(231, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:26:42', '2018-07-27 09:26:42'),
(232, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:27:36', '2018-07-27 09:27:36'),
(233, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:41:11', '2018-07-27 09:41:11'),
(234, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:54:22', '2018-07-27 09:54:22'),
(235, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:55:07', '2018-07-27 09:55:07'),
(236, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 09:55:51', '2018-07-27 09:55:51'),
(237, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:07:49', '2018-07-27 10:07:49'),
(238, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:08:03', '2018-07-27 10:08:03'),
(239, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:08:36', '2018-07-27 10:08:36'),
(240, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:09:11', '2018-07-27 10:09:11'),
(241, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:09:43', '2018-07-27 10:09:43'),
(242, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:09:49', '2018-07-27 10:09:49'),
(243, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:10:20', '2018-07-27 10:10:20'),
(244, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:20', '2018-07-27 10:11:20'),
(245, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:21', '2018-07-27 10:11:21'),
(246, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:21', '2018-07-27 10:11:21'),
(247, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:21', '2018-07-27 10:11:21'),
(248, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:22', '2018-07-27 10:11:22'),
(249, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:11:36', '2018-07-27 10:11:36'),
(250, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:21', '2018-07-27 10:12:21'),
(251, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:21', '2018-07-27 10:12:21'),
(252, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:22', '2018-07-27 10:12:22'),
(253, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:28', '2018-07-27 10:12:28'),
(254, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:35', '2018-07-27 10:12:35'),
(255, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:12:59', '2018-07-27 10:12:59'),
(256, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:13:40', '2018-07-27 10:13:40'),
(257, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:13:52', '2018-07-27 10:13:52'),
(258, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:13:55', '2018-07-27 10:13:55'),
(259, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:07', '2018-07-27 10:14:07'),
(260, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:08', '2018-07-27 10:14:08'),
(261, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:10', '2018-07-27 10:14:10'),
(262, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:23', '2018-07-27 10:14:23'),
(263, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:45', '2018-07-27 10:14:45'),
(264, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:48', '2018-07-27 10:14:48'),
(265, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:14:55', '2018-07-27 10:14:55'),
(266, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:15:31', '2018-07-27 10:15:31'),
(267, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 10:16:01', '2018-07-27 10:16:01'),
(268, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:13:08', '2018-07-27 11:13:08'),
(269, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:27:06', '2018-07-27 11:27:06'),
(270, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:27:24', '2018-07-27 11:27:24'),
(271, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:35:34', '2018-07-27 11:35:34'),
(272, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:36:31', '2018-07-27 11:36:31'),
(273, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:39:31', '2018-07-27 11:39:31'),
(274, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:39:40', '2018-07-27 11:39:40'),
(275, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:47:40', '2018-07-27 11:47:40'),
(276, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:49:17', '2018-07-27 11:49:17'),
(277, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:49:21', '2018-07-27 11:49:21'),
(278, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:50:15', '2018-07-27 11:50:15'),
(279, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:50:37', '2018-07-27 11:50:37'),
(280, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:51:11', '2018-07-27 11:51:11'),
(281, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:51:17', '2018-07-27 11:51:17'),
(282, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:51:54', '2018-07-27 11:51:54'),
(283, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:54:29', '2018-07-27 11:54:29'),
(284, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:54:31', '2018-07-27 11:54:31'),
(285, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:54:33', '2018-07-27 11:54:33'),
(286, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:54:35', '2018-07-27 11:54:35'),
(287, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 11:54:37', '2018-07-27 11:54:37'),
(288, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 12:28:10', '2018-07-27 12:28:10'),
(289, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 12:28:34', '2018-07-27 12:28:34'),
(290, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 12:30:00', '2018-07-27 12:30:00'),
(291, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 12:32:13', '2018-07-27 12:32:13'),
(292, 14, 'http://localhost/fourgirls/public', '3', '2018-07-27 13:41:14', '2018-07-27 13:41:14'),
(293, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-28 05:37:46', '2018-07-28 05:37:46'),
(294, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 05:39:52', '2018-07-28 05:39:52'),
(295, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 05:55:23', '2018-07-28 05:55:23'),
(296, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:14:04', '2018-07-28 11:14:04'),
(297, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:14:47', '2018-07-28 11:14:47'),
(298, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:19:32', '2018-07-28 11:19:32'),
(299, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:20:45', '2018-07-28 11:20:45'),
(300, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:20:52', '2018-07-28 11:20:52'),
(301, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:21:00', '2018-07-28 11:21:00'),
(302, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:21:59', '2018-07-28 11:21:59'),
(303, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:22:14', '2018-07-28 11:22:14'),
(304, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:24:29', '2018-07-28 11:24:29'),
(305, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:38:21', '2018-07-28 11:38:21'),
(306, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:38:59', '2018-07-28 11:38:59'),
(307, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 11:40:16', '2018-07-28 11:40:16'),
(308, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 12:00:36', '2018-07-28 12:00:36'),
(309, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 12:02:48', '2018-07-28 12:02:48'),
(310, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 12:04:45', '2018-07-28 12:04:45'),
(311, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 12:06:21', '2018-07-28 12:06:21'),
(312, 14, 'http://localhost/fourgirls/public', '3', '2018-07-28 12:23:50', '2018-07-28 12:23:50'),
(313, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 08:54:36', '2018-07-30 08:54:36'),
(314, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:00:08', '2018-07-30 09:00:08'),
(315, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:12', '2018-07-30 09:02:12'),
(316, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:12', '2018-07-30 09:02:12'),
(317, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:13', '2018-07-30 09:02:13'),
(318, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:13', '2018-07-30 09:02:13'),
(319, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:14', '2018-07-30 09:02:14'),
(320, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:02:14', '2018-07-30 09:02:14'),
(321, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:03', '2018-07-30 09:03:03'),
(322, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:19', '2018-07-30 09:03:19'),
(323, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:20', '2018-07-30 09:03:20'),
(324, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:20', '2018-07-30 09:03:20'),
(325, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:20', '2018-07-30 09:03:20'),
(326, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:20', '2018-07-30 09:03:20'),
(327, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:32', '2018-07-30 09:03:32'),
(328, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:33', '2018-07-30 09:03:33'),
(329, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:38', '2018-07-30 09:03:38'),
(330, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:43', '2018-07-30 09:03:43'),
(331, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:51', '2018-07-30 09:03:51'),
(332, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:03:52', '2018-07-30 09:03:52'),
(333, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:04:27', '2018-07-30 09:04:27'),
(334, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:04:27', '2018-07-30 09:04:27'),
(335, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:02', '2018-07-30 09:05:02'),
(336, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:02', '2018-07-30 09:05:02'),
(337, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:03', '2018-07-30 09:05:03'),
(338, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:53', '2018-07-30 09:05:53'),
(339, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:54', '2018-07-30 09:05:54'),
(340, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:54', '2018-07-30 09:05:54'),
(341, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:56', '2018-07-30 09:05:56'),
(342, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:05:57', '2018-07-30 09:05:57'),
(343, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:06:03', '2018-07-30 09:06:03'),
(344, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:06:04', '2018-07-30 09:06:04'),
(345, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:06:04', '2018-07-30 09:06:04'),
(346, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:06:10', '2018-07-30 09:06:10'),
(347, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:07:00', '2018-07-30 09:07:00'),
(348, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:07:02', '2018-07-30 09:07:02'),
(349, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:07:14', '2018-07-30 09:07:14'),
(350, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:08:48', '2018-07-30 09:08:48'),
(351, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:09:27', '2018-07-30 09:09:27'),
(352, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:09:41', '2018-07-30 09:09:41'),
(353, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:14:35', '2018-07-30 09:14:35'),
(354, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:42:38', '2018-07-30 09:42:38'),
(355, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:43:49', '2018-07-30 09:43:49'),
(356, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:44:59', '2018-07-30 09:44:59'),
(357, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:45:09', '2018-07-30 09:45:09'),
(358, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:45:18', '2018-07-30 09:45:18'),
(359, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:45:34', '2018-07-30 09:45:34'),
(360, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:45:46', '2018-07-30 09:45:46'),
(361, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:46:23', '2018-07-30 09:46:23'),
(362, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:46:30', '2018-07-30 09:46:30'),
(363, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:46:43', '2018-07-30 09:46:43'),
(364, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:46:51', '2018-07-30 09:46:51'),
(365, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:47:35', '2018-07-30 09:47:35'),
(366, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:47:47', '2018-07-30 09:47:47'),
(367, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:47:57', '2018-07-30 09:47:57'),
(368, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:50:21', '2018-07-30 09:50:21'),
(369, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:51:05', '2018-07-30 09:51:05'),
(370, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:51:09', '2018-07-30 09:51:09'),
(371, NULL, '//', '3', '2018-07-30 09:51:26', '2018-07-30 09:51:26'),
(372, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:52:00', '2018-07-30 09:52:00'),
(373, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:56:22', '2018-07-30 09:56:22'),
(374, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:57:09', '2018-07-30 09:57:09'),
(375, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:57:10', '2018-07-30 09:57:10'),
(376, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:57:11', '2018-07-30 09:57:11'),
(377, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:57:11', '2018-07-30 09:57:11'),
(378, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:57:54', '2018-07-30 09:57:54'),
(379, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:03', '2018-07-30 09:58:03'),
(380, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:03', '2018-07-30 09:58:03'),
(381, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:04', '2018-07-30 09:58:04'),
(382, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:10', '2018-07-30 09:58:10'),
(383, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:39', '2018-07-30 09:58:39'),
(384, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:58:39', '2018-07-30 09:58:39'),
(385, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:59:05', '2018-07-30 09:59:05'),
(386, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:59:15', '2018-07-30 09:59:15'),
(387, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 09:59:58', '2018-07-30 09:59:58'),
(388, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:00:01', '2018-07-30 10:00:01'),
(389, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:00:03', '2018-07-30 10:00:03'),
(390, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:00:23', '2018-07-30 10:00:23'),
(391, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:00:27', '2018-07-30 10:00:27'),
(392, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:01:01', '2018-07-30 10:01:01'),
(393, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:01:13', '2018-07-30 10:01:13'),
(394, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:01:18', '2018-07-30 10:01:18'),
(395, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:03:25', '2018-07-30 10:03:25'),
(396, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:03:47', '2018-07-30 10:03:47'),
(397, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:03:56', '2018-07-30 10:03:56'),
(398, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:04:14', '2018-07-30 10:04:14'),
(399, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:19:23', '2018-07-30 10:19:23'),
(400, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:34:14', '2018-07-30 10:34:14'),
(401, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:35:48', '2018-07-30 10:35:48'),
(402, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:36:56', '2018-07-30 10:36:56'),
(403, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:37:04', '2018-07-30 10:37:04'),
(404, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 10:37:18', '2018-07-30 10:37:18'),
(405, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 11:00:47', '2018-07-30 11:00:47'),
(406, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 11:01:04', '2018-07-30 11:01:04'),
(407, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-30 11:01:21', '2018-07-30 11:01:21'),
(408, 14, 'http://localhost/fourgirls/public', '3', '2018-07-30 11:51:46', '2018-07-30 11:51:46'),
(409, 14, 'http://localhost/fourgirls/public', '3', '2018-07-30 12:09:45', '2018-07-30 12:09:45'),
(410, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 03:21:05', '2018-07-31 03:21:05'),
(411, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 03:29:41', '2018-07-31 03:29:41'),
(412, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 05:23:33', '2018-07-31 05:23:33'),
(413, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 05:39:32', '2018-07-31 05:39:32'),
(414, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:11:09', '2018-07-31 06:11:09'),
(415, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:11:09', '2018-07-31 06:11:09'),
(416, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:11:30', '2018-07-31 06:11:30'),
(417, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:31', '2018-07-31 06:35:31'),
(418, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:32', '2018-07-31 06:35:32'),
(419, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:33', '2018-07-31 06:35:33'),
(420, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:34', '2018-07-31 06:35:34'),
(421, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:34', '2018-07-31 06:35:34'),
(422, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:35', '2018-07-31 06:35:35'),
(423, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:36', '2018-07-31 06:35:36'),
(424, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:38', '2018-07-31 06:35:38'),
(425, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:38', '2018-07-31 06:35:38'),
(426, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:39', '2018-07-31 06:35:39'),
(427, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:40', '2018-07-31 06:35:40'),
(428, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:40', '2018-07-31 06:35:40'),
(429, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:35:41', '2018-07-31 06:35:41'),
(430, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:11', '2018-07-31 06:37:11'),
(431, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:12', '2018-07-31 06:37:12'),
(432, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:13', '2018-07-31 06:37:13'),
(433, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:14', '2018-07-31 06:37:14'),
(434, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:15', '2018-07-31 06:37:15'),
(435, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:16', '2018-07-31 06:37:16'),
(436, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:16', '2018-07-31 06:37:16'),
(437, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:37:17', '2018-07-31 06:37:17'),
(438, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:00', '2018-07-31 06:40:00'),
(439, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:01', '2018-07-31 06:40:01'),
(440, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:02', '2018-07-31 06:40:02'),
(441, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:02', '2018-07-31 06:40:02'),
(442, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:02', '2018-07-31 06:40:02'),
(443, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:03', '2018-07-31 06:40:03'),
(444, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:03', '2018-07-31 06:40:03'),
(445, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:05', '2018-07-31 06:40:05'),
(446, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:05', '2018-07-31 06:40:05'),
(447, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:06', '2018-07-31 06:40:06'),
(448, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:07', '2018-07-31 06:40:07'),
(449, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:07', '2018-07-31 06:40:07'),
(450, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:08', '2018-07-31 06:40:08'),
(451, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:40:08', '2018-07-31 06:40:08'),
(452, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:45', '2018-07-31 06:47:45'),
(453, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:46', '2018-07-31 06:47:46'),
(454, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:46', '2018-07-31 06:47:46'),
(455, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:47', '2018-07-31 06:47:47'),
(456, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:48', '2018-07-31 06:47:48'),
(457, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:48', '2018-07-31 06:47:48'),
(458, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:49', '2018-07-31 06:47:49'),
(459, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:49', '2018-07-31 06:47:49'),
(460, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:50', '2018-07-31 06:47:50'),
(461, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:50', '2018-07-31 06:47:50'),
(462, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:50', '2018-07-31 06:47:50'),
(463, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:51', '2018-07-31 06:47:51'),
(464, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:51', '2018-07-31 06:47:51'),
(465, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:52', '2018-07-31 06:47:52'),
(466, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:52', '2018-07-31 06:47:52'),
(467, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:52', '2018-07-31 06:47:52'),
(468, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:53', '2018-07-31 06:47:53'),
(469, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:53', '2018-07-31 06:47:53'),
(470, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:54', '2018-07-31 06:47:54'),
(471, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:54', '2018-07-31 06:47:54'),
(472, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:55', '2018-07-31 06:47:55'),
(473, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:56', '2018-07-31 06:47:56'),
(474, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:47:56', '2018-07-31 06:47:56'),
(475, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:48:16', '2018-07-31 06:48:16'),
(476, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:02', '2018-07-31 06:51:02'),
(477, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:03', '2018-07-31 06:51:03'),
(478, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:03', '2018-07-31 06:51:03'),
(479, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:04', '2018-07-31 06:51:04'),
(480, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:04', '2018-07-31 06:51:04'),
(481, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:05', '2018-07-31 06:51:05'),
(482, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:05', '2018-07-31 06:51:05'),
(483, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:38', '2018-07-31 06:51:38'),
(484, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:39', '2018-07-31 06:51:39'),
(485, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:39', '2018-07-31 06:51:39'),
(486, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:40', '2018-07-31 06:51:40'),
(487, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:40', '2018-07-31 06:51:40'),
(488, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:41', '2018-07-31 06:51:41'),
(489, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:41', '2018-07-31 06:51:41'),
(490, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:42', '2018-07-31 06:51:42'),
(491, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:42', '2018-07-31 06:51:42'),
(492, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:43', '2018-07-31 06:51:43'),
(493, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:52', '2018-07-31 06:51:52'),
(494, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:52', '2018-07-31 06:51:52'),
(495, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:51:53', '2018-07-31 06:51:53'),
(496, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:52:15', '2018-07-31 06:52:15'),
(497, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:52:16', '2018-07-31 06:52:16'),
(498, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:52:16', '2018-07-31 06:52:16'),
(499, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:52:17', '2018-07-31 06:52:17'),
(500, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:52:17', '2018-07-31 06:52:17'),
(501, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:54:40', '2018-07-31 06:54:40'),
(502, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:54:42', '2018-07-31 06:54:42'),
(503, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:57:51', '2018-07-31 06:57:51'),
(504, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:57:53', '2018-07-31 06:57:53'),
(505, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:58:41', '2018-07-31 06:58:41'),
(506, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:58:43', '2018-07-31 06:58:43'),
(507, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 06:58:45', '2018-07-31 06:58:45'),
(508, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 08:37:21', '2018-07-31 08:37:21'),
(509, NULL, 'http://localhost/fourgirls/public', '3', '2018-07-31 08:48:06', '2018-07-31 08:48:06');
INSERT INTO `visitors` (`id`, `user_id`, `url`, `ip`, `created_at`, `updated_at`) VALUES
(510, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:16:14', '2018-07-31 10:16:14'),
(511, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:16:20', '2018-07-31 10:16:20'),
(512, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:16:27', '2018-07-31 10:16:27'),
(513, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:17:20', '2018-07-31 10:17:20'),
(514, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:18:04', '2018-07-31 10:18:04'),
(515, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:19:19', '2018-07-31 10:19:19'),
(516, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:21:02', '2018-07-31 10:21:02'),
(517, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:21:05', '2018-07-31 10:21:05'),
(518, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:21:05', '2018-07-31 10:21:05'),
(519, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 10:21:06', '2018-07-31 10:21:06'),
(520, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:13:23', '2018-07-31 11:13:23'),
(521, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:14:03', '2018-07-31 11:14:03'),
(522, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:14:53', '2018-07-31 11:14:53'),
(523, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:01', '2018-07-31 11:15:01'),
(524, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:16', '2018-07-31 11:15:16'),
(525, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:21', '2018-07-31 11:15:21'),
(526, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:22', '2018-07-31 11:15:22'),
(527, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:22', '2018-07-31 11:15:22'),
(528, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:22', '2018-07-31 11:15:22'),
(529, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:23', '2018-07-31 11:15:23'),
(530, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:24', '2018-07-31 11:15:24'),
(531, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:24', '2018-07-31 11:15:24'),
(532, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:25', '2018-07-31 11:15:25'),
(533, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:25', '2018-07-31 11:15:25'),
(534, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:26', '2018-07-31 11:15:26'),
(535, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:26', '2018-07-31 11:15:26'),
(536, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:26', '2018-07-31 11:15:26'),
(537, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:15:26', '2018-07-31 11:15:26'),
(538, 14, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:16:08', '2018-07-31 11:16:08'),
(539, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:17:53', '2018-07-31 11:17:53'),
(540, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:19:27', '2018-07-31 11:19:27'),
(541, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:21:21', '2018-07-31 11:21:21'),
(542, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 11:44:42', '2018-07-31 11:44:42'),
(543, NULL, 'http://localhost/fourgirls/public', '::1', '2018-07-31 12:08:31', '2018-07-31 12:08:31'),
(544, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 02:06:50', '2018-08-01 02:06:50'),
(545, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:23:03', '2018-08-01 04:23:03'),
(546, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:23:28', '2018-08-01 04:23:28'),
(547, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:03', '2018-08-01 04:24:03'),
(548, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:09', '2018-08-01 04:24:09'),
(549, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:10', '2018-08-01 04:24:10'),
(550, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:11', '2018-08-01 04:24:11'),
(551, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:11', '2018-08-01 04:24:11'),
(552, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:24:15', '2018-08-01 04:24:15'),
(553, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:25:07', '2018-08-01 04:25:07'),
(554, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 04:25:17', '2018-08-01 04:25:17'),
(555, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 10:03:45', '2018-08-01 10:03:45'),
(556, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 11:15:12', '2018-08-01 11:15:12'),
(557, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 12:18:26', '2018-08-01 12:18:26'),
(558, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 12:42:33', '2018-08-01 12:42:33'),
(559, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 12:46:45', '2018-08-01 12:46:45'),
(560, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-01 12:47:05', '2018-08-01 12:47:05'),
(561, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-02 03:25:46', '2018-08-02 03:25:46'),
(562, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 02:19:57', '2018-08-03 02:19:57'),
(563, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:02:00', '2018-08-03 10:02:00'),
(564, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:21:30', '2018-08-03 10:21:30'),
(565, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:30:45', '2018-08-03 10:30:45'),
(566, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:34:03', '2018-08-03 10:34:03'),
(567, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:43:29', '2018-08-03 10:43:29'),
(568, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 10:43:31', '2018-08-03 10:43:31'),
(569, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 11:28:02', '2018-08-03 11:28:02'),
(570, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 11:38:05', '2018-08-03 11:38:05'),
(571, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 12:08:15', '2018-08-03 12:08:15'),
(572, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-03 12:08:33', '2018-08-03 12:08:33'),
(573, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-04 05:48:56', '2018-08-04 05:48:56'),
(574, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-04 07:49:31', '2018-08-04 07:49:31'),
(575, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-07 05:19:22', '2018-08-07 05:19:22'),
(576, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-09 05:21:07', '2018-08-09 05:21:07'),
(577, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-09 05:43:15', '2018-08-09 05:43:15'),
(578, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-09 10:14:40', '2018-08-09 10:14:40'),
(579, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:10:19', '2018-08-13 02:10:19'),
(580, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:17:51', '2018-08-13 02:17:51'),
(581, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:23:12', '2018-08-13 02:23:12'),
(582, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:36:44', '2018-08-13 02:36:44'),
(583, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:36:47', '2018-08-13 02:36:47'),
(584, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:39:04', '2018-08-13 02:39:04'),
(585, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:39:33', '2018-08-13 02:39:33'),
(586, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:41:03', '2018-08-13 02:41:03'),
(587, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:41:16', '2018-08-13 02:41:16'),
(588, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:42:52', '2018-08-13 02:42:52'),
(589, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:47:46', '2018-08-13 02:47:46'),
(590, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:47:47', '2018-08-13 02:47:47'),
(591, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:47:48', '2018-08-13 02:47:48'),
(592, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:47:49', '2018-08-13 02:47:49'),
(593, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 02:47:50', '2018-08-13 02:47:50'),
(594, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:07:34', '2018-08-13 03:07:34'),
(595, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:07:53', '2018-08-13 03:07:53'),
(596, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:08:45', '2018-08-13 03:08:45'),
(597, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:09:23', '2018-08-13 03:09:23'),
(598, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:09:54', '2018-08-13 03:09:54'),
(599, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:11:22', '2018-08-13 03:11:22'),
(600, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:12:30', '2018-08-13 03:12:30'),
(601, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:15:11', '2018-08-13 03:15:11'),
(602, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:19:56', '2018-08-13 03:19:56'),
(603, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:19:58', '2018-08-13 03:19:58'),
(604, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:19:58', '2018-08-13 03:19:58'),
(605, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:20:01', '2018-08-13 03:20:01'),
(606, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:20:06', '2018-08-13 03:20:06'),
(607, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:20:06', '2018-08-13 03:20:06'),
(608, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:20:36', '2018-08-13 03:20:36'),
(609, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:20:37', '2018-08-13 03:20:37'),
(610, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:21:22', '2018-08-13 03:21:22'),
(611, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:21:23', '2018-08-13 03:21:23'),
(612, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:21:36', '2018-08-13 03:21:36'),
(613, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:09', '2018-08-13 03:22:09'),
(614, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:18', '2018-08-13 03:22:18'),
(615, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:20', '2018-08-13 03:22:20'),
(616, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:33', '2018-08-13 03:22:33'),
(617, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:34', '2018-08-13 03:22:34'),
(618, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:22:57', '2018-08-13 03:22:57'),
(619, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:23:46', '2018-08-13 03:23:46'),
(620, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:23:48', '2018-08-13 03:23:48'),
(621, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:24:12', '2018-08-13 03:24:12'),
(622, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:24:13', '2018-08-13 03:24:13'),
(623, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:24:13', '2018-08-13 03:24:13'),
(624, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:24:13', '2018-08-13 03:24:13'),
(625, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:26:07', '2018-08-13 03:26:07'),
(626, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:26:57', '2018-08-13 03:26:57'),
(627, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:27:21', '2018-08-13 03:27:21'),
(628, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:27:32', '2018-08-13 03:27:32'),
(629, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:27:57', '2018-08-13 03:27:57'),
(630, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:28:14', '2018-08-13 03:28:14'),
(631, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:28:35', '2018-08-13 03:28:35'),
(632, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:29:44', '2018-08-13 03:29:44'),
(633, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:31:05', '2018-08-13 03:31:05'),
(634, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:31:16', '2018-08-13 03:31:16'),
(635, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:31:19', '2018-08-13 03:31:19'),
(636, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:31:19', '2018-08-13 03:31:19'),
(637, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:31:35', '2018-08-13 03:31:35'),
(638, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:32:19', '2018-08-13 03:32:19'),
(639, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:32:37', '2018-08-13 03:32:37'),
(640, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:33:04', '2018-08-13 03:33:04'),
(641, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:33:46', '2018-08-13 03:33:46'),
(642, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:33:52', '2018-08-13 03:33:52'),
(643, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:35:29', '2018-08-13 03:35:29'),
(644, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:35:30', '2018-08-13 03:35:30'),
(645, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:37:35', '2018-08-13 03:37:35'),
(646, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:37:36', '2018-08-13 03:37:36'),
(647, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:38:59', '2018-08-13 03:38:59'),
(648, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:39:00', '2018-08-13 03:39:00'),
(649, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:41:49', '2018-08-13 03:41:49'),
(650, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:44:44', '2018-08-13 03:44:44'),
(651, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:45:56', '2018-08-13 03:45:56'),
(652, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:46:46', '2018-08-13 03:46:46'),
(653, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:48:34', '2018-08-13 03:48:34'),
(654, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:49:07', '2018-08-13 03:49:07'),
(655, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:51:38', '2018-08-13 03:51:38'),
(656, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:52:08', '2018-08-13 03:52:08'),
(657, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:54:23', '2018-08-13 03:54:23'),
(658, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:54:53', '2018-08-13 03:54:53'),
(659, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:58:50', '2018-08-13 03:58:50'),
(660, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 03:58:53', '2018-08-13 03:58:53'),
(661, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 04:10:12', '2018-08-13 04:10:12'),
(662, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 04:11:43', '2018-08-13 04:11:43'),
(663, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:34:25', '2018-08-13 07:34:25'),
(664, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:41:43', '2018-08-13 07:41:43'),
(665, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:43:41', '2018-08-13 07:43:41'),
(666, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:44:14', '2018-08-13 07:44:14'),
(667, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:44:34', '2018-08-13 07:44:34'),
(668, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:44:55', '2018-08-13 07:44:55'),
(669, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:44:57', '2018-08-13 07:44:57'),
(670, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:45:18', '2018-08-13 07:45:18'),
(671, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:45:37', '2018-08-13 07:45:37'),
(672, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:45:42', '2018-08-13 07:45:42'),
(673, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:51:37', '2018-08-13 07:51:37'),
(674, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:54:14', '2018-08-13 07:54:14'),
(675, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:55:52', '2018-08-13 07:55:52'),
(676, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:55:53', '2018-08-13 07:55:53'),
(677, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:57:16', '2018-08-13 07:57:16'),
(678, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 07:59:01', '2018-08-13 07:59:01'),
(679, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:01:14', '2018-08-13 08:01:14'),
(680, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:01:40', '2018-08-13 08:01:40'),
(681, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:01:44', '2018-08-13 08:01:44'),
(682, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:02:12', '2018-08-13 08:02:12'),
(683, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:03:00', '2018-08-13 08:03:00'),
(684, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:03:03', '2018-08-13 08:03:03'),
(685, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:03:30', '2018-08-13 08:03:30'),
(686, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:03:39', '2018-08-13 08:03:39'),
(687, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:04:27', '2018-08-13 08:04:27'),
(688, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:04:47', '2018-08-13 08:04:47'),
(689, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:19:36', '2018-08-13 08:19:36'),
(690, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:21:41', '2018-08-13 08:21:41'),
(691, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:22:01', '2018-08-13 08:22:01'),
(692, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 08:35:56', '2018-08-13 08:35:56'),
(693, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:10:11', '2018-08-13 09:10:11'),
(694, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:15:19', '2018-08-13 09:15:19'),
(695, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:26:27', '2018-08-13 09:26:27'),
(696, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:36:10', '2018-08-13 09:36:10'),
(697, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:36:18', '2018-08-13 09:36:18'),
(698, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:41:33', '2018-08-13 09:41:33'),
(699, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 09:44:21', '2018-08-13 09:44:21'),
(700, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 10:04:17', '2018-08-13 10:04:17'),
(701, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 10:06:47', '2018-08-13 10:06:47'),
(702, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 10:23:07', '2018-08-13 10:23:07'),
(703, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:04:16', '2018-08-13 11:04:16'),
(704, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:08:47', '2018-08-13 11:08:47'),
(705, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:17:05', '2018-08-13 11:17:05'),
(706, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:17:52', '2018-08-13 11:17:52'),
(707, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:18:12', '2018-08-13 11:18:12'),
(708, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:18:28', '2018-08-13 11:18:28'),
(709, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:19:00', '2018-08-13 11:19:00'),
(710, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:19:42', '2018-08-13 11:19:42'),
(711, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:20:31', '2018-08-13 11:20:31'),
(712, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:21:18', '2018-08-13 11:21:18'),
(713, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:21:42', '2018-08-13 11:21:42'),
(714, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:22:11', '2018-08-13 11:22:11'),
(715, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-13 11:22:46', '2018-08-13 11:22:46'),
(716, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:02:27', '2018-08-14 02:02:27'),
(717, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:47:07', '2018-08-14 02:47:07'),
(718, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:48:44', '2018-08-14 02:48:44'),
(719, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:49:45', '2018-08-14 02:49:45'),
(720, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:50:15', '2018-08-14 02:50:15'),
(721, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:50:41', '2018-08-14 02:50:41'),
(722, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 02:50:49', '2018-08-14 02:50:49'),
(723, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 03:19:59', '2018-08-14 03:19:59'),
(724, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 03:23:06', '2018-08-14 03:23:06'),
(725, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 03:23:20', '2018-08-14 03:23:20'),
(726, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 03:30:11', '2018-08-14 03:30:11'),
(727, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 08:34:03', '2018-08-14 08:34:03'),
(728, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-14 09:19:41', '2018-08-14 09:19:41'),
(729, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 02:19:31', '2018-08-16 02:19:31'),
(730, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:28:46', '2018-08-16 05:28:46'),
(731, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:30:39', '2018-08-16 05:30:39'),
(732, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:33:23', '2018-08-16 05:33:23'),
(733, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:35:09', '2018-08-16 05:35:09'),
(734, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:35:26', '2018-08-16 05:35:26'),
(735, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:36:05', '2018-08-16 05:36:05'),
(736, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:40:37', '2018-08-16 05:40:37'),
(737, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:40:49', '2018-08-16 05:40:49'),
(738, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:41:18', '2018-08-16 05:41:18'),
(739, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 05:41:34', '2018-08-16 05:41:34'),
(740, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:10:53', '2018-08-16 06:10:53'),
(741, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:13:55', '2018-08-16 06:13:55'),
(742, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:43:32', '2018-08-16 06:43:32'),
(743, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:43:33', '2018-08-16 06:43:33'),
(744, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:44:43', '2018-08-16 06:44:43'),
(745, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:46:26', '2018-08-16 06:46:26'),
(746, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:46:31', '2018-08-16 06:46:31'),
(747, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 06:47:30', '2018-08-16 06:47:30'),
(748, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-16 10:22:57', '2018-08-16 10:22:57'),
(749, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-16 11:39:44', '2018-08-16 11:39:44'),
(750, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 02:53:57', '2018-08-17 02:53:57'),
(751, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 03:23:12', '2018-08-17 03:23:12'),
(752, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 03:39:21', '2018-08-17 03:39:21'),
(753, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 03:40:01', '2018-08-17 03:40:01'),
(754, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 04:47:40', '2018-08-17 04:47:40'),
(755, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:02:21', '2018-08-17 05:02:21'),
(756, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:02:26', '2018-08-17 05:02:26'),
(757, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:21:09', '2018-08-17 05:21:09'),
(758, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:28:10', '2018-08-17 05:28:10'),
(759, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:38:32', '2018-08-17 05:38:32'),
(760, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 05:38:46', '2018-08-17 05:38:46'),
(761, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 07:10:20', '2018-08-17 07:10:20'),
(762, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 09:46:24', '2018-08-17 09:46:24'),
(763, 14, 'http://localhost/fourgirls/public', '::1', '2018-08-17 10:21:16', '2018-08-17 10:21:16'),
(764, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-17 11:09:25', '2018-08-17 11:09:25'),
(765, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-18 03:33:52', '2018-08-18 03:33:52'),
(766, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-18 03:35:13', '2018-08-18 03:35:13'),
(767, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-18 06:14:21', '2018-08-18 06:14:21'),
(768, NULL, 'http://localhost/fourgirls/public', '::1', '2018-08-18 11:33:50', '2018-08-18 11:33:50'),
(769, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-05 02:19:39', '2018-09-05 02:19:39'),
(770, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-06 05:56:06', '2018-09-06 05:56:06'),
(771, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-06 05:57:05', '2018-09-06 05:57:05'),
(772, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-06 06:09:34', '2018-09-06 06:09:34'),
(773, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-06 11:07:39', '2018-09-06 11:07:39'),
(774, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 05:32:07', '2018-09-11 05:32:07'),
(775, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 06:13:22', '2018-09-11 06:13:22'),
(776, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 06:22:21', '2018-09-11 06:22:21'),
(777, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 07:21:59', '2018-09-11 07:21:59'),
(778, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 07:24:08', '2018-09-11 07:24:08'),
(779, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 07:55:19', '2018-09-11 07:55:19'),
(780, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 07:58:31', '2018-09-11 07:58:31'),
(781, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 08:02:00', '2018-09-11 08:02:00'),
(782, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 08:51:31', '2018-09-11 08:51:31'),
(783, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 08:51:32', '2018-09-11 08:51:32'),
(784, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 08:51:38', '2018-09-11 08:51:38'),
(785, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-11 09:09:33', '2018-09-11 09:09:33'),
(786, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-17 07:07:16', '2018-09-17 07:07:16'),
(787, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-17 07:54:53', '2018-09-17 07:54:53'),
(788, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-17 08:01:01', '2018-09-17 08:01:01'),
(789, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-18 02:06:25', '2018-09-18 02:06:25'),
(790, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-18 07:22:19', '2018-09-18 07:22:19'),
(791, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-18 07:25:55', '2018-09-18 07:25:55'),
(792, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-18 07:48:19', '2018-09-18 07:48:19'),
(793, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-18 08:29:37', '2018-09-18 08:29:37'),
(794, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 04:16:29', '2018-09-19 04:16:29'),
(795, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 04:21:19', '2018-09-19 04:21:19'),
(796, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 04:30:33', '2018-09-19 04:30:33'),
(797, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 05:38:35', '2018-09-19 05:38:35'),
(798, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 07:33:17', '2018-09-19 07:33:17'),
(799, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 09:13:53', '2018-09-19 09:13:53'),
(800, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 09:32:19', '2018-09-19 09:32:19'),
(801, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 10:54:06', '2018-09-19 10:54:06'),
(802, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 11:36:13', '2018-09-19 11:36:13'),
(803, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-19 12:35:11', '2018-09-19 12:35:11'),
(804, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 05:47:18', '2018-09-20 05:47:18'),
(805, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:11:24', '2018-09-20 08:11:24'),
(806, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:17:06', '2018-09-20 08:17:06'),
(807, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:17:21', '2018-09-20 08:17:21'),
(808, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:17:29', '2018-09-20 08:17:29'),
(809, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:17:44', '2018-09-20 08:17:44'),
(810, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:45:14', '2018-09-20 08:45:14'),
(811, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:47:02', '2018-09-20 08:47:02'),
(812, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:47:32', '2018-09-20 08:47:32'),
(813, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:48:36', '2018-09-20 08:48:36'),
(814, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:49:14', '2018-09-20 08:49:14'),
(815, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:53:00', '2018-09-20 08:53:00'),
(816, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 08:53:47', '2018-09-20 08:53:47'),
(817, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:01:39', '2018-09-20 09:01:39'),
(818, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:02:05', '2018-09-20 09:02:05'),
(819, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:03:48', '2018-09-20 09:03:48'),
(820, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:04:25', '2018-09-20 09:04:25'),
(821, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:18:42', '2018-09-20 09:18:42'),
(822, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 09:23:38', '2018-09-20 09:23:38'),
(823, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 13:07:43', '2018-09-20 13:07:43'),
(824, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-20 13:52:06', '2018-09-20 13:52:06'),
(825, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 02:04:25', '2018-09-24 02:04:25'),
(826, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 02:11:41', '2018-09-24 02:11:41'),
(827, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 02:11:52', '2018-09-24 02:11:52'),
(828, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 04:57:24', '2018-09-24 04:57:24'),
(829, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-24 05:05:19', '2018-09-24 05:05:19'),
(830, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-24 05:22:48', '2018-09-24 05:22:48'),
(831, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-24 05:23:41', '2018-09-24 05:23:41'),
(832, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-24 05:30:11', '2018-09-24 05:30:11'),
(833, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-24 05:59:44', '2018-09-24 05:59:44'),
(834, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 08:48:24', '2018-09-24 08:48:24'),
(835, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-24 08:48:39', '2018-09-24 08:48:39'),
(836, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 02:02:24', '2018-09-25 02:02:24'),
(837, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 04:19:19', '2018-09-25 04:19:19'),
(838, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 04:29:28', '2018-09-25 04:29:28'),
(839, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-25 06:00:58', '2018-09-25 06:00:58'),
(840, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-25 07:41:26', '2018-09-25 07:41:26'),
(841, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 08:03:34', '2018-09-25 08:03:34'),
(842, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 08:20:29', '2018-09-25 08:20:29'),
(843, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 08:20:59', '2018-09-25 08:20:59'),
(844, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:03:10', '2018-09-25 09:03:10'),
(845, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:30:13', '2018-09-25 09:30:13'),
(846, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:32:10', '2018-09-25 09:32:10'),
(847, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:32:11', '2018-09-25 09:32:11'),
(848, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:32:45', '2018-09-25 09:32:45'),
(849, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:32:47', '2018-09-25 09:32:47'),
(850, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:34:58', '2018-09-25 09:34:58'),
(851, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 09:36:43', '2018-09-25 09:36:43'),
(852, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 10:16:09', '2018-09-25 10:16:09'),
(853, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-25 10:47:56', '2018-09-25 10:47:56'),
(854, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-26 02:07:47', '2018-09-26 02:07:47'),
(855, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:19:08', '2018-09-26 05:19:08'),
(856, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:27:07', '2018-09-26 05:27:07'),
(857, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:30:32', '2018-09-26 05:30:32'),
(858, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:31:03', '2018-09-26 05:31:03'),
(859, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:33:23', '2018-09-26 05:33:23'),
(860, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:34:48', '2018-09-26 05:34:48'),
(861, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 05:35:07', '2018-09-26 05:35:07'),
(862, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-26 06:32:51', '2018-09-26 06:32:51'),
(863, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-26 07:29:33', '2018-09-26 07:29:33'),
(864, 14, 'http://localhost/fourgirls/public', '::1', '2018-09-26 07:34:45', '2018-09-26 07:34:45'),
(865, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-28 04:12:36', '2018-09-28 04:12:36'),
(866, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-30 03:20:54', '2018-09-30 03:20:54'),
(867, NULL, 'http://localhost/fourgirls/public', '::1', '2018-09-30 03:21:43', '2018-09-30 03:21:43'),
(868, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-03 01:58:57', '2018-10-03 01:58:57'),
(869, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-03 01:59:00', '2018-10-03 01:59:00'),
(870, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-03 02:10:34', '2018-10-03 02:10:34'),
(871, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-03 02:11:24', '2018-10-03 02:11:24'),
(872, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-09 08:51:17', '2018-10-09 08:51:17'),
(873, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 02:41:13', '2018-10-10 02:41:13'),
(874, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 04:05:56', '2018-10-10 04:05:56'),
(875, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 04:23:45', '2018-10-10 04:23:45'),
(876, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 04:37:59', '2018-10-10 04:37:59'),
(877, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 08:40:18', '2018-10-10 08:40:18'),
(878, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 11:12:54', '2018-10-10 11:12:54'),
(879, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-10 11:23:51', '2018-10-10 11:23:51'),
(880, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-11 02:22:53', '2018-10-11 02:22:53'),
(881, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-11 07:19:31', '2018-10-11 07:19:31'),
(882, NULL, 'http://localhost/fourgirls/public', '::1', '2018-10-30 11:24:54', '2018-10-30 11:24:54'),
(883, NULL, 'http://localhost/fourgirls/public', '::1', '2018-11-21 04:31:06', '2018-11-21 04:31:06'),
(884, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 07:32:57', '2018-11-21 07:32:57'),
(885, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 07:33:40', '2018-11-21 07:33:40'),
(886, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 07:50:59', '2018-11-21 07:50:59'),
(887, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 07:51:00', '2018-11-21 07:51:00'),
(888, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 07:53:06', '2018-11-21 07:53:06'),
(889, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:14:27', '2018-11-21 08:14:27'),
(890, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:14:30', '2018-11-21 08:14:30'),
(891, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:14:31', '2018-11-21 08:14:31'),
(892, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:14:32', '2018-11-21 08:14:32'),
(893, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:16:37', '2018-11-21 08:16:37'),
(894, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:13', '2018-11-21 08:17:13'),
(895, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:17', '2018-11-21 08:17:17'),
(896, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:18', '2018-11-21 08:17:18'),
(897, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:19', '2018-11-21 08:17:19'),
(898, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:21', '2018-11-21 08:17:21'),
(899, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:22', '2018-11-21 08:17:22'),
(900, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:17:23', '2018-11-21 08:17:23'),
(901, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:20:12', '2018-11-21 08:20:12'),
(902, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:20:30', '2018-11-21 08:20:30'),
(903, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:20:32', '2018-11-21 08:20:32'),
(904, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:20:33', '2018-11-21 08:20:33'),
(905, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:21:15', '2018-11-21 08:21:15'),
(906, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:21:22', '2018-11-21 08:21:22'),
(907, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:21:29', '2018-11-21 08:21:29'),
(908, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:21:34', '2018-11-21 08:21:34'),
(909, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:24:36', '2018-11-21 08:24:36'),
(910, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:24:37', '2018-11-21 08:24:37'),
(911, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:24:50', '2018-11-21 08:24:50'),
(912, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:25:04', '2018-11-21 08:25:04'),
(913, 14, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:54:28', '2018-11-21 08:54:28'),
(914, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 08:54:40', '2018-11-21 08:54:40'),
(915, 14, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:00:57', '2018-11-21 09:00:57'),
(916, 14, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:02:03', '2018-11-21 09:02:03'),
(917, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:09:50', '2018-11-21 09:09:50'),
(918, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:12:13', '2018-11-21 09:12:13'),
(919, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:19:39', '2018-11-21 09:19:39'),
(920, 14, 'http://localhost/cqbycq/public', '3', '2018-11-21 09:33:47', '2018-11-21 09:33:47'),
(921, 14, 'http://localhost/cqbycq/public', '3', '2018-11-21 10:12:47', '2018-11-21 10:12:47'),
(922, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-24 10:58:54', '2018-11-24 10:58:54'),
(923, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-27 08:43:37', '2018-11-27 08:43:37'),
(924, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:20:41', '2018-11-29 07:20:41'),
(925, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:26:25', '2018-11-29 07:26:25'),
(926, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:31:25', '2018-11-29 07:31:25'),
(927, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:31:28', '2018-11-29 07:31:28'),
(928, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:32:41', '2018-11-29 07:32:41'),
(929, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:33:00', '2018-11-29 07:33:00'),
(930, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:33:03', '2018-11-29 07:33:03'),
(931, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:04', '2018-11-29 07:34:04'),
(932, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:20', '2018-11-29 07:34:20'),
(933, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:40', '2018-11-29 07:34:40'),
(934, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:41', '2018-11-29 07:34:41'),
(935, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:42', '2018-11-29 07:34:42'),
(936, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:45', '2018-11-29 07:34:45'),
(937, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:34:48', '2018-11-29 07:34:48'),
(938, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:35:39', '2018-11-29 07:35:39'),
(939, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:37:02', '2018-11-29 07:37:02'),
(940, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:38:36', '2018-11-29 07:38:36'),
(941, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:38:55', '2018-11-29 07:38:55'),
(942, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:40:27', '2018-11-29 07:40:27'),
(943, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:40:51', '2018-11-29 07:40:51'),
(944, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:40:54', '2018-11-29 07:40:54'),
(945, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:41:06', '2018-11-29 07:41:06'),
(946, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:41:13', '2018-11-29 07:41:13'),
(947, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:41:44', '2018-11-29 07:41:44'),
(948, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:41:45', '2018-11-29 07:41:45'),
(949, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:41:46', '2018-11-29 07:41:46'),
(950, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 07:56:16', '2018-11-29 07:56:16'),
(951, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:01:16', '2018-11-29 08:01:16'),
(952, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:02:54', '2018-11-29 08:02:54'),
(953, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:04:22', '2018-11-29 08:04:22'),
(954, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:08:59', '2018-11-29 08:08:59'),
(955, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:09:08', '2018-11-29 08:09:08'),
(956, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:10:12', '2018-11-29 08:10:12'),
(957, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:18:25', '2018-11-29 08:18:25'),
(958, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:25:06', '2018-11-29 08:25:06'),
(959, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:25:35', '2018-11-29 08:25:35'),
(960, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:09', '2018-11-29 08:29:09'),
(961, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:10', '2018-11-29 08:29:10'),
(962, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:30', '2018-11-29 08:29:30'),
(963, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:31', '2018-11-29 08:29:31'),
(964, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:38', '2018-11-29 08:29:38'),
(965, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:29:53', '2018-11-29 08:29:53'),
(966, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:30:38', '2018-11-29 08:30:38'),
(967, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:30:55', '2018-11-29 08:30:55'),
(968, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:31:37', '2018-11-29 08:31:37'),
(969, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:31:59', '2018-11-29 08:31:59'),
(970, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:32:13', '2018-11-29 08:32:13'),
(971, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:32:34', '2018-11-29 08:32:34'),
(972, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:32:48', '2018-11-29 08:32:48'),
(973, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:32:58', '2018-11-29 08:32:58'),
(974, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:33:00', '2018-11-29 08:33:00'),
(975, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:33:02', '2018-11-29 08:33:02'),
(976, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:34:09', '2018-11-29 08:34:09'),
(977, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:34:31', '2018-11-29 08:34:31'),
(978, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:34:39', '2018-11-29 08:34:39'),
(979, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:39:47', '2018-11-29 08:39:47'),
(980, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 08:42:51', '2018-11-29 08:42:51'),
(981, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-29 09:46:27', '2018-11-29 09:46:27'),
(982, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-30 02:05:13', '2018-11-30 02:05:13'),
(983, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-30 02:14:51', '2018-11-30 02:14:51'),
(984, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-30 07:38:25', '2018-11-30 07:38:25'),
(985, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:00:15', '2018-11-30 08:00:15'),
(986, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:21:18', '2018-11-30 08:21:18'),
(987, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:33:20', '2018-11-30 08:33:20'),
(988, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:33:39', '2018-11-30 08:33:39'),
(989, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:34:58', '2018-11-30 08:34:58'),
(990, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:35:06', '2018-11-30 08:35:06'),
(991, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 08:40:18', '2018-11-30 08:40:18'),
(992, NULL, 'http://localhost/cqbycq/public', '3', '2018-11-30 12:34:25', '2018-11-30 12:34:25'),
(993, 14, 'http://localhost/cqbycq/public', '3', '2018-11-30 13:11:12', '2018-11-30 13:11:12'),
(994, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 02:05:45', '2018-12-01 02:05:45'),
(995, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 02:44:13', '2018-12-01 02:44:13'),
(996, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 04:26:35', '2018-12-01 04:26:35'),
(997, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 04:43:22', '2018-12-01 04:43:22'),
(998, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 04:43:59', '2018-12-01 04:43:59'),
(999, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 04:44:01', '2018-12-01 04:44:01'),
(1000, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 04:44:01', '2018-12-01 04:44:01'),
(1001, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 06:51:13', '2018-12-01 06:51:13'),
(1002, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 07:38:42', '2018-12-01 07:38:42'),
(1003, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 07:51:23', '2018-12-01 07:51:23'),
(1004, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 08:58:54', '2018-12-01 08:58:54'),
(1005, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 08:58:59', '2018-12-01 08:58:59'),
(1006, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:05:07', '2018-12-01 09:05:07'),
(1007, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:12:24', '2018-12-01 09:12:24'),
(1008, 14, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:12:32', '2018-12-01 09:12:32'),
(1009, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:14:55', '2018-12-01 09:14:55'),
(1010, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:18:35', '2018-12-01 09:18:35');
INSERT INTO `visitors` (`id`, `user_id`, `url`, `ip`, `created_at`, `updated_at`) VALUES
(1011, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:18:49', '2018-12-01 09:18:49'),
(1012, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:20:42', '2018-12-01 09:20:42'),
(1013, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:22:26', '2018-12-01 09:22:26'),
(1014, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 09:28:42', '2018-12-01 09:28:42'),
(1015, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 10:33:04', '2018-12-01 10:33:04'),
(1016, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 10:34:22', '2018-12-01 10:34:22'),
(1017, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-01 10:55:03', '2018-12-01 10:55:03'),
(1018, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-03 02:04:46', '2018-12-03 02:04:46'),
(1019, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-03 02:18:41', '2018-12-03 02:18:41'),
(1020, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-03 02:51:24', '2018-12-03 02:51:24'),
(1021, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:08:32', '2018-12-03 03:08:32'),
(1022, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:24:34', '2018-12-03 03:24:34'),
(1023, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:42:42', '2018-12-03 03:42:42'),
(1024, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:43:09', '2018-12-03 03:43:09'),
(1025, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:43:11', '2018-12-03 03:43:11'),
(1026, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 03:43:12', '2018-12-03 03:43:12'),
(1027, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 04:50:17', '2018-12-03 04:50:17'),
(1028, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 04:50:54', '2018-12-03 04:50:54'),
(1029, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 04:52:39', '2018-12-03 04:52:39'),
(1030, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 05:11:12', '2018-12-03 05:11:12'),
(1031, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 05:38:23', '2018-12-03 05:38:23'),
(1032, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 07:06:27', '2018-12-03 07:06:27'),
(1033, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-03 07:28:28', '2018-12-03 07:28:28'),
(1034, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 08:54:43', '2018-12-03 08:54:43'),
(1035, 14, 'http://localhost/cqbycq/public', '3', '2018-12-03 09:15:13', '2018-12-03 09:15:13'),
(1036, 14, 'http://localhost/cqbycq/public', '3', '2018-12-04 02:03:03', '2018-12-04 02:03:03'),
(1037, 14, 'http://localhost/cqbycq/public', '3', '2018-12-04 02:24:17', '2018-12-04 02:24:17'),
(1038, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:01:19', '2018-12-04 04:01:19'),
(1039, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:19:43', '2018-12-04 04:19:43'),
(1040, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:34:35', '2018-12-04 04:34:35'),
(1041, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:35:14', '2018-12-04 04:35:14'),
(1042, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:56:17', '2018-12-04 04:56:17'),
(1043, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 04:57:40', '2018-12-04 04:57:40'),
(1044, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:00:08', '2018-12-04 05:00:08'),
(1045, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:12:45', '2018-12-04 05:12:45'),
(1046, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:12:54', '2018-12-04 05:12:54'),
(1047, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:13:02', '2018-12-04 05:13:02'),
(1048, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:14:23', '2018-12-04 05:14:23'),
(1049, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:14:26', '2018-12-04 05:14:26'),
(1050, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:16:24', '2018-12-04 05:16:24'),
(1051, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:16:32', '2018-12-04 05:16:32'),
(1052, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:18:32', '2018-12-04 05:18:32'),
(1053, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:20:18', '2018-12-04 05:20:18'),
(1054, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:21:56', '2018-12-04 05:21:56'),
(1055, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:22:07', '2018-12-04 05:22:07'),
(1056, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:22:21', '2018-12-04 05:22:21'),
(1057, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:23:15', '2018-12-04 05:23:15'),
(1058, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 05:23:38', '2018-12-04 05:23:38'),
(1059, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:43:28', '2018-12-04 06:43:28'),
(1060, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:43:56', '2018-12-04 06:43:56'),
(1061, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:44:20', '2018-12-04 06:44:20'),
(1062, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:44:23', '2018-12-04 06:44:23'),
(1063, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:44:31', '2018-12-04 06:44:31'),
(1064, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:45:20', '2018-12-04 06:45:20'),
(1065, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:45:22', '2018-12-04 06:45:22'),
(1066, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:45:25', '2018-12-04 06:45:25'),
(1067, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:45:40', '2018-12-04 06:45:40'),
(1068, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:45:58', '2018-12-04 06:45:58'),
(1069, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:46:44', '2018-12-04 06:46:44'),
(1070, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 06:47:26', '2018-12-04 06:47:26'),
(1071, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:08:26', '2018-12-04 07:08:26'),
(1072, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:09:33', '2018-12-04 07:09:33'),
(1073, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:22:57', '2018-12-04 07:22:57'),
(1074, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:23:10', '2018-12-04 07:23:10'),
(1075, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:23:15', '2018-12-04 07:23:15'),
(1076, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:23:30', '2018-12-04 07:23:30'),
(1077, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:23:35', '2018-12-04 07:23:35'),
(1078, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:24:26', '2018-12-04 07:24:26'),
(1079, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:41:09', '2018-12-04 07:41:09'),
(1080, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:53:21', '2018-12-04 07:53:21'),
(1081, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:53:35', '2018-12-04 07:53:35'),
(1082, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:55:20', '2018-12-04 07:55:20'),
(1083, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:58:48', '2018-12-04 07:58:48'),
(1084, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:58:52', '2018-12-04 07:58:52'),
(1085, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:59:08', '2018-12-04 07:59:08'),
(1086, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:59:32', '2018-12-04 07:59:32'),
(1087, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 07:59:57', '2018-12-04 07:59:57'),
(1088, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:00:35', '2018-12-04 08:00:35'),
(1089, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:07:25', '2018-12-04 08:07:25'),
(1090, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:07:30', '2018-12-04 08:07:30'),
(1091, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:07:39', '2018-12-04 08:07:39'),
(1092, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:28:47', '2018-12-04 08:28:47'),
(1093, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:32:41', '2018-12-04 08:32:41'),
(1094, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:32:55', '2018-12-04 08:32:55'),
(1095, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:33:32', '2018-12-04 08:33:32'),
(1096, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:34:47', '2018-12-04 08:34:47'),
(1097, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:35:56', '2018-12-04 08:35:56'),
(1098, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:36:19', '2018-12-04 08:36:19'),
(1099, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 08:48:09', '2018-12-04 08:48:09'),
(1100, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 09:37:48', '2018-12-04 09:37:48'),
(1101, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 09:44:23', '2018-12-04 09:44:23'),
(1102, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 10:20:20', '2018-12-04 10:20:20'),
(1103, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 10:24:47', '2018-12-04 10:24:47'),
(1104, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 10:45:27', '2018-12-04 10:45:27'),
(1105, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-04 11:26:19', '2018-12-04 11:26:19'),
(1106, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 04:20:24', '2018-12-05 04:20:24'),
(1107, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 07:14:07', '2018-12-05 07:14:07'),
(1108, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 07:14:48', '2018-12-05 07:14:48'),
(1109, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 08:23:03', '2018-12-05 08:23:03'),
(1110, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 08:23:17', '2018-12-05 08:23:17'),
(1111, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-05 09:00:25', '2018-12-05 09:00:25'),
(1112, 14, 'http://localhost/cqbycq/public', '3', '2018-12-05 10:53:18', '2018-12-05 10:53:18'),
(1113, 14, 'http://localhost/cqbycq/public', '3', '2018-12-05 11:35:11', '2018-12-05 11:35:11'),
(1114, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:30:55', '2018-12-11 04:30:55'),
(1115, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:31:02', '2018-12-11 04:31:02'),
(1116, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:33:51', '2018-12-11 04:33:51'),
(1117, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:33:51', '2018-12-11 04:33:51'),
(1118, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:38:10', '2018-12-11 04:38:10'),
(1119, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-11 04:54:42', '2018-12-11 04:54:42'),
(1120, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-13 02:17:38', '2018-12-13 02:17:38'),
(1121, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-13 09:23:13', '2018-12-13 09:23:13'),
(1122, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-13 09:36:05', '2018-12-13 09:36:05'),
(1123, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-13 12:02:19', '2018-12-13 12:02:19'),
(1124, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-13 12:20:41', '2018-12-13 12:20:41'),
(1125, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-17 05:00:06', '2018-12-17 05:00:06'),
(1126, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-17 09:44:04', '2018-12-17 09:44:04'),
(1127, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-17 22:07:20', '2018-12-17 22:07:20'),
(1128, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-18 00:04:49', '2018-12-18 00:04:49'),
(1129, 14, 'http://localhost/cqbycq/public', '3', '2018-12-18 00:05:35', '2018-12-18 00:05:35'),
(1130, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 03:06:55', '2018-12-19 03:06:55'),
(1131, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 03:08:58', '2018-12-19 03:08:58'),
(1132, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:09:28', '2018-12-19 04:09:28'),
(1133, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:13:17', '2018-12-19 04:13:17'),
(1134, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:13:44', '2018-12-19 04:13:44'),
(1135, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:14:21', '2018-12-19 04:14:21'),
(1136, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:14:48', '2018-12-19 04:14:48'),
(1137, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:15:03', '2018-12-19 04:15:03'),
(1138, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:16:09', '2018-12-19 04:16:09'),
(1139, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:16:14', '2018-12-19 04:16:14'),
(1140, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:16:32', '2018-12-19 04:16:32'),
(1141, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:16:36', '2018-12-19 04:16:36'),
(1142, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:39:47', '2018-12-19 04:39:47'),
(1143, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 04:50:50', '2018-12-19 04:50:50'),
(1144, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-19 06:44:28', '2018-12-19 06:44:28'),
(1145, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-20 06:51:59', '2018-12-20 06:51:59'),
(1146, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-27 05:24:09', '2018-12-27 05:24:09'),
(1147, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-27 06:18:44', '2018-12-27 06:18:44'),
(1148, NULL, 'http://localhost/cqbycq/public', '3', '2018-12-28 02:35:24', '2018-12-28 02:35:24'),
(1149, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-01 02:11:59', '2019-01-01 02:11:59'),
(1150, NULL, 'http://127.0.0.1:8000', '3', '2019-01-01 03:03:58', '2019-01-01 03:03:58'),
(1151, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-01 03:21:16', '2019-01-01 03:21:16'),
(1152, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-01 09:02:04', '2019-01-01 09:02:04'),
(1153, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-01 09:02:27', '2019-01-01 09:02:27'),
(1154, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-01 10:17:33', '2019-01-01 10:17:33'),
(1155, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 02:03:11', '2019-01-02 02:03:11'),
(1156, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 03:45:40', '2019-01-02 03:45:40'),
(1157, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 04:57:58', '2019-01-02 04:57:58'),
(1158, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 09:08:01', '2019-01-02 09:08:01'),
(1159, 14, 'http://localhost/cqbycq/public', '3', '2019-01-02 10:17:20', '2019-01-02 10:17:20'),
(1160, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 10:17:25', '2019-01-02 10:17:25'),
(1161, 14, 'http://localhost/cqbycq/public', '3', '2019-01-02 11:01:26', '2019-01-02 11:01:26'),
(1162, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-02 11:37:20', '2019-01-02 11:37:20'),
(1163, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-03 08:12:00', '2019-01-03 08:12:00'),
(1164, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-03 09:21:01', '2019-01-03 09:21:01'),
(1165, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-03 09:35:33', '2019-01-03 09:35:33'),
(1166, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-04 02:04:15', '2019-01-04 02:04:15'),
(1167, 14, 'http://localhost/cqbycq/public', '3', '2019-01-04 02:04:30', '2019-01-04 02:04:30'),
(1168, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-04 03:14:27', '2019-01-04 03:14:27'),
(1169, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-04 03:34:11', '2019-01-04 03:34:11'),
(1170, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 02:04:51', '2019-01-05 02:04:51'),
(1171, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 02:06:51', '2019-01-05 02:06:51'),
(1172, 14, 'http://localhost/cqbycq/public', '3', '2019-01-05 02:07:46', '2019-01-05 02:07:46'),
(1173, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 05:16:08', '2019-01-05 05:16:08'),
(1174, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 05:25:02', '2019-01-05 05:25:02'),
(1175, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 05:38:04', '2019-01-05 05:38:04'),
(1176, 14, 'http://localhost/cqbycq/public', '3', '2019-01-05 05:49:11', '2019-01-05 05:49:11'),
(1177, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-05 06:03:15', '2019-01-05 06:03:15'),
(1178, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-07 02:02:15', '2019-01-07 02:02:15'),
(1179, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-08 03:31:11', '2019-01-08 03:31:11'),
(1180, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-08 03:58:48', '2019-01-08 03:58:48'),
(1181, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-08 07:01:54', '2019-01-08 07:01:54'),
(1182, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-08 07:29:18', '2019-01-08 07:29:18'),
(1183, NULL, 'http://localhost/cqbycq/public', '3', '2019-01-08 09:29:29', '2019-01-08 09:29:29');

-- --------------------------------------------------------

--
-- Table structure for table `wish_list_items`
--

DROP TABLE IF EXISTS `wish_list_items`;
CREATE TABLE IF NOT EXISTS `wish_list_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wish_list_items`
--

INSERT INTO `wish_list_items` (`id`, `user_id`, `item_id`, `created_at`, `updated_at`) VALUES
(2, 14, 6, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
