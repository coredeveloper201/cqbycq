<?php use App\Enumeration\Role; ?>
@extends('layouts.home_layout')

@section('additionalCSS')
    <style>
        .slick-slide img {
            width: 100%;
        }
    </style>
    @stop

    @section('content')
            <!-- Slider -->
    <section class="home_banner_area">
        <div class="slider">
            @foreach($mainSliderImages as $banner)
                <div class="banner_item">
                    @if(strpos($banner->image_path, '.mp4') !== false)
                        <video loop muted preload="metadata">
                            <source src="{{ asset($banner->image_path) }}" type="video/mp4">
                        </video>
                    @else
                        <a href="#"><img src="{{ asset($banner->image_path) }}" alt="" class="img-fluid"></a>
                    @endif
                </div>
            @endforeach
        </div>
    </section>
    <!-- Slider -->

    <section class="front-banner show_mobile">
        <div class="container">
            @foreach($home_banners as $banner)
                <div class="list-item">
                    <img class="img-fluid" src="{{ $banner->image_path }}" alt="No image" width="1100" height="500">
                </div>
            @endforeach
        </div>
    </section>

    <!-- Sign Up -->
    <section class="signup_area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if (!Auth::check())
                        <div class="sign_up_inner text-center">
                            <h2><a href='{{ route('buyer_register') }}'>Create Account</a></h2>
                            {{--<h2>SIGN UP FOR OUR NEWSLETTER</h2>--}}
                            {{--<input type="text" class="form-control" placeholder="Enter your email here">--}}
                        </div>
                    @endif
                    <div class="social_inner">
                        <ul>
                            <li><a href="#">INSTAGRAM</a></li>
                            <li><a href="#">FACEBOOK</a></li>
                            <li><a href="#">TWITTER</a></li>
                            <li><a href="#">PINTEREST</a></li>
                            <li><a href="#">YOUTUBE</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Sign Up -->
    <!-- Default Modal-->
    <div class="modal fade" id="modalWelcome" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    {!! $welcome_msg !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            var sliders = <?php echo json_encode($mainSliderImages)?>;

            if (sliders[0].color == 'white')
                $('body').addClass('white_slide');

            $('.slider').on('afterChange', function (event, slick, currentSlide) {
                if (sliders[currentSlide].color == 'white') {
                    $('body').addClass('white_slide');
                } else {
                    $('body').removeClass('white_slide');
                }
            });
        });
        // Show Notification
        setTimeout(function(){
            var msg = "{{str_replace("'", "\'", json_encode($welcome_msg)) }}";

            if (msg != '')
                $('#modalWelcome').modal('show');
        }, 5000);
    </script>
@stop