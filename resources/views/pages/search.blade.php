@extends('layouts.app')

@section('content')
    {{-- <div class="shipping_cart_area"> --}}
    <div class="product_grid custom-pagination four_grid_area four_grid_show">
        <h2>Search result for '{{ request()->get('s') }}'</h2>

        <div class="row">
            @if (sizeof($items) > 0)
                @foreach($items as $item)
                    <div class="single-product col-3 col-sm-6 col-md-3 product_custom_padding">
                        <div class="product_grid_inner">
                            <a href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                @if (sizeof($item->images) > 0)
                                                                      <img src="{{ (!auth()->user())? $defaultItemImage_path : asset($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}" class="product-image img-fluid">

                                @else
                                    <img src="{{ asset('images/no-image.png') }}" alt="{{ $item->style_no }}" class="product-image img-fluid">
                                @endif
                            </a>
                            <h2><a class="p_title" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->name or ' ' }}</a></h2>
                        </div>
                    </div>
                @endforeach
            @else
                <b>No Item(s) Found.</b>
            @endif
        </div>
        <div class="row custom-pagination justify-content-center">
            <div class="pagination">
                {{ $items->appends(['s' => request()->get('s')])->links('others.pagination') }}
            </div>
        </div>
    </div>
@stop