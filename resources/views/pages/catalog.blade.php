<?php

use App\Enumeration\Role;
use App\Enumeration\Availability;

?>

@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fotorama.css') }}">
    <style>
        .color-selected {
            border: 1px solid black !important;
        }
    </style>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="content col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ $category->name }}</h4>
                        <div class="title-desc">
                            <span id="totalItem"></span>
                            <span>Items Found</span>
                        </div>
                    </div>

                    <div class="col-md-6 text-right">
                        <div class="column">
                            <div class="shop-sorting">
                                <label for="sorting">SORT BY</label>
                                <select class="form-control" id="sorting">
                                    <option value="5">Sort</option>
                                    <option value="1">New</option>
                                    <option value="2">Price Low-High</option>
                                    <option value="3">Price High-Low</option>
                                    <option value="4">Style No.</option>
                                </select>
                            </div>
                        </div>

                        {{--<div class="column">
                            <div class="shop-sorting">
                                <label for="sorting">ITEM PER PAGE</label>
                                <select class="form-control" id="sorting">
                                    <option selected>20</option>
                                    <option>40</option>
                                    <option>60</option>
                                    <option>80</option>
                                    <option>100</option>
                                </select>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
            {{ Breadcrumbs::render('catalog_page', $category) }}
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="content col-xl-10 col-lg-10 order-lg-2">
                <ul class="product-container-5x" id="product-container">
                    @foreach($items as $item)
                        <li>
                            <div class="product-card" style="margin-bottom: 20px;">
                                <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                        @if (sizeof($item->images) > 0)
                                            <img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset($item->images[0]->list_image_path) }}"
                                                 alt="product image">
                                        @else
                                            <img src="{{ asset('images/no-image.png') }}" alt="product image">
                                        @endif
                                </a>


                                <div class="product-title">
                                    {{--<b><a href="#">{{ $item->vendor->company_name }}</a></b>--}}
                                    <b><a href="{{ route('item_details_page', ['item' => $item->id]) }}" class="vendor-name">{{ $item->name }}</a></b>
                                </div>
                                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                <h3 class="product-title">
                                    <a href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>
                                        <span class="price">
                                        @if ($item->orig_price != null)
                                                <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                            @endif

                                            ${{ sprintf('%0.2f', $item->price) }}
                                    </span>
                                </h3>
                                @endif
                                <div class="product-extra-info">
                                    @if (sizeof($item->colors) > 1)
                                        <img class="multi-color" src="{{ asset('images/multi-color.png') }}"
                                             title="Multi Color Available">
                                    @endif

                                    @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                        <span title="Available On">
                                        <!--<img class="calendar-icon"
                                             src="{{ asset('images/calendar-icon.png') }}">-->Available on: {{ date('m/d/Y', strtotime($item->available_on)) }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>

                <div class="pagination">
                    {{--{{ $items->links('others.pagination') }}--}}
                </div>

                {{--<nav class="pagination">
                    <div class="column hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#"><i class="icon-arrow-left"></i>Previous&nbsp;</a></div>
                    <div class="column">
                        <ul class="pages">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li>...</li>
                            <li><a href="#">12</a></li>
                        </ul>
                    </div>
                    <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>
                </nav>--}}
            </div>

            <div class="sidebar col-xl-2 col-lg-2 order-lg-1">
                <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i
                            class="icon-layout"></i></button>
                <aside class="sidebar-offcanvas">
                    <!-- Widget Categories-->
                    <section class="content row widget widget-links">
                        <h3 class="widget-title">CATEGORY</h3>
                        <ul>
                            @foreach($parentCategory->subCategories as $sub)
                                <li>
                                    <a href="{{ route('third_category', ['subcategory' => changeSpecialChar($sub->name), 'category' => changeSpecialChar($sub->parentCategory->name), 'parent' => changeSpecialChar($sub->parentCategory->parentCategory->name)]) }}">{{ $sub->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </section>

                    <!-- Widget Brand Filter-->
                    <section class="content row widget widget-categories">
                        <h3 class="widget-title">SEARCH</h3>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="search-style-no"
                                   name="search-component" value="1" checked>
                            <label class="custom-control-label" for="search-style-no">Style No.</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="search-description"
                                   name="search-component" value="2">
                            <label class="custom-control-label" for="search-description">Description</label>
                        </div>

                        <div class="form-group">
                            <input class="form-control" id="search-input" type="text" placeholder="Search">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control" id="search-price-min" type="text" placeholder="Price Min">
                            </div>

                            <div class="col-md-6">
                                <input class="form-control" id="search-price-max" type="text" placeholder="Price Max">
                            </div>
                        </div>

                        <button class="btn btn-primary" id="btn-search">SEARCH</button>
                    </section>

                    <!-- Widget Brand Filter-->
                    <section class="content row widget">
                        <h3 class="widget-title">COLORS</h3>

                        <ul style="padding-left: 0px; list-style: none; display: block; clear:both; margin-bottom: 30px">
                            @foreach($masterColors as $mc)
                                <li class="item-color"
                                    style="position: relative; float: left; display: list-item; padding: 3px; border: 1px solid white"
                                    data-id="{{ $mc->id }}" title="{{ $mc->name }}">
                                    <img src="{{ asset($mc->image_path) }}" width="30px" height="20px">
                                </li>
                            @endforeach
                        </ul>
                    </section>
                </aside>
            </div>
        </div>
    </div>

    <template id="template-product">
        <li>
            <div class="product-card reveal">
                <a class="product-thumb">
                    <img class="product-image" src="{{ asset('images/no-image.png') }}">
                    <div class="hidden">
                        <img class="product-image2" src="{{ asset('images/no-image.png') }}">
                    </div><!-- end of .hidden -->
                </a>
                <div class="caption">
                    <div class="centered show_quick_view" data-pid="">
                        Quick View
                    </div><!-- end of .centered -->
                </div><!-- end of .caption -->
                <div class="product-title"><a href="#" class="vendor-name"><b class="p_title"></b></a></div>
                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                <h3 class="product-title">
                    <a class="style-no"></a>
                    <span class="price"></span>
                </h3>
                @endif
                <div class="product-extra-info">
                    <img class="multi-color d-none" src="{{ asset('images/multi-color.png') }}" title="Multi Color Available">

                    <span class="available-on d-none" title="Available On">
                    <img class="calendar-icon" src="{{ asset('images/calendar-icon.png') }}"> <span
                                class="available-date"></span>
                </span>
                </div>
            </div>
        </li>
    </template>
@stop

@section('modal')
<div class="modal fade" id="quickViewArea" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <div class="product-img-box">
                            <div id="fotormaPopup" class="fotorama" data-nav="thumbs" data-thumbwidth="80" data-thumbheight="120" data-width="100%"></div>
                        </div>
                    </div>

                    <div class="col-md-7">
                        <div class="pt-1 mb-2"><span class="text-medium stock_item_preview">Stock:</span>

                        </div>
                        <div class="product-title"><h1></h1></div>
                        <div class="product-sku"><h2 class="text-normal"><span class="text-medium">Style#:</span><span class="product_style_sku"></span> </h2>
                        </div>
                        <div class="product_price"></div>

                        <div class="modal_color_table">

                        </div>

                        <div class="d-flex flex-wrap justify-content-between">
                            <div class="sp-buttons mt-2 mb-2">
                                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                    <button class="btn btn-primary" id="btnAddToCart"><i class="icon-bag"></i> Add to Shopping Bag</button>

                                    <a href="{{ route('show_cart') }}" class="btn btn-secondary"> Checkout</a>
                                @else
                                    <a href="/login" class="btn btn-primary">Login to Add to Cart</a>
                                @endif
                            </div>
                        </div>

                        <input type="hidden" id="itemInPack">
                        <input type="hidden" id="itemPrice">
                        <input type="hidden" id="modalItemId">
                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                            <input type="hidden" id="loggedIn" value="true">
                        @else
                            <input type="hidden" id="loggedIn" value="false">
                        @endif

                        <br>
                        <h4>Description</h4>
                        <p class="text-xs description"> Description</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalShopFilters" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Filters</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <section class="widget widget-links">
                    <h3 class="widget-title">CATEGORY</h3>
                    <ul>
                        @foreach($parentCategory->subCategories as $sub)
                            <li>
                                <a href="{{ route('third_category', ['category' => $sub->name, 'second' => $sub->parentCategory->name, 'parent' => $sub->parentCategory->parentCategory->name]) }}">{{ $sub->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </section>

                <!-- Widget Brand Filter-->
                <section class="widget widget-categories">
                    <h3 class="widget-title">SEARCH</h3>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input class="custom-control-input" type="radio" id="mobile-search-style-no"
                               name="search-component" value="1" checked>
                        <label class="custom-control-label" for="mobile-search-style-no">Style No.</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input class="custom-control-input" type="radio" id="mobile-search-description"
                               name="search-component" value="2">
                        <label class="custom-control-label" for="mobile-search-description">Description</label>
                    </div>

                    <div class="form-group">
                        <input class="form-control" id="search-input" type="text" placeholder="Search">
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" id="search-price-min" type="text" placeholder="Price Min">
                        </div>

                        <div class="col-md-6">
                            <input class="form-control" id="search-price-max" type="text" placeholder="Price Max">
                        </div>
                    </div>

                    <button class="btn btn-primary" id="btn-search">SEARCH</button>
                </section>


                <!-- Widget Brand Filter-->
                <section class="widget">
                    <h3 class="widget-title">COLORS</h3>

                    <ul style="padding-left: 0px; list-style: none; display: block; clear:both; margin-bottom: 30px">
                        @foreach($masterColors as $mc)
                            <li class="item-color"
                                style="position: relative; float: left; display: list-item; padding: 3px; border: 1px solid white"
                                data-id="{{ $mc->id }}" title="{{ $mc->name }}">
                                <img src="{{ asset($mc->image_path) }}" width="30px" height="20px">
                            </li>
                        @endforeach
                    </ul>
                </section>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/fotorama.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var page = 1;
            var search_text = '';
            var search_option = '';
            var search_price_min = '';
            var search_price_max = '';


            $('.checkbox-category, .vendor-checkbox, .checkbox-body-size, .checkbox-pattern, .checkbox-length, .checkbox-style, .checkbox-fabric').change(function () {
                filterItem();
            });

            $('#sorting').change(function () {
                filterItem();
            });

            $('.item-color').click(function () {
                if ($(this).hasClass('color-selected'))
                    $(this).removeClass('color-selected');
                else
                    $(this).addClass('color-selected');

                filterItem();
            });

            $('#btn-search').click(function () {
                search_text = $('#search-input').val();
                search_option = $('input[name=search-component]:checked').val();
                search_price_min = $('#search-price-min').val();
                search_price_max = $('#search-price-max').val();

                filterItem();
            });

            function filterItem(page) {
                page = typeof page !== 'undefined' ? page : 1;
                var categories = ['{{ $category->id }}'];
                var vendors = [];
                var masterColors = [];
                var bodySizes = [];
                var patterns = [];
                var lengths = [];
                var styles = [];
                var fabrics = [];
                var sorting = $('#sorting').val();

                // Vendor
                $('.vendor-checkbox').each(function () {
                    if ($(this).is(':checked'))
                        vendors.push($(this).data('id'));
                });

                // Master Color
                $('.item-color').each(function () {
                    if ($(this).hasClass('color-selected'))
                        masterColors.push($(this).data('id'));
                });

                // Body Size
                $('.checkbox-body-size').each(function () {
                    if ($(this).is(':checked'))
                        bodySizes.push($(this).data('id'));
                });

                // Pattern
                $('.checkbox-pattern').each(function () {
                    if ($(this).is(':checked'))
                        patterns.push($(this).data('id'));
                });

                // Length
                $('.checkbox-length').each(function () {
                    if ($(this).is(':checked'))
                        lengths.push($(this).data('id'));
                });

                // Style
                $('.checkbox-style').each(function () {
                    if ($(this).is(':checked'))
                        styles.push($(this).data('id'));
                });

                // Master Fabric
                $('.checkbox-fabric').each(function () {
                    if ($(this).is(':checked'))
                        fabrics.push($(this).data('id'));
                });


                $.ajax({
                    method: "POST",
                    url: "{{ route('get_items_sub_category') }}",
                    data: {
                        categories: categories,
                        vendors: vendors,
                        masterColors: masterColors,
                        bodySizes: bodySizes,
                        patterns: patterns,
                        lengths: lengths,
                        styles: styles,
                        fabrics: fabrics,
                        sorting: sorting,
                        searchText: search_text,
                        searchOption: search_option,
                        priceMin: search_price_min,
                        priceMax: search_price_max,
                        page: page
                    }
                }).done(function (data) {
                    var products = data.items.data;
                    $('.pagination').html(data.pagination);
                    $('#totalItem').html(data.items.total);

                    $('#product-container').html('');
                    var backOrder = '{{ Availability::$ARRIVES_SOON }}';

                    $.each(products, function (index, product) {
                        var html = $('#template-product').html();
                        var row = $(html);

                        row.find('.product-title b.p_title').html(product.name);
                        row.find('.product-title a').attr('href', product.detailsUrl);
                        row.find('.product-image').attr('src', product.imagePath);
                        if(product.imagePath2){
                            row.find('.product-image2').attr('src', product.imagePath2);
                        }else{
                            row.find('.product-image2').parent().addClass('no_image');
                        }
                        row.find('.product-thumb').attr('href', product.detailsUrl);
                        // row.find('.vendor-name').html(product.vendor.company_name);
                        row.find('.vendor-name').attr('href', product.vendorUrl);
                        row.find('.style-no').html(product.style_no);
                        row.find('.style-no').attr('href', product.detailsUrl);
                        row.find('.price').html(product.price);
                        row.find('.show_quick_view').attr('data-pid', product.id);

                        if (product.colors.length > 1)
                            row.find('.multi-color').removeClass('d-none');

                        if (product.colors.length == 0)
                            row.find('.product-thumb').removeAttr("href");

                        if (product.availability == backOrder && product.available_on != '') {
                            row.find('.available-on').removeClass('d-none');
                            var date = new Date(product.available_on);
                            row.find('.available-date').html((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear());
                        }

                        $('#product-container').append(row);
                    });

                    var pos = 0;
                    var changePos = localStorage['change_pos'];
                    if (changePos) {
                        localStorage.removeItem('change_pos');

                        pos = parseInt(localStorage.getItem('previous_position'));
                    }

                    $("html, body").animate({ scrollTop: pos }, "fast");
                });
            }

            // WishList
            $(document).on('click', '.btnAddWishList', function () {
                var id = $(this).data('id');
                $this = $(this);

                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_wishlist') }}",
                    data: {id: id}
                }).done(function (data) {
                    toastr.success('Added to Wish List.');

                    $this.removeClass('btnAddWishList');
                    $this.removeClass('btn-default');
                    $this.addClass('btnRemoveWishList');
                    $this.addClass('btn-danger');
                });
            });

            // Click to Show Quick View Functionalities
            $(document).on('click', '.show_quick_view', function () {
                var pid = $(this).attr('data-pid');
                $.ajax({
                    method: "POST",
                    url: "{{ route('quick_view_item') }}",
                    data: {item: pid}
                }).done(function (data) {
                    if(data.item){
                        var item = data.item;
                        // var $fotoramaDiv = $('.fotorama').fotorama();
                        // var fotorama = $fotoramaDiv.data('fotorama');
                        // if(fotorama){
                        //     fotorama.destroy();
                        // }
                        if(item && item.images && item.images.length){
                            $('#itemPrice').val(item.price);
                            $('#modalItemId').val(item.id);
                            var loggedIn = $('#loggedIn').val();
                            var lPriceRow, ltPriceRow;

                            if(loggedIn && loggedIn === 'true'){
                                lPriceRow = '<span class="price">$0.00</span> <input class="input-price" type="hidden" value="0">';
                                ltPriceRow = '<b><span id="totalPrice">$0.00</span></b>';
                            }else{
                                lPriceRow = '<span class="login">$xxx</span>';
                                ltPriceRow = '<b><span id="loginrequire">$xxx</span></b>';
                            }

                            // Table Maker
                            var cTable = '<table class="table table-bordered">\n' +
                                '<thead class="bgfame">\n' +
                                '<tr>' +
                                '<th></th><th>Color</th>';

                            cTable+='<th width="20%">Min Qty</th>' +
                                '<th class="hidden-sm-down" width="20%">Qty</th>' +
                                '<th width="20%">Amount</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody class="text-sm">';

                            $.each(item.colors, function (i, color) {
                                cTable+='<tr>';

                                if (color.image != '')
                                    cTable+='<td><a href="" data-index="'+color.image_index+'" class="btnThumb"><img src="'+color.image+'" style="height: 50px"></a></td>';
                                else
                                    cTable+='<td></td>';

                                cTable+='<td>'+color.name+'</td>';
                                cTable+='<td>'+item.min_qty+'</td>';
                                cTable+='<td><input class="form-control pack" data-color="'+color.id+'" name="input-pack[]" type="text"></td>';
                                cTable+='<td>'+ lPriceRow +'</td>';
                                cTable+='</tr>';
                            });

                            cTable+='<tr>';
                            cTable+='<td colspan="3"><b>Total</b></td>' +
                                '<td><b><span id="totalQty">0</span></b></td>' +
                                '<td>'+ltPriceRow+'</td>';
                            cTable+='</tr>';


                            cTable+='</tbody>' +
                                '</table>';

                            $('.modal_color_table').html(cTable);


                            $('.product_style_sku').html(item.style_no);

                            if (item.price != '') {
                                if (item.orig_price) {
                                    $('.product_price').html('<span class="h2 d-block"> <del>$' + parseFloat(item.orig_price).toFixed(2) + '</del> $' + parseFloat(item.price).toFixed(2) + '</span>');
                                } else {
                                    $('.product_price').html('<span class="h2 d-block">$' + parseFloat(item.price).toFixed(2) + '</span>');
                                }
                            }

                            var imgData = item.images.map(function (image) {
                                return {img: image.image_path, thumb: image.thumbs_image_path, full: image.image_path};
                            });

                            // new fotorama load
                            $imgSlider = '<div id="fotormaPopup" class="fotorama" data-nav="thumbs" data-thumbwidth="80" data-thumbheight="120" data-width="100%">';
                            item.images.forEach(function (image) {
                                $imgSlider+= '<a href="'+image.image_path+'"><img src="'+image.image_path+'"></a>';
                            });
                            $imgSlider+= '</div>';
                            $('.product-img-box').html($imgSlider);

                            $('#fotormaPopup').fotorama();

                            // new fotorama load


                            // setTimeout(function () {
                            //     $('.fotorama').fotorama().data('fotorama').resize({width: '100%'});
                            // }, 1000);

                            // $.each(item.images, function (index, image) {
                            //     $('.product-img-box .fotorama').html('<a href="'+image.image_path+'"><img src="'+image.list_image_path+'"></a>');
                            // });
                            $('.product-title h1').html(item.name);
                            var itemAvailability = ['Unspecified', 'Arrives soon / Back Order', 'In Stock'];
                            $('.stock_item_preview').html('Stock: ' + itemAvailability[item.availability])
                            $('#quickViewArea .description').html(item.description)
                        }
                        setTimeout(function () {
                            $('#quickViewArea').modal('show');
                        });
                    }
                });

            });

            $(document).on('click', '.btnThumb', function (e) {
                e.preventDefault();
                var index = parseInt($(this).data('index'));

                var $fotoramaDiv = $('#fotormaPopup').fotorama();
                var fotorama = $fotoramaDiv.data('fotorama');

                fotorama.show(index);
            });

            var totalQty = 0;
            $(document).on('keyup', '.pack', function () {
                var i = 0;
                var val = $(this).val();

                if (isInt(val)) {
                    i = parseInt(val);

                    if (i < 0)
                        i = 0;
                }

                var perPrice = $('#itemPrice').val();

                $(this).closest('tr').find('.qty').html( i);
                $(this).closest('tr').find('.price').html('$' + (i * perPrice).toFixed(2));
                $(this).closest('tr').find('.input-price').val(i * perPrice);

                calculate();

                $(this).focus();
            });
            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }
            function calculate() {
                totalQty = 0;
                var totalPrice = 0;

                $('.pack').each(function () {
                    i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            i = 0;
                    }

                    totalQty += i;
                });

                $('.input-price').each(function () {
                    totalPrice += parseFloat($(this).val());
                });

                $('#totalQty').html(totalQty);
                $('#totalPrice').html('$' + totalPrice.toFixed(2));
            }
            $(document).on('click', '#btnAddToCart', function () {
                var colors = [];
                var qty = [];
                var vendor_id = '';


                if (totalQty == 0) {
                    alert('Please select an item.');
                    return;
                }

                var valid = true;
                $('.pack').each(function () {
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            return valid = false;
                    } else {
                        if (val != '')
                            return valid = false;
                    }

                    if (i != 0) {
                        colors.push($(this).data('color'));
                        qty.push(i);
                    }
                });

                if (!valid) {
                    alert('Invalid Quantity.');
                    return;
                }

                var itemId = $('#modalItemId').val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_cart') }}",
                    data: { itemId: itemId, colors: colors, qty: qty, vendor_id: vendor_id }
                }).done(function( data ) {
                    if (data.success)
                        window.location.replace("{{ route('add_to_cart_success') }}");
                    else
                        alert(data.message);
                });
            });

            $(document).on('click', '.btnRemoveWishList', function () {
                var id = $(this).data('id');
                $this = $(this);

                $.ajax({
                    method: "POST",
                    url: "{{ route('remove_from_wishlist') }}",
                    data: {id: id}
                }).done(function (data) {
                    toastr.success('Remove from Wish List.');

                    $this.removeClass('btnRemoveWishList');
                    $this.removeClass('btn-danger');
                    $this.addClass('btnAddWishList');
                    $this.addClass('btn-default');
                });
            });

            // Pagination
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                page = getURLParameter(url, 'page');

                filterItem(page);
            });

            function getURLParameter(url, name) {
                return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
            }

            // Hold Position
            $(window).bind('beforeunload', function(){
                localStorage['previous_page'] = page;
                localStorage['previous_position'] = $(document).scrollTop()+'';
            });

            var changePage = localStorage['change_page'];
            if (changePage) {
                localStorage.removeItem('change_page');

                page = parseInt(localStorage.getItem('previous_page'));
            }

            filterItem(page);
        });
    </script>
@stop