@extends('layouts.app')

@section('content')
    <section class="shipping_cart_area">
        <form action="{{ route('single_checkout_post') }}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ request()->get('id') }}" id="orders">

            <div class="row">
                <div class="col-md-7">
                    <div class="content margin-bottom-1x mb-4">
                        <h3>Shipping Address</h3>
                        <p id="address_text">
                            @if ($address != null)
                                {{ $address->address }}, {{ $address->city }}, {{ ($address->state == null) ? $address->state_text : $address->state->name }},
                                <br>
                                {{ $address->country->name }} - {{ $address->zip }}
                            @endif
                        </p>

                        <button class="btn btn-primary" role="button" id="btnChangeAddress">Change</button>
                        <button class="btn btn-primary" role="button" id="btnAddShippingAddress">Add New Shipping Address</button>
                        <input type="hidden" name="address_id" value="{{ ($address != null) ? $address->id : '' }}" id="address_id">
                        <hr>
                        <div class="row">
                                  @csrf
                                <div class="col-md-8">
                                    <div class="cart_bottom_inner">
                                        <input type="text" id="coupon_{{ $order->id }}" value="{{ $order->coupon }}" placeholder="Enter your promo code" class="form-control" name="code">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="cart_bottom_inner">
                                    <button type="submit" class="btn btn-outline-primary btnApplyCoupon" data-order-id="{{ $order->id }}">Apply</button>
                                    </div>
                                </div>
                        </div>

                    </div>

                    <div class="content margin-bottom-1x">
                        <h3>Shipping Method</h3>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="thead-default">
                                <tr>
                                    <th></th>
                                    <th>Shipping method</th>
                                    <th>Fee</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($shipping_methods as $shipping_method)
                                    <tr>
                                        <td class="align-middle">
                                            <div class="custom-control custom-radio mb-0">
                                                <input class="custom-control-input shipping_method" type="radio"
                                                       id="{{ $shipping_method->id }}" name="shipping_method"
                                                       value="{{ $shipping_method->id }}" data-index="{{ $loop->index }}"
                                                        {{ old('shipping_method') == $shipping_method->id ? 'checked' : '' }}>
                                                <label class="custom-control-label"
                                                       for="{{ $shipping_method->id }}"></label>
                                            </div>
                                        </td>

                                        <td class="align-middle">
                                            <span class="text-medium">{{ $shipping_method->courier->name }}</span><br>
                                            <span class="text-muted text-sm">{{ $shipping_method->name }}</span>
                                        </td>

                                        <td>
                                            @if ($shipping_method->fee === null)
                                                Actual Rate
                                            @else
                                                ${{ number_format($shipping_method->fee, 2, '.', '') }}
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                            </table>

                            @if ($errors->has('shipping_method'))
                                <div class="form-control-feedback text-danger">Select a shipping method</div>
                            @endif
                        </div>

                        <p class="text-muted">
                            Flat rate prices are for Continental US ONLY <br>
                            Prices for Expedited shipping will be determined by weight, dimensions, and shipping address
                        </p>
                    </div>

                    <div class="content">
                        <h3>Payment Method</h3>
                        <p class="text-danger">Please click on the title below to select your payment method</p>
                        <input type="hidden" id="paymentMethod" name="paymentMethod" value="{{ old('paymentMethod') }}">

                        @if ($errors->has('paymentMethod'))
                            <div class="form-control-feedback text-danger">Select a Payment Method</div>
                        @endif

                        <div class="accordion" id="accordion1" role="tablist">
                            <div class="card">
                                {{-- <div class="card-header" role="tab">
                                    <h6><a href="#collapseOne" data-toggle="collapse" class="btnPM {{ old('paymentMethod') == '1' ? '' : 'collapsed' }}" aria-expanded="{{ old('paymentMethod') == '1' ? 'true' : 'false' }}" data-id="1">Wire Transfer</a></h6>
                                </div> --}}
                                <div class="collapse {{ old('paymentMethod') == '1' ? 'show' : '' }}" id="collapseOne" data-parent="#accordion1" role="tabpanel" style="">
                                    <div class="card-body">We will contact you about the total amount and account information for payment.</div>
                                </div>
                            </div>
                            <div class="card credit-card">
                                <div class="card-header" role="tab">
                                    <h6><a class="{{ old('paymentMethod') == '2' ? '' : 'collapsed' }} btnPM" href="#collapseTwo" data-toggle="collapse" aria-expanded="{{ old('paymentMethod') == '2' ? 'true' : 'false' }}" data-id="2">Credit Card</a></h6>
                                </div>
                                <div class="credit-card-collapse collapse {{ old('paymentMethod') == '2' ? 'show' : '' }}" id="collapseTwo" data-parent="#accordion1" role="tabpanel" style="">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group col-sm-6{{ $errors->has('name') ? ' has-danger' : '' }}">
                                                    <div class="mb-2">Card Holder's Name:</div>
                                                    <input class="form-control" type="text" name="name" placeholder="Full Name"
                                                           value="{{ empty(old('name')) ? ($errors->has('name') ? '' : $order->card_full_name) : old('name') }}">
                                                </div>
                                                <div class="form-group col-sm-6{{ $errors->has('number') ? ' has-danger' : '' }}">
                                                    <div class="mb-2">Card Number:</div>
                                                    <input class="form-control" type="text" name="number" placeholder="Card Number"
                                                           value="{{ empty(old('number')) ? ($errors->has('number') ? '' : $order->card_number) : old('number') }}">
                                                </div>
                                                <div class="form-group col-sm-3{{ $errors->has('expiry') ? ' has-danger' : '' }}">
                                                    <div class="mb-2">Expiration Date:</div>
                                                    <input class="form-control" type="text" name="expiry" placeholder="MM/YY"
                                                           data-inputmask="'mask': '99/99'" id="expiry"
                                                           value="{{ empty(old('expiry')) ? ($errors->has('expiry') ? '' : $order->card_expire) : old('expiry') }}">
                                                </div>
                                                <div class="form-group col-sm-3{{ $errors->has('cvc') ? ' has-danger' : '' }}">
                                                    <div class="mb-2"> Secure Code (CVV2):</div>
                                                    <input class="form-control" type="text" name="cvc" placeholder="CVC"
                                                           value="{{ empty(old('cvc')) ? ($errors->has('cvc') ? '' : $order->card_cvc) : old('cvc') }}">
                                                </div>
                                                <p>CVV2 is that extra set of numbers after the normal 16 or 14 digits of the account usually printed on the back of the credit card. The "CVV2 security code", as it is formally referred to, provides an extra measure of security and we require it on all transactions.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab">
                                    <h6><a class="{{ old('paymentMethod') == '3' ? '' : 'collapsed' }} btnPM" href="#collapseThree" data-toggle="collapse" data-id="3" aria-expanded="{{ old('paymentMethod') == '3' ? 'true' : 'false' }}">PayPal</a></h6>
                                </div>
                                <div class="collapse {{ old('paymentMethod') == '3' ? 'show' : '' }}" id="collapseThree" data-parent="#accordion1" role="tabpanel">
                                    <div class="card-body">
                                        Redirect to PayPal Authentication
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="sidebar col-md-5">
                    <div class="content">
                        <h3>Order Summary</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Style No</th>
                                    <th>Color</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($order->items as $item)
                                    <tr>
                                        <td>
                                            <?php
                                            $thumb = null;

                                            for($i=0; $i < sizeof($item->item->images); $i++) {
                                                if ($item->item->images[$i]->color != null) {
                                                    if ($item->item->images[$i]->color->name == $item->color) {
                                                        $thumb = $item->item->images[$i];
                                                        break;
                                                    }
                                                }
                                            }
                                            ?>

                                            @if ($thumb)
                                                <img src="{{ asset($thumb->list_image_path) }}" width="50px">
                                            @endif

                                        </td>
                                        <td>{{ $item->style_no }}</td>
                                        <td>{{ $item->color }}</td>
                                        <td>${{ number_format($item->per_unit_price, 2, '.', '') }}</td>
                                        <td>{{ $item->total_qty }}</td>
                                        <td>${{ number_format($item->amount, 2, '.', '') }}</td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <th colspan="5">Sub Total</th>
                                    <td>${{ number_format($order->subtotal, 2, '.', '') }}</td>
                                </tr>
                                @if ($order->discount != null || $order->discount != 0)
                                 <tr>
                                     <th colspan="5">Discount</th>
                                    <td>-${{ number_format($order->discount, 2, '.', '') }}</td>
                                </tr>
                                @endif

                                    <tr>
                                    <th colspan="5">Store Credit</th>
                                    <td>${{ number_format($order->store_credit, 2, '.', '') }}</td>
                                </tr>

                                <tr>
                                    <th colspan="5">Shipping Cost</th>
                                    <td>
                                        <span id="shippingCost">${{ number_format($order->shipping_cost, 2, '.', '') }}</span>
                                    </td>
                                </tr>

                                <tr>
                                    <th colspan="5">Total</th>
                                    <td><span id="total">${{ number_format($order->total, 2, '.', '') }}</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <h3>Note</h3>
                        <textarea class="form-control" name="order_note" cols="30" rows="10"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-4">
                    <div class="content">
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input class="custom-control-input" type="checkbox" name="checkbox-agree" id="checkbox-agree">
                            <label class="custom-control-label" for="checkbox-agree">By Selecting this box and clicking the "Place My Order button", I agree that I have read the Policy. Your order may not be complete! Would you like us to contact you before shipping your order?</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="can_call_yes" name="can_call" value="1">
                            <label class="custom-control-label" for="can_call_yes">YES! Please call me if anything is missing from my order!</label>
                        </div>

                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="can_call_no" name="can_call" value="0" checked>
                            <label class="custom-control-label" for="can_call_no">NO! Do not call me if something is missing. Just ship me what you have! </label>
                        </div>
                    </div>
                </div>
            </div>

            <input id="btnSubmit" type="submit" class="btn btn-primary place-my-order-btn" value="Place My Order button">
            {{-- <p class="show_dekstop">
                If your order is declined, please call us at (213)489-4610. DO NOT try to charge the card again.
            </p> --}}
        </form>
    </section>

    <div class="modal fade" id="addEditShippingModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <form id="modalForm">
                <input type="hidden" id="editAddressId" name="id">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Shipping Address</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="small-rounded-input">Location</label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input location" type="radio" id="locationUS" name="location" value="US" checked>
                                        <label class="custom-control-label" for="locationUS">United States</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input location" type="radio" id="locationCA" name="location" value="CA">
                                        <label class="custom-control-label" for="locationCA">Canada</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input location" type="radio" id="locationInt" name="location" value="INT">
                                        <label class="custom-control-label" for="locationInt">International</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Store No.</label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="store_no" name="store_no">
                                </div>
                            </div>

                            <div class="col-md-7">
                                <div class="form-group" id="form-group-address">
                                    <label for="small-rounded-input">Address <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="address" name="address">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="small-rounded-input">Unit #</label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="unit" name="unit">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="form-group-city">
                                    <label for="small-rounded-input">City <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="city" name="city">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" id="form-group-state">
                                    <label for="small-rounded-input">State <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="state" name="state">
                                </div>

                                <div class="form-group" id="form-group-state-select">
                                    <label for="small-rounded-input">State <span class="required">*</span></label>
                                    <select class="form-control form-control-rounded form-control-sm" id="stateSelect" name="stateSelect">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="form-group-country">
                                    <label for="small-rounded-input">Country <span class="required">*</span></label>
                                    <select class="form-control form-control-rounded form-control-sm" id="country" name="country">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option data-code="{{ $country->code }}" value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" id="form-group-zip">
                                    <label for="small-rounded-input">Zip Code <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="zipCode" name="zipCode">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="form-group-phone">
                                    <label for="small-rounded-input">Phone <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="phone" name="phone">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="small-rounded-input">Fax</label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="fax" name="fax">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="showroomCommercial" name="showroomCommercial" value="1">
                                        <label class="custom-control-label" for="showroomCommercial">This address is commercial.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-secondary btn-sm" type="button" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary btn-sm" type="button" id="modalBtnAdd">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="selectShippingModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Shipping Address</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-bordered">
                                @foreach($shippingAddresses as $address)
                                    <tr>
                                        <td>
                                            <b>{{ $address->store_no }}</b><br>
                                            {{ $address->address }}, {{ $address->city }}, {{ ($address->state == null) ? $address->state_text : $address->state->name }},
                                            <br>
                                            {{ $address->country->name }} - {{ $address->zip }}
                                        </td>

                                        <td class="text-center">
                                            <button class="btn btn-primary btnSelectAddress" data-index="{{ $loop->index }}" data-id="{{ $address->id }}">Select</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/inputmask/js/inputmask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/inputmask/js/jquery.inputmask.js') }}"></script>
    <script>
        $(function () {
            /*Add new shipping address text change*/
            if ( $(window).width() <= 480 ) {
                $('#btnAddShippingAddress').text("Add New Shipping");

                // Open credit card by default
                $('.credit-card-collapse').removeClass('collapse');
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            var shippingAddresses = <?php echo json_encode($shippingAddresses); ?>;
            var shippingMethods = <?php echo json_encode($shipping_methods); ?>;
            var usStates = <?php echo json_encode($usStates); ?>;
            var caStates = <?php echo json_encode($caStates); ?>;

            $('#expiry').inputmask();


            $('.shipping_method').change(function () {
                var index = parseInt($(".shipping_method:checked").data('index'));
                var storeCredit = parseFloat('{{ $order->store_credit }}');

                if (!isNaN(index)) {
                    var subTotal = parseFloat('{{ $order->subtotal }}');

                    var discount = parseFloat('{{ $order->discount }}');

                    var sm = shippingMethods[index];

                    if (sm.fee === null)
                        shipmentFee = 0;
                    else
                        shipmentFee = parseFloat(sm.fee);

                    $('#total').html('$' + ((subTotal - discount)+ shipmentFee - storeCredit).toFixed(2));
                    $('#shippingCost').html('$' + shipmentFee.toFixed(2));
                }
            });
            //promo code
            $('.btnApplyCoupon').click(function (e) {
                e.preventDefault();

                var orderId = $(this).data('order-id');
                var coupon = $('#coupon_'+orderId).val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('buyer_apply_coupon') }}",
                    data: { id: orderId, coupon: coupon }
                }).done(function( data ) {
                    if (data.success) {
                        location.reload();
                    } else {
                        alert(data.message);
                    }
                });
            });

            $('.shipping_method').trigger('change');

            $('.btnPM').click(function () {
                var id = $(this).data('id');

                if($(this).attr('aria-expanded') == 'true') {
                    $('#paymentMethod').val('');
                } else {
                    $('#paymentMethod').val(id);
                }

            });

            $('#checkbox-agree').change(function () {
                if ($(this).is(':checked')) {
                    $('#btnSubmit').prop('disabled', false);
                } else {
                    $('#btnSubmit').prop('disabled', true);
                }
            });

            $('#checkbox-agree').trigger('change');

            // Shipping Address
            $('#btnChangeAddress').click(function (e) {
                e.preventDefault();
                $('#selectShippingModal').modal('show');
            });

            $('.btnSelectAddress').click(function () {
                var id = $(this).data('id');
                var index = $(this).data('index');

                $('#address_id').val(id);

                var address = shippingAddresses[index];

                $('#address_text').html(address.address + ', ' + address.city + ', ');

                if (address.state == null) {
                    $('#address_text').append(address.state_text + ', ');
                } else {
                    $('#address_text').append(address.state.name + ', ');
                }

                $('#address_text').append('<br>' + address.country.name + ' - ' + address.zip);

                $('#selectShippingModal').modal('hide');
            });

            $('#btnAddShippingAddress').click(function (e) {
                e.preventDefault();
                $('#addEditShippingModal').modal('show');
            });

            $('.location').change(function () {
                var location = $('.location:checked').val();

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#country').val('1');
                    else
                        $('#country').val('2');

                    $('#country').prop('disabled', 'disabled');
                    $('#form-group-state-select').show();
                    $('#stateSelect').val('');
                    $('#form-group-state').hide();

                    $('#stateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#country').prop('disabled', false);
                    $('#form-group-state-select').hide();
                    $('#form-group-state').show();
                    $('#country').val('');
                }
            });

            $('.location').trigger('change');

            $('#country').change(function () {
                var countryId = $(this).val();

                if (countryId == 1) {
                    $("#locationUS").prop("checked", true);
                    $('.location').trigger('change');
                } else if (countryId == 2) {
                    $("#locationCA").prop("checked", true);
                    $('.location').trigger('change');
                }
            });

            $('#modalBtnAdd').click(function () {
                if (!shippingAddressValidate()) {
                    $('#country').prop('disabled', false);

                    $.ajax({
                        method: "POST",
                        url: "{{ route('buyer_add_shipping_address') }}",
                        data: $('#modalForm').serialize(),
                    }).done(function( data ) {
                        setAddressId(data.id);
                    });

                    $('#country').prop('disabled', true);
                }
            });

            function setAddressId(id) {
                var orders = $('#orders').val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('checkout_address_select') }}",
                    data: { shippingId: id, id: orders },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            }

            $('#addEditShippingModal').on('hide.bs.modal', function (event) {
                $("#locationUS").prop("checked", true);
                $('.location').trigger('change');

                $('#store_no').val('');
                $('#address').val('');
                $('#unit').val('');
                $('#city').val('');
                $('#stateSelect').val('');
                $('#state').val('');
                $('#zipCode').val('');
                $('#phone').val('');
                $('#fax').val('');
                $('#showroomCommercial').prop('checked', false);

                clearModalForm();
            });

            function clearModalForm() {
                $('#form-group-address').removeClass('has-danger');
                $('#form-group-city').removeClass('has-danger');
                $('#form-group-state-select').removeClass('has-danger');
                $('#form-group-state').removeClass('has-danger');
                $('#form-group-country').removeClass('has-danger');
                $('#form-group-zip').removeClass('has-danger');
                $('#form-group-phone').removeClass('has-danger');
            }

            function shippingAddressValidate() {
                var error = false;
                var location = $('.location:checked').val();

                clearModalForm();

                if ($('#address').val() == '') {
                    $('#form-group-address').addClass('has-danger');
                    error = true;
                }

                if ($('#city').val() == '') {
                    $('#form-group-city').addClass('has-danger');
                    error = true;
                }

                if ((location == 'US' || location == 'CA') && $('#stateSelect').val() == '') {
                    $('#form-group-state-select').addClass('has-danger');
                    error = true;
                }

                if (location == 'INT' && $('#state').val() == '') {
                    $('#form-group-state').addClass('has-danger');
                    error = true;
                }

                if ($('#country').val() == '') {
                    $('#form-group-country').addClass('has-danger');
                    error = true;
                }

                if ($('#zipCode').val() == '') {
                    $('#form-group-zip').addClass('has-danger');
                    error = true;
                }

                if ($('#phone').val() == '') {
                    $('#form-group-phone').addClass('has-danger');
                    error = true;
                }

                return error;
            }
        });
    </script>
@stop