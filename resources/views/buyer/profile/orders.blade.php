<?php use App\Enumeration\OrderStatus; ?>
@extends('layouts.my_account')

@section('content')
    <div class="table-responsive">
        <table class="table table-hover margin-bottom-none">
            <thead>
                <tr>
                    <th>Order #</th>
                    <th>Date Purchased</th>
                    <th>Status</th>
                    <th>Shipped At</th>
                    <th>Total</th>
                    <th>Tracking Number</th>
                    <th>Images</th>
                </tr>
            </thead>

            <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td><a class="text-medium navi-link" href="{{ route('show_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                        <td>{{ date('F d, Y', strtotime($order->created_at)) }}</td>
                        <td>
                            {{ $order->statusText() }}
                        </td>
                        <td>
                            @if ($order->status == OrderStatus::$FULLY_SHIPPED_ORDER)
                                {{ date('F j, Y H:i a', strtotime($order->fully_shipped_at)) }}
                            @endif
                        </td>
                        <td><span class="text-medium">${{ sprintf('%0.2f', $order->total) }}</span></td>
                        <td><a href="https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums={{ $order->tracking_number }}" target="_blank">{{ $order->tracking_number }}</a></td>
                        <td><a href="{{ route('download_order_images', ['order' => $order->id]) }}" target="_blank">Download</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop