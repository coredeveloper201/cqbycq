@extends('admin.layouts.main')

@section('additionalCSS')
    <style>
        .required {
            color: red;
        }

        .form-group label {
            padding-left: 0px !important;
        }
        .custom-radio [type="radio"]:checked, .custom-radio [type="radio"]:not(:checked) {

            position: absolute;
            left: -9999px;

        }
        .input[type="radio"], input[type="checkbox"] {

             box-sizing: border-box;
             padding: 0;

         }
        .custom-control-input {
            position: unset!important;

            z-index: -1;
        }
        .custom-radio {

            padding-left: 0;

        }
        .custom-control {

            position: relative;
            display: unset;
            min-height: 1.5rem;
            padding-left: 15px;

        }
        .custom-control-input {

            position: unset!important;

            z-index: -1;
            opacity: 1.5;

        }
    </style>
@stop

@section('content')
    <form action="{{ route('admin_buyer_edit_post', ['buyer' => $buyer->id]) }}" method="POST">
        @csrf
        <h6 class="text-muted text-normal">Customer Information</h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">First Name <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('firstName') ? ' is-invalid' : '' }}"
                           type="text" id="firstName" name="firstName"
                           value="{{ empty(old('firstName')) ? ($errors->has('firstName') ? '' : $buyer->user->first_name) : old('firstName') }}">

                    @if ($errors->has('firstName'))
                        <div class="form-control-feedback">{{ $errors->first('firstName') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">Email <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           type="email" id="email" name="email"
                           value="{{ empty(old('email')) ? ($errors->has('email') ? '' : $buyer->user->email) : old('email') }}">

                    @if ($errors->has('email'))
                        <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Last Name <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('lastName') ? ' is-invalid' : '' }}"
                           type="text" id="lastName" name="lastName"
                           value="{{ empty(old('lastName')) ? ($errors->has('lastName') ? '' : $buyer->user->last_name) : old('lastName') }}">

                    @if ($errors->has('lastName'))
                        <div class="form-control-feedback">{{ $errors->first('lastName') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">Password <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           type="password" id="password" name="password">

                    @if ($errors->has('password'))
                        <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <h6 class="text-muted text-normal">Customer Company Information</h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Company Name <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('companyName') ? ' is-invalid' : '' }}"
                           type="text" id="companyName" name="companyName"
                           value="{{ empty(old('companyName')) ? ($errors->has('companyName') ? '' : $buyer->company_name) : old('companyName') }}">

                    @if ($errors->has('companyName'))
                        <div class="form-control-feedback">{{ $errors->first('companyName') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Primary Customer Market <span class="required">*</span></label>

                    <select class="form-control" id="primaryCustomerMarket" name="primaryCustomerMarket">
                        <option value="1"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '1' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '1' ? 'selected' : '') }}>All</option>
                        <option value="2"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '2' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '2' ? 'selected' : '') }}>African</option>
                        <option value="3"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '3' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '3' ? 'selected' : '') }}>Asian</option>
                        <option value="4"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '4' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '4' ? 'selected' : '') }}>Caucasian</option>
                        <option value="5"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '5' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '5' ? 'selected' : '') }}>Latino/Hispanic</option>
                        <option value="6"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '6' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '6' ? 'selected' : '') }}>Middle Eastern</option>
                        <option value="7"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '7' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '7' ? 'selected' : '') }}>Native American</option>
                        <option value="8"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '8' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '8' ? 'selected' : '') }}>Pacific Islander</option>
                        <option value="9"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '9' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '9' ? 'selected' : '') }}>Other</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group{{ $errors->has('sellerPermitNumber') ? ' has-danger' : '' }}">
                    <label for="small-rounded-input">Seller Permit Number <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('sellerPermitNumber') ? ' is-invalid' : '' }}"
                           type="text" id="sellerPermitNumber" name="sellerPermitNumber"
                           value="{{ empty(old('sellerPermitNumber')) ? ($errors->has('sellerPermitNumber') ? '' : $buyer->seller_permit_number) : old('sellerPermitNumber') }}">

                    @if ($errors->has('sellerPermitNumber'))
                        <div class="form-control-feedback">{{ $errors->first('sellerPermitNumber') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <label for="small-rounded-input">Do you sell online ?</label> &nbsp;

                <label for="sellOnlineYes" class="custom-control custom-radio">
                    <input class="custom-control-input sellOnline" type="radio" id="sellOnlineYes" name="sellOnline" value="1"
                            {{ empty(old('sellOnline')) ? ($buyer->sell_online == "1" ? 'checked' : '') :
                                    (old('sellOnline') == '1' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Yes</span>
                </label>

                <label for="sellOnlineNo" class="custom-control custom-radio">
                    <input class="custom-control-input sellOnline" type="radio" id="sellOnlineNo" name="sellOnline" value="0"
                            {{ empty(old('sellOnline')) ? ($buyer->sell_online == "0" ? 'checked' : '') :
                                    (old('sellOnline') == '0' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">No</span>
                </label>

                <input class="form-control" type="text" id="website"
                       name="website" placeholder="http://www.mywebsite.com"
                       value="{{ empty(old('website')) ? ($errors->has('website') ? '' : $buyer->website) : old('website') }}">

                @if ($errors->has('website'))
                    <div class="form-control-feedback">{{ $errors->first('website') }}</div>
                @endif
            </div>
        </div>

        <h6 class="text-muted text-normal">Shipping Address</h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-12">
                <label for="small-rounded-input">Location</label><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="locationUS" class="custom-control custom-radio">
                        <input class="custom-control-input location" type="radio" id="locationUS" name="location" value="US"
                                {{ empty(old('location')) ? ($buyerShippingAddress->location == "US" ? 'checked' : '') :
                                        (old('location') == 'US' ? 'checked' : '') }}>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">United States</span>
                    </label>

                    <label for="locationCA" class="custom-control custom-radio">
                        <input class="custom-control-input location" type="radio" id="locationCA" name="location" value="CA"
                                {{ empty(old('location')) ? ($buyerShippingAddress->location == "CA" ? 'checked' : '') :
                                        (old('location') == 'CA' ? 'checked' : '') }}>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Canada</span>
                    </label>

                    <label for="locationInt" class="custom-control custom-radio">
                        <input class="custom-control-input location" type="radio" id="locationInt" name="location" value="INT"
                                {{ empty(old('location')) ? ($buyerShippingAddress->location == "INT" ? 'checked' : '') :
                                        (old('location') == 'INT' ? 'checked' : '') }}>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">International</span>
                    </label>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Store No.</label>
                    <input class="form-control{{ $errors->has('store_no') ? ' is-invalid' : '' }}"
                           type="text" id="store_no" name="store_no"
                           value="{{ empty(old('store_no')) ? ($errors->has('store_no') ? '' : $buyerShippingAddress->store_no) : old('store_no') }}">

                    @if ($errors->has('store_no'))
                        <div class="form-control-feedback">{{ $errors->first('store_no') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Attention</label>
                    <input class="form-control{{ $errors->has('attention') ? ' is-invalid' : '' }}"
                           type="text" id="attention" name="attention"
                           value="{{ empty(old('attention')) ? ($errors->has('attention') ? '' : $buyer->attention) : old('attention') }}">

                    @if ($errors->has('attention'))
                        <div class="form-control-feedback">{{ $errors->first('attention') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="small-rounded-input">Address <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                           type="text" id="address" name="address"
                           value="{{ empty(old('address')) ? ($errors->has('address') ? '' : $buyerShippingAddress->address) : old('address') }}">

                    @if ($errors->has('address'))
                        <div class="form-control-feedback">{{ $errors->first('address') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                    <label for="small-rounded-input">Unit #</label>
                    <input class="form-control{{ $errors->has('unit') ? ' is-invalid' : '' }}"
                           type="text" id="unit" name="unit"
                           value="{{ empty(old('unit')) ? ($errors->has('unit') ? '' : $buyerShippingAddress->unit) : old('unit') }}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">City <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}"
                           type="text" id="city" name="city"
                           value="{{ empty(old('city')) ? ($errors->has('city') ? '' : $buyerShippingAddress->city) : old('city') }}">

                    @if ($errors->has('city'))
                        <div class="form-control-feedback">{{ $errors->first('city') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group" id="form-group-state">
                    <label for="small-rounded-input">State <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}"
                           type="text" id="state" name="state"
                           value="{{ empty(old('state')) ? ($errors->has('state') ? '' : $buyerShippingAddress->state_text) : old('state') }}">

                    @if ($errors->has('state'))
                        <div class="form-control-feedback">{{ $errors->first('state') }}</div>
                    @endif
                </div>

                <div class="form-group" id="form-group-state-select">
                    <label for="small-rounded-input">State <span class="required">*</span></label>
                    <select class="form-control{{ $errors->has('stateSelect') ? ' is-invalid' : '' }}"
                            id="stateSelect" name="stateSelect">
                        <option value="">Select State</option>
                    </select>

                    @if ($errors->has('stateSelect'))
                        <div class="form-control-feedback">{{ $errors->first('stateSelect') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Zip Code <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('zipCode') ? ' is-invalid' : '' }}"
                           type="text" id="zipCode" name="zipCode"
                           value="{{ empty(old('zipCode')) ? ($errors->has('zipCode') ? '' : $buyerShippingAddress->zip) : old('zipCode') }}">

                    @if ($errors->has('zipCode'))
                        <div class="form-control-feedback">{{ $errors->first('zipCode') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <label for="small-rounded-input">Country <span class="required">*</span></label>
                    <select class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}"
                            id="country" name="country">
                        <option value="">Select Country</option>
                        @foreach($countries as $country)
                            <option data-code="{{ $country->code }}" value="{{ $country->id }}"
                                    {{ empty(old('country')) ? ($errors->has('country') ? '' : ($buyerShippingAddress->country_id == $country->id ? 'selected' : '')) :
                                    (old('country') == $country->id ? 'selected' : '') }}>{{ $country->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('country'))
                        <div class="form-control-feedback">{{ $errors->first('country') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Phone <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                           type="text" id="phone" name="phone"
                           value="{{ empty(old('phone')) ? ($errors->has('phone') ? '' : $buyerShippingAddress->phone) : old('phone') }}">

                    @if ($errors->has('phone'))
                        <div class="form-control-feedback">{{ $errors->first('phone') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Fax</label>
                    <input class="form-control{{ $errors->has('fax') ? ' is-invalid' : '' }}"
                           type="text" id="fax" name="fax"
                           value="{{ empty(old('fax')) ? ($errors->has('fax') ? '' : $buyerShippingAddress->fax) : old('fax') }}">

                    @if ($errors->has('fax'))
                        <div class="form-control-feedback">{{ $errors->first('fax') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="showroomCommercial" name="showroomCommercial" value="1"
                            {{ empty(old('showroomCommercial')) ? ($buyerShippingAddress->commercial == 1 ? 'checked' : '') :
                                    (old('showroomCommercial') ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">This address is commercial.</span>
                </label>
            </div>
        </div>

        <h6 class="text-muted text-normal">Billing Address</h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-12">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="sameAsShowroomAddress" name="sameAsShowroomAddress" {{ old('sameAsShowroomAddress') ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Check here if same as shipping address.</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label for="small-rounded-input">Location</label><br>
                <label for="factoryLocationUS" class="custom-control custom-radio">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationUS" name="factoryLocation" value="US"
                            {{ empty(old('factoryLocation')) ? ($buyer->billing_location == "US" ? 'checked' : '') :
                                    (old('factoryLocation') == 'US' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">United States</span>
                </label>

                <label for="factoryLocationCA" class="custom-control custom-radio">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationCA" name="factoryLocation" value="CA"
                            {{ empty(old('factoryLocation')) ? ($buyer->billing_location == "CA" ? 'checked' : '') :
                                    (old('factoryLocation') == 'CA' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Canada</span>
                </label>

                <label for="factoryLocationInt" class="custom-control custom-radio">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationInt" name="factoryLocation" value="INT"
                            {{ empty(old('factoryLocation')) ? ($buyer->billing_location == "INT" ? 'checked' : '') :
                                    (old('factoryLocation') == 'INT' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">International</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="small-rounded-input">Address <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('factoryAddress') ? ' is-invalid' : '' }}"
                           type="text" id="factoryAddress" name="factoryAddress"
                           value="{{ empty(old('factoryAddress')) ? ($errors->has('factoryAddress') ? '' : $buyer->billing_address) : old('factoryAddress') }}">

                    @if ($errors->has('factoryAddress'))
                        <div class="form-control-feedback">{{ $errors->first('factoryAddress') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                    <label for="small-rounded-input">Unit #</label>
                    <input class="form-control{{ $errors->has('factoryUnit') ? ' is-invalid' : '' }}"
                           type="text" id="factoryUnit" name="factoryUnit"
                           value="{{ empty(old('factoryUnit')) ? ($errors->has('factoryUnit') ? '' : $buyer->billing_unit) : old('factoryUnit') }}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">City <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('factoryCity') ? ' is-invalid' : '' }}"
                           type="text" id="factoryCity" name="factoryCity"
                           value="{{ empty(old('factoryCity')) ? ($errors->has('factoryCity') ? '' : $buyer->billing_city) : old('factoryCity') }}">

                    @if ($errors->has('factoryCity'))
                        <div class="form-control-feedback">{{ $errors->first('factoryCity') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group" id="form-group-factory-state">
                    <label for="small-rounded-input">State <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('factoryState') ? ' is-invalid' : '' }}"
                           type="text" id="factoryState" name="factoryState"
                           value="{{ empty(old('factoryState')) ? ($errors->has('factoryState') ? '' : $buyer->billing_state) : old('factoryState') }}">

                    @if ($errors->has('factoryState'))
                        <div class="form-control-feedback">{{ $errors->first('factoryState') }}</div>
                    @endif
                </div>

                <div class="form-group" id="form-group-factory-state-select">
                    <label for="small-rounded-input">State <span class="required">*</span></label>
                    <select class="form-control{{ $errors->has('factoryStateSelect') ? ' is-invalid' : '' }}"
                            id="factoryStateSelect" name="factoryStateSelect">
                        <option value="">Select State</option>
                    </select>

                    @if ($errors->has('factoryStateSelect'))
                        <div class="form-control-feedback">{{ $errors->first('factoryStateSelect') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Zip Code <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('factoryZipCode') ? ' is-invalid' : '' }}"
                           type="text" id="factoryZipCode" name="factoryZipCode"
                           value="{{ empty(old('factoryZipCode')) ? ($errors->has('factoryZipCode') ? '' : $buyer->billing_zip) : old('factoryZipCode') }}">

                    @if ($errors->has('factoryZipCode'))
                        <div class="form-control-feedback">{{ $errors->first('factoryZipCode') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <label for="small-rounded-input">Country <span class="required">*</span></label>
                    <select class="form-control{{ $errors->has('factoryCountry') ? ' is-invalid' : '' }}"
                            id="factoryCountry" name="factoryCountry">
                        <option value="">Select Country</option>
                        @foreach($countries as $country)
                            <option data-code="{{ $country->code }}" value="{{ $country->id }}"
                                    {{ empty(old('factoryCountry')) ? ($errors->has('factoryCountry') ? '' : ($buyer->billing_country_id == $country->id ? 'selected' : '')) :
                                    (old('factoryCountry') == $country->id ? 'selected' : '') }}>{{ $country->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('factoryCountry'))
                        <div class="form-control-feedback">{{ $errors->first('factoryCountry') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Phone <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('factoryPhone') ? ' is-invalid' : '' }}"
                           type="text" id="factoryPhone" name="factoryPhone"
                           value="{{ empty(old('factoryPhone')) ? ($errors->has('factoryPhone') ? '' : $buyer->billing_phone) : old('factoryPhone') }}">

                    @if ($errors->has('factoryPhone'))
                        <div class="form-control-feedback">{{ $errors->first('factoryPhone') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Fax</label>
                    <input class="form-control{{ $errors->has('factoryFax') ? ' is-invalid' : '' }}"
                           type="text" id="factoryFax" name="factoryFax"
                           value="{{ empty(old('factoryFax')) ? ($errors->has('factoryFax') ? '' : $buyer->billing_fax) : old('factoryFax') }}">

                    @if ($errors->has('factoryFax'))
                        <div class="form-control-feedback">{{ $errors->first('factoryFax') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="factoryCommercial" name="factoryCommercial" value="1"
                            {{ empty(old('factoryCommercial')) ? ($buyer->billing_commercial == 1 ? 'checked' : '') :
                                    (old('factoryCommercial') ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">This address is commercial.</span>
                </label>
            </div>
        </div>

        <h6 class="text-muted text-normal">How did you hear about us ? <span class="required">*</span></h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-12">
                <label for="hearAboutUsGoogle" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsGoogle" name="hearAboutUs" value="google"
                            {{ empty(old('hearAboutUs')) ? ($buyer->hear_about_us == "google" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'google' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Google</span>
                </label>

                <label for="hearAboutUsYahoo" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsYahoo" name="hearAboutUs" value="yahoo"
                            {{ empty(old('hearAboutUs')) ? ($buyer->hear_about_us == "yahoo" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'yahoo' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Yahoo</span>
                </label>

                <label for="hearAboutUsBing" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsBing" name="hearAboutUs" value="bing"
                            {{ empty(old('hearAboutUs')) ? ($buyer->hear_about_us == "bing" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'bing' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Bing</span>
                </label>

                <label for="hearAboutUsOtherSearch" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsOtherSearch" name="hearAboutUs" value="other_search"
                            {{ empty(old('hearAboutUs')) ? ($buyer->hear_about_us == "other_search" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'other_search' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Other search</span>
                </label>

                <label for="hearAboutUsMagicShow" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsMagicShow" name="hearAboutUs" value="magic_show"
                            {{ empty(old('hearAboutUs')) ? ($buyer->hear_about_us == "magic_show" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'magic_show' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">MAGIC Show</span>
                </label>

                <label for="hearAboutUsVendor" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsVendor" name="hearAboutUs" value="vendor"
                            {{ empty(old('hearAboutUs')) ? ($buyer->hear_about_us == "vendor" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'vendor' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Referred by a Vendor</span>
                </label>

                <label for="hearAboutUsFriend" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsFriend" name="hearAboutUs" value="friend"
                            {{ empty(old('hearAboutUs')) ? ($buyer->hear_about_us == "friend" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'friend' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Referred by a Friend</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1">
                <label for="hearAboutUsOther" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsOther" name="hearAboutUs" value="other"
                            {{ empty(old('hearAboutUs')) ? ($buyer->hear_about_us == "other" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'other' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Other</span>
                </label>
            </div>

            <div class="col-md-5">
                <div class="form-group">
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('hearAboutUsOtherText') ? ' is-invalid' : '' }}"
                           type="text" id="hearAboutUsOtherText" name="hearAboutUsOtherText"
                           value="{{ empty(old('hearAboutUsOtherText')) ? ($errors->has('hearAboutUsOtherText') ? '' : $buyer->hear_about_us_other) : old('hearAboutUsOtherText') }}">

                    @if ($errors->has('hearAboutUsOtherText'))
                        <div class="form-control-feedback">{{ $errors->first('hearAboutUsOtherText') }}</div>
                    @endif
                </div>
            </div>
        </div>

        @if ($errors->has('hearAboutUs'))
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group has-danger">
                        <div class="form-control-feedback">{{ $errors->first('hearAboutUs') }}</div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="receiveSpecialOffers" value="1" name="receiveSpecialOffers"
                            {{ ($buyer->receive_offers == 1) ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Sign up to receive special offers and information.</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input class="btn btn-primary" type="submit" value="UPDATE">
            </div>
        </div>
    </form>
@stop

@section('additionalJS')
    <script>
        var usStates = <?php echo json_encode($usStates); ?>;
        var caStates = <?php echo json_encode($caStates); ?>;
        var oldState = '{{ empty(old('stateSelect')) ? ($errors->has('stateSelect') ? '' : $buyer->shipping_state_id) : old('stateSelect') }}';
        var oldFactoryState = '{{ empty(old('factoryStateSelect')) ? ($errors->has('factoryStateSelect') ? '' : $buyer->billing_state_id) : old('factoryStateSelect') }}';

        $(function () {
            $('form').bind('submit', function () {
                $(this).find(':input').prop('disabled', false);
            });

            $('#address').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryAddress').val(text);
            });

            $('#unit').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryUnit').val(text);
            });

            $('#city').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryCity').val(text);
            });

            $('#state').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryState').val(text);
            });

            $('#zipCode').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryZipCode').val(text);
            });

            $('#phone').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryPhone').val(text);
            });

            $('#fax').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryFax').val(text);
            });

            $('#sameAsShowroomAddress').change(function () {
                $('#address').trigger('keyup');
                $('#unit').trigger('keyup');
                $('#city').trigger('keyup');
                $('#state').trigger('keyup');
                $('#zipCode').trigger('keyup');
                $('#phone').trigger('keyup');
                $('#fax').trigger('keyup');

                var location = $('.location:checked').val();
                $('.factoryLocation[value=' + location + ']').prop('checked', true);
                $('.factoryLocation').trigger('change');

                $('#factoryCountry').val($('#country').val());
                $('#factoryState').val($('#state').val());
                $('#factoryStateSelect').val($('#stateSelect').val());
            });

            $('.location').change(function () {
                var location = $('.location:checked').val();

                if ($("#sameAsShowroomAddress").is(':checked')) {
                    $('.factoryLocation[value=' + location + ']').prop('checked', true);
                    $('.factoryLocation').trigger('change');
                }

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#country').val('1');
                    else
                        $('#country').val('2');


                    $('#country').prop('disabled', 'disabled');
                    $('#form-group-state-select').show();
                    $('#stateSelect').val('');
                    $('#form-group-state').hide();

                    $('#stateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            if (value.id == oldState)
                                $('#stateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            if (value.id == oldState)
                                $('#stateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#country').prop('disabled', false);
                    $('#form-group-state-select').hide();
                    $('#form-group-state').show();
                }
            });

            $('.factoryLocation').change(function () {
                var location = $('.factoryLocation:checked').val();

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#factoryCountry').val('1');
                    else
                        $('#factoryCountry').val('2');

                    $('#factoryCountry').prop('disabled', 'disabled');
                    $('#form-group-factory-state-select').show();
                    $('#factoryStateSelect').val('');
                    $('#form-group-factory-state').hide();

                    $('#factoryStateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#factoryCountry').prop('disabled', false);
                    $('#form-group-factory-state-select').hide();
                    $('#form-group-factory-state').show();
                }
            });

            $('#country').change(function () {
                var countryId = $(this).val();

                if (countryId == 1) {
                    $("#locationUS").prop("checked", true);
                    $('.location').trigger('change');
                } else if (countryId == 2) {
                    $("#locationCA").prop("checked", true);
                    $('.location').trigger('change');
                }
            });

            $('.sellOnline').change(function () {
                if ($('#sellOnlineYes').is(':checked')) {
                    $('#website').show();
                } else {
                    $('#website').hide();
                }
            });

            $('.location').trigger('change');
            $('.factoryLocation').trigger('change');
            $('.sellOnline').trigger('change');
        })
    </script>
@stop