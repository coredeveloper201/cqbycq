<?php use App\Enumeration\VendorImageType; ?>
@extends('admin.layouts.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin_logo_add_post') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                @csrf

                <div class="form-group row">
                    <div class="col-3">
                        <label class="col-form-label">White Logo:</label>
                    </div>
                    <div class="col-4">
                        <input type="file" class="form-control{{ $errors->has('logo') ? ' is-invalid' : '' }}" name="logo">

                        @if ($errors->has('logo'))
                            <div class="form-control-feedback">{{ $errors->first('logo') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-3">
                        <label class="col-form-label">Black Logo:</label>
                    </div>
                    <div class="col-4">
                        <input type="file" class="form-control{{ $errors->has('logo2') ? ' is-invalid' : '' }}" name="logo2" accept="image/*">

                        @if ($errors->has('logo2'))
                            <div class="form-control-feedback">{{ $errors->first('logo2') }}</div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-3">
                        <label class="col-form-label">No Login Image:</label>
                    </div>
                    <div class="col-4">
                        <input type="file" class="form-control{{ $errors->has('logo3') ? ' is-invalid' : '' }}" name="logo3" accept="image/*">

                        @if ($errors->has('logo3'))
                            <div class="form-control-feedback">{{ $errors->first('logo3') }}</div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-7 text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-6 bg-dark">
            <img src="{{ asset($white->value) }}" alt="" width="80%">
        </div>

        <div class="col-6">
            <img src="{{ asset($black->value) }}" alt="" width="80%">
        </div>

        <div class="col-md-6">
            <img src="{{ asset($defaultItemImage->value) }}" alt="" width="80%">

        </div>
    </div>
@stop
