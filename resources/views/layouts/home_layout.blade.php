<?php use App\Enumeration\Role; ?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $meta_title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ $meta_description }}" />

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel='stylesheet' href='{{ asset('themes/cq/fonts/stylesheet.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/owl.theme.default.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/owl.carousel.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/slick.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/magnific-popup.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/main.css') }}?id={{ rand() }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/custom.css') }}'/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    @yield('additionalCSS')

</head>

<!-- Body-->
<body class="product_page home_page">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Header -->
    @include('layouts.shared.header')

    @include('layouts.shared.left_menu')
    <!-- Header -->

    <!-- Content -->
    @yield('content')
    <!-- Content -->

    <!-- Footer -->
    <footer class="footer_area">
        <div class="footer_bottom">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 no-padding-left">
                    </div>
                    <div class="col-md-6 no-padding-right">
                        <div class="footer_copyright text-right">
                            <ul>
                                <li><a href="{{ route('privacy_policy') }}">Privacy policy</a></li>
                                <li><a href="{{ route('terms_conditions') }}">Terms of use</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer -->

<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src='{{ asset('themes/cq/js/owl.carousel.js') }}'></script>
<script src='{{ asset('themes/cq/js/jquery.magnific-popup.js') }}'></script>
<script src='{{ asset('themes/cq/js/slick.js') }}'></script>
<script src='{{ asset('themes/cq/js/main.js') }}?id={{ rand() }}'></script>
@yield('additionalJS')
</body>
</html>