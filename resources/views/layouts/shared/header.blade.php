<?php use App\Enumeration\Role; ?>

<div class="full-page-search">
    <div class="full-page-search-inner">
        <form action="{{ route('search') }}">
            <input type="text" name="s" placeholder="">
        </form>
        <img src="{{ asset('themes/cq/images/cross_icbig.png') }}" alt="">
    </div>
</div>

<header class="header_area fixed-top">
    @if (Request::route()->getName() != 'buyer_login')
        <div class="menu_arrow">
            <span></span>
            <span></span>
            <span></span>
        </div>
    @endif

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="header_menu">
                    <div class="logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ $black_logo_path }}" alt="" class="img-responsive black_logo">
                            <img src="{{ $white_logo_path }}" alt="" class="img-responsive white_logo">
                        </a>
                    </div>
                </div>
                <div class="header_search">
                    <div class="header_search_inner">
                        <a href="#" style="cursor: auto;">search</a>
                        <a href="#" style="cursor: auto;"><span></span></a>
                    </div>
                </div>
                <div class="header_right_menu">
                    <ul>
                        @if (!Auth::check() || (Auth::check() && Auth::user()->role != Role::$BUYER))
                        <li><a href="{{ route('buyer_login') }}">LOG IN</a></li>
                        @else
                            <li class="text-uppercase">
                                {{-- <a href="{{ route('buyer_show_overview') }}">{{ Auth::user()->name }}</a> --}}
                            </li>
                            <!-- Dropdown -->
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                ACCOUNT
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">
                                    {{-- <a href="{{ route('buyer_show_overview') }}">{{ Auth::user()->name }}</a> --}}
                                    <a  href="{{ route('buyer_show_overview') }}">Dashboard</a>
                                </a>
                                <a class="dropdown-item" style="margin-top: 10px;" href="#">
                                    <form id="logoutForm" class="" action="{{ route('logout_buyer') }}" method="post">
                                        {{ csrf_field() }}<button style="width: 90%" class="logout-link">Logout</button>
                                    </form>
                                </a>
                            </div>
                            </li>
                        @endif
                        @if (Auth::check() || (Auth::check() && Auth::user()->role == Role::$BUYER))
                            <li class="show_desktop_only"><a href="{{ route('contact_us') }}">CONTACT</a></li>
                        @else 
                            <li><a href="{{ route('contact_us') }}">CONTACT</a></li>
                        @endif
                        <li>
                            <a href="{{ route('show_cart') }}">
                                <img src="{{ asset('themes/cq/images/shopping-bag.svg') }}" class="black_shipping_bag" alt="">
                                <img src="{{ asset('themes/cq/images/shopping-bag-white.svg') }}" alt="" class="white_shipping_bag">
                                <span>{{ count($cart_items)-1 }}</span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>