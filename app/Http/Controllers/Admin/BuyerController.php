<?php

namespace App\Http\Controllers\Admin;

use App\Model\BuyerShippingAddress;
use App\Model\Country;
use App\Model\MetaBuyer;
use App\Model\State;
use App\Model\StoreCredit;
use App\Model\StoreCreditTransection;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\UserRegistered;
use Illuminate\Support\Facades\Hash;

class BuyerController extends Controller
{
    protected $allCustomerLists;

    public function __construct(MetaBuyer $allCustomerLists)
    {
        $this->allCustomerLists = $allCustomerLists;
    }
    public function allBuyer(Request $request) {
        $data['page_title'] = 'All Customer';
        $all=$this->allCustomerLists;

        if($request->has('status') && $request->status != null)
        {
          if($request->status == 'verified' || $request->status == 'notverified'){
                $statusVarified = ($request->status == 'verified') ? 1:0;
                $all=$all->where('verified','=',$statusVarified);
            }
            if($request->status == 'active' || $request->status == 'inactive'){
                $statusActive = ($request->status == 'active') ? 1:0;
                $all=$all->where('active','=',$statusActive);
            }
        }
        if($request->has('company_name')&& $request->company_name!=null){
            $all= $all->where('company_name','like','%' .$request->company_name. '%');
        }

        $data['buyers'] = $all->orderBy('created_at', 'desc')->paginate(20);

        return view('admin.dashboard.customer.all', $data);
    }

    public function changeStatus(Request $request) {
        $buyers = MetaBuyer::where('id', $request->id)->first();
        $buyers->active = $request->status;
        $buyers->save();
    }

    public function changeVerified(Request $request) {
        $buyers = MetaBuyer::where('id', $request->id)->first();
        $buyers->verified = $request->status;
        $buyers->save();
    }

    public function changeMailingList(Request $request) {
        $buyers = MetaBuyer::where('id', $request->id)->first();
        $buyers->mailing_list = $request->mailing_list;
        $buyers->save();

        // Get user data by id
        $user = User::find($request->user_id);
        $user->phone = $request->billing_phone;
        
        // Trigger event for mailchimp
        event(new UserRegistered($user));
    }

    public function changeBlock(Request $request) {
        $buyers = MetaBuyer::where('id', $request->id)->first();
        $buyers->block = $request->status;
        $buyers->save();
    }

    public function changeMinOrder(Request $request) {
        $buyers = MetaBuyer::where('id', $request->id)->first();
        $buyers->min_order = $request->status;
        $buyers->save();
    }

    public function edit(MetaBuyer $buyer) {
        $buyer->load('user');
        $countries = Country::orderBy('name')->get();
        $usStates = State::where('country_id', 1)->orderBy('name')->get()->toArray();
        $caStates =State::where('country_id', 2)->orderBy('name')->get()->toArray();
        $buyerShippingAddress=BuyerShippingAddress::where('user_id', $buyer->user->id)->where('default', 1)->first();

        return view('admin.dashboard.customer.edit', compact('countries', 'usStates', 'caStates', 'buyer','buyerShippingAddress'))->with('page_title', 'Edit Customer');
    }

    public function editPost(MetaBuyer $buyer, Request $request) {
        $messages = [
            'required' => 'This field is required.',
        ];

        $rules = [
            'companyName' => 'required|string|max:255',
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$buyer->user->id,
            'sellerPermitNumber' => 'required|string|max:255',
            'address' => 'required|string|max:255',
             'unit' => 'nullable|string|max:255',
            'city' => 'required|string|max:255',
            'zipCode' => 'required|string|max:255',
            'country' => 'required',
            'phone' => 'required|max:255',
            'fax' => 'nullable|max:255',
            'factoryAddress' => 'required|string|max:255',
            'factoryUnit' => 'nullable|string|max:255',
            'factoryCity' => 'required|string|max:255',
            'factoryZipCode' => 'required|string|max:255',
            'factoryCountry' => 'required',
            'factoryPhone' => 'required|max:255',
            'factoryFax' => 'nullable|max:255',
            'hearAboutUs' => 'required',
        ];

        if ($request->location == "INT")
            $rules['state'] = 'required|string|max:255';
        else
            $rules['stateSelect'] = 'required';

        if ($request->factoryLocation == "INT")
            $rules['factoryState'] = 'required|string|max:255';
        else
            $rules['factoryStateSelect'] = 'required';

        if ($request->hearAboutUs && $request->hearAboutUs == "other")
            $rules['hearAboutUsOtherText'] = 'required|string|max:255';

        if ($request->sellOnline && $request->sellOnline == '1')
            $rules['website'] = 'required|string|max:255';

        if ($request->password != '')
            $rules['password'] = 'required|string|min:6';

        $request->validate($rules, $messages);

        $state_id = null;
        $state = null;
        $factory_state_id = null;
        $factory_state = null;
        $hearFromOtherText = null;

        if ($request->location == "INT")
            $state = $request->state;
        else
            $state_id = $request->stateSelect;

        if ($request->factoryLocation == "INT")
            $factory_state = $request->factoryState;
        else
            $factory_state_id = $request->factoryStateSelect;

        if ($request->hearAboutUs && $request->hearAboutUs == "other")
            $hearFromOtherText = $request->hearAboutUsOtherText;

        $buyer->company_name = $request->companyName;
        $buyer->primary_customer_market = $request->primaryCustomerMarket;
        $buyer->seller_permit_number = $request->sellerPermitNumber;
        $buyer->sell_online = $request->sellOnline;
        $buyer->website = $request->website;
/*        $buyer->shipping_location = $request->location;
        $buyer->store_no = $request->store_no;*/
        $buyer->attention = $request->attention;
/*        $buyer->shipping_address = $request->address;
        $buyer->shipping_unit = $request->unit;
        $buyer->shipping_city = $request->city;
        $buyer->shipping_state_id = $state_id;
        $buyer->shipping_state = $state;
        $buyer->shipping_zip = $request->zipCode;
        $buyer->shipping_country_id = $request->country;
        $buyer->shipping_phone = $request->phone;
        $buyer->shipping_fax = $request->fax;
        $buyer->shipping_commercial = ($request->showroomCommercial == null) ? 0 : 1;*/
        $buyer->billing_location = $request->factoryLocation;
        $buyer->billing_address = $request->factoryAddress;
        $buyer->billing_unit = $request->factoryUnit;
        $buyer->billing_city = $request->factoryCity;
        $buyer->billing_state_id = $factory_state_id;
        $buyer->billing_state = $factory_state;
        $buyer->billing_zip = $request->factoryZipCode;
        $buyer->billing_country_id = $request->factoryCountry;
        $buyer->billing_phone = $request->factoryPhone;
        $buyer->billing_fax = $request->factoryFax;
        $buyer->billing_commercial = ($request->factoryCommercial == null) ? 0 : 1;
        $buyer->hear_about_us = $request->hearAboutUs;
        $buyer->hear_about_us_other = $hearFromOtherText;
        $buyer->receive_offers = $request->receiveSpecialOffers;



        $buyer->user->first_name = $request->firstName;
        $buyer->user->last_name = $request->lastName;
        $buyer->user->email = $request->email;
        $checkBuyerShippingAddress = BuyerShippingAddress::where('user_id', $buyer->user->id)->where('default', 1)->first();
        if(isset($checkBuyerShippingAddress)){

        $buyerShipping = BuyerShippingAddress::find($checkBuyerShippingAddress->id);
            $buyerShipping->default = 1;
            $buyerShipping->store_no = $request->store_no;
            $buyerShipping->location = $request->location;
            $buyerShipping->address = $request->address;
            $buyerShipping->unit = $request->unit;
            $buyerShipping->city = $request->city;
            $buyerShipping->state_id = $state_id;
            $buyerShipping->state_text = $state;
            $buyerShipping->zip = $request->zipCode;
            $buyerShipping->country_id = $request->country;
            $buyerShipping->phone = $request->phone;
            $buyerShipping->fax = $request->fax;
            $buyerShipping->commercial = ($request->showroomCommercial == null) ? 0 : 1;
            $buyerShipping->save();
        }
        if ($request->password != '')
            $buyer->user->password = Hash::make($request->password);

        $buyer->save();
        $buyer->user->save();

        return redirect()->route('admin_all_buyer')->with('message', 'Updated!');
    }

    public function delete(Request $request) {
        $meta = MetaBuyer::where('id', $request->id)->first();
        $user = User::where('id', $meta->user->id)->first();
        $user->email = $user->email.'-deleted';
        $user->save();

        StoreCredit::where('user_id', $user->id)->delete();
        StoreCreditTransection::where('user_id', $user->id)->delete();

        $user->delete();
        $meta->delete();
    }
}
