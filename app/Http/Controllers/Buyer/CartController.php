<?php

namespace App\Http\Controllers\Buyer;

use App\Model\CartItem;
use App\Model\Item;
use App\Model\MetaVendor;
use App\Model\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CartController extends Controller
{
    public function addToCart(Request $request) {
        $item = Item::where('status', 1)->where('id', $request->itemId)->first();

        $itemOrder = 0;

        for ($i=0; $i < sizeof($request->qty); $i++)
            $itemOrder += (int) $request->qty[$i];

        if ($itemOrder < $item->min_qty)
            return response()->json(['success' => false, 'message' => 'Minimum quantity is : '. $item->min_qty]);

        // if ( $request->total_price < $item->min_order ) {
        //     return response()->json(['success' => false, 'message' => 'Minimum order amount is : '. $item->min_order]);
        // }

        for ($i=0; $i < sizeof($request->colors); $i++) {
            $cartItem = CartItem::where([
                ['user_id', Auth::user()->id],
                ['item_id', $request->itemId],
                ['color_id', $request->colors[$i]]
            ])->first();

            if ($cartItem) {
                $cartItem->quantity += (int) $request->qty[$i];
                $cartItem->save();
            } else {
                CartItem::create([
                    'user_id' => Auth::user()->id,
                    'item_id' => $request->itemId,
                    'color_id' => $request->colors[$i],
                    'quantity' => $request->qty[$i],
                ]);
            }
        }

        return response()->json(['success' => true, 'message' => 'Success']);
    }

    public function addToCartSuccess() {
        return back()->with('message', 'Added to cart.');
    }

    public function showCart() {
        $temp = [];
        $cartItems = [];
        $vendor = MetaVendor::where('id', 1)->first();

        $cartObjs = CartItem::where('user_id', Auth::user()->id)
            ->with('item', 'color')
            ->get();

        foreach($cartObjs as $obj) {
            $temp[$obj->item->id][] = $obj;
        }

        $itemCounter = 0;

        foreach($temp as $itemId => $item) {
            $cartItems[$itemCounter] = $item;
            $itemCounter++;
        }

        return view('pages.cart', compact('cartItems', 'vendor'))->with('page_title', 'Cart');
    }

    public function updateCart(Request $request) {
        //$cartItems = CartItem::whereIn('id', $request->ids)->with('item')->get();

        $data = [];
        for($i=0; $i < sizeof($request->ids); $i++) {
            $ci = CartItem::where('id', $request->ids[$i])->first();

            $c = 0;

            if (isset($data[$ci->item->id]))
                $c = $data[$ci->item->id];

            $data[$ci->item->id] = (int) ($request->qty[$i]) + $c;
        }

        foreach ($data as $itemId => $q) {
            $item = Item::where('id', $itemId)->first();

            if ($item->min_qty > $q)
                return response()->json(['success' => false, 'message' => $item->style_no.' minimum order qty is '. $item->min_qty]);
        }

        for($i=0; $i < sizeof($request->ids); $i++) {
            if ($request->qty[$i] == '0')
                CartItem::where('id', $request->ids[$i])->delete();
            else {
                CartItem::where('id', $request->ids[$i])->update(['quantity' => $request->qty[$i]]);
            }
        }

        return response()->json(['success' => true, 'message' => 'Success']);
    }

    public function updateCartSuccess() {
        return back()->with('message', 'Cart Updated!');
    }

    public function deleteCart(Request $request) {
        CartItem::where('id', $request->id)->delete();
    }

    public function deleteCartAll(Request $request) {
        CartItem::where([])->delete();
    }

}
