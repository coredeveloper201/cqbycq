<?php
    namespace App\Enumeration;

    class VendorImageType {
        public static $LOGO = 1;
        public static $HOME_PAGE_BANNER = 2;
        public static $SMALL_AD_BANNER = 3;
        public static $MOBILE_MAIN_BANNER = 4;
        public static $BIDDING_BIG_BANNER = 5;
        public static $BIDDING_SMALL_BANNER = 6;
        public static $MAIN_SLIDER = 7;
        public static $FRONT_PAGE_BANNER = 8;
    }