<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Test
//Route::get('/fg', 'TestController@fgTest');
//Route::get('/mail_test', 'TestController@mailTest');
//Route::get('images/size', 'TestController@changeImageSize');
//Route::get('/cat', 'TestController@changeDC');
//Route::get('sort', 'TestController@sort');
//Route::get('order_count', 'TestController@orderCount');

use App\Enumeration\Role;
use Illuminate\Support\Facades\Hash;

Route::get('/', 'HomeController@index')->name('home');

// Static Pages
Route::get('/about_us', 'StaticPageController@aboutUs')->name('about_us');
Route::get('/contact_us', 'StaticPageController@contactUs')->name('contact_us');
Route::post('/contact_us', 'StaticPageController@contactUsPost')->name('contact_us_post');
Route::get('/privacy_policy', 'StaticPageController@privacyPolicy')->name('privacy_policy');
Route::get('/return_info', 'StaticPageController@returnInfo')->name('return_info');
Route::get('/billing_shipping', 'StaticPageController@billingShipping')->name('billing_shipping');
Route::get('/large_quantities', 'StaticPageController@largeQuantities')->name('large_quantities');
Route::get('/terms_conditions', 'StaticPageController@termsCondiditions')->name('terms_conditions');
Route::get('/cookies_policy', 'StaticPageController@cookiesPolicy')->name('cookies_policy');
Route::get('/refunds', 'StaticPageController@refunds')->name('refunds');
Route::get('/faq', 'StaticPageController@faq')->name('faq');

// New Arrival
Route::get('/new_in', 'NewArrivalController@showItems')->name('new_arrival_page');
Route::post('/new_arrival/items', 'NewArrivalController@getNewArrivalItems')->name('get_new_arrival_items');

// New Arrival
Route::get('/best_seller', 'HomeController@bestSellerPage')->name('best_seller_page');

// Product Item Details
Route::get('/details/{item}', 'HomeController@itemDetails')->name('item_details_page');
Route::post('/quick_view_item', 'HomeController@quickViewItemDetails')->name('quick_view_item');

// Authorize and Capture
Route::post('/authorize_capture', 'Buyer\CheckoutController@authorizeAndCapture')->name('authorize_capture');
//Route::get('/authorize_capture/{order}', 'Buyer\CheckoutController@captureAuthorizedAmount')->name('authorize_capturess');
Route::post('/authorize_capture_only', 'Buyer\CheckoutController@captureAuthorizedAmount')->name('authorize_capture_only');

//Route::post('/authorize_only', 'Buyer\CheckoutController@authorizeOnly')->name('authorize_only');
//Route::get('/authorize_only/{order}', 'Buyer\CheckoutController@authorizeOnly')->name('authorize_only');

// Post Method Item
Route::post('/items/get/sub_category', 'HomeController@getItemsSubCategory')->name('get_items_sub_category');
Route::post('/items/get/category', 'HomeController@getItemsCategory')->name('get_items_category');

Route::get('/search', 'HomeController@searchPage')->name('search');

// Checkout Test
//Route::get('/test/checkout', 'Buyer\CheckoutController@chargeCreditCard');

// Cart
Route::get('cart', 'Buyer\CartController@showCart')->name('show_cart')->middleware('buyer');
Route::post('cart/add', 'Buyer\CartController@addToCart')->name('add_to_cart')->middleware('buyer');
//promo or coupon code
Route::post('checkout/apply_coupon', 'Buyer\CheckoutController@applyCoupon')->name('buyer_apply_coupon')->middleware('buyer');

Route::get('/cart/add/success', 'Buyer\CartController@addToCartSuccess')->name('add_to_cart_success')->middleware('buyer');
Route::post('cart/update', 'Buyer\CartController@updateCart')->name('update_cart')->middleware('buyer');
Route::get('cart/update/success', 'Buyer\CartController@updateCartSuccess')->name('update_cart_success')->middleware('buyer');
Route::post('cart/delete', 'Buyer\CartController@deleteCart')->name('delete_cart')->middleware('buyer');
Route::post('cart/delete/all', 'Buyer\CartController@deleteCartAll')->name('delete_cart_all')->middleware('buyer');


// Auth
Route::get('/register', 'Buyer\AuthController@register')->name('buyer_register');
Route::post('/register/post', 'Buyer\AuthController@registerPost')->name('buyer_register_post');
Route::get('/register/complete', 'Buyer\AuthController@registerComplete')->name('buyer_register_complete');
Route::get('login', 'Buyer\AuthController@login')->name('buyer_login');
Route::post('login/post', 'Buyer\AuthController@loginPost')->name('buyer_login_post');
Route::post('logout', 'Buyer\AuthController@logout')->name('logout_buyer');

Route::get('reset', 'Buyer\AuthController@resetPassword')->name('password_reset_buyer');
Route::post('reset/post', 'Buyer\AuthController@resetPasswordPost')->name('password_reset__buyer_post');
Route::get('reset/new', 'Buyer\AuthController@newPassword')->name('new_password_buyer');
Route::post('reset/new/post', 'Buyer\AuthController@newPasswordPost')->name('new_password_post_buyer');

// Checkout
/*Route::get('checkout', 'Buyer\CheckoutController@index')->name('show_checkout')->middleware('buyer');
Route::post('checkout/address/post', 'Buyer\CheckoutController@addressPost')->name('checkout_address_post')->middleware('buyer');
Route::post('checkout/address/select', 'Buyer\CheckoutController@addressSelect')->name('checkout_address_select')->middleware('buyer');
Route::get('checkout/shipping', 'Buyer\CheckoutController@shipping')->name('show_shipping_method')->middleware('buyer');
Route::post('checkout/shipping/post', 'Buyer\CheckoutController@shippingPost')->name('show_shipping_method_post')->middleware('buyer');
Route::get('checkout/payment', 'Buyer\CheckoutController@payment')->name('show_payment')->middleware('buyer');
Route::post('checkout/payment/post', 'Buyer\CheckoutController@paymentPost')->name('checkout_payment_post')->middleware('buyer');
Route::get('checkout/review', 'Buyer\CheckoutController@review')->name('show_checkout_review')->middleware('buyer');
Route::get('checkout/complete', 'Buyer\CheckoutController@completeView')->name('checkout_complete_view')->middleware('buyer');
Route::post('checkout/complete', 'Buyer\CheckoutController@complete')->name('checkout_complete')->middleware('buyer');*/

Route::post('checkout/address/select', 'Buyer\CheckoutController@addressSelect')->name('checkout_address_select')->middleware('buyer');
Route::get('checkout', 'Buyer\CheckoutController@singlePageCheckout')->name('show_checkout')->middleware('buyer');
Route::post('checkout/create', 'Buyer\CheckoutController@create')->name('create_checkout')->middleware('buyer');
Route::post('checkout/single/post', 'Buyer\CheckoutController@singlePageCheckoutPost')->name('single_checkout_post')->middleware('buyer');
Route::get('checkout/complete', 'Buyer\CheckoutController@complete')->name('checkout_complete')->middleware('buyer');

// Wishlist
Route::get('wishlist', 'Buyer\WishListController@index')->name('view_wishlist')->middleware('buyer');
Route::post('wishlist/add', 'Buyer\WishListController@addToWishList')->name('add_to_wishlist')->middleware('buyer');
Route::post('wishlist/remove', 'Buyer\WishListController@removeWishListItem')->name('remove_from_wishlist')->middleware('buyer');
Route::post('wishlist/item/details', 'Buyer\WishListController@itemDetails')->name('wishlist_item_details')->middleware('buyer');
Route::post('wishlist/addToCart', 'Buyer\WishListController@addToCart')->name('wishlist_add_to_cart')->middleware('buyer');


// Order
Route::get('order/{order}', 'Buyer\OtherController@showOrderDetails')->name('show_order_details')->middleware('buyer');
Route::post('order/reject/status', 'Buyer\OtherController@orderRejectStatusChange')->name('order_reject_status_change')->middleware('buyer');
Route::get('order/images/{order}', 'Buyer\OtherController@orderImages')->name('download_order_images')->middleware('buyer');

// Profile
Route::get('profile/overview', 'Buyer\ProfileController@overview')->name('buyer_show_overview')->middleware('buyer');
Route::get('profile', 'Buyer\ProfileController@index')->name('buyer_show_profile')->middleware('buyer');
Route::post('profile/update', 'Buyer\ProfileController@updateProfile')->name('buyer_update_profile')->middleware('buyer');
Route::get('profile/orders', 'Buyer\ProfileController@orders')->name('buyer_show_orders')->middleware('buyer');
Route::get('profile/address', 'Buyer\ProfileController@address')->name('buyer_show_address')->middleware('buyer');
Route::post('profile/address', 'Buyer\ProfileController@addressPost')->name('buyer_update_address')->middleware('buyer');
Route::post('profile/add/shipping_address', 'Buyer\ProfileController@addShippingAddress')->name('buyer_add_shipping_address')->middleware('buyer');
Route::post('profile/edit/shipping_address', 'Buyer\ProfileController@editShippingAddress')->name('buyer_edit_shipping_address')->middleware('buyer');
Route::post('profile/change/default_shipping_address', 'Buyer\ProfileController@defaultShippingAddress')->name('buyer_default_shipping_address')->middleware('buyer');
Route::post('profile/delete/shipping_address', 'Buyer\ProfileController@deleteShippingAddress')->name('buyer_delete_shipping_address')->middleware('buyer');
//Route::get('profile/feedback', 'Buyer\ProfileController@feedback')->name('buyer_show_feedback')->middleware('buyer');
//Route::post('profile/feedback/post', 'Buyer\ProfileController@feedbackPost')->name('buyer_feedback_post')->middleware('buyer');
Route::get('orders/print/pdf', 'Buyer\OtherController@printPdf')->name('order_invoice_print_pdf')->middleware('buyer');
Route::get('orders/print/pdf/without_image', 'Buyer\OtherController@printPdfWithOutImage')->name('order_invoice_print_pdf_without_image')->middleware('buyer');

// Notification
Route::get('notification/all', 'Buyer\OtherController@allNotification')->name('view_all_notification')->middleware('buyer');
Route::get('notification/view', 'Buyer\OtherController@viewNotification')->name('view_notification')->middleware('buyer');

Route::prefix('admin')->group(function () {
    // Auth
    Route::get('login', 'Admin\AuthController@login')->name('login_admin');
    Route::post('login/post', 'Admin\AuthController@loginPost')->name('login_admin_post');
    Route::post('logout', 'Admin\AuthController@logout')->name('logout_admin');

    // Dashboard
    Route::get('dashboard', 'Admin\DashboardController@index')->name('admin_dashboard')->middleware('employee');

    // Category
    Route::get('category', 'Admin\CategoryController@index')->name('admin_category')->middleware('employee');
    Route::post('category/add', 'Admin\CategoryController@addCategory')->name('admin_category_add')->middleware('employee');
    Route::post('category/delete', 'Admin\CategoryController@deleteCategory')->name('admin_category_delete')->middleware('employee');
    Route::post('category/update', 'Admin\CategoryController@updateCategory')->name('admin_category_update')->middleware('employee');
    Route::post('category/update/parent', 'Admin\CategoryController@updateCategoryParent')->name('admin_category_parent_update')->middleware('employee');
    Route::post('category/update/sort', 'Admin\CategoryController@sortCategory')->name('admin_sort_category')->middleware('employee');

    // Master Color
    Route::get('master_color', 'Admin\MasterColorController@index')->name('admin_master_color')->middleware('employee');
    Route::post('master_color/add/post', 'Admin\MasterColorController@addPost')->name('admin_master_color_add')->middleware('employee');
    Route::post('master_color/delete', 'Admin\MasterColorController@delete')->name('admin_master_color_delete')->middleware('employee');
    Route::post('master_color/update', 'Admin\MasterColorController@update')->name('admin_master_color_update')->middleware('employee');

    // Color
    Route::get('color', 'Admin\ColorController@index')->name('admin_color')->middleware('employee');
    Route::post('color/add/post', 'Admin\ColorController@addPost')->name('admin_color_add_post')->middleware('employee');
    Route::post('color/edit/post', 'Admin\ColorController@editPost')->name('admin_color_edit_post')->middleware('employee');
    Route::post('color/delete', 'Admin\ColorController@delete')->name('admin_color_delete')->middleware('employee');

    // Pack
    Route::get('pack', 'Admin\PackController@index')->name('admin_pack')->middleware('employee');
    Route::post('pack/add/post', 'Admin\PackController@addPost')->name('admin_pack_add_post')->middleware('employee');
    Route::post('pack/edit/post', 'Admin\PackController@editPost')->name('admin_pack_edit_post')->middleware('employee');
    Route::post('pack/delete', 'Admin\PackController@delete')->name('admin_pack_delete')->middleware('employee');
    Route::post('pack/change_status', 'Admin\PackController@changeStatus')->name('admin_pack_change_status')->middleware('employee');
    Route::post('pack/change_default', 'Admin\PackController@changeDefault')->name('admin_pack_change_default')->middleware('employee');

    // Master Fabric
    Route::get('master_fabric', 'Admin\MasterFabricController@index')->name('admin_master_fabric')->middleware('employee');
    Route::post('master_fabric/add/post', 'Admin\MasterFabricController@addPost')->name('admin_master_fabric_add')->middleware('employee');
    Route::post('master_fabric/delete', 'Admin\MasterFabricController@delete')->name('admin_master_fabric_delete')->middleware('employee');
    Route::post('master_fabric/update', 'Admin\MasterFabricController@update')->name('admin_master_fabric_update')->middleware('employee');

    // Items settings other
    Route::get('product_settings/others', 'Admin\ItemSettingsOthersController@index')->name('admin_item_settings_others')->middleware('employee');

    // Made In Country
    Route::post('item_settings/made_in_country/add/post', 'Admin\ItemSettingsOthersController@madeInCountryAdd')->name('admin_made_in_country_add')->middleware('employee');
    Route::post('item_settings/made_in_country/update/post', 'Admin\ItemSettingsOthersController@madeInCountryUpdate')->name('admin_made_in_country_update')->middleware('employee');
    Route::post('item_settings/made_in_country/delete/post', 'Admin\ItemSettingsOthersController@madeInCountryDelete')->name('admin_made_in_country_delete')->middleware('employee');
    Route::post('item_settings/made_in_country/change_status/post', 'Admin\ItemSettingsOthersController@madeInCountryChangeStatus')->name('admin_made_in_country_change_status')->middleware('employee');
    Route::post('item_settings/made_in_country/change_default/post', 'Admin\ItemSettingsOthersController@madeInCountryChangeDefault')->name('admin_made_in_country_change_default')->middleware('employee');

    // Fabric
    Route::post('item_settings/fabric/add/post', 'Admin\ItemSettingsOthersController@fabricAdd')->name('admin_fabric_add')->middleware('employee');
    Route::post('item_settings/fabric/update/post', 'Admin\ItemSettingsOthersController@fabricUpdate')->name('admin_fabric_update')->middleware('employee');
    Route::post('item_settings/fabric/delete/post', 'Admin\ItemSettingsOthersController@fabricDelete')->name('admin_fabric_delete')->middleware('employee');
    Route::post('item_settings/fabric/change_status/post', 'Admin\ItemSettingsOthersController@fabricChangeStatus')->name('admin_fabric_change_status')->middleware('employee');
    Route::post('item_settings/fabric/change_default/post', 'Admin\ItemSettingsOthersController@fabricChangeDefault')->name('admin_fabric_change_default')->middleware('employee');

    // Create a new item
    Route::get('create_new_item', 'Admin\ItemController@createNewItemIndex')->name('admin_create_new_item')->middleware('employee');
    Route::post('create_new_item/post', 'Admin\ItemController@createNewItemPost')->name('admin_create_new_item_post')->middleware('employee');
    Route::post('create_new_item/upload/image', 'Admin\ItemController@uploadImage')->name('admin_item_upload_image')->middleware('employee');
    Route::post('create_new_item/add/color', 'Admin\ItemController@addColor')->name('admin_item_add_color')->middleware('employee');

    // Edit Item
    Route::get('item/edit/{item}', 'Admin\ItemController@editItem')->name('admin_edit_item')->middleware('employee');
    Route::post('item/edit/{item}', 'Admin\ItemController@editItemPost')->name('admin_edit_item_post')->middleware('employee');

    // Clone Item
    Route::get('item/clone/{item}', 'Admin\ItemController@cloneItem')->name('admin_clone_item')->middleware('employee');
    Route::post('item/clone/{old_item}', 'Admin\ItemController@cloneItemPost')->name('admin_clone_item_post')->middleware('employee');

    // Item List
    Route::get('items/all', 'Admin\ItemController@itemListAll')->name('admin_item_list_all')->middleware('employee');
    Route::post('item_list/change_to_inactive', 'Admin\ItemController@itemsChangeToInactive')->name('admin_item_list_change_to_inactive')->middleware('employee');
    Route::post('item_list/change_to_active', 'Admin\ItemController@itemsChangeToActive')->name('admin_item_list_change_to_active')->middleware('employee');
    Route::post('item_list/delete', 'Admin\ItemController@itemsDelete')->name('admin_item_list_delete')->middleware('employee');
    Route::get('item/category/{category}', 'Admin\ItemController@itemListByCategory')->name('admin_item_list_by_category')->middleware('employee');

    // Item Import
    Route::get('items/import', 'Admin\ItemController@dataImportView')->name('admin_data_import')->middleware('employee');
    Route::post('items/import/read_file', 'Admin\ItemController@dataImportReadFile')->name('admin_data_import_read_file');
    Route::post('items/import/upload', 'Admin\ItemController@dataImportUpload')->name('admin_data_import_upload')->middleware('employee');
    Route::post('items/import/images', 'Admin\ItemController@dataImportImage')->name('admin_data_import_image')->middleware('employee');

    // Banner
    Route::get('banner', 'Admin\BannerController@index')->name('admin_banner')->middleware('employee');
    Route::post('banner/add/post', 'Admin\BannerController@addPost')->name('admin_banner_add_post')->middleware('employee');
    Route::post('banner/delete', 'Admin\BannerController@delete')->name('admin_banner_delete')->middleware('employee');
    Route::post('banner/active', 'Admin\BannerController@active')->name('admin_banner_active')->middleware('employee');

    // Logo
    Route::post('logo/add/post', 'Admin\BannerController@logoPost')->name('admin_logo_add_post')->middleware('employee');

    // Banner Items
    Route::get('banner/items', 'Admin\BannerController@bannerItems')->name('admin_banner_items')->middleware('employee');
    Route::post('banner/item/add', 'Admin\BannerController@bannerItemAdd')->name('admin_banner_item_add')->middleware('employee');
    Route::post('banner/item/remove', 'Admin\BannerController@bannerItemRemove')->name('admin_banner_item_remove')->middleware('employee');
    Route::post('banner/item/sort', 'Admin\BannerController@bannerItemsSort')->name('admin_banner_item_sort')->middleware('employee');

    // Banner Main Slider
    Route::get('banner/main_slider', 'Admin\BannerController@mainSliderItems')->name('admin_main_slider_items')->middleware('employee');
    Route::post('banner/main_slider/add', 'Admin\BannerController@mainSliderItemAdd')->name('admin_main_slider_item_add')->middleware('employee');
    Route::post('banner/main_slider/sort', 'Admin\BannerController@mainSliderItemsSort')->name('admin_main_slider_items_sort')->middleware('employee');
    Route::post('banner/main_slider/delete', 'Admin\BannerController@mainSliderItemDelete')->name('admin_main_slider_item_delete')->middleware('employee');

    // Banner Front Page
    Route::get('banner/front_page', 'Admin\BannerController@frontPageBannerItems')->name('admin_front_page_banner_items')->middleware('employee');
    Route::post('banner/front_page/add', 'Admin\BannerController@frontPageBannerItemAdd')->name('admin_front_page_banner_item_add')->middleware('employee');
    Route::post('banner/edit', 'Admin\BannerController@editPost')->name('admin_banner_edit_post')->middleware('employee');

    // Banner Top
    Route::get('banner/top', 'Admin\BannerController@topBanner')->name('admin_top_banners')->middleware('employee');
    Route::post('banner/top/add', 'Admin\BannerController@topBannerAdd')->name('admin_top_banner_add')->middleware('employee');
    Route::post('banner/top/edit', 'Admin\BannerController@topBannerEditPost')->name('admin_top_banner_edit_post')->middleware('employee');
    Route::post('banner/top/delete', 'Admin\BannerController@topBannerDelete')->name('admin_top_banner_delete')->middleware('employee');

    // Administration -> Vendor Information
    Route::get('administration/admin_information', 'Admin\VendorInformationController@index')->name('admin_admin_information')->middleware('employee');
    Route::post('administration/company_information/post', 'Admin\VendorInformationController@companyInformationPost')->name('admin_company_information_post')->middleware('employee');
    Route::post('administration/size_chart/post', 'Admin\VendorInformationController@sizeChartPost')->name('admin_size_chart_post')->middleware('employee');
    Route::post('administration/order_notice/post', 'Admin\VendorInformationController@orderNoticePost')->name('admin_order_notice_post')->middleware('employee');
    Route::post('administration/style_pick/post', 'Admin\VendorInformationController@stylePickPost')->name('admin_style_pick_post')->middleware('employee');
    Route::post('administration/save/settings', 'Admin\VendorInformationController@saveSetting')->name('admin_save_setting_post')->middleware('employee');

    // Account Setting
    Route::get('administration/account_setting', 'Admin\AccountSettingController@index')->name('admin_account_setting')->middleware('employee');
    Route::post('administration/admin_id/post', 'Admin\AccountSettingController@adminIdPost')->name('admin_admin_id_post')->middleware('employee');
    Route::post('administration/manage_account/add/post', 'Admin\AccountSettingController@addAccountPost')->name('admin_add_account_post')->middleware('employee');
    Route::post('administration/manage_account/delete/post', 'Admin\AccountSettingController@deleteAccountPost')->name('admin_delete_account_post')->middleware('employee');
    Route::post('administration/manage_account/update/post', 'Admin\AccountSettingController@updateAccountPost')->name('admin_update_account_post')->middleware('employee');
    Route::post('administration/manage_account/status_update/post', 'Admin\AccountSettingController@statusUpdateAccountPost')->name('admin_status_update_account_post')->middleware('employee');
    Route::post('administration/store_setting/save/post', 'Admin\AccountSettingController@saveStoreSetting')->name('admin_save_store_setting_post')->middleware('employee');


    // Courier
    Route::get('courier', 'Admin\CourierController@index')->name('admin_courier')->middleware('employee');
    Route::post('courier/add/post', 'Admin\CourierController@addPost')->name('admin_courier_add')->middleware('employee');
    Route::post('courier/delete', 'Admin\CourierController@delete')->name('admin_courier_delete')->middleware('employee');
    Route::post('courier/update', 'Admin\CourierController@update')->name('admin_courier_update')->middleware('employee');

    // Ship Method
    Route::get('ship_method', 'Admin\ShipMethodController@index')->name('admin_ship_method')->middleware('employee');
    Route::post('ship_method/add/post', 'Admin\ShipMethodController@addPost')->name('admin_ship_method_add')->middleware('employee');
    Route::post('ship_method/delete', 'Admin\ShipMethodController@delete')->name('admin_ship_method_delete')->middleware('employee');
    Route::post('ship_method/update', 'Admin\ShipMethodController@update')->name('admin_ship_method_update')->middleware('employee');
// Coupon
    Route::get('coupon', 'Admin\CouponController@index')->name('admin_coupon')->middleware('employee');
    Route::post('coupon/add/post', 'Admin\CouponController@addPost')->name('admin_coupon_add_post')->middleware('employee');
    Route::post('coupon/edit/post', 'Admin\CouponController@editPost')->name('admin_coupon_edit_post')->middleware('employee');
    Route::post('coupon/delete', 'Admin\CouponController@delete')->name('admin_coupon_delete')->middleware('employee');

    // Orders
    Route::get('orders/all', 'Admin\OrderController@allOrders')->name('admin_all_orders')->middleware('employee');
    Route::get('orders/new', 'Admin\OrderController@newOrders')->name('admin_new_orders')->middleware('employee');
    Route::get('orders/confirmed', 'Admin\OrderController@confirmOrders')->name('admin_confirmed_orders')->middleware('employee');
    Route::get('orders/backed', 'Admin\OrderController@backedOrders')->name('admin_backed_orders')->middleware('employee');
    Route::get('orders/shipped', 'Admin\OrderController@shippedOrders')->name('admin_shipped_orders')->middleware('employee');
    Route::get('orders/cancelled', 'Admin\OrderController@cancelledOrders')->name('admin_cancelled_orders')->middleware('employee');
    Route::get('orders/returned', 'Admin\OrderController@returnedOrders')->name('admin_returned_orders')->middleware('employee');
    Route::get('orders/details/{order}', 'Admin\OrderController@orderDetails')->name('admin_order_details')->middleware('employee');
    Route::post('orders/details/post/{order}', 'Admin\OrderController@orderDetailsPost')->name('admin_order_details_post')->middleware('employee');
    Route::get('orders/incomplete', 'Admin\OrderController@incompleteOrders')->name('admin_incomplete_orders')->middleware('employee');
    Route::get('orders/incomplete/{user}', 'Admin\OrderController@incompleteOrderDetails')->name('admin_incomplete_order_detail')->middleware('employee');
    Route::post('orders/backorder/create', 'Admin\OrderController@createBackorder')->name('admin_create_back_order')->middleware('employee');
    Route::post('orders/out_of_stock', 'Admin\OrderController@outOfStock')->name('admin_out_of_stock')->middleware('employee');
    Route::post('orders/delete_item', 'Admin\OrderController@deleteOrderItem')->name('admin_delete_order_item')->middleware('employee');
    Route::post('order/delete', 'Admin\OrderController@deleteOrder')->name('admin_delete_order')->middleware('employee');
    Route::post('order/item_details', 'Admin\OrderController@itemDetails')->name('admin_get_item_details')->middleware('employee');
    Route::post('order/add/item', 'Admin\OrderController@addItem')->name('admin_order_add_item')->middleware('employee');
    Route::post('order/change/status', 'Admin\OrderController@changeStatus')->name('admin_change_order_status')->middleware('employee');

    Route::get('orders/print/pdf', 'Admin\OrderController@printPdf')->name('admin_print_pdf')->middleware('employee');
    Route::get('orders/print/pdf/without_image', 'Admin\OrderController@printPdfWithOutImage')->name('admin_print_pdf_without_image')->middleware('employee');
    Route::get('orders/print/packlist', 'Admin\OrderController@printPacklist')->name('admin_print_packlist')->middleware('employee');

    Route::post('orders/check_password', 'Admin\OrderController@checkPassword')->name('admin_order_check_password')->middleware('employee');
    Route::post('orders/mask/card_number', 'Admin\OrderController@maskCardNumber')->name('admin_order_mask_card_number')->middleware('employee');
    //Incomplete order send mail
    Route::post('orders/incomplete/sendmail', 'Admin\OrderController@incompleteOrderSendMail')->name('admin_incomplete_order_send_mail')->middleware('employee');

    // Buyer Home
    Route::get('buyer_home', 'Admin\OtherController@buyerHome')->name('admin_buyer_home')->middleware('employee');
    Route::post('buyer_home/save', 'Admin\OtherController@buyerHomeSave')->name('admin_buyer_home_save')->middleware('employee');

    // Welcome Notification
    Route::get('welcome_notification', 'Admin\OtherController@welcomeNotification')->name('admin_welcome_notification')->middleware('employee');
    Route::post('welcome_notification/save', 'Admin\OtherController@welcomeNotificationSave')->name('admin_welcome_notification_save')->middleware('employee');

    // Customer
    Route::get('customer/all', 'Admin\BuyerController@allBuyer')->name('admin_all_buyer')->middleware('employee');
    Route::post('customer/change/status', 'Admin\BuyerController@changeStatus')->name('admin_buyer_change_status')->middleware('employee');
    Route::post('customer/change/verified', 'Admin\BuyerController@changeVerified')->name('admin_buyer_change_verified')->middleware('employee');
    Route::post('customer/change/mailing_list', 'Admin\BuyerController@changeMailingList')->name('admin_buyer_change_mailing_list')->middleware('employee');
    Route::post('customer/change/block', 'Admin\BuyerController@changeBlock')->name('admin_buyer_change_block')->middleware('employee');
    Route::post('customer/change/minOrder', 'Admin\BuyerController@changeMinOrder')->name('admin_buyer_change_min_order')->middleware('employee');
    Route::get('customer/edit/{buyer}', 'Admin\BuyerController@edit')->name('admin_buyer_edit')->middleware('employee');
    Route::post('customer/edit/post/{buyer}', 'Admin\BuyerController@editPost')->name('admin_buyer_edit_post')->middleware('employee');
    Route::post('customer/delete', 'Admin\BuyerController@delete')->name('admin_buyer_delete')->middleware('employee');

    // Store Credit
    Route::post('/store_credit/add', 'Admin\StoreCreditController@add')->name('admin_add_store_credit')->middleware('employee');
    Route::get('/store_credit', 'Admin\StoreCreditController@view')->name('admin_store_credit_view')->middleware('employee');

    // Feedback
    /*Route::get('feedback', 'Admin\ReviewController@index')->name('admin_feedback')->middleware('employee');
    Route::post('feedback/save', 'Admin\ReviewController@saveFeedback')->name('admin_save_feedback')->middleware('employee');*/

    // Export to SP
    Route::get('export/sp', 'Admin\OtherController@exportToSPView')->name('admin_export_to_sp_view')->middleware('employee');
    Route::post('export/sp', 'Admin\OtherController@exportToSPPost')->name('admin_export_to_sp_post')->middleware('employee');

    // Sort Items
    Route::get('sort/items', 'Admin\SortController@index')->name('admin_sort_items_view')->middleware('employee');
    Route::post('sort/items/save', 'Admin\SortController@save')->name('admin_sort_items_save')->middleware('employee');

    // Pages
    Route::get('page/{id}', 'Admin\PageController@index')->name('admin_page_view')->middleware('employee');
    Route::post('page/save{id}', 'Admin\PageController@save')->name('admin_page_save')->middleware('employee');

    // Meta
    Route::get('meta/page/{page}', 'Admin\MetaController@page')->name('admin_meta_page')->middleware('employee');
    Route::get('meta/category/{category}', 'Admin\MetaController@category')->name('admin_meta_category')->middleware('employee');
    Route::get('meta/vendor/{vendor}', 'Admin\MetaController@vendor')->name('admin_meta_vendor')->middleware('employee');
    Route::post('meta/save', 'Admin\MetaController@save')->name('admin_meta_save')->middleware('employee');

    // Others
    Route::post('modal/items', 'Admin\OtherController@getItems')->name('admin_get_items_for_modal')->middleware('employee');
    //import customer data
    Route::get('/read-excel',function(){
        $fileD = fopen(public_path('customers5.csv'), "r");
        $column = fgetcsv($fileD);
        while (!feof($fileD)) {
            $rowData[] = fgetcsv($fileD);
        }
        foreach ($rowData as $key => $value) {
            $inserted_data = array('company_name' => $value[0], 'first_name' => $value[1], 'email' => $value[2], 'address' => $value[3], 'unit' => $value[4],
                'phone' => $value[5], 'city' => $value[6], 'state' => $value[7], 'zipcode' => $value[8],
                'fax' => $value[9], 'website' => $value[10], 'approved' => $value[11], 'approved_at' => $value[12], 'created_at' => $value[13],
                'orders' => $value[14], 'logins' => $value[15], 'last_login' => $value[16], 'password' => $value[17]);
            $name = explode(' ', $inserted_data['first_name']);
            $approvedAt=null;
            $createdAt = null;
            $lastLogin = null;
            if ($approvedAt) {

                $approvedAt = explode(' ', $inserted_data['approved_at']);
            }
            if ($createdAt) {

                $createdAt = explode(' ', $inserted_data['created_at']);
            }
            if ($lastLogin) {

                $lastLogin = explode(' ', $inserted_data['last_login']);
            }

            $userCheck = App\Model\User::where('email', $inserted_data['email'])->first();

            if ($inserted_data['approved'] == 'Y' && !isset($userCheck)) {
                $meta = App\Model\MetaBuyer::create([
                    'verified' => 1,
                    'active' => ($inserted_data['approved'] == 'Y') ? 1 : 0,
                    'user_id' => 0,
                    'primaryCustomerMarket' => 1,
                    'primary_customer_market' => 1,
                    'seller_permit_number' => '123456',
                    'company_name' => $inserted_data['company_name'],
                    'website' => $inserted_data['website'],
                    'billing_location' => (!isset($inserted_data['address'])) ? 'INT' : 'US',
                    'billing_address' => $inserted_data['address'],
                    'billing_unit' => $inserted_data['unit'],
                    'billing_state' => $inserted_data['state'],
                    'billing_city' => $inserted_data['city'],
                    'billing_country_id' => (!isset($inserted_data['address'])) ? 20 : 1,
                    'billing_zip' => $inserted_data['zipcode'],
                    'hear_about_us' => 'google',
                    'receive_offers' => 1,
                    'billing_phone' => $inserted_data['phone'],
                    'billing_fax' => $inserted_data['fax']]);

                $user = App\Model\User::create([
                    'first_name' => (isset($name[0])) ? $name[0] : '',
                    'last_name' => (isset($name[1])) ? $name[1] : '',
                    'active' => ($inserted_data['approved'] == 'Y') ? 1 : 0,
                    'updated_at' => date('Y-m-d', strtotime($approvedAt[0].' '.$approvedAt[1])),
                    'created_at' => date('Y-m-d', strtotime($createdAt[0].' '.$createdAt[1])),
                    'last_login' => date('Y-m-d', strtotime($lastLogin[0].' '.$lastLogin[1])),
                    'order_count' => $inserted_data['orders'],
                    'email' => $inserted_data['email'],
                    'password' => Hash::make($inserted_data['password']),
                    'role' => Role::$BUYER,
                    'buyer_meta_id' => $meta->id,
                ]);

                App\Model\BuyerShippingAddress::create([
                    'user_id' => $user->id,
                    'default' => 1,
                    'location' => (!isset($inserted_data['address'])) ? 'INT' : 'US',
                    'address' => $inserted_data['address'],
                    'unit' => $inserted_data['unit'],
                    'commercial' => 0,
                    'state_text' => $inserted_data['state'],
                    'country_id' => (!isset($inserted_data['address'])) ? 20 : 1,
                    'city' => $inserted_data['city'],
                    'zip' => $inserted_data['zipcode'],
                    'phone' => $inserted_data['phone'],
                    'fax' => $inserted_data['fax'],
                ]);

                $meta->user_id = $user->id;
                $meta->save();
            }

        }
        print_r($rowData);
    });
});

// Category Page
Route::get('/{category}', 'HomeController@CategoryPage')->name('category_page');
Route::get('/{parent}/{category}', 'HomeController@secondCategory')->name('second_category');
Route::get('/{parent}/{category}/{subcategory}', 'HomeController@thirdCategory')->name('third_category');

// Sitemap
Route::get('/sitemap', 'SitemapController@index');
Route::get('/sitemap/items', 'SitemapController@items')->name('sitemap_items');
Route::get('/sitemap/vendors', 'SitemapController@vendors')->name('sitemap_vendors');
Route::get('/sitemap/categories', 'SitemapController@categories')->name('sitemap_categories');
Route::get('/sitemap/static', 'SitemapController@staticPages')->name('sitemap_static');

